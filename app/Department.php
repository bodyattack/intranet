<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Department extends Model
{
    protected $guarded=['id'];

    public $presentation_short = 'Abteilung';
    public $presentation_long = 'Abteilungen';


    public function user() {
        return $this->belongsToMany(User::class,'users_departments');
    }

    public function leader()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


    public function invoiceApprovers() {
        return $this->belongsToMany('App\User', 'ir_department_releaseusers', 'department_id', 'user_id');
    }

    // invoices that are processed by this department
    public function processesInvoices() {
        return $this->belongsToMany('App\Departments', 'ir_billers_departments', 'biller_id', 'department_id');
    }


    // static! all departments with invoice auditors
    public static function withInvoiceApprovers()
    {
        // TODO : Änderung am 21.01. gemacht und auch verweise angepast!
        // TODO : Beobachten und wenn gewünschtes ergebniss, alle Notizen hier entfernen!

        // TODO : Beide Abfragen verbinden, damit Abteilungen ausgegeben werden wenn entweder ein Mitarbeiter zugeordet ist
        // TODO : oder eine erweiterte Rechnungsfreigabe exsitiert.   (Anpassen in BillController 167 und Biller edit.blade 87)

        $departments_users =  DB::table('departments')
            ->join('users_departments', 'departments.id', '=', 'users_departments.department_id')
            ->select('departments.*')
            ->groupBy('users_departments.department_id')
            ->orderBy('departments.name')
            ->get()
            ->keyBy('id');

        $extended_sharing =  DB::table('departments')
            ->join('ir_department_releaseusers', 'departments.id', '=', 'ir_department_releaseusers.department_id')
            ->select('departments.*')
            ->groupBy('ir_department_releaseusers.department_id')
            ->orderBy('departments.name')
            ->get()
            ->keyBy('id');

        $aprovers = new \Illuminate\Support\Collection();
        foreach ($departments_users AS $key => $departmens) {
            $aprovers[$key] = $departmens;
        }
        foreach ($extended_sharing AS $key => $departmens) {
            $aprovers[$key] = $departmens;
        }

        return $aprovers->sortBy('name');

    }


}
