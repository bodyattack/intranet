<?php

namespace App;

use App\Permission;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{

    protected $guarded=['id'];

    public function permissions() {
        return $this->belongsToMany(Permission::class,'areas_permissions');
    }


    public function hasPermission($permission) {
        return (bool) $this->permissions->where('id', $permission->id)->count();
    }

    public function getArea() {
        return $this->name;
    }
}
