<?php

namespace App\Actions\Files;

class Base64Decoder
{
    /**
     * To avoid problems when trying to let base64_decode decode base64-strings longer than ~5k chars.
     * The base64-decoding function is a homomorphism between modulo 4 and modulo 3-length segmented strings.
     * That motivates a divide and conquer approach: Split the encoded string into substrings counting modulo 4 chars,
     * then decode each substring and concatenate all of them.
     *
     * @param string $encoded
     *
     * @return string
     */
    public function handle(string $encoded): string
    {
        $decoded = '';

        for ($i = 0; $i < ceil(strlen($encoded) / 256); ++$i) {
            $decoded = $decoded.base64_decode(substr($encoded, $i * 256, 256));
        }

        return $decoded;
    }
}
