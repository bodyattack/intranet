<?php

namespace App\Actions\Printjobs;

use Illuminate\Support\Facades\Storage;

class CheckAddressFieldPosition
{
    use PdfTools;

    const TEMPLATE_STANDARD = 'letter_standard.pdf';

    public function handle(int $printjobId=0, string $path): string
    {
        return $this->putStampToFirstPage($path, Storage::disk('service')->path(self::TEMPLATE_STANDARD));
    }
}
