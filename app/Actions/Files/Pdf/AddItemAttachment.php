<?php

namespace App\Actions\Files\Pdf;

class AddItemAttachment
{
    use PdfTools;

    /**
     * @param string $filePath
     * @param string $attachmentPath
     *
     * @return bool
     */
    public function handle(string $filePath, string $attachmentPath): bool
    {
        $mergedFile = $this->addPagesToPdf($filePath, $attachmentPath);

        return (bool) file_put_contents($filePath, $mergedFile);
    }
}
