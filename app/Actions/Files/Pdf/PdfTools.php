<?php

namespace App\Actions\Files\Pdf;

/**
 * apt install pdftk (2.02).
 *
 * apt install pdfgrep
 *
 * apt install pdfgrep-utils
 */
trait PdfTools
{
    /**
     * Get pdf info, trim keys and values, lowercase keys and
     * replace white spaces with underscore, then return as array.
     *
     * Tagged:         no
     * UserProperties: no
     * Suspects:       no
     * Form:           none
     * JavaScript:     no
     * Pages:          8
     * Encrypted:      no
     * Page size:      595.247 x 841.861 pts (A4)
     * Page rot:       0
     * File size:      194508 bytes
     * Optimized:      no
     * PDF version:    1.4
     *
     * @param string $path
     *
     * @return array
     */
    private function getPdfInfo(string $path): array
    {

        $rawInfoData = explode("\n", shell_exec('pdfinfo '.$path));
        $data = [];
        foreach ($rawInfoData as $value) {
            if (!empty($value)) {
                list($k, $v) = explode(':', $value); // sub_string 16!
                $data[strtolower(str_replace(' ', '_', trim($k)))] = trim($v);
            }
        }

        return $data;
    }

    /**
     * Cut pages from the pdf file and return as string.
     *
     * @param string $path
     * @param int    $start
     * @param int    $end
     *
     * @return string
     */
    private function cutOutPagesFromPdf(string $path, int $start, int $end): string
    {
        return shell_exec('pdftk '.$path.' cat '.$start.'-'.$end.' output -');
    }

    /**
     * Put together two pdf files and return the result file as string.
     *
     * @param string $path
     * @param string $attachmentPath
     *
     * @return string
     */
    private function addPagesToPdf(string $path, string $attachmentPath): string
    {
        return shell_exec('pdftk '.$path.' '.$attachmentPath.' cat output -');
    }

    /**
     * @param string $path
     * @param int    $x
     * @param int    $y
     * @param int    $width
     * @param int    $height
     * @param int    $page
     *
     * @return string
     */
    private function cropTextFromPdf(string $path, int $x, int $y, int $width, int $height, int $page = 1): string
    {
        return shell_exec('pdftotext -f '.$page.' -l '.$page.' -x '.$x.' -y '.$y.' -W '.$width.' -H '.$height.' '.$path.' -');
    }

    /**
     * @param string $path
     * @param string $stampPath
     *
     * @return string
     */
    private function putStampToFirstPage(string $path, string $stampPath): string
    {
        $pages = (int) $this->getPdfInfo($path)['pages'];

        if ($pages > 1) {
            return shell_exec('cat '.$path.' | pdftk - cat 1-1 output - | pdftk - stamp '.$stampPath.' output - | pdftk A=- B='.$path.' cat A1 B2-end output -');
        }

        return shell_exec('cat '.$path.' | pdftk - stamp '.$stampPath.' output -');
    }

    /**
     * @param string $path
     * @param string $stampPath
     *
     * @return string
     */
    private function putBackgroundToLastPage(string $path, string $backgroundPath): string
    {
        $pages = (int) $this->getPdfInfo($path)['pages'];

        if ($pages > 1) {
            return shell_exec('cat '.$path.' | pdftk - cat '.$pages.'-1 output - | pdftk - background '.$backgroundPath.' output - | pdftk A=- B='.$path.' cat B1-'.($pages - 1).' A1 output -');
        }

        return shell_exec('cat '.$path.' | pdftk - background '.$backgroundPath.' output -');
    }

    /**
     * Return an array with all page numbers containts the searched string.
     *
     * @param string $path
     * @param string $searchStr
     *
     * @return array
     */
    // private function getPageNumberBySearchString(string $path, string $searchStr): array
    // {
    //     $pagesTotal = (int) $this->getPdfInfo($path)['pages'];

    //     $pages = [];
    //     for ($i = 2; $i < $pagesTotal; ++$i) {
    //         if (shell_exec('pdftotext -f '.$i.' -l '.$i.' '.$path.' - | grep '.$searchStr)) {
    //             $pages[] = $i;
    //         }
    //     }

    //     return $pages;
    // }

    /**
     * sudo apt install pdfgrep.
     *
     * 1:searched string
     * 3:searched string
     * 5:searched string
     * 7:searched string
     *
     * @param string $path
     * @param string $searchStr
     *
     * @return array
     */
    public function getPageNumberBySearchString(string $path, string $searchStr): array
    {
        $pages = [];
        if ($result = trim(shell_exec('pdfgrep -n '.$searchStr.' '.$path))) {
            if ($records = explode("\n", $result)) {
                foreach ($records as $record) {
                    $pages[] = explode(':', $record)[0];
                }
            }
        }

        return $pages;
    }

    /**
     * sudo apt install ghostscript.
     *
     * -dPDFSETTINGS=/screen   (screen-view-only quality, 72 dpi images)
     * -dPDFSETTINGS=/ebook    (low quality, 150 dpi images)
     * -dPDFSETTINGS=/printer  (high quality, 300 dpi images)
     * -dPDFSETTINGS=/prepress (high quality, color preserving, 300 dpi imgs)
     * -dPDFSETTINGS=/default  (almost identical to /screen)
     *
     * Scale all pages to i.e. a4.
     *
     * @param string $path
     * @param string $size
     */
    public function normalizePdf(string $path, string $size = 'a4'): void
    {
        shell_exec('cat '.$path.' | gs -dBATCH -dNOPAUSE -dQUIET -dPDFSETTINGS=/printer -sDEVICE=pdfwrite -sPAPERSIZE='.$size.' -dFIXEDMEDIA -dPDFFitPage -o '.$path.' -');
    }
}
