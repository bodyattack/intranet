<?php

namespace App\Actions\Files\Pdf;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ShowPdf
{

    public function handle(int $printjobItemId)
    {
        $item = PrintjobItem::findOrFail($printjobItemId);

        if (Storage::disk('pdfitem')->exists($item->filename)) {
            $tmpFile = Str::uuid()->toString();

            // letter marks
            Storage::disk('pdfitem')->put($tmpFile, (new CheckAddressFieldPosition())->handle($item->printjob->id, Storage::disk('printjobs')->path($item->filename)));

            $content = Storage::disk('pdfitem')->get($tmpFile);
            Storage::disk('pdfitem')->delete($tmpFile);

            return response()->make($content, 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$item->filename.'"',
            ]);
        }

        abort(404, 'File does not exist!');
    }
}
