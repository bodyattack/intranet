<?php

namespace App\Actions\Files\Pdf;

class CheckPdfFile
{
    use PdfTools;

    /**
     * Check PDF file and if OK normalize it.
     *
     * @param string $path
     */
    public function handle(string $path): bool
    {
        if ($this->isEncrypted($path)) {
            return false;
        }

        if (!$this->isA4Portrait($path)) {
            return false;
        }

        // $this->normalizePdf($path);

        return true;
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    private function isEncrypted(string $path): bool
    {
        $pdfInfo = $this->getPdfInfo($path);


        return 'no' != $pdfInfo['encrypted'];
    }

    /**
     * phpinfo output [Page size: 595.247 x 841.861 pts (A4)].
     *
     * ISO 216:         595 × 842 points
     * American Quarto: 612 × 792 points.
     *
     * @param string $path
     *
     * @return bool
     */
    private function isA4Portrait(string $path): bool
    {
        $pdfInfo = $this->getPdfInfo($path);


        list($width, $height) = explode(' x ', trim(explode('pts', $pdfInfo['page_size'])[0]));

        #var_dump($width);
        #var_dump($height);
        #dd($pdfInfo);

        if ((int) $width <= 613 && (int) $height <= 842) {
            return true;
        }

        return false;
    }
}
