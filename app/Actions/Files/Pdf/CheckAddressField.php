<?php

namespace App\Actions\Files\Pdf;

class CheckAddressField
{
    /**
     * Check the address field for correctness.
     * The address must have between 2 and 6 lines
     * and a five-digit postal code.
     *
     * @param int $itemId
     *
     * @return bool
     */
    public function handle(string $full_address): bool
    {

        $address = array_filter(explode(PHP_EOL, $full_address), function ($row) {
            return !empty(trim($row));
        });

        return count($address) > 1 && count($address) < 7 && preg_match('/[0-9]{5}/', implode(' ', $address));
    }
}
