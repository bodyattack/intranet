<?php

namespace App\Actions\Files\Pdf;

use Illuminate\Support\Str;

class SplitPdfByPageNumber
{
    use PdfTools;

    /**
     * Split an pdf file into multiple parts and store them.
     * Return an array with parts filenames.
     *
     * @param string $path
     * @param int    $pages
     *
     * @return array
     */
    public function handle(string $path, int $pages = 1): array
    {
        if (!is_file($path)) {
            throw new \Exception("File {$path} does not exist");
        }

        $pagesTotal = (int) $this->getPdfInfo($path)['pages'];

        $items = [];
        for ($i = 1; $i <= ($pagesTotal - $pages + 1); $i += $pages) {
            $pathItem = dirname($path).'/'.Str::uuid()->toString();

            file_put_contents($pathItem, $this->cutOutPagesFromPdf($path, $i, ($i + $pages - 1)));

            // $this->normalizePdf($pathItem);

            $items[] = $pathItem;
        }

        return $items;
    }
}
