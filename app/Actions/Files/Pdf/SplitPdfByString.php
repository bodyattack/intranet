<?php

namespace App\Actions\Files\Pdf;

use Illuminate\Support\Str;

class SplitPdfByString
{
    use PdfTools;

    /**
     * Split an pdf file into multiple parts and store them.
     * Return an array with parts filenames.
     *
     * @param string $path
     * @param string $searchStr
     *
     * @return array
     */
    public function handle(string $path, string $searchStr): array
    {
        if (!is_file($path)) {
            throw new \Exception("File {$path} does not exist");
        }

        // [1, x, x, x]
        $hits = $this->getPageNumberBySearchString($path, $searchStr);

        $pagesTotal = (int) $this->getPdfInfo($path)['pages'];

        // split
        for ($i = 0; $i < count($hits); ++$i) {
            $pathItem = dirname($path).'/'.Str::uuid()->toString();

            $start = $hits[$i];
            $end = (array_key_exists(($i + 1), $hits)) ? ($hits[$i + 1] - 1) : $pagesTotal;

            file_put_contents($pathItem, $this->cutOutPagesFromPdf($path, $start, $end));

            // $this->normalizePdf($pathItem);

            $items[] = $pathItem;
        }

        return $items;
    }
}
