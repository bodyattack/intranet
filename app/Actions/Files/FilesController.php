<?php

namespace App\Actions\Files;

use App\File;
use Illuminate\Http\Request;

class FilesController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\File  $files
     * @return \Illuminate\Http\Response
     */
    public function show(File $files)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\File  $files
     * @return \Illuminate\Http\Response
     */
    public function edit(File $files)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\File  $files
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $files)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\File  $files
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $files)
    {
        //
    }
}
