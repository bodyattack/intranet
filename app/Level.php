<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $guarded=['id'];

    public $presentation_short = 'Benutzerlevel';
    public $presentation_long = 'Benutzerlevel';


    public function user() {
        return $this->hasMany('App\User');
    }


}
