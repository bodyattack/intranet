<?php

namespace App;

use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class TempOrder extends Model
{

    use Sortable;
    use Filterable;

    public $sortable = [
        'id',
        'transfer',
        'gesamtsumme',
        'datum',
        'TxId',
        'intranet_id',
        'transaction_key',
        'kunde',
    ];

    protected $casts = [
        'kunde' => 'array',
        'warenkorb_string' => 'array',
        'gurado' => 'array'
    ];

    protected $guarded=['id'];

    public $presentation_short = 'Bestellung';
    public $presentation_long = 'Bestellungen';


    public function coupon_codes() {
        return $this->belongsToMany(CouponCode::class,'coupon_codes_orders', 'order_id', 'coupon_code_id');
    }

    public function payonePayments()
    {
        return $this->hasMany('App\PayonePayments');
    }

}
