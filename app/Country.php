<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded=['id'];

    protected $table = "countries";

    public $presentation_short = 'Land';
    public $presentation_long = 'Länder';


    public function addresses()
    {
        return $this->hasMany('App\CustomerAddress');
    }
}
