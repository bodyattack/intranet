<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Encryptable;

class ApiAccount extends Model
{
    use Encryptable;

    protected $guarded=['id'];

    protected $encryptable = ['key'];

    public $presentation_short = 'Api-Account';
    public $presentation_long = 'Api-Accounts';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
