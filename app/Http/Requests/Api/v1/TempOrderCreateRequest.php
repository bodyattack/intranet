<?php
namespace App\Http\Requests\Api\v1;

class TempOrderCreateRequest extends BaseRequest
{

    public function rules()
    {
        return [
            'datum' => 'sometimes|required|date',
            'verifydate' => 'sometimes|required|date',
            'warenkorb_string' => 'required|string',
            'warenkorb_nachlass' => 'sometimes|nullable|int',
            'gutschein' => 'sometimes|nullable|string',
            'gurado' => 'sometimes|nullable|string',
            'gesamtsumme' => 'required|int',
            'versandkosten' => 'required|int',
            'zahlungsart' => 'required|string',
            'kunde' => 'required|string',
            'lieferlandabweichung' => 'sometimes|nullable|int',
            'TxId' => 'required|int',
            'userid_payone' => 'string',
            'transaction_key' => 'required|string',
        ];
    }

}

