<?php
namespace App\Http\Requests\Api\v1\Coupons;

use App\Http\Requests\Api\v1\BaseRequest;

class GuradoCreateRequest extends BaseRequest
{

    public function rules()
    {
        return [
            'create' => 'required|int',
            'amount' => 'required|numeric',
        ];
    }

}

