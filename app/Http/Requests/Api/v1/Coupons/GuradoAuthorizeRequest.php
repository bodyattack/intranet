<?php
namespace App\Http\Requests\Api\v1\Coupons;

use App\Http\Requests\Api\v1\BaseRequest;

class GuradoAuthorizeRequest extends BaseRequest
{

    public function rules()
    {
        return [
            'externalId' => 'required|string',
            'amount' => 'required|numeric',
        ];
    }

}

