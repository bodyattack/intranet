<?php

namespace App\Http\Resources\Api\v1;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CouponResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        dd(0);
        if($this->collection->count()>1) {
            // many emelents
            $elements = $this->collection->map(function ($element) {
                return [
                    'id' => $element->id,
                    'art' => $element->art,
                    'code' => '',
                    'wert' => $element->value,
                    'cheaper_check' => $element->cheaper_check,
                    'use_max' => $element->use_max,
                    'versandkostenfrei' => $element->free_shipping_countries,
                    'name' => $element->description,
                    'shop' => $element->shops,
                    'laufzeitstart' => $element->date_from,
                    'laufzeitende' => $element->date_to,
                    'minbestellwert' => $element->min_order_value,
                    'maxbestellwert' => $element->max_order_value,
                    'produktzuordnung' => $element->mandatory_products,
                    'gratisprodukt' => $element->free_products,
                    'gratisartikel' => $element->free_articles,
                    'hersteller' => $element->manufacturers,
                    'aktiv' => $element->active,
                ];
            });

            return $elements;
        }
        else {
            // one element
            $element = $this->collection->first();
            return [
                'id' => $element->id,
                'art' => $element->art,
                'code' => '',
                'wert' => $element->value,
                'cheaper_check' => $element->cheaper_check,
                'use_max' => $element->use_max,
                'versandkostenfrei' => $element->free_shipping_countries,
                'name' => $element->description,
                'shop' => $element->shops,
                'laufzeitstart' => $element->date_from,
                'laufzeitende' => $element->date_to,
                'minbestellwert' => $element->min_order_value,
                'maxbestellwert' => $element->max_order_value,
                'produktzuordnung' => $element->mandatory_products,
                'gratisprodukt' => $element->free_products,
                'gratisartikel' => $element->free_articles,
                'hersteller' => $element->manufacturers,
                'aktiv' => $element->active,
            ];
        }

    }
}
