<?php

namespace App\Http\Resources\Api\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'erp_kdnr' => $this->erp_kdnr,
            'gender' => $this->gender,
            'titel' => $this->titel,
            'firstname' => $this->firstname,
            'surname' => $this->lastname,
            'email' => $this->email,
            'phone' => $this->phone,
            'dob' => $this->dob,
            'email_verified_at' => $this->email_verified_at,
            'last_login_at' => $this->last_login_at,
        ];
    }
}
