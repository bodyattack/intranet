<?php

namespace App\Http\Resources\Api\v1\Coupons;

use Illuminate\Http\Resources\Json\JsonResource;

class GuradoCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {

        $voucher = $this->resource['voucher'];

        return [
            'code' => $voucher['code'],
            'balance' => $voucher['balance'],
            'currencyCode' => $voucher['currencyCode'],
            'status' => $voucher['status'],
            'isExpired' => $voucher['isExpired'],
            'validFromDate' => $voucher['validFromDate'],
            'validToDate' => $voucher['validToDate'],
            'creationDate' => $voucher['creationDate'],
            'canUseMultipleTimes' => $voucher['canUseMultipleTimes'],
            'numberOfUsesAllowed' => $voucher['numberOfUsesAllowed'],
        ];

    }
}
