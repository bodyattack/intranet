<?php

namespace App\Http\Resources\Api\v1\Coupons;

use Illuminate\Http\Resources\Json\JsonResource;

class CouponCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {

        $coupon = $this->coupon;

        return [
            'id' => $coupon->id,
            'art' => $coupon->art,
            'code' => $this->code,
            'wert' => $coupon->value,
            'cheaper_check' => $coupon->cheaper_check,
            'multi' => $coupon->multi,
            'use_max' => $coupon->use_max ?? '',
            'use_count' => $this->orders->count(),
            'versandkostenfrei' => $coupon->free_shipping_countries,
            'name' => $coupon->description ?? '',
            'shop' => $coupon->shops ?? '',
            'laufzeitstart' => $coupon->date_from ?? '',
            'laufzeitende' => $coupon->date_to ?? '',
            'minbestellwert' => $coupon->min_order_value ?? '',
            'maxbestellwert' => $coupon->max_order_value ?? '',
            'produktzuordnung' => $coupon->mandatory_products ?? '',
            'gratisprodukt' => $coupon->free_products ?? '',
            'gratisartikel' => $coupon->free_articles ?? '',
            'hersteller' => $coupon->manufacturers ?? '',
            'aktiv' => $coupon->active,
        ];


    }
}
