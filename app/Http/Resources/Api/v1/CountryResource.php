<?php

namespace App\Http\Resources\Api\v1;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CountryResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $elements = $this->collection->map(function ($element)  {
            return [
                'id'                    => $element->id,
                'name'                  => $element->name,
                'name_de'               => $element->name_de,
                'code'                  => $element->code,
                'shipping'              => $element->shipping,
                'shipping_free_limit'   => $element->shipping_free_limit,
            ];
        });

        return $elements;
    }
}
