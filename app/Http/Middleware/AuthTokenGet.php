<?php

namespace App\Http\Middleware;

use Closure;

class AuthTokenGet
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->method()=='GET') {

            $token = $request->get('token');

            if(!empty($token)) {

                $salt = '$gsu&56';
                $date = date("Ym");
                $md5 = md5($date . $salt);

                if($token==$md5) return $next($request);

            }
        };

        return response()->json(['status' => 403, 'message' => 'Token mismatch'], 403);
    }
}
