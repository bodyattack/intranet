<?php

namespace App\Http\Middleware;

use Closure;

class AreaMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $area, $permission = null)
    {
        if(!$request->user()->hasArea($area)) {
            abort(404);
        }
        if($permission !== null && !$request->user()->can($permission)) {
            abort(404);
        }
        return $next($request);
    }
}
