<?php

namespace App\Http\Middleware;

use Closure;
use App\ApiAccount;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthCredentialsData
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!$request->has(['auth.api_user', 'auth.api_key'])) {
            return response()->json(['message' => 'Unauthorized, no credentials.'], 401);
        }

        $auth = $request->get('auth');
        unset($request['auth']);

        $api_user = $auth['api_user'];
        $api_key = $auth['api_key'];

        if (empty($api_user) || empty($api_key)) {
            return response()->json(['message' => 'Unauthorized, empty credentials.'], 401);
        }


        if (!$user = User::where('email', $api_user)->first()) {
            return response()->json(['message' => 'Unauthorized.'], 401);
        }

        // Variante über seperate ApiAccount Tabelle
        /*
        if (!$apiAccount = ApiAccount::where('username', $auth->username)->first()) {
            return response()->json(['message' => 'Unauthorized 07.'], 401);
        }

        if ($api_key != $user->api_key) {
            return response()->json(['message' => 'Unauthorized 05.'], 401);
        }

        $user = User::findOrFail($apiAccount->user_id);
        */

        if ($api_key != $user->api_key) {
            return response()->json(['message' => 'Unauthorized!'], 401);
        }

        Auth::onceUsingId($user->id);

        return $next($request);
    }
}
