<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     *
     * {
     *      "auth": {
     *           "email": "xxxxx@example.com",
     *           "password": "secret"
     *       }
     *   }
     *
     */

    public function handle($request, Closure $next)
    {

        try {

            if(!Auth::once(array_merge($request->all())))  {
                return response()->json(['message' => 'Unauthorized.'], 401);
            }

            return $next($request);

        } catch (\Throwable $t) {
            return response()->json(['message' => 'Unauthorized.'], 401);
        }
    }
}
