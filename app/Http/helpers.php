<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cookie;



/*
 * Convert special characters within a string.
 */
if (!function_exists('convert_special_characters')) {
    function convert_special_characters(string $str)
    {
        $converter = ['Ä' => 'AE', 'Ö' => 'OE', 'Ü' => 'UE', 'ä' => 'ae', 'ö' => 'oe', 'ü' => 'ue', 'ß' => 'ss', '"' => ''];

        return strtr($str, $converter);
    }
}


/*
 * Show status icon.
 */
if (!function_exists('show_status_icon')) {
    function show_status_icon(string $status)
    {
        $translator = [
            'draft' => '<i class="fas fa-edit"></i>',
            'hold' => '<i class="far fa-pause-circle"></i>',
            'queue' => '<i class="far fa-clock"></i>',
            'done' => '<i class="fas fa-check text-success"></i>',
            'warning' => '<i class="fas fa-exclamation-triangle text-warning"></i>',
            'error' => '<i class="fas fa-exclamation-circle text-danger"></i>',
            'sent' => '<i class="fas fa-check text-success"></i>',
            'active' => '<i class="fas fa-check text-success"></i>',
            'locked' => '<i class="fas fa-lock text-danger"></i>',
        ];

        return $translator[$status] ?? $status;
    }
}

/*
 * Show status text.
 */
if (!function_exists('show_status_text')) {
    function show_status_text(string $status)
    {
        $translator = [
            'draft' => 'Entwurf',
            'hold' => 'Angehalten',
            'queue' => 'Warteschlange',
            'done' => 'Abgeschlossen',
            'warning' => 'Warnung!',
            'error' => 'Feheler!!!',
            'sent' => 'Verschickt',
        ];

        return $translator[$status] ?? $status;
    }
}
