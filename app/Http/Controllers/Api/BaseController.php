<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class BaseController extends Controller
{
    private $status = [
        '200' => 'OK',
        '400' => 'Anfrage fehlerhaft',
        '401' => 'Anmeldung fehlgeschlagen',
        '403' => 'Verboten',
        '404' => 'Keine passenden Daten gefunden',
        '405' => 'Ungueltiger Aufruf',
        '500' => 'Serverfehler - Anfrage fehlgeschlagen',
    ];

    /**
     * Success response method.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($code, $data, $message = null)
    {
        $response = [
            'status' => $code,
            'message' => ($message) ?? $this->status[$code],
            'data' => $data,
        ];

        return response()->json($response, $code);
    }

    /**
     * Error response method.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError($code, $message = null)
    {
        $public_msg = $this->status[$code];
        $privat_msg = ($message) ?? $public_msg;

        // error logging
        $this->logError($code.' - '.$privat_msg);

        $response = [
            'status' => $code,
            'message' => $public_msg,
        ];

        return response()->json($response, $code);
    }

    /**
     * Success response method.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSuccess($code, $message = null)
    {
        $response = [
            'status' => $code,
            'message' => ($message) ?? $this->status[$code],
        ];

        return response()->json($response, $code);
    }

    /**
     * @param string $message
     */
    private function logError(string $message): void
    {
        $data = ['method' => request()->method(), 'uri' => request()->getRequestUri(), 'user_id' => auth()->id()];

        Log::channel('api')->error($message, $data);
    }


    public function default()
    {
        return $this->sendError('405');
    }


}
