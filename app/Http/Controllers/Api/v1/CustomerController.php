<?php

namespace App\Http\Controllers\Api\v1;


use App\Customer;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Api\v1\CustomerResource;
use App\Http\Requests\Api\v1\CustomerVerifyRequest;
use Illuminate\Http\Request;

class CustomerController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        try {
            return $this->sendResponse(200, new CustomerResource(Customer::findOrFail(auth()->user()->id)));
        } catch (\Throwable $t) {
            return $this->sendError(500, $t->getMessage());
        }
    }

    public function verify(Request $request)
    {

        try {
            $customer = Customer::where(['email' => $request->email])->first();

            if ($customer && Hash::check($request->password, $customer->password)) {
                return $this->sendResponse(200, new CustomerResource($customer));
            }

            if(empty($customer)) return $this->sendError(404);

        } catch (\Throwable $t) {
            return $this->sendError(500, $t->getMessage());
        }
    }


    public function verify1(CustomerVerifyRequest $request)
    {
        try {
            $customer = Customer::where(['email' => $request->email])->first();

            if ($customer && Hash::check($request->password, $customer->password)) {
                return $this->sendResponse(200, new CustomerResource($customer));
            }

            if(empty($customer)) return $this->sendError(404);

        } catch (\Throwable $t) {
            return $this->sendError(500, $t->getMessage());
        }
    }



}
