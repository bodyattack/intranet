<?php

namespace App\Http\Controllers\Api\v1;


use App\Country;
use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Api\v1\CountryResource;
use Illuminate\Http\Request;

class CountryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        try {
            return $this->sendResponse(200, new CountryResource(Country::all()));
        } catch (\Throwable $t) {
            return $this->sendError(500, $t->getMessage());
        }
    }




}
