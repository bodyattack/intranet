<?php

namespace App\Http\Controllers\Api\v1;


use App\Externals\Gurado\AbstractRequest;
use App\Externals\Gurado\Vouchers;
use App\Externals\Gurado\PreAuthorization;
use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Api\v1\Coupons\GuradoAuthorizeRequest;
use App\Http\Requests\Api\v1\Coupons\GuradoCreateRequest;
use App\Http\Resources\Api\v1\Coupons\GuradoCodeResource;

class GuradoCodeController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function show($gurado)
    {

        try {

            $voucher = new Vouchers();
            $code_information = $voucher->getVoucherInformation($gurado);

            // Wenn Status vorhanden wurde ein Fehler durchgereicht
            if(isset($code_information['status'])) {
                return $this->sendResponse($code_information['status'], $code_information['response']);
            }

            return $this->sendResponse(200, new GuradoCodeResource($code_information));

        } catch (\Throwable $t) {
            return $this->sendError(500, $t->getMessage());
        }
    }


    public function create(GuradoCreateRequest $request)
    {

        try {
            $data = $request->all();

            $amount = $data['amount'];

            if($amount) {

                $voucher = new Vouchers();
                $code_information = $voucher->create($amount);

                if (!$code_information) return $this->sendError(404);

                return $this->sendResponse(200, new GuradoCodeResource($code_information));
            }

            return $this->sendError(500, '');

        } catch (\Throwable $t) {
            return $this->sendError(500, $t->getMessage());
        }
    }


    public function authorization($gurado, GuradoAuthorizeRequest $request)
    {

        try {
            $data = $request->all();

            $code = $gurado;
            $amount = $data['amount'];
            $external = $data['externalId'];

            $voucher = new PreAuthorization();
            $response = $voucher->authorization($code, $external, $amount);

            // Wenn Status vorhanden wurde ein Fehler durchgereicht
            if(isset($response['status'])) {
                return $this->sendResponse($response['status'], $response['response']);
            }

            if($response['messageCode'] == 'VOUCHER_AUTHORIZED_SUCCESSFULLY') {
                return $this->sendResponse(200, $response);
            }

        } catch (\Throwable $t) {
            return $this->sendError(500, $t->getMessage());
        }
    }



    public function release($external)
    {

        try {
            $voucher = new PreAuthorization();
            $response = $voucher->release($external);

            // Wenn Status vorhanden wurde ein Fehler durchgereicht
            if(isset($response['status'])) {
                return $this->sendResponse($response['status'], $response['response']);
            }

            // sonst der response
            if($response['messageCode'] == 'PREAUTORIZED_VOUCHER_TRANSACTION_RELEASED_SUCCESSFULLY') {
                return $this->sendResponse(200, $response);
            }

        } catch (\Throwable $t) {

            return $this->sendError(500, $t->getMessage());
        }
    }




}
