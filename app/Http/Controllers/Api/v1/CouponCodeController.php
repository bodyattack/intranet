<?php

namespace App\Http\Controllers\Api\v1;


use App\CouponCode;
use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Api\v1\Coupons\CouponCodeResource;

class CouponCodeController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function show($coupon)
    {

        try {
            $couponCode = CouponCode::where('code', $coupon)->get();

            if($couponCode->isEmpty()) return $this->sendError(404);

            if(!$couponCode->first()->isActive()) return $this->sendError(404, 'Code ist nicht mehr gueltig');

            return $this->sendResponse(200, new CouponCodeResource($couponCode->first()));

        } catch (\Throwable $t) {
            return $this->sendError(500, $t->getMessage());
        }
    }




}
