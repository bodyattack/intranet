<?php

namespace App\Http\Controllers\Api\v1;

use App\CouponCode;
use App\TempOrder;
use App\Gurado;
use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Api\v1\TempOrderCreateRequest;

class TempOrdersController extends BaseController
{

    const STATUS_TRANSFERRED = '1';

    /**
     * Display a listing of the resource.
     *
     * @param TempOrderCreateRequest $request
     * @return JsonResponse
     */
    public function store(TempOrderCreateRequest $request)
    {

        $data = $request->all();

        try {
            // Gurado einlösen
            $gurado = NULL;
            if(isset($data['gurado']) && !empty(trim($data['gurado']))) {
                $gurado = json_decode($data['gurado']);

/*
                $guradoObjekte = (new Gurado())->initJSON($data['gurado']);
                foreach ($guradoObjekte AS $guradoObjekt) {
                    if(!$guradoObjekt->redemByOrder()) {
                        return $this->sendResponse(400, $data, 'Gurado Fehler, einloesung nicht moeglich!');
                    }
                }
*/				
            }

            // B2B
            $b2b = 0;
            if(isset($data['haendlerart']) && !empty(trim($data['haendlerart']))) {
                $b2b = 1;
            }

            $tempOrder = TempOrder::create([

                'datum' => $data['datum'],
                'verifydate' => $data['verifydate'],
                'warenkorb_string' => json_decode($data['warenkorb_string']),
                'warenkorb_nachlass' => $data['warenkorb_nachlass'],
                'gutschein' => $data['gutschein'],
                'gurado' => $gurado,
                'gesamtsumme' => $data['gesamtsumme'],
                'versandkosten' => $data['versandkosten'],
                'zahlungsart' => $data['zahlungsart'],
                'kunde' => json_decode($data['kunde']),
                'lieferlandabweichung' => $data['lieferlandabweichung'] ?? 0, // Wird aktuell nicht übermittel, fehlt daher im Array!
                'TxId' => $data['TxId'],
                'userid_payone' => $data['userid_payone'] ?? '',
                'transaction_key' => $data['transaction_key'],
                'status' => $data['status'] ?? 0,

                 // B2B
                'b2b_string' => $data['intranetstring'] ?? '',
                'zahlung' => $data['zahlung'] ?? '',
                'vertriebler' => $data['vertriebler'] ?? '',
                'ansprechpartner' => $data['ansprechpartner'] ?? '',
                'haendlerart' => $data['haendlerart'] ?? '',
                'kunden_id' => $data['kunden_id'] ?? 0,
                'b2b' => $b2b,

            ]);

            // Gutschein verknüpfen
            if(isset($data['gutschein']) && !empty(trim($data['gutschein']))) {
                $gutschein = explode("|", $data['gutschein']);
                $couponCode = CouponCode::where('code', trim($gutschein[0]))->first();
                if($couponCode) {
                    $couponCode->orders()->attach($tempOrder);
                }
            }

            return $this->sendResponse(200, ['id'=>$tempOrder->id]);

        } catch (\Throwable $t) {
            return $this->sendError(500, $t->getMessage());
        }
    }

    public function destroy(int $id)
    {
        // not found
        if (!$tempOrder = TempOrder::find($id)) {
            return $this->sendError(404);
        }

        if ($tempOrder->transfer == self::STATUS_TRANSFERRED) {
            return $this->sendError(403, 'This Order can not be deleted, because is already transferred to processing');
        }

        try {
            $tempOrder->delete();

            return $this->sendSuccess(200, 'Order deleted successfully');

        } catch (\Throwable $t) {
            return $this->sendError(500, $t->getMessage());
        }
    }

}
