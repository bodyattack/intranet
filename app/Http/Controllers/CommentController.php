<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{


    public function __construct()
    {
        parent::__construct();
    }


    public function _validate()
    {

        $attributes = request()->validate([
            'comment' => 'required|string|min:10|max:500',
        ]);

        return $attributes;
    }

    public function update(Comment $comment) {

        $attributes = $this->_validate();

        $comment->update(['updated_by' => $this->user->id]);

        Comment::create([
            'parent_id' => $comment->id,
            'comment' => $attributes['comment'],
            'related' => $comment->related,
            'foreign_id' => $comment->foreign_id,
            'created_by' => $this->user->id,
        ]);

        return back()->withSuccess('Kommentar erfolgreich bearbeitet.');

    }

    public function destroy(Comment $comment) {

        $childs = $comment->children_rec;
        foreach ($childs AS $child) {
            $child->delete();
        }

        $comment->delete();

        return back()->withSuccess('Kommentar erfolgreich gelöscht.');
    }


}
