<?php

namespace App\Http\Controllers\Control;

use App\Level;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class LevelController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:backend_user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levels=Level::all();
        return view('control.level.index')->with(['level' => new Level, 'levels' => $levels]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $level = new Level;
        return view('control.level.edit')->with(['level' => $level]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();
        $attributes['slug'] = Str::slug($attributes['name']);

        $level = Level::create($attributes);

        return redirect(route('control.level.index'))->withSuccess($level->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Level $level)
    {
        return view('control.level.edit')->with(['level' => $level]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Level $level)
    {
        return view('control.level.edit')->with(['level' => $level]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Level $level)
    {
        $attributes = $this->_validate();
        $attributes['slug'] = Str::slug($attributes['name']);

        $level->update($attributes);

        return back()->withSuccess($level->presentation_short.' bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $level)
    {
        $level->delete();
        return back()->withDanger($level->presentation_short.' gelöscht!');
    }


    public function _validate()
    {
        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50',
        ]);

        return $attributes;
    }
}
