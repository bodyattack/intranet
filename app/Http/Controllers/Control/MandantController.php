<?php

namespace App\Http\Controllers\Control;

use App\Mandant;
use App\Http\Controllers\Controller;

class MandantController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:backend_user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mandanten=Mandant::all();
        return view('control.mandant.index')->with(['mandant' => new mandant, 'mandanten' => $mandanten]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mandant = new Mandant;
        return view('control.mandant.edit')->with(['mandant' => $mandant]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();

        $mandant = Mandant::create($attributes);

        return redirect(route('control.mandant.index'))->withSuccess($mandant->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mandant $mandant)
    {
        return view('control.mandant.edit')->with(['mandant' => $mandant]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Mandant $mandant)
    {
        return view('control.mandant.edit')->with(['mandant' => $mandant]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Mandant $mandant)
    {
        $attributes = $this->_validate();
        $mandant->update($attributes);

        return back()->withSuccess($mandant->presentation_short.' bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mandant $mandant)
    {
        $mandant->delete();
        return back()->withDanger($mandant->presentation_short.' gelöscht!');
    }


    public function _validate()
    {
        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50',
            'short_name' => 'required|string|min:2|max:10',
            'datev_email' => 'sometimes|nullable|email|max:128',
        ]);

        return $attributes;
    }
}
