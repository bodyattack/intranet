<?php

namespace App\Http\Controllers\Control\Auth;

use App\Http\Controllers\Control\Auth\Traits\AuthenticatesBackendUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginBackendUserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Business User Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesBackendUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'control/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:backend_user')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/control');
    }

}