<?php

namespace App\Http\Controllers\Control;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Permission;
use App\Area;

class AreaController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:backend_user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $areas=Area::all();
        return view('control.areas.index')->with(['areas' => $areas]);
    }


    public function create()
    {
        return view('control.areas.edit');
    }


    public function store()
    {
        $area = new Area();
        $area->name = request('name');
        $area->slug = Str::slug( $area->name);

        $area->save();
        $insert_id = $area->id;

        $stay = (request('submit-type')=='stay'?true:false);
        
        if(!$stay) {
            $areas=Area::all();
        }

        return ($stay)
            ? redirect('backend/areas/'.$area->id)
            : view('control.areas.index')->with(['areas' => $areas, 'insert_id' => $insert_id]);
    }

    public function show(Area $area)
    {
        return view('control.areas.edit')->with(['area' => $area]);
    }


    public function update(Area $area)
    {
        $area->name = request('name');
        $area->slug = Str::slug( $area->name);
        $area->save();

        return view('control.areas.edit')->with(['area' => $area]);
    }

    public function delete(Area $area)
    {
        $area->delete();
        return redirect()->route('control.areas.index')->withSuccess('Rolle erfolgreich gelöscht');
    }



    public function permissionAssing(Area $area, Permission $permission)
    {
        return $area->hasPermission($permission)
            ? $area->permissions()->detach($permission)
            : $area->permissions()->attach($permission);
    }


    public function showToAssing(Area $area)
    {

        $permissions['area'] = Permission::where('area_id', $area->id)->get();
        $permissions['global'] = Permission::where('area_id', 0)->get();

        return view('control.areas.assing')->with(['area' => $area, 'permissions' => $permissions]);
    }



}
