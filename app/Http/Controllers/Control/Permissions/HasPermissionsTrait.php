<?php
/**
 * Created by PhpStorm.
 * User: cstein
 * Date: 12.06.2019
 * Time: 14:32
 */

namespace App\Http\Controllers\Control\Permissions;

use App\Permission;
use App\Area;

trait HasPermissionsTrait {


    public function areas() {
        return $this->belongsToMany(Area::class,'users_areas');

    }

    public function permissions() {
        return $this->belongsToMany(Permission::class,'users_permissions');
    }


    public function hasArea($area) {
        if(!$objekt = $this->validateType($area, Area::class)) return false;
        return (bool) $this->areas->contains('id', $objekt->id);
    }


    public function hasPermission($permission) {
        if(!$objekt = $this->validateType($permission, Permission::class)) return false;
        return (bool) $this->permissions->contains('id', $objekt->id);
    }



    protected function getAllPermissions(array $permissions) {
        return Permission::whereIn('id', $permissions)->get();
    }


    public function deletePermissions($permissions ) {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);
        return $this;
    }


    // not in use, possible to connect all area rights on activation to a user
    public function givePermissionsTo($permissions) {
        $permissions = $this->getAllPermissions($permissions); // ? on area
        $this->permissions()->saveMany($permissions);
        return $this;
    }


    protected function validateType($objekt, $class) {
        // check of object is a instance of required type and try to instantiate
        if(is_a($objekt, $class)) return $objekt;

        if(is_string($objekt)) {
            return  $class::where('slug', $objekt)->first();
        }

    }


    public function groupAreasAndPermissions() {

        $array['areas']=[];
        foreach ($this->areas AS $area) {
            $array['areas'][$area->slug] = 1;
        }
        $array['permissions']=[];
        foreach ($this->permissions AS $permission) {
            $array['permissions'][$permission->slug] = 1;
        }

        return $array;
    }

}