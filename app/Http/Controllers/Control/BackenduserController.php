<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;
use App\BackendUser;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Mail\ReleaseInformation;

class BackenduserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:backend_user');
    }


    public function showUsers()
    {
        $salt = '$gsu&56';
        $date = date("Ym");
        $api_token = 'Api-Token für '.date("M Y").': '. md5($date . $salt);

        $users = BackendUser::all();
        return view('control.auth.users')->with(['users' => $users, 'api_token' => $api_token]);
    }


    public function deleteUser(BackendUser $user)
    {
        if($user->email=='it@body-attack.de') return $this->showUsers(); // Admin nicht löschen

        $user->delete();
        return $this->showUsers();
    }


    function showPasswortChangeForm() {

        return view('control.auth.reset');

    }

    function storePasswortChange(BackendUser $user) {

        $attributes = request()->validate([
            'password' => 'required|string|max:32',
        ]);
        if(!empty($attributes['password'])) $user->password = Hash::make($attributes['password']);

        $user->save();

        return redirect(route('control.dashboard'))->withSuccess('Kennwort erfolgreich bearbeitet');

    }



}
