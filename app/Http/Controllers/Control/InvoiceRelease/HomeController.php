<?php

namespace App\Http\Controllers\Control\InvoiceRelease;

use App\Coupon;
use App\CouponCode;
use App\Http\Controllers\Controller;
use App\IrMailbox;
use App\IrBiller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class HomeController extends Controller
{

  public function __construct()
  {
      parent::__construct();
      $this->middleware('auth:backend_user');
  }

  public function index()
  {
      $mailboxes=IrMailbox::all();
      return view('control.invoicerelease.index')->with(['mailboxes' => $mailboxes, 'mailbox' => new IrMailbox]);

  }


    public function uploadBiller(Request $request) {

        $attributes = request()->validate([
            'file' => 'required',
            'head_row' => 'required|integer',
        ]);

        $file =  $request->file->getRealPath();
        $array_rows = file($file);

        if($attributes['head_row']) array_shift($array_rows);

        //"Bezeichnung;Freigabe Abteilung;Zahlart;Zuordnungs- Betreff;Zuordnungs- E-Mail;Zahlungsziel;Skonto Tage;Skonto %;Kerditoren-Nr."
        foreach ($array_rows as $row) {
            $data = str_getcsv(utf8_encode(trim($row)), ";");

            $biller = new IrBiller();
            $biller->name = $data[0];
            $biller->payment = $data[2];
            $biller->match_subject = (empty(trim($data[3]))?null:trim($data[3]));
            $biller->match_email =(empty(trim($data[4]))?null:trim($data[4]));
            $biller->due_days = $data[5];
            $biller->discount_days = $data[6];
            $biller->discount = $data[7];
            $biller->creditor_number = $data[8];
            $biller->costcenter_id = 1;

            $biller->save();
            $biller->distributor()->attach((empty(trim($data[1]))?2:trim($data[1])));

        }

        return $this->index();

    }

    // Legt einen Mulit-Gutschein an, Index ist der Wert und die Inhalte im Array (Strings) die jeweiligen Codes
    public function createCoupons(Request $request) {

        $coupon_codes[10] = array();

        foreach ($coupon_codes AS $key => $item) {

            $attributes = array();
            $attributes['art'] = '1';
            $attributes['active'] = 1;
            $attributes['value'] = intval($key);
            $attributes['cheaper_check'] = 0;
            $attributes['use_max'] = 1;
            $attributes['multi'] = count($item);
            $attributes['created_by'] = 22;
            $coupon = Coupon::create($attributes);

            foreach ($item AS $code) {
                $couponCode = new CouponCode();
                $couponCode->code = $code;
                $coupon->codes()->save($couponCode);
            }

        }

    }



}
