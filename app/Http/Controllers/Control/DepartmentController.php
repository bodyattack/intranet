<?php

namespace App\Http\Controllers\Control;

use App\Department;
use App\Actions\Department\StoreInvoiceApprovers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Str;

class DepartmentController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:backend_user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments=Department::all();
        return view('control.department.index')->with(['department' => new Department, 'departments' => $departments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = new Department;
        return view('control.department.edit')->with(['department' => $department]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();

        $department = Department::create([
            'name' => $attributes['name'],
            'slug' => Str::slug($attributes['name']),
            'short_name' => $attributes['short_name'],
            'user_id' => $attributes['user_id'],
        ]);

        if (request()->has('invoice_approvers')) {
            $department->invoiceApprovers()->sync($attributes['invoice_approvers']);
        }

        return redirect(route('control.department.index'))->withSuccess($department->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        return view('control.department.edit')->with(['department' => $department]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        return view('control.department.edit')->with(['department' => $department]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Department $department)
    {
        $attributes = $this->_validate();

        $department->update([
            'name' => $attributes['name'],
            'slug' => Str::slug($attributes['name']),
            'short_name' => $attributes['short_name'],
            'user_id' => $attributes['user_id'],
            ]);

        if (request()->has('invoice_approvers')) {
            $department->invoiceApprovers()->sync($attributes['invoice_approvers']);
        }
        else {
            $department->invoiceApprovers()->detach();
        }

        return back()->withSuccess($department->presentation_short.' bearbeitet!');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();
        return back()->withDanger($department->presentation_short.' gelöscht!');
    }


    public function _validate()
    {
        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50',
            'short_name' => 'sometimes|nullable|string|min:2|max:50',
            'user_id' => 'sometimes|nullable',
            'invoice_approvers' => 'sometimes|nullable',
        ]);

        return $attributes;
    }
}
