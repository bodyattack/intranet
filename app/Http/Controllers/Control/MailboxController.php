<?php

namespace App\Http\Controllers\Control;

use App\IrMailbox;
use App\Http\Controllers\Controller;

class MailboxController extends Controller
{


    public function __construct()
    {
        parent::__construct();
       # $this->middleware('auth:backend_user');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mailbox = new IrMailbox;
        return view('control.mailbox.edit')->with(['mailbox' => $mailbox]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();

        $mailbox = IrMailbox::create($attributes);

        return redirect(route('control.invoicerelease.index'))->withSuccess($mailbox->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(IrMailbox $mailbox)
    {
        return view('control.mailbox.edit')->with(['mailbox' => $mailbox]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(IrMailbox $mailbox)
    {
        return view('control.mailbox.edit')->with(['mailbox' => $mailbox]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IrMailbox $mailbox)
    {
        $attributes = $this->_validate();
        $mailbox->update($attributes);

        return back()->withSuccess($mailbox->presentation_short.' bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(IrMailbox $mailbox)
    {
        $mailbox->delete();
        return back()->withDanger($mailbox->presentation_short.' gelöscht!');
    }


    public function _validate()
    {
        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50',
            'conf' => 'required|string|min:2|max:50',
            'user' => 'required|string|min:2|max:50',
            'pass' => 'required|string|min:2|max:50',
            'mandant_id' => 'required|not_in:0',
        ]);

        return $attributes;
    }


    public function toggleActive(IrMailbox $mailbox) {
        $mailbox->update(['active' => !$mailbox->active]);
        return back()->withSuccess($mailbox->presentation_short.' wurde '.(!$mailbox->active?'deaktiviert':'aktiviert'));
    }



}
