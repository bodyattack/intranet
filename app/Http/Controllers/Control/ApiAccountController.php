<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;
use App\ApiAccount;
use Illuminate\Support\Str;

class ApiAccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:backend_user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function _validate()
    {
        $attributes = request()->validate([
            'username' => 'required|max:255|unique:api_accounts',
            'key' => 'required|max:255',
            'user_id' => 'required|numeric',
        ]);


        return $attributes;
    }


    public function index()
    {

        $apiaccounts=ApiAccount::all();
        return view('control.apiaccounts.index')->with(['apiaccounts' => $apiaccounts, 'apiaccount' => new ApiAccount()]);
    }


    public function create()
    {
        return view('control.apiaccounts.edit')->with(['apiaccount' => new ApiAccount()]);
    }


    public function store()
    {
        ApiAccount::create($this->_validate());

        return redirect()->route('control.apiaccount.index')->with('success', 'API Account Erstellt!');
    }

    public function show(ApiAccount $apiaccount)
    {
        return view('control.apiaccounts.edit')->with(['apiaccount' => $apiaccount]);
    }


    public function update(ApiAccount $apiaccount)
    {

        $apiaccount->update($this->_validate());
        return view('control.apiaccounts.edit')->with(['apiaccount' => $apiaccount]);
    }

    public function destroy(ApiAccount $apiaccount)
    {
        $apiaccount->delete();
        return redirect()->route('control.apiaccount.index')->withSuccess('API Account erfolgreich gelöscht');
    }





}
