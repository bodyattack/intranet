<?php

namespace App\Http\Controllers\Control;

use App\Country;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{


    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        $countrys=Country::all();
        return view('control.country.index')->with(['country' => new Country, 'countrys' => $countrys]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country = new Country;
        return view('control.country.edit')->with(['country' => $country]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();

        $country = Country::create($attributes);

        return redirect(route('control.country.index'))->withSuccess($country->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        return view('control.country.edit')->with(['country' => $country]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        return view('control.country.edit')->with(['country' => $country]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Country $country)
    {
        $attributes = $this->_validate();
        $country->update($attributes);

        return back()->withSuccess($country->presentation_short.' bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        $country->delete();
        return back()->withDanger($country->presentation_short.' gelöscht!');
    }


    public function _validate()
    {

        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50',
            'name_de' => 'required|string|min:2|max:50',
            'code' => 'required|string|min:2|max:50',
            'shipping' => 'required|numeric',
            'shipping_free_limit' => 'required|regex:([0-9]+[.]?[0-9]*)',
        ]);

        return $attributes;
    }


    public function toggleActive(Country $country) {
        $country->update(['active' => !$country->active]);
        return back()->withSuccess($country->presentation_short.' wurde '.(!$country->active?'deaktiviert':'aktiviert'));
    }



}
