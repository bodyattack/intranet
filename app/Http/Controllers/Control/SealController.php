<?php

namespace App\Http\Controllers\Control;

use App\Seal;
use App\Http\Controllers\Controller;

class SealController extends Controller
{


    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        $seals=Seal::all();
        return view('control.seal.index')->with(['seal' => new Seal, 'seals' => $seals]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $seal = new Seal;
        return view('control.seal.edit')->with(['seal' => $seal]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();

        $seal = Seal::create($attributes);

        return redirect(route('control.seal.index'))->withSuccess($seal->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Seal $seal)
    {
        return view('control.seal.edit')->with(['seal' => $seal]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Seal $seal)
    {
        return view('control.seal.edit')->with(['seal' => $seal]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Seal $seal)
    {
        $attributes = $this->_validate();
        $seal->update($attributes);

        return back()->withSuccess($seal->presentation_short.' bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seal $seal)
    {
        $seal->delete();
        return back()->withDanger($seal->presentation_short.' gelöscht!');
    }


    public function _validate()
    {

        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50',
            'name_en' => 'sometimes|nullable|string|min:2|max:50',
            'description' => 'sometimes|nullable|string|min:25|max:500',
            'description_en' => 'sometimes|nullable|string|min:25|max:500',
            'icon' => 'sometimes|nullable|string|min:2|max:50',
            'filter_link' => 'sometimes|nullable|string|min:2|max:50',
            'filter_link_en' => 'sometimes|nullable|string|min:2|max:50',
        ]);

        return $attributes;
    }


    public function toggleActive(Seal $seal) {
        $seal->update(['active' => !$seal->active]);
        return back()->withSuccess($seal->presentation_short.' wurde '.(!$seal->active?'deaktiviert':'aktiviert'));
    }



}
