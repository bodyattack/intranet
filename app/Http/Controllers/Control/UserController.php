<?php

namespace App\Http\Controllers\Control;

use App\Department;
use App\Area;
use App\Permission;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function _filterValidate()
    {
        $rules = [
            'filter' => 'sometimes|nullable|numeric',
            'filter_department' => 'sometimes|nullable|int',
            'filter_word' => 'sometimes|nullable|regex:/^[\w]+$/',
            'filter_row' => 'sometimes|nullable|required_with:filter_word,filter|string|regex:/^[\w]+$/',
            'filter_order' => 'sometimes|nullable|required_with:filter|string|in:desc,asc',
        ];

        $attributes = [
            'filter' => 'Filter',
            'filter_department' => 'Abteilung',
            'filter_word' => 'Suchbegriff',
            'filter_row' => 'Suchbereich',
            'filter_order' => 'Sortierung',
        ];

        $attributes = $this->validate(request(), $rules, array(), $attributes);

        return $attributes;
    }

    public function filterQuery($filter_attributes, $paginate=true)
    {
        $query = User::with('level', 'departments', 'areas');

        if (isset($filter_attributes['filter'])) {

            if (isset($filter_attributes['filter_word']) && $filter_attributes['filter_row'] != 1) {
                $query->where($filter_attributes['filter_row'], 'LIKE', '%' . $filter_attributes['filter_word'] . '%');
                if($filter_attributes['filter_row'] == 'name') {
                    $query->orWhere('surname', 'LIKE', '%' . $filter_attributes['filter_word'] . '%');
                }
            }

            #if (isset($filter_attributes['filter_department']) && $filter_attributes['filter_department'] != '') {
            #    $query->where('mandant_id', '=', $filter_attributes['filter_mandant']);
            #}

            $query->orderBy($filter_attributes['filter_row'], $filter_attributes['filter_order']);
        }

        if(!$paginate) return $query->get();

        return $query->paginate($this->elements_per_page);

    }


    public function index()
    {
        $filter_attributes = $this->_filterValidate();

        $users = $this->filterQuery($filter_attributes);

        return view('control.user.index')->with(['user' => new User, 'users' => $users, 'filter_attributes' => $filter_attributes]);

    }


    public function create()
    {
        return view('control.user.edit');
    }


    function show(User $user) {
        return view('control.user.edit')->with(['user' => $user]);
    }


    function edit(User $user) {
        return view('control.user.edit')->with(['user' => $user]);
    }


    public function delete(User $user)
    {
        $user->delete();
        return back()->withDanger('Benutzer gelöscht');
    }


    function store() {

        $attributes = request()->validate([
            'name' => 'required|string|max:128',
            'surname' => 'required|string|max:128',
            'email' => 'required|string|email|max:128|unique:users',
            'password' => 'required|confirmed|min:6',
           # 'position_id' => 'required|not_in:0',
            'erp_name' => 'sometimes|nullable|string|min:2|max:50',
            'tel' => 'sometimes|nullable|string|min:2|max:50',
            'invoice_approvers' => 'sometimes|nullable',
            'api_key' => 'sometimes|nullable|string|min:2|max:100',
        ]);

        #$area_id = request()->validate(['area' => 'required|not_in:0']);

        $invoice_approvers = [];
        if (request()->has('invoice_approvers')) {
            $invoice_approvers = $attributes['invoice_approvers'];
            unset($attributes['invoice_approvers']);
        }

        if(!empty($attributes['password'])) {
            $attributes['password'] = Hash::make($attributes['password']);
            $attributes['email_verified_at'] = date('Y-m-d');
        }

        $user = User::create($attributes);
        #DB::table('users_areas')->insert(['user_id' => $user->id, 'area_id' => $area_id['area']]);

        if (!empty($invoice_approvers)) {
            $user->auditorForDepartments()->sync($invoice_approvers);
        }

        return redirect(route('control.user.index'))->withSuccess('Benutzer erfolgreich angelegt');
    }


    function update(User $user) {

        $attributes = request()->validate([
            'name' => 'required|string|max:128',
            'surname' => 'required|string|max:128',
            'email' => 'required|string|email|max:128|unique:users,email,'.$user->id,
            'password' => '',
            'erp_name' => 'sometimes|nullable|string|min:2|max:50',
            'tel' => 'sometimes|nullable|string|min:2|max:50',
            'invoice_approvers' => 'sometimes|nullable',
            'api_key' => 'sometimes|nullable|string|min:2|max:100',
        ]);

       # $area_id = request()->validate(['area' => 'required|not_in:0']);

        if (request()->has('invoice_approvers')) {
            $user->auditorForDepartments()->sync($attributes['invoice_approvers']);
        }
        else {
            $user->auditorForDepartments()->detach();
        }

        $user->name = $attributes['name'];
        $user->surname = $attributes['surname'];
        $user->email = $attributes['email'];
        $user->erp_name = $attributes['erp_name'];
        $user->tel = $attributes['tel'];
        $user->api_key = $attributes['api_key'];

        if(!empty($attributes['password'])) $user->password = Hash::make($attributes['password']);

        $user->save();

        #DB::table('users_areas')->where('user_id', '=', $user->id)->delete();
        #DB::table('users_areas')->insert(['user_id' => $user->id, 'area_id' => $area_id['area']]);

        return redirect(route('control.user.index'))->withSuccess('Benutzer erfolgreich bearbeitet');

    }

    public function toggleIsActive(User $user) {
        $user->update(['active' => !$user->active]);
        return back()->withSuccess('Benutzer wurde '.(!$user->active?'deaktiviert':'aktiviert'));
    }




    public function departmentAssingShow(User $user) {
        return view('control.user.assingDepartments')->with(['user' => $user]);
    }

    public function areaAssingShow(User $user) {
        return view('control.user.assingAreas')->with(['user' => $user]);
    }


    public function permissionAssingShow(User $user, Area $area) {
        return view('control.user.assingPermissions')->with(['area' => $area, 'user' => $user]);
    }



    public function departmentAssingStore(User $user, Department $department)
    {
        $hasDep = $user->hasDepartment($department);
        
        // Leitung der Abteilung entfernen, sofern an diesen Nutzer vergeben.
        if($hasDep && $department->user_id == $user->id) {
            $department->user_id = null;
            $department->update();
        }

        return $hasDep
            ? $user->departments()->detach($department)
            : $user->departments()->attach($department);
    }


    public function areaAssingStore(User $user, Area $area)
    {
        $url = '';
        if($user->hasArea($area)) {
            $user->areas()->detach($area);
            $user->deletePermissions($area->permissions()->pluck('permission_id')->toArray());
            #$user->deletePermissions(Permission::where('area_id', $area->id)->pluck('id')->toArray());
        }
        else {
            $user->areas()->attach($area);
            $url = route('control.user.permission.assing.show', ['user' => $user->id, 'area' => $area->id]);
        };

        return ['area' => $area->id, 'url' => $url];

    }

    public function permissionAssingStore(User $user, Permission $permission) {

        return $user->hasPermission($permission)
            ? $user->permissions()->detach($permission)
            : $user->permissions()->attach($permission);
    }



    public function user_switch_start( $new_user )
    {
        \Session::put( 'admin_user', Auth::id() );
        Auth::guard('backend_user')->logout();
        Auth::guard('web')->loginUsingId($new_user);
        Auth::logout();
        return redirect(route('app.dashboard'));
    }

    public function user_switch_stop()
    {
        $id = \Session::pull( 'admin_user' );
        Auth::guard('web')->logout();
        Auth::guard('backend_user')->loginUsingId($id);
        return redirect(route('control.user.index'));
    }

}
