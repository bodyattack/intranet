<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;
use App\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PermissionsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:backend_user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $permissions=Permission::with('area')->get();

        return view('control.permissions.index')->with(['permissions' => $permissions]);
    }


    public function _validate()
    {
        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50',
            'description' => 'sometimes|nullable|string|min:10',
            'area_id' => 'required|integer',
        ]);

        $attributes['slug'] = Str::slug($attributes['name']);

        return $attributes;
    }


    public function create()
    {
        return view('control.permissions.edit');
    }


    public function store()
    {

        $attributes = $this->_validate();
        $permission = Permission::create($attributes);
        $insert_id = $permission->id;

        $stay = (request('submit-type')=='stay'?true:false);
        
        if(!$stay) {
            $permissions=Permission::all();
        }

        return ($stay)
            ? redirect('backend/permissions/'.$permission->id)
            : view('control.permissions.index')->with(['permissions' => $permissions, 'insert_id' => $insert_id]);
    }


    public function show(Permission $permission)
    {
        return view('control.permissions.edit')->with(['permission' => $permission]);
    }


    public function update(Permission $permission)
    {
        $attributes = $this->_validate();
        $permission->update($attributes);

        return view('control.permissions.edit')->with(['permission' => $permission]);
    }

    public function delete(Permission $permission)
    {
        $permission->delete();
        return redirect()->route('control.permissions.index')->withSuccess('Berechtigung erfolgreich gelöscht');
    }

}
