<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;
use App\Mail\UserWelcome;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReleaseInformation;

use  App\Externals\Gurado\Vouchers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:backend_user');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {

        #dd((new PreAuthorization)->authorization('8923496476277', '60DC839D-1'));
        #dd((new Vouchers)->create(25));

        return view('control.dashboard');
    }


    public function mailtest()
    {
     Mail::to('c.stein@body-attack.com')->send(new UserWelcome());
      return view('control.dashboard');
    }
}
