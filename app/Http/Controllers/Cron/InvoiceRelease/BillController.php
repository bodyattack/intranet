<?php

namespace App\Http\Controllers\Cron\InvoiceRelease;

use App\Http\Controllers\App\InvoiceRelease\BillItemController;

use App\IrMailbox as Mail;
use App\IrBill as Bill;
use App\IrBiller as Biller;
use App\Http\Controllers\Controller;
use App\Library\ImapController;

class BillController extends Controller
{

    public function loadFromMailbox() {

        $develop = env('APP_DEBUG');

        $mailboxes = Mail::with('mandant')->where('active', 1)->get();

        $imap = new ImapController();

        foreach ($mailboxes as $mailbox) {

            if(!$imap->connect($mailbox->conf, $mailbox->user, $mailbox->pass)) continue;

            if($imap->checkMails()) {

                $imap->sender_address = 'noreply@body-attack.com';
                $imap->forwarding_address = $mailbox->mandant->datev_email;
                if($develop) $imap->forwarding_address = 'c.stein@body-attack.com';  // testing

                $imap->checkRequiredFolders();

                foreach ($imap->getMailIds() As $key => $mail_id) {

                    if($develop && $key > 1) continue;  // testing

                    $email = $imap->getMailById($mail_id, !$develop);

                    // Generate invoice if assignable and attachments are available
                    if ($valid = $email->hasAttachments()) {

                        $attachments = $imap->validateAttachments($email->getAttachments());

                        if ($valid = empty($imap->getError())) {

                            $subject = $email->subject;
                            $sender = $email->fromAddress;

                            // Versuche Treffer auf E-Mail
                            $biller = Biller::where('match_email', $sender)->first();

                            // Versuche Treffer Betreff
                            if(!$biller && trim($subject) != '') {
                                $biller = Biller::where('match_subject', $subject)->first();
                            }

                            // Versuche * Treffer auf E-Mail Domain
                            if(!$biller) {
                                $sender_part = explode('@', $sender);
                                $biller = Biller::where('match_email', 'like', '%' . $sender_part[1])->first();
                            }

                            $mandant_id = $mailbox->mandant_id;
                            $mailbox_id = $mailbox->id;

                            if(!$biller) {
                                // Nichts gefunden
                                $due_date = date('Y-m-d');
                                $payment_method = 'Rechnung';
                                $currency = 'EUR';
                                $biller_id = 0;
                                $department_id = 0;
                                $costcenter_id = 0;
                                $status = 'not_assigned';
                            }
                            else {
                                // Eintrag gefunden
                                $department_assing = ( count($biller->distributor) == 0 ? 0 : $biller->distributor[0]->id);
                                $due_date = date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $biller->due_days . ' days'));
                                $payment_method = $biller->payment;
                                $currency = $biller->currency;
                                $biller_id = $biller->id;
                                $department_id = $department_assing;
                                $costcenter_id = $biller->costcenter_id;
                                $status = ( $department_assing == 0 ? 'not_assigned' : 'neu' );
                            }

                            foreach ($attachments AS $attachment) {
                                $bill = Bill::create([
                                    'due_date' => $due_date,
                                    'payment_method' => $payment_method,
                                    'currency' => $currency,
                                    'mailbox_id' => $mailbox_id,
                                    'mandant_id' => $mandant_id,
                                    'biller_id' => $biller_id,
                                    'department_id' => $department_id,
                                    'costcenter_id' => $costcenter_id,
                                    'status' => $status,
                                ]);

                                $base64_file = base64_encode($attachment->getContents());
                                $bill_item = (new BillItemController())->create($base64_file);
                                if (!empty($bill_item)) {
                                    $bill_item->update(['bill_id' => $bill->id]);
                                    $imap->forwardMailById($email, $attachment, 'datev');
                                }
                            }
                            $imap->moveToFolderByID($mail_id, 'INBOX/Erledigt');
                            #$imap->deleteMailById($mail_id);
                        }
                    }
                    else {
                        $imap->setError('Nachricht hat keine Anlagen!');
                    }

                    if(!$valid) {
                        if(!$develop) $imap->forwarding_address = 'buchhaltung@body-attack.de';
                        $imap->sender_address = 'noreply@body-attack.com';
                        $imap->forwardMailById($email, [], 'error');
                        $imap->moveToFolderByID($mail_id, 'INBOX/Error');
                    }

                } // Mails handle

            } // Mails exist

        } // Mailbox loop


    }


}

