<?php

namespace App\Http\Controllers\Cron;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\TempOrder;
use App\PayonePayments;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Externals\BusinessCentral\Orders;

class TempOrdersController extends Controller
{

    public function pushOrders($limit = 1)
    {

        // Hole Jobs zum übertragen
        $ordes_querry = TempOrder::where('transfer', 0)->where('cron_processing', 0)->take($limit);
        $orders = $ordes_querry->get();

        // Blockiere Daten für cron Verarbeitung
        $ordes_querry->update(['cron_processing'=>'1']);

        foreach ($orders as $order) {

            // erkennen das cron_progressing verändert wurde
            $order->refresh();

            $post_array = [];
            $MS_BC_XML = "<s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'><s11:Body><ns1:CreateSalesOrder xmlns:ns1='urn:microsoft-dynamics-schemas/codeunit/InboundCreateInboundOrder_TWC'><ns1:inboundCreateInbOrder>";
            $MS_BC_XML.="<Header_General>";
            $MS_BC_XML.="<Document_No></Document_No>";
            $MS_BC_XML.="<Order_Date>".Carbon::parse($order->datum)->format('Y-m-d')."</Order_Date>";
            $MS_BC_XML.="<Currency_Code></Currency_Code>";
            $MS_BC_XML.="<Shipping_Agent_Code>DHL_DE</Shipping_Agent_Code>"; // Zusteller
            $MS_BC_XML.="<Shipping_Agent_Service_Code>NATIONAL</Shipping_Agent_Service_Code>"; // Zustellertransportartencode
            $MS_BC_XML.="<External_Document_No>".$order->transaction_key."</External_Document_No>"; // Referenz Nummer Shop Auftrag
            $MS_BC_XML.="<Payment_Method_Code>PAYONE</Payment_Method_Code>";
            $MS_BC_XML.="<Prices_Incl_VAT>true</Prices_Incl_VAT>";
            $MS_BC_XML.="</Header_General>";

                // Kundendaten
            $post_array['anrede'] = $order->kunde['salutation'];
            $post_array['vorname'] = $order->kunde['firstname'];
            $post_array['nachname'] = $order->kunde['lastname'];
            $post_array['strasse'] = $order->kunde['street'];
            $post_array['plz'] = $order->kunde['zip'];
            $post_array['ort'] = $order->kunde['city'];
            $post_array['land'] = $order->kunde['country'];
            $post_array['zusatz'] = $order->kunde['company'] ?? '';
            $post_array['telefon'] = $order->kunde['telephonenumber'] ?? '';
            $post_array['email'] = $order->kunde['email'];
            $post_array['emailcheck'] = $order->kunde['email'];

            $country = $order->kunde['country'];

            $post_array['vorname'] = mb_convert_encoding($post_array['vorname'], 'Windows-1252', 'UTF-8');
            $post_array['nachname'] = mb_convert_encoding($post_array['nachname'], 'Windows-1252', 'UTF-8');
            $post_array['strasse'] = mb_convert_encoding($post_array['strasse'], 'Windows-1252', 'UTF-8');
            $post_array['ort'] = mb_convert_encoding($post_array['ort'], 'Windows-1252', 'UTF-8');
            $post_array['land'] = mb_convert_encoding($post_array['land'], 'Windows-1252', 'UTF-8');
            $post_array['zusatz'] = mb_convert_encoding($post_array['zusatz'], 'Windows-1252', 'UTF-8');

            // Anrede ?
            $MS_BC_XML.="<Header_Sell_To_Customer>";
            $MS_BC_XML.="<Contact_No></Contact_No>"; // Keine Pflicht, sinnvoll es später mit zu übergeben und über das ERP zum Shop zu übertragen
            $MS_BC_XML.="<Name>".(!empty(trim($order->kunde['firstname'])) ? $order->kunde['firstname'] : '').(!empty(trim($order->kunde['lastname'])) ? " ".$order->kunde['lastname'] : '')."</Name>";
            $MS_BC_XML.="<Name_2></Name_2>";
            $MS_BC_XML.="<First_Name>".(!empty(trim($order->kunde['firstname'])) ? $order->kunde['firstname'] : '')."</First_Name>";
            $MS_BC_XML.="<Surname>".(!empty(trim($order->kunde['lastname'])) ? $order->kunde['lastname'] : '')."</Surname>";
            $MS_BC_XML.="<Address>".(!empty(trim($order->kunde['street'] )) ? $order->kunde['street']  : '')."</Address>";
            $MS_BC_XML.="<Address_2>".(!empty(trim($order->kunde['company'])) ?  $order->kunde['company'] : '')."</Address_2>";
            $MS_BC_XML.="<Post_Code>".(!empty(trim($order->kunde['zip'])) ? $order->kunde['zip'] : '')."</Post_Code>";
            $MS_BC_XML.="<City>".(!empty(trim($order->kunde['city'])) ? $order->kunde['city'] : '')."</City>";
            $MS_BC_XML.="<Country_Code>".(!empty(trim($order->kunde['country'])) ? $order->kunde['country'] : '')."</Country_Code>";
            $MS_BC_XML.="<Template_Code>INL_PRIVAT</Template_Code>"; // Debitoren Vorlagen-Code - Optional (Alternativ würde auf das Land und der dort zugeordneten Vorlage geschaut)
            $MS_BC_XML.="<E_Mail_Address>".(!empty(trim($order->kunde['email'])) ? $order->kunde['email'] : '')."</E_Mail_Address>";
            $MS_BC_XML.="</Header_Sell_To_Customer>";
            $MS_BC_XML.="<Header_Bill_To_Customer></Header_Bill_To_Customer>";  // B2B relevant

            // ggf. abweichende Lieferdaten
            $post_array['lieferabweich'] = 0;
            if (isset($order->kunde['lfirstname']) && !empty(trim($order->kunde['lfirstname']))) {
                $post_array['lieferabweich'] = 1;

                $post_array['lanrede'] = $order->kunde['lsalutation'];
                $post_array['lvorname'] = $order->kunde['lfirstname'];
                $post_array['lnachname'] = $order->kunde['llastname'];
                $post_array['lstrasse'] = $order->kunde['lstreet'];
                $post_array['lplz'] = $order->kunde['lzip'];
                $post_array['lort'] = $order->kunde['lcity'];
                $post_array['lland'] = $order->kunde['lcountry'];
                $post_array['ltelefon'] = $order->kunde['ltelephonenumber'] ?? '';
                $post_array['lcompany'] = $order->kunde['lcompany'] ?? '';

                if( preg_match('/Packstation/i', $post_array['lstrasse']) ) {
                    $post_array['lvorname'] = $post_array['lvorname'].' '.$post_array['lnachname'];
                    $post_array['lnachname'] = $post_array['lcompany'];
                }

                $post_array['lvorname'] = mb_convert_encoding($post_array['lvorname'], 'Windows-1252', 'UTF-8');
                $post_array['lnachname'] = mb_convert_encoding($post_array['lnachname'], 'Windows-1252', 'UTF-8');
                $post_array['lstrasse'] = mb_convert_encoding($post_array['lstrasse'], 'Windows-1252', 'UTF-8');
                $post_array['lort'] = mb_convert_encoding($post_array['lort'], 'Windows-1252', 'UTF-8');
                $post_array['lland'] = mb_convert_encoding($post_array['lland'], 'Windows-1252', 'UTF-8');

                if (isset($order->kunde['lcountry']) && !empty(trim($order->kunde['lcountry']))) {
                    $country = $order->kunde['lcountry'];
                }

                $MS_BC_XML.="<Header_Ship_To_Customer>";
                $MS_BC_XML.="<Name>".(!empty(trim($order->kunde['lfirstname'])) ? $order->kunde['lfirstname'] : '').(!empty(trim($order->kunde['lfirstname'])) ? " ".$order->kunde['lfirstname'] : '')."</Name>";
                $MS_BC_XML.="<Name_2>".(!empty(trim($order->kunde['lcompany'])) ? $order->kunde['lcompany'] : '')."</Name_2>";
                $MS_BC_XML.="<First_Name>".(!empty(trim($order->kunde['lfirstname'])) ? $order->kunde['lfirstname'] : '')."</First_Name>";
                $MS_BC_XML.="<Surname>".(!empty(trim($order->kunde['lfirstname'])) ? $order->kunde['lfirstname'] : '')."</Surname>";
                $MS_BC_XML.="<Address>".(!empty(trim($order->kunde['lstreet'] )) ? $order->kunde['lstreet']  : '')."</Address>";
                $MS_BC_XML.="<Address_2>".(!empty(trim($order->kunde['lcompany'])) ? $order->kunde['lcompany'] : '')."</Address_2>";
                $MS_BC_XML.="<Post_Code>".(!empty(trim($order->kunde['lzip'])) ? $order->kunde['lzip'] : '')."</Post_Code>";
                $MS_BC_XML.="<City>".(!empty(trim($order->kunde['lcity'])) ? $order->kunde['lcity'] : '')."</City>";
                $MS_BC_XML.="<Country_Code>".(!empty(trim($order->kunde['lcountry'] )) ? $order->kunde['lcountry']  : '')."</Country_Code>";
                if( preg_match('/Packstation/i', $order->kunde['lstreet']) ) {  // 2. Zeile Adresse - Postnummer!?
                    $MS_BC_XML.="<ShipmentType>Package Station</ShipmentType>";
                    $MS_BC_XML.="<Post_No>".(!empty(trim($order->kunde['lcompany'])) ? $order->kunde['lcompany'] : '')."</Post_No>";
                    $MS_BC_XML.="<Parcel_Station_No>".(!empty(trim($order->kunde['lstreet'])) ? $order->kunde['lstreet'] : '')."</Parcel_Station_No>";
                }
                else $MS_BC_XML.="<ShipmentType>Standard</ShipmentType>";
                $MS_BC_XML.="</Header_Ship_To_Customer>";

            }

            if($post_array['lieferabweich']==0) {


                $post_array['lanrede'] = $order->kunde['lsalutation'];
                $post_array['lvorname'] = $order->kunde['lfirstname'];
                $post_array['lnachname'] = $order->kunde['llastname'];
                $post_array['lstrasse'] = $order->kunde['lstreet'];
                $post_array['lplz'] = $order->kunde['lzip'];
                $post_array['lort'] = $order->kunde['lcity'];
                $post_array['lland'] = $order->kunde['lcountry'];
                $post_array['ltelefon'] = $order->kunde['ltelephonenumber'] ?? '';
                $post_array['lcompany'] = $order->kunde['lcompany'] ?? '';


                $MS_BC_XML.="<Header_Ship_To_Customer>";
                $MS_BC_XML.="<Name>".(!empty(trim($order->kunde['lfirstname'])) ? $order->kunde['lfirstname'] : '').(!empty(trim($order->kunde['llastname'])) ? " ".$order->kunde['llastname'] : '')."</Name>";
                $MS_BC_XML.="<Name_2>".(!empty(trim($order->kunde['lcompany'])) ? $order->kunde['lcompany'] : '')."</Name_2>";
                $MS_BC_XML.="<First_Name>".(!empty(trim($order->kunde['lfirstname'])) ? $order->kunde['lfirstname'] : '')."</First_Name>";
                $MS_BC_XML.="<Surname>".(!empty(trim($order->kunde['llastname'])) ? $order->kunde['llastname'] : '')."</Surname>";
                $MS_BC_XML.="<Address>".(!empty(trim($order->kunde['lstreet'] )) ? $order->kunde['lstreet']  : '')."</Address>";
                $MS_BC_XML.="<Address_2>".(!empty(trim($order->kunde['lcompany'])) ? $order->kunde['lcompany'] : '')."</Address_2>";
                $MS_BC_XML.="<Post_Code>".(!empty(trim($order->kunde['lzip'])) ? $order->kunde['lzip'] : '')."</Post_Code>";
                $MS_BC_XML.="<City>".(!empty(trim($order->kunde['lcity'])) ? $order->kunde['lcity'] : '')."</City>";
                $MS_BC_XML.="<Country_Code>".(!empty(trim($order->kunde['lcountry'] )) ? $order->kunde['lcountry']  : '')."</Country_Code>";
                if( preg_match('/Packstation/i', $order->kunde['lstreet']) ) {  // 2. Zeile Adresse - Postnummer!?
                    $MS_BC_XML.="<ShipmentType>Package Station</ShipmentType>";
                    $MS_BC_XML.="<Post_No>".(!empty(trim($order->kunde['lcompany'])) ? $order->kunde['lcompany'] : '')."</Post_No>";
                    $MS_BC_XML.="<Parcel_Station_No>".(!empty(trim($order->kunde['lstreet'])) ? $order->kunde['lstreet'] : '')."</Parcel_Station_No>";
                }
                else $MS_BC_XML.="<ShipmentType>Standard</ShipmentType>";
                $MS_BC_XML.="</Header_Ship_To_Customer>";

            }

            if($order->lieferlandabweichung == 1) {
                $post_array['lieferabweich'] = 1;
            }

            // Relevante Daten für den Eintrag via Schnittstelle
            $shop_id = '1';
            $post_array['AKTION'] = 'neuerauftrag';
            #$post_array['warenkorb'] = $this->getWarenkorbString($order->warenkorb_string, $shop_id, $country);
            $post_array['warenkorb'] = mb_convert_encoding($this->getWarenkorbString($order->warenkorb_string, $shop_id, $country), 'Windows-1252', 'UTF-8');

            $post_array['SHOP_ID'] = $shop_id;
            $post_array['transcode'] = $this->getTranscode();
            $post_array['IP'] = $order->kunde['ip'];
            $post_array['CFIDCFTOKEN'] = '202101010:12345678'; // Static value, wurde zuvor im Warenkorb / Kasse benötigt
            $post_array['Browser'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0';
            $post_array['REFERERNUN'] = 'http://www.body-attack.de';
            $post_array['agbok'] = 'on';

            // $post_array['shipping_method'] = 'DHL INL'; // Bewußt nicht definiert, wird über Schnittstelle automatisch zu 'DHL STAND'
            $post_array['rabatt'] = '';
            $rabatt = '0';
            if (isset($order->gutschein) && !empty(trim($order->gutschein))) {
                $gutschein = explode("|", $order->gutschein);
                $post_array['gutscheincode'] = $gutschein[0];

                $rabatt = round($order->warenkorb_nachlass / 100, 2);
                if ($order->coupon_codes->isNotEmpty() && $order->coupon_codes->first()->coupon->art == 1) {
                    $post_array['rabatt'] = $rabatt;
                }
            }

            $gurado_betrag = 0;
            if(!empty($order->gurado)) {
                foreach ($order->gurado as $gurado) {
                    $gurado_betrag += floatval($gurado['amount']);
                }
            }

            $versandkosten = round($order->versandkosten / 100, 2);
            $produktkosten = round(($order->gesamtsumme - $order->versandkosten) / 100, 2);
            $guradozahlung = round($gurado_betrag, 2);
            $preis = $produktkosten . '+' . $versandkosten . '+' . $guradozahlung;

            $post_array['payment_method'] = 'payone_any';
            $post_array['payment_method_type'] = $order->transaction_key . ';' . $order->TxId . ';' . $order->userid_payone . ';' . $order->zahlungsart . ';' . $preis . ';' . $rabatt;
            $post_array['versandkosten'] = $versandkosten;

            $MS_BC_XML.="<Header_Payment>";
            $MS_BC_XML.="<TX_ID>".$order->TxId."</TX_ID>";
            $MS_BC_XML.="<TX_Code>".$order->zahlungsart."</TX_Code>";  // Zahlcode Payone
            $MS_BC_XML.="<TX_Amount>".$produktkosten."</TX_Amount>"; // Float, ganze euro
            $MS_BC_XML.="<PmtTransactionAmount>".$produktkosten."</PmtTransactionAmount>";  // Was hier rein?
            $MS_BC_XML.="</Header_Payment>";

            $MS_BC_XML.="<Sales_Lines>";
            $MS_BC_XML.= mb_convert_encoding($this->getWarenkorbStringXML($order->warenkorb_string), 'Windows-1252', 'UTF-8');
            $MS_BC_XML.="</Sales_Lines>";

            $MS_BC_XML.="<Comment_Lines>";
            $MS_BC_XML.="<Comment_Line>";
            $MS_BC_XML.="<Comment>Auftrag aus BA-Shop</Comment>";
            $MS_BC_XML.="</Comment_Line>";
            $MS_BC_XML.="</Comment_Lines>";

            $MS_BC_XML.="</ns1:inboundCreateInbOrder></ns1:CreateSalesOrder></s11:Body></s11:Envelope>";

            // Zusätzliche leere Daten, so auch zuvor vom Kassenserver mit übergeben
            $params = explode(',', 'ERPKDNR,fax,bemerkung,Customers_ID,kennwort,kennwortcheck,bestellzeichen,geburtstag,cc_owner,cc_number,cc_checkcode,cc_type,cc_monat,cc_jahr,freundeemail,freundepraemie,freundegeschmack,kto_number,kto_blz,werbequelle');
            foreach ($params as $param) $post_array[$param] = '';
            #dd($post_array);

            // POST call to -> http://80.151.203.43:88/api_test_hetzner.cfm
            $response = $this->sendRequest($post_array, $MS_BC_XML);
            #dd($response);

            // Rückgabe spliten wenn erstes Element "OK" und min 1 Zahl enthält, dann Erfolg!
            $data = explode('~', $response);
            $uebertragen=0;
            if (isset($data[0]) && preg_match('/(OK)([0-9]+)/i', $data[0])) {
                $order->intranet_id = substr($data[0], 2);
                $order->transfer = 1;
                $uebertragen=1;
            } elseif (isset($data[0]) && !preg_match('/Server busy/i', $data[0])) {
                Log::channel('api')->error('LOG aus Cron TempOrders / Intranet response : ' . $response);
                if($data[0] != 'Server down') $order->transfer = -1;
                $error = mb_convert_encoding($response, 'UTF-8', 'Windows-1252');
                $order->intranet_api_error = "$error";
            }

            $order->cron_processing = 0;
            $order->update();

            if($uebertragen) {
                $orders = new Orders();
                $orders->create($MS_BC_XML);
            }

        }

    }

    public function resetCronStatus()
    {
        DB::statement('UPDATE temp_orders SET cron_processing = 0 WHERE 1 ');
    }


    public function getOrderDetails($limit=1) {

        // hole Jobs älter 7 Tage
        $current_date = Carbon::now();
        $date = Carbon::parse($current_date)->subDays(7)->format('Y-m-d');

        $ordes_querry = TempOrder::doesntHave('payonePayments')
            ->whereDate('created_at', '<=', $date)
            ->whereDay('updated_at', '<>', $current_date->day)
            ->where('intranet_orders_data_replay_count', '<', '7')
            ->where('intranet_id', '<>', '')
            ->where('cron_processing', 0)->take($limit);
        $orders = $ordes_querry->get();

        // blockiere daten
        $ordes_querry->update(['cron_processing'=>'1']);

        foreach ($orders as $order) {
            // erkennen das cron_progressing verändert wurde
            $order->refresh();

            $post_array['aktion'] = 'payment_data';
            $post_array['custom_orders_id'] = $order->intranet_id;

            // {"ERP_Kundenr":"369729","Rechnungsnummern":"RG1212058960","custom_orders_id":"4484836"}
            $response = $this->sendRequest($post_array);

            if($response!='Server down') {

                $data = json_decode($response, true);

                if (!empty($data)) {

                    // Überspringen wenn keine Daten vorhanden, aber als Versuch zählen!
                    $valid = true;
                    if (empty(trim($data['ERP_Kundenr']))) $valid = false;
                    if (empty(trim($data['Rechnungsnummern']))) $valid = false;

                    if ($valid) {
                        $payonePayment = PayonePayments::updateOrCreate(
                            ['txid' => $order->TxId],
                            ['temp_order_id' => $order->id, 'ERP_Kundenr' => $data['ERP_Kundenr'], 'custom_orders_id' => $data['custom_orders_id'], 'Rechnungsnummern' => $data['Rechnungsnummern']]
                        );
                    }

                } else {
                    Log::channel('api')->error('LOG aus Cron TempOrders / getOrderDetails, keine Daten erhalten zu ' . $order->intranet_id);
                }
            }

            $order->intranet_orders_data_replay_count++;
            $order->cron_processing = 0;
            $order->update();

        }

    }

    private function sendRequest($post_array, $xml='') {

        $url = 'http://80.151.203.43:88/api_test_hetzner.cfm';
        if(!env('APP_DEBUG')) $url = 'http://80.151.203.43:88/index.cfm';
        else $post_array['xml'] = $xml;

        if(!$this->accessible($url)) return 'Server down';

        try {
            $client = new Client();

            $response = $client->request('POST', $url, [
                'form_params' => $post_array,
                'connect_timeout' => 2,
            ]);
            $response = (string) $response->getBody()->getContents();

            if ( !preg_match("/OK/i", $response) AND preg_match("/404/i", $response) ) return 'URL not found';

            // Leerzeilen und Zeichen entfernen und Ausgabe als String
            return  trim(preg_replace('/\s\s+/', ' ', $response));

        } catch (ClientException $e) {
            $context = '';
            if(!empty($e->getMessage())) $context.= (string) $e->getMessage();
            if(!empty($e->getResponse())) $context.= (string) $e->getResponse()->getBody();
            Log::channel('api')->error('LOG aus TempOrders Controller: '.$context);

            return 'Api ClientException ERROR';
        }
        catch (TransferException $e) {
            $context = '';
            if(!empty($e->getMessage())) $context.= (string) $e->getMessage();
            if(!empty($e->getResponse())) $context.= (string) $e->getResponse()->getBody();
            Log::channel('api')->error('LOG aus TempOrders Controller: '.$context);

            return 'Api TransferException ERROR';
        }

    }

    private function accessible($url) {
        $hdrs = @get_headers($url);
        return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
    }


    private function getTranscode() {

        return rand(1,9).date('Ymd').rand(10,99).date('His').rand(1,9);

    }


    private function getWarenkorbString($warebkorb, $shop_id, $country='DE') {

        // $country wird aktuell nicht verarbeitet, da die logik dahinter unklar ist
        // [shop],[artnr],[menge],[preis],_[varianten],0,0,_CH1,0,_[staffel]^

        // WARENKORB: 1,2138,2,17.4900,_Geschmack: Chocolate,0,0,_,0,_0^1,4291,1,11.9900,_,0,0,_,0,_^

        $wk_string = '';
        foreach ($warebkorb AS $key => $item) {

            $menge = $item['menge'];

            $variante='_,0,0,_,0'; // Part Variante
            if (isset($item['variante']) && !empty(trim($item['variante']))) {
                // Pakete
                if (isset($item['paketvariantenprodukt']) && trim($item['paketvariantenprodukt']) != "") {
                    $varianten= explode(',', $item['variante']);
                    $varianten_produkte= explode('^', $item['paketvariantenprodukt']);
                    $variante='_';
                    foreach ($varianten_produkte AS $key => $varianten_produkt) {
                        if($variante!='_') $variante.='|';
                        $variante.=$varianten_produkt.': '.$varianten[$key];
                    }
                    $variante.=',0,0,_,0';

                }
                else {
                    $variante='_Geschmack: '.$item['variante'].',0,0,_,0';
                }
            }

            $staffel='_'; // Part Staffel
            if (isset($item['staffel']) && trim($item['staffel']) != "") {
                $staffel='_0';
            }

            $wk_string.=$shop_id.',';
            $wk_string.=$item['product_id'].',';
            $wk_string.=$menge.',';
            $wk_string.=$item['einzelpreis'].',';
            $wk_string.=$variante.',';
            $wk_string.=$staffel;
            $wk_string.='^';

        }

        return $wk_string;

    }


    private function getWarenkorbStringXML($warebkorb) {

        $wk_string = '';

        foreach ($warebkorb AS $key => $item) {

            // Zwischenlösung bis Theis es impimentiert hat.
            $art_no = $item['product_id'];
            $artikelliste = DB::table('artikelliste')->where('products_id', $item['product_id'])->where('geschmack', $item['variante'])->orderByDesc('artnr')->first();
            if(!empty($artikelliste)) {
                $art_no = $artikelliste->artnr;
            }

            $wk_string.= '<Sales_Line>';
            $wk_string.= '<Line_No>'.(10000+($key*10000)).'</Line_No>';  // Nummer in 10T Schritten
            $wk_string.= '<Type>Item</Type>'; //  Item oder Sachkonto <Type>G/L Account</Type>
            $wk_string.= '<No>'.$art_no.'</No>';
            $wk_string.= '<Description>'.$item['product_name'].'</Description>';
            $wk_string.= '<Description_2>'.$item['variante'].'</Description_2>';
            $wk_string.= '<Quantity>'.$item['menge'].'</Quantity>';
            $wk_string.= '<Line_Amount_Incl_VAT>'.($item['menge']*$item['einzelpreis']).'</Line_Amount_Incl_VAT>'; // Gesamtpreis der Zeile bezogen auf die Menge
            $wk_string.= '<Unit_Price>'.$item['einzelpreis'].'</Unit_Price>'; // Einzellpreis
            # $wk_string.= '<Unit_Of_Measure_Code>'.mb_convert_encoding('STÜCK', 'Windows-1252', 'UTF-8').'</Unit_Of_Measure_Code>'; // optional
            $wk_string.= '</Sales_Line>';
            
        }

        return $wk_string;

    }


}
