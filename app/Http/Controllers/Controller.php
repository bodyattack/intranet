<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $elements_per_page = 15;

    protected $entry;

    protected $user;


    public function __construct()
    {
        // if the auth user is a standard user, the entry settings are stored and shared to view
        $this->middleware(function ($request, $next) {

            $this->user = Auth::user();

            if(is_a($this->user, User::class)) {
                $this->entry = $this->user->groupAreasAndPermissions();
                $this->entry['departments'] = $this->user->getDepartmentsBySlug();
                $this->entry['leader'] = $this->user->departmentsLeader();

                view()->share(
                    [
                    'entry' => $this->entry,
                    'user' => $this->user
                    ]
                );
            }
            else {
                view()->share(
                    [
                        'backenduser' => $this->user
                    ]
                );
            }

            return $next($request);

        });
    }

    public function checkEntry($type, $value)
    {
        if(array_key_exists($type, $this->entry) && array_key_exists($value, $this->entry[$type])) {
            return true;
        }
        return false;
    }


    private function getRelatedName() {
        return Str::of(get_class($this))->afterLast('\\');
    }

    public function csv()
    {
        if(method_exists($this , '_filterValidate')) {
            $filter_attributes = $this->_filterValidate();
            $this->elements_per_page=0;

            return $this->filterQuery($filter_attributes, false);
        }

        return null;
    }


    # TODO / Austauschen im Code
    public function getJsonCsvData()
    {
        if(method_exists($this , '_filterValidate')) {
            $filter_attributes = $this->_filterValidate();
            $this->elements_per_page=0;

            return $this->filterQuery($filter_attributes, false);
        }

        return null;
    }


    # TODO / Fertigstellen udn je Daten typ / Input global verfügbar machen
    public function exportToCsv($data=[], $header=[], $fileName = 'codes.csv')
    {

        if(empty($data) && method_exists($this , '_filterValidate')) {
            $data = $this->filterQuery($filter_attributes);
        }

        if(!empty($data)) {

            $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );

            $callback = function () use ($data, $header) {
                $file = fopen('php://output', 'w');
                if(!empty($header)) {
                    fputcsv($file, $header);
                }

                foreach ($data as $row) {
                    fputcsv($file, $row);
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);

        }

        return null;

    }



}
