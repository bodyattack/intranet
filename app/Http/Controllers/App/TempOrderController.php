<?php

namespace App\Http\Controllers\App;

use App\TempOrder;
use App\Http\Controllers\Controller;
use App\Filters\TempOrdersFilters;
use Illuminate\Http\Request;

class TempOrderController extends Controller
{


    public function index(TempOrdersFilters $tempOrdersFilters)
    {
        $last_month = date('Y-m-01 00:00:00', strtotime("-1 months"));
        $tempOrdersFilters->setDefaultFilters(['date_from' => $last_month]);
        $temporders = Temporder::filter($tempOrdersFilters)->sortable(['transfer' => 'ASC'])->orderBy('id', 'DESC')->paginate(25);

        $filter = $tempOrdersFilters->getFilters();

        return view('app.temporder.index')->with(['temporder' => new Temporder, 'temporders' => $temporders, 'filter' => $filter]);
    }


    public function create()
    {
        $temporder = new TempOrder;
        return view('app.temporder.edit')->with(['temporder' => $temporder]);
    }


    public function store()
    {
        $attributes = $this->_validate();

        $temporder = TempOrder::create($attributes);

        return redirect(route('app.temporder.index'))->withSuccess($temporder->presentation_short.' erfolgreich angelegt!');
    }



    public function dashboardSearch(Request $request, TempOrdersFilters $tempOrdersFilters)
    {
        $quest = $request->get('quest');
        $art = $request->get('art');
        if(empty($quest)) return back()->withDanger('Sie müssen einen Suchbegriff eingeben!');

        $tempOrdersFilters->setDefaultFilters([$art => $quest], 1);
        $temporders = Temporder::filter($tempOrdersFilters)->paginate(15);
        $filter = $tempOrdersFilters->getFilters();

        if(empty($temporders))  return back()->withDanger('Es wurden keine Daten zu Ihrer Suche gefunden');

        return view('app.temporder.index')->with(['temporder' => new Temporder, 'temporders' => $temporders, 'filter' => $filter]);

    }



    public function show(TempOrder $temporder)
    {
        return view('app.temporder.edit')->with(['temporder' => $temporder]);
    }


    
    public function destroy(TempOrder $temporder)
    {
        if(!$this->checkEntry('permissions', 'bestellungen-loschen')) {
            return back()->withDanger('Du bist nicht berechtigt für diese Aktion');
        };
        $temporder->delete();
        return back()->withDanger($temporder->presentation_short.' gelöscht!');
    }


    public function getOrderAddress(TempOrder $temporder) {


        if(!empty($temporder)) {
            return view('app.temporder._editAddress')->with([
                'temporder' => $temporder,
            ]);
        }
        return '';

    }


    public function setOrderAddress(TempOrder $temporder) {

        $keys = ['salutation', 'firstname', 'lastname', 'company', 'street', 'zip', 'city', 'country', 'telephonenumber', 'email', 'lsalutation',  'lfirstname', 'llastname', 'lcompany', 'lstreet', 'lzip', 'lcity' , 'lcountry' , 'ltelephonenumber', 'kundenid', 'ip' ];
        $attributes = request()->validate([
            'salutation' => 'sometimes|nullable|string|min:1',
            'firstname' => 'required|string|min:1',
            'lastname' => 'required|string|min:1',
            'company' => 'sometimes|nullable|string|min:1',
            'street' => 'required|string|min:1',
            'zip' => 'required|string|min:1',
            'city' => 'required|string|min:1',
            'country' => 'sometimes|nullable|string|min:1',
            'telephonenumber' => 'sometimes|nullable|string|min:1',
            'email' => 'sometimes|nullable|string|min:1',

            'lfirstname' => 'sometimes|nullable|string|min:1',
            'llastname' => 'sometimes|nullable|string|min:1',
            'lstreet' => 'sometimes|nullable|string|min:1',
            'lzip' => 'sometimes|nullable|string|min:1',
            'lcity' => 'sometimes|nullable|string|min:1',
            'lcountry' => 'sometimes|nullable|string|min:1',
        ]);

        foreach ($keys AS $param) {
            if(!array_key_exists($param, $attributes)) $attributes[$param]="";
        }

        $temporder->kunde = $attributes;
        $temporder->update();

        return redirect(route('app.temporder.show', ['temporder'=>$temporder->id] ))->with(['temporder' => $temporder])->withSuccess('Adresse bearbeitet');


    }


    public function getOrderWarenkorb(TempOrder $temporder) {

        if(!empty($temporder)) {
            $warenkorb_string = json_encode($temporder->warenkorb_string);
            return view('app.temporder._editWarenkorb')->with([
                'temporder' => $temporder,
                'warenkorb_string' => $warenkorb_string,
            ]);
        }
        return '';
    }


    public function setOrderWarenkorb(TempOrder $temporder) {

        $attributes = request()->validate(['warenkorb_string' => 'required|string|min:1']);
        $temporder->warenkorb_string = json_decode(($attributes['warenkorb_string']));
        $temporder->update();

        return redirect(route('app.temporder.show', ['temporder'=>$temporder->id] ))->with(['temporder' => $temporder])->withSuccess('Warenkorb bearbeitet');

    }


    public function orderRetransmit(TempOrder $temporder) {

        $temporder->transfer = 0;
        $temporder->update();

        return redirect(route('app.temporder.show', ['temporder'=>$temporder->id] ))->with(['temporder' => $temporder])->withSuccess('Erneute Übertrageung beim nächsten Cron-Lauf');

    }


    public function orderRetransmitNew(TempOrder $temporder) {

        $temporder->transfer = 0;
        $temporder->intranet_id = NULL;
        $temporder->update();

        return redirect(route('app.temporder.show', ['temporder'=>$temporder->id] ))->with(['temporder' => $temporder])->withSuccess('Erneute Übertrageung beim nächsten Cron-Lauf');

    }


    public function toggleCron(TempOrder $temporder) {

        $temporder->update(['cron_processing' => !$temporder->cron_processing]);
        return redirect(route('app.temporder.index'))->withSuccess($temporder->presentation_short.' erfolgreich zurückgesetzt!');
    }


}
