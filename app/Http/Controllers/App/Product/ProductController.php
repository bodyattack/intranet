<?php

namespace App\Http\Controllers\App\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Product;


class ProductController extends Controller
{

    public function _filterValidate()
    {
        $rules = [
            'filter' => 'sometimes|nullable|numeric',
            'filter_reset' => 'sometimes|nullable|numeric',
            'filter_word' => 'sometimes|nullable|regex:/^[\w]+$/',
            'filter_row' => 'sometimes|nullable|required_with:filter_word,filter|string|regex:/^[\w]+$/',
            'filter_order' => 'sometimes|nullable|required_with:filter|string|in:desc,asc',
        ];

        $attributes = [
            'filter' => 'Filter',
            'filter_reset' => 'Filter löschen',
            'filter_word' => 'Suchbegriff',
            'filter_row' => 'Suchbereich',
            'filter_order' => 'Sortierung',
        ];


        $attributes = $this->validate(request(), $rules, array(), $attributes);

        return $attributes;
    }

    public function filterQuery($filter_attributes, $paginate=true)
    {

        if (isset($filter_attributes['filter_reset'])) {
            request()->session()->forget('product_filter');
        }

        $query = Product::with('manufacturer');

        if (isset($filter_attributes['filter'])) {

            request()->session()->put('product_filter', $filter_attributes);

            if (isset($filter_attributes['filter_word']) && $filter_attributes['filter_row'] != 1) {

                $query->where($filter_attributes['filter_row'], 'LIKE', '%' . $filter_attributes['filter_word'] . '%');
                if($filter_attributes['filter_row'] == 'firstname') {
                    $query->orWhere('lastname', 'LIKE', '%' . $filter_attributes['filter_word'] . '%');
                }
            }

            $query->orderBy($filter_attributes['filter_row'], $filter_attributes['filter_order']);
        }

        if(!$paginate) return $query->get();

        return $query->paginate($this->elements_per_page);
        #return $query->paginate($this->elements_per_page, ['*'], 'p_page');

    }


    public function index()
    {

        $filter_attributes = $this->_filterValidate();
        if (empty($filter_attributes) && request()->session()->has('product_filter')) {
            $filter_attributes = request()->session()->get('product_filter');
        }

        $products = $this->filterQuery($filter_attributes);

        return view('app.product.index')->with(['product' => new Product, 'products' => $products, 'filter_attributes' => $filter_attributes]);

    }

    function edit(Product $product) {
        return view('app.product.edit')->with(['product' => $product]);
    }


    function direct_edit(Product $product) {
        return view('app.product.direct.edit')->with(['product' => $product]);
    }

    function direct_address(Product $product) {
        return view('app.product.direct.address')->with(['product' => $product]);
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect(route('app.product.index'))->withSuccess($product->presentation_short.' erfolgreich gelöscht');
    }



    function store() {

        $attributes = request()->validate([
            'erp_kdnr' => 'sometimes|nullable|int',
            'gender' => 'sometimes|nullable|string|in:male,female,div',
            'title' => 'sometimes|nullable|string',
            'firstname' => 'required|string|max:128',
            'lastname' => 'required|string|max:128',
            'email' => 'required|string|email|max:128|unique:products',
            'phone' => 'sometimes|nullable|string',
            'dob' => 'sometimes|nullable|date|date_format:Y-m-d',
            'password' => 'required|confirmed|min:6',
        ]);

        if(!empty($attributes['password'])) {
            $attributes['password'] = Hash::make($attributes['password']);
        }

        $product = Product::create($attributes);
        

        return redirect(route('app.product.index'))->withSuccess($product->presentation_short.' erfolgreich angelegt');
    }


    function update(Product $product) {

        $attributes = request()->validate([
            'erp_kdnr' => 'sometimes|nullable|int',
            'gender' => 'sometimes|nullable|string|in:male,female,div',
            'title' => 'sometimes|nullable|string',
            'firstname' => 'required|string|max:128',
            'lastname' => 'required|string|max:128',
            'email' => 'required|string|email|max:128|unique:products,email,'.$product->id,
            'phone' => 'sometimes|nullable|string',
            'dob' => 'sometimes|nullable|date|date_format:Y-m-d',
            'password' => 'sometimes|nullable|string',
        ]);

        $product->erp_kdnr = $attributes['erp_kdnr'];
        $product->gender = $attributes['gender'];
        $product->title = $attributes['title'];
        $product->firstname = $attributes['firstname'];
        $product->lastname = $attributes['lastname'];
        $product->email = $attributes['email'];
        $product->phone = $attributes['phone'];
        $product->dob = $attributes['dob'];

        if(!empty($attributes['password'])) $product->password = Hash::make($attributes['password']);

        $product->update();

        return back()->withSuccess($product->presentation_short.' erfolgreich bearbeitet');

    }


    public function toggleActive(Product $product) {

        $product->update(['active' => !$product->active]);
        return back()->withSuccess($product->presentation_short.' wurde '.(!$product->active?'deaktiviert':'aktiviert'));
    }

    
}
