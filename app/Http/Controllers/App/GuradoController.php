<?php
namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Externals\Gurado\Vouchers;

class GuradoController extends Controller
{


    public function index()
    {
        return view('app.gurado.index');
    }

    public function show()
    {
        $attributes = $this->_validate();

        $voucher = new Vouchers();
        #$code_information = $voucher->getVoucherInformation($attributes['code']);
        $code_information = $voucher->getVoucherTransactions($attributes['code']);


        if(isset($code_information['status']) && $code_information['status']!=200) {
            return back()->with([ 'code' => $attributes['code']])->withDanger('Es wurde kein Gutschein zu diesem Code gefunden');
        }

        if($code_information) {
            #$code_information = $voucher->getVoucherTransactions($attributes['code']);
            return view('app.gurado.index')->with([
                'code' => $attributes['code'],
                'code_information' => $code_information,
            ]);
        }

        return back()->with([ 'code' => $attributes['code']])->withDanger('Es wurde kein Gutschein zu diesem Code gefunden');

    }

    public function store()
    {
        $attributes = $this->_validate(1);

        $voucher = new Vouchers();
        $response = $voucher->redeemVoucher($attributes['code'], $attributes);

        if(isset($response['status']) && $response['status']!=200) {
            return back()->with([ 'code' => $attributes['code']])->withDanger('Vorgang konnte nicht abgeschlossen werden!');
        }

        if($response['messageStatus']=='SUCCESS') {
            return redirect(route('app.gurado.show', ['code'=>$attributes['code']]))->withSuccess('Gutschein erfolgreich bearbeitet!');
        }

        return back()->with([ 'code' => $attributes['code']])->withDanger('Es wurde kein Gutschein zu diesem Code gefunden');

    }

    public function _validate($update=0)
    {
        $rules = [
            'code' => 'string',
        ];
        $attributes = [
            'code' => 'Gutscheincode',
        ];

        if($update) {
            $rules['amount'] = 'required|numeric';
            $rules['kassierernummer'] = 'sometimes|nullable|int';
            $rules['bonnummer'] = 'sometimes|nullable|int';
            $attributes['amount'] = 'Wert';
            $attributes['kassierernummer'] = 'Kassierer Nummer';
            $attributes['bonnummer'] = 'Bon Nummer';
        }

        return $this->validate(request(), $rules, array(), $attributes);

    }

}