<?php
namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;

class TopCategoriesController extends Controller
{
    protected $api_url = 'https://www.body-attack.de/includes/api_c5ezushejust/v1/';

    private function token()
    {
       $zeitstempel = date("Ymdh");
       $secret = 'Y!10';

       return md5($zeitstempel.$secret);
    }

    private function getTopCategorie($topCategorie)
    {
       $entpoint = "?action=toprubrik&ausgabe=1&categories_top_id=".$topCategorie."&token=".$this->token();
        $options = array(
            "http"=>array(
                "header"=>"User-Agent: Mozilla/5.0\r\n"
            )
        );
       $context = stream_context_create($options);
       $jsonreview = file_get_contents($this->api_url.$entpoint, false, $context);

       return json_decode($jsonreview);
    }

    private function setTopCategorie($data, $update=0)
    {

        $data = array(
            "categories_top_id" => $data['categories_top_id'],
            "categories_name" => $data['categories_name'],
            "categories_rewrite" => $data['categories_rewrite'],
            "categories_title" => $data['categories_title'],
            "categories_description" => $data['categories_description'],
            "categories_image" => $data['categories_image'] ?? '',
            "sort_order" => $data['sort_order'] ?? '',
            "categories_top" => $data['categories_top'],
            "categories_desc_unten" => $data['categories_desc_unten'],
            "categories_keywords" => $data['categories_keywords'],
            "categories_meta_desc" => $data['categories_meta_desc'],
            "categories_meta_title" => $data['categories_meta_title'],
            "HealthClaimOK" => $data['HealthClaimOK'],
            "menuestatus" => $data['menuestatus']
        );


        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\nUser-Agent: Mozilla/5.0\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);

        $entpoint = "?action=toprubrik&anlegen=1&token=".$this->token();
        if($update) $entpoint = "?action=toprubrik&update=1&token=".$this->token();

        return file_get_contents($this->api_url.$entpoint, false, $context);
    }

    public function index()
    {
        $entpoint = "?action=toprubrik&liste=1&token=".$this->token();
        $options = array(
            "http"=>array(
                "header"=>"User-Agent: Mozilla/5.0\r\n"
            )
        );
        $context = stream_context_create($options);
        $response = file_get_contents($this->api_url.$entpoint, false, $context);

        return view('app.topCategories.index')->with(['topCategories' => json_decode($response)]);

    }

    public function create()
    {
        return view('app.topCategories.edit')->with(['topCategorie' => $this->initEmptyTopCategorie() ]);
    }

    public function show($topCategorie)
    {
        $response = $this->getTopCategorie($topCategorie);

        return view('app.topCategories.edit')->with(['topCategorie' => $response]);
    }

    public function store()
    {
        $result = $this->setTopCategorie($this->_validate());

        if($result === FALSE || $result == "404" || $result == "") {
            return redirect(route('app.topCategories.index'))
                ->withDanger('Fehler eim erstellen der Top Kategorie!');
        }

        return redirect(route('app.topCategories.index'))->withSuccess('Top Kategorie erfolgreich erstellt!');
    }


    public function update()
    {
        $result = $this->setTopCategorie($attributes = $this->_validate(), 1);

        if($result === FALSE || $result == "404" || $result == "") {
            return redirect(route('app.topCategories.show'))
                ->withDanger('Fehler eim bearbeiten der Top Kategorie!');
        }

        return redirect(route('app.topCategories.show', ['topCategorie'=>$attributes['categories_top_id']]))
            ->withSuccess('Top Kategorie erfolgreich bearbeitet!');
    }


    public function destroy($topCategorie)
    {

        $entpoint = "?action=toprubrik&delete=1&categories_top_id=".$topCategorie."&token=".$this->token();
        $options = array(
            "http"=>array(
                "header"=>"User-Agent: Mozilla/5.0\r\n"
            )
        );
        $context = stream_context_create($options);
        $jsonreview = file_get_contents($this->api_url.$entpoint,false, $context);

        if($jsonreview==200 ) {
            return redirect(route('app.topCategories.index'))->withDanger('Top Kategorie wurde gelöscht!');
        }

        return redirect(route('app.topCategories.index'))->withDanger('Fehler beim löschen der Top Kategorie!');

    }



    public function toggleActive($topCategorie) {

        $entpoint = "?action=toprubrik&toggle_HealthClaimOK=1&categories_top_id=".$topCategorie."&token=".$this->token();
        $options = array(
            "http"=>array(
                "header"=>"User-Agent: Mozilla/5.0\r\n"
            )
        );
        $context = stream_context_create($options);
        $jsonreview = file_get_contents($this->api_url.$entpoint, false, $context);

        if($jsonreview==200 ) {
            return back()->withSuccess('Status Top Kategorie wurde geändert');
        }

        return redirect(route('app.topCategories.index'))->withDanger('Fehler beim Statusänderung der Top Kategorie!');

    }



    public function _validate()
    {
        $rules = [
            'categories_top_id' => 'numeric',
            'categories_name' => 'string|required',
            'categories_rewrite' => 'string|required',
            'categories_title' => 'string|required',
            'categories_description' => 'string|required',
            'categories_top' => 'string|required',
            'categories_keywords' => 'string|required',
            'categories_desc_unten' => 'sometimes|nullable|string',
            'categories_image' => 'sometimes|required|nullable|string',
            'categories_meta_desc' => 'string|required',
            'categories_meta_title' => 'string|required',
            'HealthClaimOK' => 'nullable|numeric|in:1,0',
            'menuestatus' => 'numeric|required',
            'sort_order' => 'sometimes|required|nullable|numeric',
        ];
        $attributes = [
            'categories_top_id' => 'ID',
            'categories_name' => 'Name',
            'categories_rewrite' => 'Rewrite',
            'categories_title' => 'Titel',
            'categories_top' => 'Top',
            'categories_keywords' => 'Keywords',
            'categories_description' => 'Beschreibung',
            'categories_desc_unten' => 'Beschreibung Unten',
            'categories_image' => 'Bild',
            'categories_meta_title' => 'Meta Titel',
            'categories_meta_desc' => 'Meta Beschreibung',
            'HealthClaimOK' => 'Aktiv',
            'menuestatus' => 'Menüstatus',
            'sort_order' => 'Sortierung',
        ];


        return $this->validate(request(), $rules, array(), $attributes);

    }

    private function initEmptyTopCategorie() {

        $topCategorie = new \stdClass();
        $topCategorie->categories_top_id = '';
        $topCategorie->categories_name = '';
        $topCategorie->categories_rewrite = '';
        $topCategorie->categories_title = '';
        $topCategorie->categories_description = '';
        $topCategorie->categories_top = '';
        $topCategorie->categories_keywords = '';
        $topCategorie->categories_desc_unten = '';
        $topCategorie->categories_image = '';
        $topCategorie->categories_meta_desc = '';
        $topCategorie->categories_meta_title = '';
        $topCategorie->HealthClaimOK = '';
        $topCategorie->menuestatus = 0;
        $topCategorie->sort_order = '';

        return $topCategorie;
    }

}