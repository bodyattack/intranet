<?php

namespace App\Http\Controllers\App;

use App\Productgroup;
use App\Http\Controllers\Controller;

class ProductgroupController extends Controller
{



    public function index()
    {
        $productgroups=Productgroup::all();
        return view('app.productgroup.index')->with(['productgroup' => new Productgroup, 'productgroups' => $productgroups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productgroup = new Productgroup;
        return view('app.productgroup.edit')->with(['productgroup' => $productgroup]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();

        $productgroup = Productgroup::create($attributes);

        return redirect(route('app.productgroup.index'))->withSuccess($productgroup->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Productgroup $productgroup)
    {
        return view('app.productgroup.edit')->with(['productgroup' => $productgroup]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Productgroup $productgroup)
    {
        return view('app.productgroup.edit')->with(['productgroup' => $productgroup]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Productgroup $productgroup)
    {
        $attributes = $this->_validate($productgroup);
        $productgroup->update($attributes);

        return back()->withSuccess($productgroup->presentation_short.' bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Productgroup $productgroup)
    {
        $productgroup->delete();
        return back()->withDanger($productgroup->presentation_short.' gelöscht!');
    }


    public function _validate($productgroup=NULL)
    {
        $id_exclude = '';
        if($productgroup!=NULL) $id_exclude = ','.$productgroup->id;

        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50|unique:productgroups,name'.$id_exclude,
            'name_en' => 'sometimes|nullable|string|min:2|max:50',
            'description' => 'sometimes|nullable|string|min:20|max:500',
            'description_en' => 'sometimes|nullable|string|min:20|max:500',
            'icon' => 'sometimes|nullable|string|min:2|max:50',
            'filter_link' => 'sometimes|nullable|string|min:2|max:50',
            'filter_link_en' => 'sometimes|nullable|string|min:2|max:50',
        ]);

        return $attributes;
    }


    public function toggleActive(Productgroup $productgroup) {
        $productgroup->update(['active' => !$productgroup->active]);
        return back()->withSuccess($productgroup->presentation_short.' wurde '.(!$productgroup->active?'deaktiviert':'aktiviert'));
    }



}
