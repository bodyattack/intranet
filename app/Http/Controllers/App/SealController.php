<?php

namespace App\Http\Controllers\App;

use App\Seal;
use App\Http\Controllers\Controller;

class SealController extends Controller
{

    public function index()
    {
        $seals=Seal::paginate($this->elements_per_page);
        return view('app.seal.index')->with(['seal' => new Seal, 'seals' => $seals]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $seal = new Seal;
        return view('app.seal.edit')->with(['seal' => $seal]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();

        $seal = Seal::create($attributes);

        return redirect(route('app.seal.index'))->withSuccess($seal->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Seal $seal)
    {
        return view('app.seal.edit')->with(['seal' => $seal]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Seal $seal)
    {
        return view('app.seal.edit')->with(['seal' => $seal]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Seal $seal)
    {
        $attributes = $this->_validate($seal);
        $seal->update($attributes);

        return back()->withSuccess($seal->presentation_short.' bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seal $seal)
    {
        $seal->delete();
        return back()->withDanger($seal->presentation_short.' gelöscht!');
    }


    public function _validate($seal=NULL)
    {
        $id_exclude = '';
        if($seal!=NULL) $id_exclude = ','.$seal->id;

        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50|unique:seals,name'.$id_exclude,
            'name_en' => 'sometimes|nullable|string|min:2|max:50',
            'description' => 'sometimes|nullable|string|min:20|max:500',
            'description_en' => 'sometimes|nullable|string|min:20|max:500',
            'icon' => 'sometimes|nullable|string|min:2|max:50',
            'filter_link' => 'sometimes|nullable|string|min:2|max:50',
            'filter_link_en' => 'sometimes|nullable|string|min:2|max:50',
        ]);

        return $attributes;
    }


    public function toggleActive(Seal $seal) {
        $seal->update(['active' => !$seal->active]);
        return back()->withSuccess($seal->presentation_short.' wurde '.(!$seal->active?'deaktiviert':'aktiviert'));
    }



}
