<?php

namespace App\Http\Controllers\App;

use App\Allergen;
use App\Http\Controllers\Controller;

class AllergenController extends Controller
{

    public function index()
    {
        $allergens=Allergen::all();
        return view('app.allergen.index')->with(['allergen' => new Allergen, 'allergens' => $allergens]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allergen = new Allergen;
        return view('app.allergen.edit')->with(['allergen' => $allergen]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();

        $allergen = Allergen::create($attributes);

        return redirect(route('app.allergen.index'))->withSuccess($allergen->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Allergen $allergen)
    {
        return view('app.allergen.edit')->with(['allergen' => $allergen]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Allergen $allergen)
    {
        return view('app.allergen.edit')->with(['allergen' => $allergen]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Allergen $allergen)
    {
        $attributes = $this->_validate($allergen);
        $allergen->update($attributes);

        return back()->withSuccess($allergen->presentation_short.' bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Allergen $allergen)
    {
        $allergen->delete();
        return back()->withDanger($allergen->presentation_short.' gelöscht!');
    }


    public function _validate($allergen=NULL)
    {
        $id_exclude = '';
        if($allergen!=NULL) $id_exclude = ','.$allergen->id;

        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50|unique:allergens,name'.$id_exclude,
            'name_en' => 'sometimes|nullable|string|min:2|max:50',
            'description' => 'sometimes|nullable|string|min:20|max:500',
            'description_en' => 'sometimes|nullable|string|min:20|max:500',
            'icon' => 'sometimes|nullable|string|min:2|max:50',
            'filter_link' => 'sometimes|nullable|string|min:2|max:50',
            'filter_link_en' => 'sometimes|nullable|string|min:2|max:50',
        ]);

        return $attributes;
    }


    public function toggleActive(Allergen $allergen) {
        $allergen->update(['active' => !$allergen->active]);
        return back()->withSuccess($allergen->presentation_short.' wurde '.(!$allergen->active?'deaktiviert':'aktiviert'));
    }



}
