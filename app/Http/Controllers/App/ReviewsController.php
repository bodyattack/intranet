<?php
namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;

class ReviewsController extends Controller
{
    protected $api_url = 'https://www.body-attack.de/includes/api_c5ezushejust/v1/';

    private function token()
    {
       $zeitstempel = date("Ymdh");
       $secret = 'Y!10';

       return md5($zeitstempel.$secret);
    }

    private function getRewiew($data)
    {

       if(isset($data['reviewid']) &&  !empty($data['reviewid']))  {
           $entpoint = "?action=bewertung&reviewid=".$data['reviewid']."&token=".$this->token();
       }
       else {
           $entpoint = "?action=bewertung&kundennummer=".$data['kundennummer']."&ratingid=".$data['ratingid']."&token=".$this->token();
       }

       $options = array(
           "http"=>array(
               "header"=>"User-Agent: Mozilla/5.0\r\n"
           )
       );
       $context = stream_context_create($options);
       $jsonreview = file_get_contents($this->api_url.$entpoint, false, $context);

       return json_decode($jsonreview,true);
    }

    private function setRewiew($data)
    {
        $data = array(
            "action" => "bewertung",
            "antwort" => $data['description'],
            "kundennummer" => $data['kundennummer'],
            "ratingid" => $data['ratingid'],
            "reviewid" => $data['reviewid'],
            "token" => $this->token());

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\nUser-Agent: Mozilla/5.0\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);

        return file_get_contents($this->api_url, false, $context);
    }

    public function index()
    {
        return view('app.reviews.index');
    }

    public function show()
    {

        $review = $this->getRewiew($attributes = $this->_validate());

        if(empty($review))   return redirect(route('app.reviews.index'))->withDanger('Es konnte keine Bewertung gefunden werden!');

        if(!isset($attributes['kundennummer']))  $attributes['kundennummer']='';
        if(!isset($attributes['ratingid']))  $attributes['ratingid']='';

            return view('app.reviews.index')->with([
            'ratingid' => $attributes['ratingid'],
            'kundennummer' => $attributes['kundennummer'],
            'reviewid' => $attributes['reviewid'],
            'review' => array_shift($review)
        ]);
    }

    public function store()
    {
        $result = $this->setRewiew($attributes = $this->_validate(1));

        if($result === FALSE || $result == "404" || $result == "") {
            return redirect(route('app.reviews.show', ['ratingid'=>$attributes['ratingid'],'kundennummer'=>$attributes['kundennummer']]))
                ->withDanger('Fehler eim bearbeiten der Bewertung!');
        }

        if(isset($attributes['reviewid']) && !empty($attributes['reviewid']))  {
            return redirect(route('app.reviews.show', ['reviewid'=>$attributes['reviewid']]))
                ->withSuccess('Bewertung erfolgreich bearbeitet!');
        }
        else {
            return redirect(route('app.reviews.show', ['ratingid'=>$attributes['ratingid'],'kundennummer'=>$attributes['kundennummer']]))
                ->withSuccess('Bewertung erfolgreich bearbeitet!');
        }

    }


    public function destroy()
    {

        $attributes = $this->_validate();

        if(isset($attributes['reviewid']) && !empty($attributes['reviewid']))  {
            $entpoint = "?action=bewertung&delete=1&reviewid=".$attributes['reviewid']."&token=".$this->token();
        }
        else {
            $entpoint = "?action=bewertung&delete=1&kundennummer=".$attributes['kundennummer']."&ratingid=".$attributes['ratingid']."&token=".$this->token();
        }

        $options = array(
            "http"=>array(
                "header"=>"User-Agent: Mozilla/5.0\r\n"
            )
        );

        $context = stream_context_create($options);
        $result = file_get_contents($this->api_url.$entpoint, false, $context);

        if($result === FALSE || $result == "404" || $result == "") {
            return redirect(route('app.reviews.index'))->withDanger('Fehler beim löschen der Bewertung!');
        }

        return redirect(route('app.reviews.index'))->withDanger('Bewertung wurde gelöscht!');

    }




    public function _validate($update=0)
    {
        $rules = [
            'reviewid' => 'sometimes|nullable|numeric',
            'kundennummer' => 'sometimes|nullable|numeric',
            'ratingid' => 'sometimes|nullable|numeric',
        ];
        $attributes = [
            'reviewid' => 'Review-ID',
            'kundennummer' => 'Kundennummer',
            'ratingid' => 'Bewertungs-ID',
        ];

        if($update) {
            $rules['description'] = 'required|string|min:5';
            $attributes['description'] = 'Antwort Text';
        }

        return $this->validate(request(), $rules, array(), $attributes);

    }

}