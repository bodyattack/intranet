<?php

namespace App\Http\Controllers\App\InvoiceRelease;

use App\Filters\IrBillFilters;
use App\IrBill as Bill;
use App\IrBiller as Biller;
use App\IrFilter as Filter;
use App\Department;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class TransactionsController extends Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if(!$this->checkEntry('permissions', 'rechnungskontrolle-zahlungsverkehr')) {
                return back()->withDanger('Du bist nicht berechtigt für diese Aktion');
            };
            return $next($request);
        });
    }

    public function _filterValidate()
    {

        $rules = [
            'filter' => 'sometimes|nullable|numeric',
            'mandant_filter' => 'sometimes|nullable|int',
            'biller_filter' => 'sometimes|nullable|int',
            'department_filter' => 'sometimes|nullable|int',
            'date_month' => 'sometimes|nullable|int',
            'date_year' => 'sometimes|nullable|int',
            'date_day' => 'sometimes|nullable|int',
            'order_by' => 'sometimes|nullable|string',
        ];

        $attributes = [
            'filter' => 'Filter',
            'mandant_filter' => 'Mandant',
            'biller_filter' => 'Rechnungsteller',
            'department_filter' => 'Abteilung',
            'date_month' => 'Monat',
            'date_year' => 'Jahr',
            'date_day' => 'Tag',
            'order_by' => 'Sortierung',
        ];

        $attributes = $this->validate(request(), $rules, array(), $attributes);

        return $attributes;
    }


    public function filterQuery($filter, $paginate=true)
    {

        // define sql field list
        $select = ['ir_bills.id','ir_bills.payment_method','ir_bills.status','ir_bills.department_id','ir_bills.biller_id','ir_bills.mandant_id','ir_bills.amount','ir_bills.due_date','ir_bills.bill_number','ir_bills.created_at', 'ir_bills.amount', DB::Raw('DAY(ir_bills.due_date) AS due_day')];
        $select = array_merge($select, ['ir_billers.name AS biller']);
        $select = array_merge($select, ['departments.name AS department']);
        $select = array_merge($select, ['mandanten.short_name AS mandant']);

        // generate the base query
        $query = DB::table('ir_bills');
        $query = $query->select($select);
        $query = $query->leftJoin('ir_billers', 'ir_bills.biller_id', '=', 'ir_billers.id');
        $query = $query->join('departments', 'ir_bills.department_id', '=', 'departments.id');
        $query = $query->join('mandanten', 'ir_bills.mandant_id', '=', 'mandanten.id');

        if(!empty($filter)) {

            if(!empty($filter['biller_filter'])) {
                $query->where('ir_bills.biller_id', $filter['biller_filter']);
            }
            if(!empty($filter['department_filter'])) {
                $query->where('ir_bills.department_id', $filter['department_filter']);
            }
            if(!empty($filter['mandant_filter'])) {
                $query->where('ir_bills.mandant_id', $filter['mandant_filter']);
            }

            if(!empty($filter['date_month']) && !empty($filter['date_year'])) {
                $start_day='01';
                $end_day='31';
                if(!empty($filter['date_day'])) {
                    $start_day=$filter['date_day'];
                    $end_day=$filter['date_day'];
                }
                $date_string_from=$filter['date_year'].'-'.$filter['date_month'].'-'.$start_day.' 00:00:00';
                $date_string_to=$filter['date_year'].'-'.$filter['date_month'].'-'.$end_day.' 23:59:59';
                #print_r($date_string_from);
                #print_r($date_string_to);
                $query->whereBetween('ir_bills.due_date', [$date_string_from, $date_string_to]);
            }
        }

        $query->orderByRaw('DATE_FORMAT(ir_bills.due_date, "%y-%m-%d")');

        if(!empty($filter['order_by'])) {
            foreach (explode('|', $filter['order_by']) AS $order) {
                $query->orderBy($order);
            }
        }

        // execute with page limit, if paginate are true
        #echo $query->toSql();
        if(!$paginate) return $query->get();

        return $query->paginate($this->elements_per_page);
    }


    public function calendar() {

        $filter_attributes = $this->_filterValidate();

        $date_string='';
        if(!empty($filter_attributes)) {
            $date_part_one = $filter_attributes['date_year'].'-'.str_pad($filter_attributes['date_month'], 1, "0", STR_PAD_LEFT);
            $date = \Carbon\Carbon::parse(date($date_part_one.'-01'));
            $date_string=$date_part_one.'-'.$date->daysInMonth;
        }
        $calendar_settings=$this->_initCalendar($date_string);

        if(empty($filter_attributes)) {
            $filter_attributes['date_year']=$calendar_settings['year_real'];
            $filter_attributes['date_month']=$calendar_settings['month_real'];
            $filter_attributes['mandant_filter'] = '1'; // BASN
        }

        $bills = $this->filterQuery($filter_attributes, false);

        $calendar_settings['data']=[];
        foreach ($bills AS $bill) {
            if (!array_key_exists($bill->due_day, $calendar_settings['data'])) {
                $calendar_settings['data'][$bill->due_day] = [];
                $calendar_settings['data'][$bill->due_day]['methods'] = [];
                $calendar_settings['data'][$bill->due_day]['data'] = [];
            }

            if (!array_key_exists($bill->payment_method, $calendar_settings['data'][$bill->due_day]['methods'])) {
                $calendar_settings['data'][$bill->due_day]['methods'][$bill->payment_method]['bezahlt'] = 0;
                $calendar_settings['data'][$bill->due_day]['methods'][$bill->payment_method]['offen'] = 0;
                $calendar_settings['data'][$bill->due_day]['offen_anona'] = 0;
            }

            if($bill->status=='bezahlt') $calendar_settings['data'][$bill->due_day]['methods'][$bill->payment_method]['bezahlt'] += $bill->amount;
            else {
                $calendar_settings['data'][$bill->due_day]['methods'][$bill->payment_method]['offen'] += $bill->amount;
                if($bill->biller_id==env('BILLER_ID_ANONA')) $calendar_settings['data'][$bill->due_day]['offen_anona'] += $bill->amount;
            }

            $calendar_settings['data'][$bill->due_day]['data'][] = $bill;

        }

        // Summenbildung unter Kalender
        $yesterday = date("Y-m-d", strtotime( '-1 days' ) );

        $query = DB::table('ir_bills');
        $query = $query->join('ir_mailboxes', 'ir_bills.mailbox_id', '=', 'ir_mailboxes.id');
        $query = $query->select(DB::raw('sum(amount) as open, sum(if(biller_id = \''.env('BILLER_ID_ANONA').'\',amount,0)) As open_anona'));
        if(!empty($filter_attributes['mandant_filter'])) {
            $query->where('ir_mailboxes.mandant_id', $filter_attributes['mandant_filter']);
        }
        $query = $query->where('status', '!=', 'bezahlt');
        $total_period_open = $query->get();


        $query = DB::table('ir_bills');
        $query = $query->select(DB::raw('sum(amount) as unsettled'));
        $query = $query->join('ir_mailboxes', 'ir_bills.mailbox_id', '=', 'ir_mailboxes.id');
        if(!empty($filter_attributes['mandant_filter'])) {
            $query->where('ir_mailboxes.mandant_id', $filter_attributes['mandant_filter']);
        }
        $query = $query->whereNotIn('status', ['bezahlt']);
        $query = $query->whereDate('due_date', '<', $yesterday);
        $total_period_unsettled = $query->get();

        $total_open['open']=0;
        $total_open['open_anona']=0;
        $total_open['unsettled']=0;
        if(!empty($total_period_open[0]->open))  $total_open['open'] = $total_period_open[0]->open;
        if(!empty($total_period_open[0]->open_anona))  $total_open['open_anona'] = $total_period_open[0]->open-$total_period_open[0]->open_anona;
        if(!empty($total_period_unsettled[0]->unsettled))  $total_open['unsettled'] = $total_period_unsettled[0]->unsettled;

        // Ausgabe
        return view('app.invoicerelease.transactions._calendar')->with([
            'calendar_settings' => $calendar_settings,
            'filter_attributes' => $filter_attributes,
            'total_open' => $total_open,
            'bills' => $bills,
        ]);

    }

    public function approval(IrBillFilters $bills_filter) {

        $bills_querry = Bill::with(['mandant', 'department', 'costcenter', 'biller'])->filter($bills_filter)->sortable();
        $bills_querry->whereNotIn('status', ['bezahlt','freigabe','bezahlen']);
        $bills_querry->orderByRaw('DATE_FORMAT(due_date, "%y-%m-%d")');
        $bills = $bills_querry->paginate($this->elements_per_page);

        $filter = $bills_filter->getFilters();

        return view('app.invoicerelease.transactions._approval')->with([
            'bill' => new Bill(),
            'bills' => $bills,
            'filter' => $filter,
        ]);

    }


    public function unsettled(IrBillFilters $bills_filter) {

        $yesterday = date("Y-m-d", strtotime( '-1 days' ) );
        $bills_querry = Bill::with(['mandant', 'department', 'costcenter', 'biller'])->filter($bills_filter)->sortable();
        $bills_querry->whereNotIn('status', ['bezahlt']);
        $bills_querry->whereDate('due_date', '<', $yesterday);
        $bills_querry->orderByRaw('DATE_FORMAT(due_date, "%y-%m-%d")');
        $bills = $bills_querry->paginate($this->elements_per_page);

        $filter = $bills_filter->getFilters();

        return view('app.invoicerelease.transactions._unsettled')->with([
            'bill' => new Bill(),
            'bills' => $bills,
            'filter' => $filter,
        ]);

    }

    public function due_yesterday() {

        $calendar_settings=$this->_initCalendar();
        $default_date=$calendar_settings['year_real'].'-'.$calendar_settings['month_real'].'-01 00:00:01';

        $select = ['ir_bills.id','ir_bills.payment_method','ir_bills.status','ir_bills.bill_number','ir_bills.department_id','ir_bills.biller_id','ir_bills.amount','ir_bills.due_date','ir_bills.created_at', 'ir_bills.amount', DB::Raw('DAY(ir_bills.due_date) AS due_day')];
        $select = array_merge($select, ['ir_billers.name AS biller']);
        $select = array_merge($select, ['departments.name AS department']);

        $query = DB::table('ir_bills');
        $query = $query->select($select);
        $query = $query->leftJoin('ir_billers', 'ir_bills.biller_id', '=', 'ir_billers.id');
        $query = $query->join('departments', 'ir_bills.department_id', '=', 'departments.id');
        $query = $query->whereNotIn('status', ['bezahlt']);
        $query = $query->where('due_date', '<', $default_date);
        $query = $query->orderByRaw('DATE_FORMAT(due_date, "%y-%m-%d")');

        $bills = $query->paginate($this->elements_per_page);

        return view('app.invoicerelease.transactions._unsettled')->with([
            'bill' => new Bill(),
            'bills' => $bills
        ]);

    }


    public function getBillsByDate() {

        $filter_attributes = $this->_filterValidate();
        $bills = $this->filterQuery($filter_attributes, false);

        if(!empty($bills)) {
            return view('app.invoicerelease.transactions._billsTableWithOptions')->with([
                'bill' => new Bill(),
                'bills' => $bills
            ]);
        }
        return '';

    }


    public function setStatus() {

        $params = request()->all();

        $bill = Bill::find($params['id']);

        if(!empty($bill)) {
            $bill->status = $params['status'];
            return (int) $bill->update();
        }

        return 0;

    }

    private function _initCalendar($date_string='') {

        $calendar_settings=[];

        $date=date('Y-m-d');
        if($date_string!='') $date=date('Y-m-d', strtotime($date_string));

        $date = \Carbon\Carbon::parse(date($date));
        $date_real = \Carbon\Carbon::parse(date('Y-m-d'));
        $day = $date->day;
        $dayOfWeek = $date->startOfMonth()->dayOfWeek;

        $calendar_settings['first_day_of_month']= $dayOfWeek;
        $calendar_settings['last_of_month']= $date->daysInMonth;
        $calendar_settings['day']= $day;
        $calendar_settings['year']= $date->year;
        $calendar_settings['year_real']= $date_real->year;
        $calendar_settings['month']= $date->month;
        $calendar_settings['month_real']= $date_real->month;

        if($calendar_settings['month']<12) $calendar_settings['next_month'] = $calendar_settings['month']+1;
        else $calendar_settings['next_month'] = 1;
        if($calendar_settings['month']>1) $calendar_settings['last_month'] = $calendar_settings['month']-1;
        else $calendar_settings['last_month'] = 12;
        $calendar_settings['last_year'] = $calendar_settings['year'];
        $calendar_settings['next_year'] = $calendar_settings['year'];
        if($calendar_settings['next_month']==1) $calendar_settings['next_year'] = $calendar_settings['year']+1;
        if($calendar_settings['last_month']==12) $calendar_settings['last_year'] = $calendar_settings['year']-1;

        $calendar_settings['cells_per_week']=8;
        $calendar_settings['cells_offset_start']=($dayOfWeek==0?6:($dayOfWeek-1)%7);
        $calendar_settings['cells_sum_offset_start']=$calendar_settings['last_of_month']+$calendar_settings['cells_offset_start'];
        $calendar_settings['cells_offset_end']= $calendar_settings['cells_sum_offset_start']<=35 ? ((($calendar_settings['cells_sum_offset_start']+4) % $calendar_settings['cells_per_week'])-$calendar_settings['cells_per_week'])*-1 : ((($calendar_settings['cells_sum_offset_start']+5) % $calendar_settings['cells_per_week'])-$calendar_settings['cells_per_week'])*-1;
        $calendar_settings['cells_sum_total']= $calendar_settings['cells_sum_offset_start']<=35 ? $calendar_settings['cells_sum_offset_start']+$calendar_settings['cells_offset_end']+4 : $calendar_settings['cells_sum_offset_start']+$calendar_settings['cells_offset_end']+5;

        return $calendar_settings;
    }

}


