<?php

namespace App\Http\Controllers\App\InvoiceRelease;

use App\IrBill as Bill;
use App\IrBiller as Biller;
use App\Filters\IrBillFilters;
use App\Department;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use function GuzzleHttp\Psr7\copy_to_string;


class BillController extends Controller
{

    public function index(IrBillFilters $bills_filter)
    {
        $user = Auth::user();

        // show departments in connection to the given user
        $allowed=[];
        if($user->hasDepartment('Buchhaltung')) $departments = Department::withInvoiceApprovers();
        else {
            $departments = $user->auditorAndRelatedDepartments();
            $allowed['departments'] = $departments->pluck('id')->toArray();
        }

        $bills_querry = Bill::with(['mandant', 'department', 'costcenter', 'biller'])->filter($bills_filter)->sortable(['id' => 'DESC']);
        if(!empty($allowed)) {
            if(!empty($allowed['departments'])) {
                $bills_querry->whereIn('ir_bills.department_id', $allowed['departments']);
            }
        }
        $bills = $bills_querry->paginate($this->elements_per_page);
        $filter = $bills_filter->getFilters();

        // pass date to view
        return view('app.invoicerelease.index')->with([
            'bill' => new Bill(),
            'bills' => $bills,
            'filter' => $filter,
            'departments' => $departments,
        ]);
    }


    public function show(Bill $bill)
    {
        return $this->edit($bill);
    }


    public function create(Bill $bill)
    {
        return view('app.invoicerelease.create')->with(['bill' => $bill]);
    }


    public function edit(Bill $bill)
    {

        return view('app.invoicerelease.edit')->with(['bill' => $bill]);
    }


    public function destroy(Bill $bill)
    {

        if(!$this->checkEntry('permissions', 'rechnung-loeschen')) {
            return back()->withDanger('Du bist nicht berechtigt für diese Aktion');
        };

        if($this->removeBill($bill)) return redirect()->back()->withSuccess('Rechnung wurde gelöscht!');
    }


    public function removeBill($bill)
    {
        $item = $bill->items;
        if (!$bill->items->isEmpty() && Storage::disk('invoicerelease')->delete(basename($item[0]->filename))) {
            $item[0]->delete();
        }
        return $bill->delete();
    }


    public function _validate()
    {

        $userType = request()->validate(
            [
                'userBuchhaltung' => 'sometimes|nullable|numeric',
                'userAbteilungsleiter' => 'sometimes|nullable|numeric'
            ]);

        $rules = [
            'currency' => 'exclude_if:status,reklamieren|sometimes|nullable|string|in:EUR,GBP,USD,AUD,CHF',
            'amount' => 'exclude_if:status,reklamieren|required|regex:([0-9]+[.,]?[0-9]*)|not_in:0',
            'deposit' => 'exclude_if:status,reklamieren|sometimes|nullable|numeric',              // Anzahlung
            'date' => 'exclude_if:status,reklamieren|required|date|date_format:Y-m-d',            // Rechnungsdatum
            'due_date' => 'exclude_if:status,reklamieren|required|date|date_format:Y-m-d',        // Fälligkeitsdatum
            'delivery_date' => 'exclude_if:status,reklamieren|sometimes|nullable|date|date_format:Y-m-d',  // Lieferdatum
            'bill_number' => 'exclude_if:status,reklamieren|required|string|min:2',
            'process_number' => 'exclude_if:status,reklamieren|sometimes|nullable|string|min:4',
            'comment' => 'sometimes|nullable|string|min:5',
            'status' => 'required|string|not_in:0',
            'onward_debit' => 'exclude_if:status,reklamieren|sometimes|nullable|numeric|in:1,0',  // Weiterbelastung
            'urlQuerry' => 'sometimes|nullable|string',   // loop filter query through backwards button
        ];

        $attributes = [
            'amount' => 'Betrag',
            'currency' => 'Währung',
            'deposit' => 'Zahlungsstatus',
            'onward_debit' => 'Weiterbelastung',
            'date' => 'Rechnungsdatum',
            'due_date' => 'Fälligkeitsdatum',
            'delivery_date' => 'Lieferdatum',
            'bill_number' => 'Rechnungsnummer',
            'process_number' => 'Vorgangsnummer',
            'comment' => 'Bemerkung',
            'status' => 'Status',
        ];

        // override rule for special department
        if(array_key_exists('einkauf', $this->entry['departments']) && count($this->entry['departments']) == 1 ){
            $rules['delivery_date'] = 'required|date|date_format:Y-m-d';
        }

        if(!empty($userType['userBuchhaltung'])) {
            $rules['biller_id'] = 'required|not_in:0';
            $rules['department_id'] = 'sometimes|not_in:0';
            $rules['costcenter_id'] = 'sometimes|not_in:0';
            $rules['payment_method'] = 'required|not_in:0';

            $attributes['biller_id'] = 'Rechnungssteller';
            $attributes['department_id'] = 'Abteilung';
            $attributes['costcenter_id'] = 'Kostenstelle';
            $attributes['payment_method'] = 'Zahlart';
        }

        $attributes = $this->validate(request(), $rules, array(), $attributes);

        // format german amont value to db form
        if(isset($attributes['amount'])) {
            $attributes['amount'] = str_replace(',', '.', $attributes['amount']);
        }
        return $attributes;
    }


    public function update(Bill $bill)
    {

        // Steht hier, weil zum direkten löschen sonst jeder andere Filter eine Ausnahmebehandlung (Nicht prüfen wenn...) bräuchte
        $status = $this->validate(request(), ['status' => 'required|string'], array(), ['status' => 'Status']);

        if($status['status']=='destroy') {
            $this->removeBill($bill);
            return redirect(route('app.invoicerelease.bills.index', request()->get('urlQuerry')))->withSuccess('Rechnung wurde gelöscht!');
        }

        $attributes = $this->_validate();

        $status_aenderung='';
        if('neu'!=$attributes['status'] && 'save'!=$attributes['status'] && $bill->status!=$attributes['status'] ) {
            $text='';
            switch($attributes['status']) {
                case 'reklamieren' : $text='Reklamieren'; break;
                case 'freigabe' : $text='Freigeben'; break;
                case 'vorab_freigabe' : $text='Vorab-Freigeben'; break;
                case 'blocken' : $text='Zurückstellen'; break;
                case 'bezahlen' : $text='Bezahlen'; break;
                case 'bezahlt' : $text='Bezahlt'; break;
            }
            $status_aenderung=$text.' Status wurde gesetzt.';
        }

        if('save'==$attributes['status']) {
            $attributes['status']=$bill->status;
        }

        if(!empty($attributes['comment']) || !empty($status_aenderung)) {
            $coment_text='';
            if(!empty($status_aenderung)) $coment_text=$status_aenderung."<br>";
            $coment_text.=$attributes['comment'];
            $bill->storeComment($coment_text, $this->user->id);
        }

        $urlQuerry=$attributes['urlQuerry'];
        unset($attributes['urlQuerry']);
        unset($attributes['comment']);

        $bill->update($attributes);

        if( !empty(Bill::where('bill_number', $bill->bill_number)->where('amount', $bill->amount)->where('biller_id', $bill->biller_id)->where('id', '<>', $bill->id)->first()) ) {
            $text = '<b>ACHTUNG</b> Eingaben wurden gespeichert, doch existiert eine identische Rechnung! <a href="'.route('app.invoicerelease.bills.edit', ['bill'=>$bill->id] ).'">siehe</a>';
            return redirect(route('app.invoicerelease.bills.index', $urlQuerry))->withDanger($text);
        }

        // Direkter Rücksprung auf Übersicht auf Wunsch von Markus BUHA - 10.12.2020
        return redirect(route('app.invoicerelease.bills.index', $urlQuerry))->withSuccess('Angaben erfolgreich gespeichert!');

    }


    public function store()
    {
        if (!request()->session()->has('upload.letter')) {
            return back()->withErrors('Bitte mindestens ein PDF hochladen');
        }

        // save bill
        $attributes = request()->validate([
            'biller_id' => 'required|not_in:0',
            'mandant_id' => 'required|not_in:0',
            'status' => 'required|not_in:0'
        ]);

        $biller = Biller::where('id', $attributes['biller_id'])->first();

        $bill = Bill::create([
            'biller_id' => $attributes['biller_id'],
            'mandant_id' => $attributes['mandant_id'],
            'status' => $attributes['status'],
            'due_date' => date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $biller->due_days . ' days')),
            'payment_method' => $biller->payment,
            'currency' => $biller->currency,
            'department_id' => $biller->distributor[0]->id,
            'costcenter_id' => $biller->costcenter_id,
        ]);

        // download, save and generate bill items
        foreach (request()->session()->pull('upload.letter') as $base64_file) {
            $bill_item = (new BillItemController())->create($base64_file);
            if(empty($bill_item)) return back()->withErrors('Sie haben kein valides PDF Dokument hochgeladen!');
            $bill_item->update(['bill_id' => $bill->id]);
        }

        return redirect(route('app.invoicerelease.bills.index'))->withSuccess('Rechnung erfolgreich angelegt');
    }



    public function csv()
    {

        $bills_filter = new IrBillFilters(request());
        $bills = Bill::with(['mandant', 'department', 'costcenter', 'biller'])->filter($bills_filter)->sortable('id')->get();

        if(!empty($bills)) {

            $fileName = 'Rechnungen_Export_' . date('Y-m-d') . '.csv';

            $headers = array(
                "Content-Encoding" => "UTF-8",
                "Content-type" => "text/csv; charset=UTF-8",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );

            $columns = array('Status', 'Mandant', 'Rechnungssteller', 'Rechnungsnummer', 'Vorgangsnummer', 'Freigabeabteilung', 'Kostenstelle', 'Betrag', 'Waehrung', 'Weiterberechnung', 'Zahlart', 'Rechnungseingang', 'Rechnungdatum', 'Faelligkeitsdatum', 'Lieferdatum', 'Freigabedatum');

            $callback = function () use ($bills, $columns) {

                $file = fopen('php://output', 'w');
                fputcsv($file, $columns, ';', '"');

                foreach ($bills as $bill) {

                    switch ($bill->status) {
                        case 'not_assigned' :  $status_text='Nicht zugeordnet';; break;
                        case 'neu' :  $status_text='Wartet auf Freigabe'; break;
                        case 'vorab_freigabe' :  $status_text='Vorab-Freigegeben'; break;
                        case 'freigabe' :  $status_text='Freigegeben'; break;
                        case 'bezahlt' :  $status_text='Bezahlt'; break;
                        case 'bezahlen' :  $status_text='Kann bezahlt werden'; break;
                        case 'blocken' :  $status_text='Bezahlung zurückhalten'; break;
                        case 'reklamieren' :  $status_text='Reklamiert';  break;
                        default : $status_text=$bill->status; break;
                    }

                    $mandant = $bill->mandant;
                    $department = $bill->department;
                    $costcenter = $bill->costcenter;
                    $biller = $bill->biller;

                    $row['status'] = $status_text;
                    $row['mandant'] = ( empty($mandant) ? '' :  $mandant->name );
                    $row['biller'] = ( empty($biller) ? '' :  $biller->name );
                    $row['bill_number'] = '"'.( empty($bill->bill_number) ? '' :  $bill->bill_number ).'"' ;
                    $row['process_number'] = $bill->process_number;
                    $row['department'] = ( empty($department) ? '' :  $department->name );
                    $row['costcenter_id'] = ( empty($costcenter) ? '' :  $costcenter->value );
                    $row['amount'] = str_replace('.', ',', $bill->amount);
                    $row['currency'] = $bill->currency;
                    $row['onward_debit'] = $bill->onward_debit;
                    $row['payment_method'] = $bill->payment_method;
                    $row['created_at'] = $bill->created_at;
                    $row['date'] = $bill->date;
                    $row['due_date'] = $bill->due_date;
                    $row['delivery_date'] = $bill->delivery_date;
                    $row['release_date'] = $bill->release_date;


                    $row = array_map(function ($cell) {
                        return mb_convert_encoding($cell, 'UTF-16LE', 'UTF-8');
                    }, $row);

                    fputcsv($file, $row, ';', '"');

                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        }

        return back()->withDanger('Keine Daten vorhanden');
    }




}


