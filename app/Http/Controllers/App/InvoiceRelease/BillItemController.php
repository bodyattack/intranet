<?php

namespace App\Http\Controllers\App\InvoiceRelease;

use App\Actions\Files\Base64Decoder;
use App\Actions\Files\Pdf\CheckPdfFile;
use App\IrBillItem as BillItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;



class BillItemController extends Controller
{

    protected $filename = '';
    protected $path_absolut = '';

    private function createUniqueFileName() {
        return $this->filename = date("YmdHi").'-'.Str::uuid()->toString().'.pdf';
    }


    public function create($base64_file) {

        $this->path_absolut = $this->storeBase64File($base64_file);

        /*
        if (!(new CheckPdfFile())->handle($this->path_absolut)) {
            $this->deleteFile($this->path_absolut);
            return null;
        }
        */

        $bill_item = BillItem::create(['filename' => $this->filename, 'bill_id' => 0]);

        return $bill_item;

    }


    public function storeBase64File(string $base64_file)
    {
        $filename = $this->createUniqueFileName();

        // store original file
        if (!Storage::disk('invoicerelease')->put($filename, (new Base64Decoder())->handle($base64_file))) {
            abort(500, 'Error storing file');
        }

        return Storage::disk('invoicerelease')->path($filename);
    }


    public function loadFile(Request $request) {

        return response()->make(Storage::disk('invoicerelease')->get($request['filename']), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="Vorschau.pdf"'
        ]);

    }


    public function deleteFile(string $file_path_absolute)
    {
        return Storage::disk('invoicerelease')->delete(basename($file_path_absolute));
    }



    public function upload(Request $request)
    {
        $base64_file = base64_encode(file_get_contents($request->file->getRealPath()));

        $request->session()->push('upload.letter', $base64_file);
    }


    public function removeFiles(Request $request)
    {
        $request->session()->forget('upload.letter');
    }


}


