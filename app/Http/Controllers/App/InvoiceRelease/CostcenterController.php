<?php

namespace App\Http\Controllers\App\InvoiceRelease;

use App\Http\Controllers\Controller;
use App\IrCostcenter as Costcenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CostcenterController extends Controller
{


    public function _filterValidate()
    {
        $rules = [
            'filter' => 'sometimes|nullable|numeric',
            'filter_mandant' => 'sometimes|nullable|int',
            'filter_word' => 'sometimes|nullable|regex:/^[\w]+$/',
            'filter_row' => 'sometimes|nullable|required_with:filter_word,filter|string|regex:/^[\w]+$/',
            'filter_order' => 'sometimes|nullable|required_with:filter|string|in:desc,asc',
            'filter_active' => 'sometimes|nullable|numeric|in:1,0,-1',
        ];

        $attributes = [
            'filter' => 'Filter',
            'filter_mandant' => 'Mandant',
            'filter_word' => 'Suchbegriff',
            'filter_row' => 'Suchbereich',
            'filter_order' => 'Sortierung',
            'filter_active' => 'Aktive',
        ];

        $attributes = $this->validate(request(), $rules, array(), $attributes);

        return $attributes;
    }

    public function filterQuery($filter_attributes, $paginate=true)
    {
       $query = Costcenter::with(['mandant']);

        if (isset($filter_attributes['filter'])) {

            if (isset($filter_attributes['filter_word']) && $filter_attributes['filter_row'] != 1) {
                $query->where($filter_attributes['filter_row'], 'LIKE', '%' . $filter_attributes['filter_word'] . '%');
            }
            if (isset($filter_attributes['filter_mandant']) && $filter_attributes['filter_mandant'] != '') {
                $query->where('mandant_id', '=', $filter_attributes['filter_mandant']);
            }
            if (isset($filter_attributes['filter_active']) && $filter_attributes['filter_active'] != '') {
                if($filter_attributes['filter_active']!='-1') {
                    $query->where('active', '=', $filter_attributes['filter_active']);
                }
            }

            $query->orderBy($filter_attributes['filter_row'], $filter_attributes['filter_order']);
        }

        if(!$paginate) return $query->get();

        return $query->paginate($this->elements_per_page);
    }


    public function index()
    {
        $filter_attributes = $this->_filterValidate();

        $costcenters = $this->filterQuery($filter_attributes);

        return view('app.invoicerelease.costcenter.index')->with(['costcenter' => new Costcenter, 'costcenters' => $costcenters, 'filter_attributes' => $filter_attributes]);

    }

    public function show(Costcenter $costcenter)
    {
        return $this->edit($costcenter);
    }

    public function create(Costcenter $costcenter)
    {
        return $this->edit($costcenter);
    }


    public function edit(Costcenter $costcenter)
    {
        return view('app.invoicerelease.costcenter.edit')->with(['costcenter' => $costcenter]);
    }

    public function store()
    {
        $attributes = $this->_validate(new Costcenter());

        $costcenter = Costcenter::create([
            'name' => $attributes['name'],
            'value' => $attributes['value'],
            'mandant_id' => $attributes['mandant_id'],
        ]);

        return redirect(route('app.invoicerelease.costcenter.index'))->withSuccess($costcenter->presentation_short.' erfolgreich angelegt!');
    }


    public function update(Costcenter $costcenter)
    {
        $attributes = $this->_validate($costcenter);

        $costcenter->update([
            'name' => $attributes['name'],
            'value' => $attributes['value'],
            'mandant_id' => $attributes['mandant_id'],
        ]);


        return back()->withSuccess($costcenter->presentation_short.' bearbeitet!');
    }


    public function destroy(Costcenter $costcenter)
    {
        $costcenter->delete();
        return back()->withDanger($costcenter->presentation_short.' gelöscht!');
    }


    public function _validate()
    {
        $attributes = request()->validate([
            'mandant_id' => 'required|not_in:0',
            'name' => 'required|string|min:2|max:50',
            'value' => 'string|min:2|max:15',
        ]);

        return $attributes;
    }

    public function toggleActive(Costcenter $costcenter) {
        $costcenter->update(['active' => !$costcenter->active]);
        return back()->withSuccess($costcenter->presentation_short.' wurde '.(!$costcenter->active?'deaktiviert':'aktiviert'));
    }


}
