<?php

namespace App\Http\Controllers\App\InvoiceRelease;

use App\PayonePayments;
use App\Http\Controllers\Controller;
use App\Filters\PayonePaymentsFilters;
use Illuminate\Http\Request;

class PayonePaymentController extends Controller
{


    public function index(PayonePaymentsFilters $paymentsFilters)
    {

        $payone_payments =  PayonePayments::filter($paymentsFilters)->sortable('created_at')->paginate(15);
        $filter = $paymentsFilters->getFilters();

        return view('app.invoicerelease.payone.index')->with(['payone_payments' => $payone_payments, 'filter' => $filter]);
    }

    public function importShow(Request $request)
    {

        return view('app.invoicerelease.payone.import');

    }

    public function import(Request $request)
    {

        $attributes = $request->validate(['file' => 'required']);
        $file =  $request->file->getRealPath();
        $array_rows = file($file);
        array_shift($array_rows);

        $anz = count($array_rows);
        $error = false;

        //"aid;txid;reference;userid;customerid;create_time;booking_date;document_date;document_reference;param;event;clearingtype;clearingsubtype;amount;currency"
        foreach ($array_rows as $row) {

            $data = str_getcsv(trim($row), ";");

            if(count($data)!=15) {
                $error = true;
                continue;
            }

            $aid = $data[0];
            $txid = $data[1];
            $reference = $data[2];
            $userid = $data[3];
            $customerid = empty($data[4]) ? 0 : $data[4];
            $create_time = $data[5];
            $booking_date = $data[6];
            $document_date = $data[7];
            $document_reference = $data[8];
            $param = $data[9];
            $event = $data[10];
            $clearingtype = $data[11];
            $clearingsubtype = $data[12];
            $amount = str_replace(',', '.', $data[13]);
            $currency = $data[14];

            $response = PayonePayments::updateOrCreate(
                ['txid' => $txid],
                ['aid' => $aid,
                'reference' => $reference,
                'userid' => $userid,
                'customerid' => $customerid,
                'create_time' => $create_time,
                'booking_date' => $booking_date,
                'document_date' => $document_date,
                'document_reference' => $document_reference,
                'param' => $param,
                'event' => $event,
                'clearingtype' => $clearingtype,
                'clearingsubtype' => $clearingsubtype,
                'amount' => $amount,
                'currency' => $currency]
            );

        }

        if($error) {
            return redirect(route('app.invoicerelease.payone.importShow'))->withDanger('Es ist ein Fehler aufgetreten, ggf. wurde eine falsche oder unvolständige Datei importiert! Bitte die IT informieren!');
        }
        return redirect(route('app.invoicerelease.payone.importShow'))->withSuccess($anz.' Zeilen erfolgreich verarbeitet!');

    }


    public function csv()
    {

        $paymentsFilters = new PayonePaymentsFilters(request());
        $payone_payments = PayonePayments::with('tempOrder')->filter($paymentsFilters)->sortable('created_at')->get();
        $filter = $paymentsFilters->getFilters();

        if(!empty($payone_payments)) {

            $date_from = 'no-from-date';
            if(isset($filter['date_from'])) $date_from = $filter['date_from'];
            $date_to = 'no-to-date';
            if(isset($filter['date_to'])) $date_to = $filter['date_to'];

            $fileName = 'PayONE_'.$date_from.'_'.$date_to.'.csv';

            $headers = array(
                "Content-Encoding" => "UTF-8",
                "Content-type" => "text/csv; charset=UTF-8",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );

            #$columns = array('DATUM', 'BETRAG', 'WAEHR', 'KDNR', 'RGNR', 'BESTNR', 'TXID', 'Kontonummer');
            $columns = array('DATUM', 'BETRAG', 'WAEHR', 'KDNR', 'RGNR', 'BESTNR, TXID', 'Kontonummer', 'Gutschein');
            $accountnumber = PayonePayments::getAccountnumber();

            $callback = function () use ($payone_payments, $columns, $accountnumber) {

                $file = fopen('php://output', 'w');
                fputcsv($file, $columns, ';', '"');

                $incomplete_data = [];

                foreach ($payone_payments as $payone_payment) {

                    $incomplete = false;
                    if(empty($payone_payment->booking_date) || empty($payone_payment->Rechnungsnummern)) {
                        $incomplete = true;
                    }

                    $BETRAG = str_replace('.', ',', $payone_payment->amount);

                    $KDNR = $payone_payment->ERP_Kundenr;
                    if(strlen($KDNR) == 6)  $KDNR = $KDNR * 100;
                    if(empty($KDNR)) $KDNR = 10000002;

                    $RGNR = str_replace(['RG', 'LS'], '', $payone_payment->Rechnungsnummern);
                    $RGNR_array = explode(',', $RGNR);

                    $clearingsubtype = $payone_payment->clearingsubtype;
                    if(empty($clearingsubtype)) $clearingsubtype = 'NULL';

                    if(count($RGNR_array) > 1) {
                        foreach ($RGNR_array AS $RGNR) {
                            $row['DATUM'] = $payone_payment->booking_date;
                            $row['BETRAG'] = $BETRAG;
                            $row['WAEHR'] = $payone_payment->currency;
                            $row['KDNR'] = $KDNR;
                            $row['RGNR'] = $RGNR;
                            $row['BESTNR_TXID'] = $payone_payment->custom_orders_id.', '.$payone_payment->txid;
                            $row['Kontonummer'] = $accountnumber[$clearingsubtype];
                            $row['Gutschein'] = '';
                            $BETRAG=0;
                            if($incomplete) {
                                $incomplete_data[] = $row;
                                continue;
                            }
                            fputcsv($file, $row, ';', '"');
                        }
                    }
                    else {
                        $RGNR = $RGNR_array[0];
                        $row['DATUM'] = $payone_payment->booking_date;
                        $row['BETRAG'] = $BETRAG;
                        $row['WAEHR'] = $payone_payment->currency;
                        $row['KDNR'] = $KDNR;
                        $row['RGNR'] = $RGNR;
                        $row['BESTNR_TXID'] = $payone_payment->custom_orders_id.', '.$payone_payment->txid;
                        $row['Kontonummer'] = $accountnumber[$clearingsubtype];
                        $row['Gutschein'] = '';
                        if($incomplete) {
                            $incomplete_data[] = $row;
                            continue;
                        }
                        fputcsv($file, $row, ';', '"');
                    }

                    if(!empty($payone_payment->temporder->gurado)) {
                        foreach ($payone_payment->temporder->gurado AS $gurado) {
                            $row['DATUM'] = $payone_payment->booking_date;
                            $row['BETRAG'] = str_replace(',', '.', $gurado['amount']);
                            $row['WAEHR'] = $payone_payment->currency;
                            $row['KDNR'] = $KDNR;
                            $row['RGNR'] = $RGNR;
                            $row['BESTNR_TXID'] = $payone_payment->custom_orders_id.', '.$payone_payment->txid;
                            $row['Kontonummer'] = $accountnumber['NULL'];
                            $row['Gutschein'] = $gurado['code'];

                            fputcsv($file, $row, ';', '"');

                        }
                    }

                }

                foreach ($incomplete_data AS $row) {
                    fputcsv($file, $row, ';', '"');
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        }

        return back()->withDanger('Keine Daten vorhanden');
    }


}
