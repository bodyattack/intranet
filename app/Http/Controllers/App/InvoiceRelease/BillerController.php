<?php

namespace App\Http\Controllers\App\InvoiceRelease;

use App\Http\Controllers\Controller;
use App\IrBiller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BillerController extends Controller
{

    public function _filterValidate()
    {
        $rules = [
            'filter' => 'sometimes|nullable|numeric',
            'filter_costcenter' => 'sometimes|nullable|int',
            'filter_word' => 'sometimes|nullable|regex:/^[\wÄÖÜäöüß]+$/',
            'filter_row' => 'sometimes|nullable|required_with:filter_word,filter|string|regex:/^[\w]+$/',
            'filter_order' => 'sometimes|nullable|required_with:filter|string|in:desc,asc',
            //'filter_payment' => 'sometimes|nullable|string|regex:/^[\w]+$/',
            'filter_skonto' => 'sometimes|nullable|string|in:1,0',
        ];

        $attributes = [
            'filter' => 'Filter',
            'filter_costcenter' => 'Kostenstelle',
            'filter_word' => 'Suchbegriff',
            'filter_row' => 'Suchbereich',
            'filter_order' => 'Sortierung',
            //'filter_payment' => 'Zahlart',
            'filter_skonto' => 'Skonto',
        ];


        $attributes = $this->validate(request(), $rules, array(), $attributes);

        return $attributes;
    }

    public function filterQuery($filter_attributes, $paginate=true)
    {
        $query = IrBiller::with('costcenter');

        if (isset($filter_attributes['filter'])) {

            if (isset($filter_attributes['filter_word']) && $filter_attributes['filter_row'] != 1) {
                $query->where($filter_attributes['filter_row'], 'LIKE', '%' . $filter_attributes['filter_word'] . '%');
            }
            if (isset($filter_attributes['filter_costcenter']) && $filter_attributes['filter_costcenter'] != '') {
                $query->where('costcenter_id', '=', $filter_attributes['filter_costcenter']);
            }
            /*
            if (isset($filter_attributes['filter_payment']) && !empty($filter_attributes['filter_payment'])) {
                $query->where('payment', '=', $filter_attributes['filter_payment']);
            }
            */
            if (isset($filter_attributes['filter_skonto']) && $filter_attributes['filter_skonto'] != null) {
                $operator = ($filter_attributes['filter_skonto']=='1'?'>=':'=');
                $query->where('discount_days', $operator, $filter_attributes['filter_skonto']);
            }

            $query->orderBy($filter_attributes['filter_row'], $filter_attributes['filter_order']);
        }

        #var_dump($filter_attributes);
        #echo $query->toSql();

        if(!$paginate) return $query->get();

        return $query->paginate($this->elements_per_page);
    }

    public function index()
    {
        $filter_attributes = $this->_filterValidate();

        $billers = $this->filterQuery($filter_attributes);

        return view('app.invoicerelease.biller.index')->with(['biller' => new IrBiller, 'billers' => $billers, 'filter_attributes' => $filter_attributes]);

    }


    public function match_subjects()
    {

        $billers = IrBiller::where('match_subject', '<>', '')->orderBy('name')->get();

        return view('app.info.match_subjects')->with(['billers' => $billers]);
    }

    public function show(IrBiller $biller)
    {
        return $this->edit($biller);
    }

    public function showAjax(IrBiller $biller)
    {

        echo IrBiller::with('distributor')->where('id', $biller->id)->get();

    }

    public function create(IrBiller $biller)
    {
        return $this->edit($biller);
    }


    public function edit(IrBiller $biller)
    {
        return view('app.invoicerelease.biller.edit')->with(['biller' => $biller]);
    }

    public function store()
    {
        $attributes = $this->_validate(new IrBiller());

        $biller = IrBiller::create([
            'name' => $attributes['name'],
            'creditor_number' => $attributes['creditor_number'],
            'payment' => $attributes['payment'],
            'due_days' => $attributes['due_days'],
            'discount_days' => $attributes['discount_days'],
            'discount' => $attributes['discount'],
            'costcenter_id' => $attributes['costcenter_id'],
            'match_subject' => $attributes['match_subject'],
            'match_email' => $attributes['match_email'],
        ]);

        $biller->distributor()->attach( $attributes['department_id']);


        return redirect(route('app.invoicerelease.biller.index'))->withSuccess($biller->presentation_short.' erfolgreich angelegt!');
    }


    public function update(IrBiller $biller)
    {
        $attributes = $this->_validate($biller);

        $biller->update([
            'name' => $attributes['name'],
            'creditor_number' => $attributes['creditor_number'],
            'payment' => $attributes['payment'],
            'due_days' => $attributes['due_days'],
            'discount_days' => $attributes['discount_days'],
            'discount' => $attributes['discount'],
            'costcenter_id' => $attributes['costcenter_id'],
            'match_subject' => $attributes['match_subject'],
            'match_email' => $attributes['match_email'],
        ]);

        $biller->distributor()->sync( $attributes['department_id']);

        return back()->withSuccess($biller->presentation_short.' bearbeitet!');
    }


    public function destroy(IrBiller $biller)
    {
        $biller->delete();
        return back()->withDanger($biller->presentation_short.' gelöscht!');
    }


    public function _validate($biller)
    {
        $attributes = request()->validate([
            'department_id' => 'required|not_in:0',
            'name' => 'required|string|min:2|max:50',
            'creditor_number' => 'sometimes|nullable|string|min:2|max:50',
            'payment' => 'required|string|min:2|max:50',
            'match_email' => 'sometimes|nullable|required_without:match_subject|string|min:2|max:100|unique:ir_billers,match_email,'.$biller->id,
            'match_subject' => 'sometimes|nullable|required_without:match_email|string|min:2|max:100|unique:ir_billers,match_subject,'.$biller->id,
            'costcenter_id' => 'sometimes|nullable|int|not_in:0',
            'due_days' => 'sometimes|int',
            'discount_days' => 'int|sometimes',
            'discount' => 'numeric|sometimes',
        ]);

        if($attributes['discount_days']!=0) {
            request()->validate([
                'discount' => 'numeric|min:1|not_in:0',
            ]);
        }

        if($attributes['discount']!=0) {
            request()->validate([
                'discount_days' => 'int|min:1|not_in:0',
            ]);
        }


        return $attributes;
    }

}
