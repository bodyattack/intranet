<?php

namespace App\Http\Controllers\App;

use App\Manufacturer;
use App\Filters\ManufacturerFilters;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ManufacturerController extends Controller
{



    public function index(ManufacturerFilters $filters)
    {
        $manufacturers =  Manufacturer::filter($filters)->sortable('pos')->paginate(5);

        return view('app.manufacturer.index')->with(['manufacturer' => new Manufacturer, 'manufacturers' => $manufacturers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manufacturer = new Manufacturer;
        return view('app.manufacturer.edit')->with(['manufacturer' => $manufacturer]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();

        $manufacturer = Manufacturer::create($attributes);

        return redirect(route('app.manufacturer.index'))->withSuccess($manufacturer->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Manufacturer $manufacturer)
    {
        return view('app.manufacturer.edit')->with(['manufacturer' => $manufacturer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Manufacturer $manufacturer)
    {
        return view('app.manufacturer.edit')->with(['manufacturer' => $manufacturer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Manufacturer $manufacturer)
    {
        $attributes = $this->_validate($manufacturer);
        $manufacturer->update($attributes);

        return back()->withSuccess($manufacturer->presentation_short.' bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Manufacturer $manufacturer)
    {
        $manufacturer->delete();
        return back()->withDanger($manufacturer->presentation_short.' gelöscht!');
    }


    public function _validate($manufacturer=NULL)
    {
        $id_exclude = '';
        if($manufacturer!=NULL) $id_exclude = ','.$manufacturer->id;

        $attributes = request()->validate([
            'name' => 'required|string|min:2|max:50|unique:manufacturers,name'.$id_exclude,
            'description' => 'sometimes|nullable|string|min:20|max:500',
            'metatext' => 'sometimes|nullable|string|min:20|max:500',
            'keywords' => 'sometimes|nullable|string|min:20|max:500',
            'icon' => 'sometimes|nullable|string|min:2|max:50',
            'rewrite' => 'sometimes|nullable|string|min:2|max:50',
        ]);

        return $attributes;
    }


    public function toggleActive(Manufacturer $manufacturer) {
        $manufacturer->update(['active' => !$manufacturer->active]);
        return back()->withSuccess($manufacturer->presentation_short.' wurde '.(!$manufacturer->active?'deaktiviert':'aktiviert'));
    }

    public function updateOrder()
    {
        $pos = json_decode(request('pos'), true);
        if(!empty($pos)) {
            foreach ($pos AS $key => $value) {
                DB::table('manufacturers')->where('id', $value)->update(['pos' => $key]);
            }
        }

    }


}
