<?php

namespace App\Http\Controllers\App;

use App\Coupon;
use App\CouponCode;
use App\Http\Controllers\Controller;
use App\TempOrder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CouponController extends Controller
{


    public function _filterValidate()
    {
        $rules = [
            'filter' => 'sometimes|nullable|numeric',
            'filter_art' => 'sometimes|nullable|string|in:0,1,2,3,4',
            'filter_multi' => 'sometimes|nullable|string|in:0,1,2',
            'filter_cheaper_check' => 'sometimes|nullable|string|in:1,0',
            'filter_code' => 'sometimes|nullable|string|min:3',
            'filter_row' => 'sometimes|nullable|required_with:filter_word,filter|string|regex:/^[\w]+$/',
            'filter_order' => 'sometimes|nullable|required_with:filter|string|in:desc,asc',
        ];

        $attributes = [
            'filter' => 'Filter',
            'filter_art' => 'Art',
            'filter_multi' => 'Multicodes',
            'filter_cheaper_check' => 'Rabatt Prüffung',
            'filter_code' => 'Gutscheincode',
            'filter_row' => 'Suchbereich',
            'filter_order' => 'Sortierung',
        ];


        $attributes = $this->validate(request(), $rules, array(), $attributes);

        return $attributes;
    }

    public function filterQuery($filter_attributes, $paginate=true)
    {
        $coupons = Coupon::with(['codes','userCreate']);

        if (isset($filter_attributes['filter'])) {

            if (isset($filter_attributes['filter_word']) && $filter_attributes['filter_row'] != 1) {
                $coupons->where($filter_attributes['filter_row'], 'LIKE', '%' . $filter_attributes['filter_word'] . '%');
            }

            if (isset($filter_attributes['filter_art']) && !empty($filter_attributes['filter_art'])) {
                $coupons->where('art', '=', $filter_attributes['filter_art']);
            }

            if (isset($filter_attributes['filter_multi']) && !empty($filter_attributes['filter_multi'])) {
                $operator = '<>';
                if($filter_attributes['filter_multi'] == 2) $operator = '=';
                $coupons->where('multi', $operator, 1);
            }

            if (isset($filter_attributes['filter_cheaper_check']) && !empty($filter_attributes['filter_cheaper_check'])) {
                $coupons->where('cheaper_check', '=', $filter_attributes['filter_cheaper_check']);
            }

            if (isset($filter_attributes['filter_row']) && !empty($filter_attributes['filter_row'])) {
                $coupons->orderBy($filter_attributes['filter_row'], $filter_attributes['filter_order']);
            }

            if (isset($filter_attributes['filter_code']) && !empty($filter_attributes['filter_code'])) {
                $code = trim($filter_attributes['filter_code']);
                $coupons = $coupons->whereHas('codes', function ($query) use ($code) {
                    $query->where('code', 'like', '%'.$code.'%');
                });
            }

        }

        #var_dump($filter_attributes);
        #echo $coupons->toSql();

        if(!$paginate) return $coupons->get();

        return $coupons->paginate($this->elements_per_page);
    }


    public function index()
    {

        /*
        $order = TempOrder::where('id', 487)->first();
        $couponCode = CouponCode::where('code', trim('BASN-XA21-XC7F'))->first();

        $couponCode->orders()->attach($order);
        */

        $filter_attributes = $this->_filterValidate();

        $coupons = $this->filterQuery($filter_attributes);

        return view('app.coupon.index')->with(['coupon' => new Coupon, 'coupons' => $coupons, 'filter_attributes' => $filter_attributes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $coupon = new Coupon;
        return view('app.coupon.edit')->with(['coupon' => $coupon]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        return view('app.coupon.edit')->with(['coupon' => $coupon]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        return view('app.coupon.edit')->with(['coupon' => $coupon]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function details(Coupon $coupon)
    {
        return view('app.coupon.details')->with(['coupon' => $coupon]);
    }


    public function generateCodes($coupon, $code, $multi=1)
    {
        $multi = intval($multi);

        if($multi==1) {
            $couponCode = new CouponCode();
            $couponCode->code = $code;

            $coupon->codes()->save($couponCode);
        }
        else {
            $rand = rand(1999, 4599);
            for($i=0; $i<$multi; $i++) {

                $string_first = strtoupper(dechex($rand+$i));
                $code_extension_first = str_pad($string_first, 4, "X", STR_PAD_LEFT);

                $string_mid= strtoupper(dechex(intval($coupon->id)+1000));
                $code_extension_mid = str_pad($string_mid, 4, "X", STR_PAD_LEFT);

                $string_last = strtoupper(dechex(rand(999, 9999)));
                $code_extension_last = str_pad($string_last, 4, "X", STR_PAD_LEFT);

                $code_start = 'BASN-';
                if(!empty(trim($code))) $code_start = $code.'-';

                $couponCode = new CouponCode();
                $couponCode->code = $code_start.$code_extension_first.'-'.$code_extension_mid.'-'.$code_extension_last;

                $coupon->codes()->save($couponCode);

            }

        }

        return $coupon;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->_validate();

        $code = $attributes['code'];
        unset($attributes['code']);

        $attributes = $this->helperArray2String($attributes);
        $attributes['created_by'] = Auth::user()->id;
        $coupon = Coupon::create($attributes);

        $coupon = $this->generateCodes($coupon, $code, $attributes['multi']);

        return redirect(route('app.coupon.index'))->withSuccess($coupon->presentation_short.' erfolgreich angelegt!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Coupon $coupon)
    {
        $attributes = $this->_validate($coupon);
        $attributes = $this->helperArray2String($attributes);

        // Verändern nur bei neuem Code und generell nur wenn kein Multi Code
        //Änderung löscht Bestellverweise, da diese an den Code gebunden sind
        if($coupon->multi == 1 && $attributes['code']!=$coupon->codes->first()->code) {
            $code = $attributes['code'];
            CouponCode::where('coupon_id', $coupon->id)->delete();
            $coupon = $this->generateCodes($coupon, $code, $attributes['multi']);
        }
        unset($attributes['code']);
        $attributes['updated_by'] = Auth::user()->id;

        $coupon->update($attributes);

        if(empty($attributes['min_order_value']) || ($attributes['min_order_value'] < $attributes['value'] )) {
            \Session::flash('warning', 'Hinweis!</br> Der Gutschein wurde gespeichert, doch der Min-Bestellwert ist kleiner dem Gutscheinwert - siehe Info-Icon.');
        }

        return back()->withSuccess($coupon->presentation_short.' bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
        return back()->withDanger($coupon->presentation_short.' gelöscht!');
    }


    public function _validate($coupon=Null)
    {

        $id_exclude = '';
        if($coupon!=NULL) $id_exclude = ','.$coupon->id.',coupon_id';

        $rules = [
            'art' => 'required|not_in:0',
            'code' => 'required_if:multi,1|nullable|string|min:2|max:50|unique:coupon_codes,code'.$id_exclude,
            'value' => 'required_unless:art,4,3|nullable|numeric',
            'cheaper_check' => 'required_unless:art,4|integer',
            'free_shipping_countries' => 'sometimes|nullable',
            'description' => 'sometimes|nullable|string|min:10|max:500',
            'description_private' => 'sometimes|nullable|string|min:10|max:500',
            'shops' => 'sometimes|nullable',
            'date_from' => 'sometimes|nullable|date_format:Y-m-d H:i:s',
            'date_to' => 'sometimes|nullable|date_format:Y-m-d H:i:s',
            'multi' => 'sometimes|nullable|integer|min:1|max:4000',
            'use_max' => 'sometimes|nullable|integer',
            'min_order_value' => 'sometimes|nullable|numeric',
            'max_order_value' => 'sometimes|nullable|numeric',
            'mandatory_products' => 'sometimes|nullable|string|min:4',
            'free_products' => 'required_with:free_articles|required_if:art,3|nullable|string|min:4',
            'free_articles' => 'sometimes|nullable|string|min:4',
            'manufacturers' => 'sometimes|nullable',
        ];

        $attributes = [
            'art' => 'Art des Gutscheins',
            'code' => 'Gutscheincode',
            'value' => 'Gutscheinwert',
            'cheaper_check' => 'Rabatt-Prüfung',
            'free_shipping_countries' => 'Versandkosten freie Länder',
            'description' => 'Interne Beschreibung',
            'description_private' => 'Öffentliche Beschreibung',
            'shops' => 'Shopzuordnung',
            'date_from' => 'Gültig von Datum',
            'date_to' => 'Gültig bis Datum',
            'multi' => 'Multicodes / Gutschein-Anzahl',
            'use_max' => 'Anzahl Einlösungen',
            'min_order_value' => 'Min. Bestellwert',
            'max_order_value' => 'Max. Bestellwert',
            'mandatory_products' => 'Pflicht Produkte',
            'free_products' => 'Gratis Produkte',
            'free_articles' => 'Gratis Artikel',
            'manufacturers' => 'Hersteller',
        ];

        return $this->validate(request(), $rules, array(), $attributes);
    }


    private function helperArray2String($attributes) {

        if(!empty($attributes['free_shipping_countries'])) {
            $attributes['free_shipping_countries'] = implode(',', $attributes['free_shipping_countries']);
        } else {
            $attributes['free_shipping_countries'] = '';
        }
        if(!empty($attributes['shops'])) {
            $attributes['shops'] = implode(',', $attributes['shops']);
        } else {
            $attributes['shops'] = '';
        }
        if(!empty($attributes['manufacturers'])) {
            $attributes['manufacturers'] = implode(',', $attributes['manufacturers']);
        } else {
            $attributes['manufacturers'] = '';
        }

        return $attributes;
    }



    public function toggleActive(Coupon $coupon) {
        $coupon->update(['active' => !$coupon->active]);
        return back()->withSuccess($coupon->presentation_short.' wurde '.(!$coupon->active?'deaktiviert':'aktiviert'));
    }


    public function codesCsv(Coupon $coupon)
    {
        $fileName = 'codes.csv';
        $codes = $coupon->unusedCodes;

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Gutscheincodes');

        $callback = function() use($codes, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($codes as $code) {
                $row['code']  = $code->code;

                fputcsv($file, array($row['code']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }



}
