<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use App\User;


class UserController extends Controller
{

  function showPasswortChangeForm() {

      return view('app.user.reset');
  }

  function storePasswortChange(User $user) {

    $attributes = request()->validate([
      'password' => 'required|string|max:32',
    ]);
    if(!empty($attributes['password'])) $user->password = Hash::make($attributes['password']);

    $user->save();

    return redirect(route('app.dashboard'))->withSuccess('Kennwort erfolgreich bearbeitet');

  }


    public function index()
    {
        $users = User::all();
        return view('app.user.index')->with(['users' => $users]);
    }

    public function request()
    {

        return view('app.user.request');
    }


}
