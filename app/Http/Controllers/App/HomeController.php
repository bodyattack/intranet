<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        $departments = $user->auditorAndRelatedDepartments();

        $settings['bills'] = [];
        if(!$departments->isEmpty()) {
            $departments_string = implode(',', $departments->pluck('id', 'id')->toArray());
            $settings['bills'] = DB::select(DB::raw("SELECT COUNT(id) AS neu, (SELECT COUNT(id) AS blocken FROM ir_bills WHERE department_id IN (" . $departments_string . ") AND status = 'blocken') AS blocken, (SELECT COUNT(id) AS inquiry FROM ir_bills WHERE department_id IN (" . $departments_string . ") AND status = 'reklamieren') AS inquiry FROM ir_bills WHERE department_id IN (" . $departments_string . ")  AND status = 'neu' "));
        }

        $settings['temp_orders'] = DB::select( DB::raw("
            SELECT COUNT(id) AS today, 
                   (SELECT COUNT(id) AS week FROM temp_orders WHERE datum BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()  ) AS week,
                  (SELECT COUNT(id) AS open FROM temp_orders WHERE transfer = 0 ) AS open ,
                 (SELECT COUNT(id) AS error FROM temp_orders WHERE transfer = -1 ) AS error 
                FROM temp_orders WHERE DATE(datum) = CURDATE()" ) );

        #$settings['xxx'] = DB::select( DB::raw("SELECT COUNT(id) AS sum, (SELECT COUNT(id) AS month FROM xxx WHERE created_at BETWEEN DATE_SUB(CURDATE(), INTERVAL  30 DAY) AND CURDATE()  ) AS month FROM xxx" ) );
        #$settings['yyy'] = DB::select( DB::raw("SELECT SUM(CAST(value AS DECIMAL(10,2))) AS sum, (SELECT SUM(CAST(value AS DECIMAL(10,2))) AS month FROM yyy WHERE status = 'paid' AND created_at BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND CURDATE()  ) AS month FROM yyy WHERE status = 'paid' " ) );

        /*

        $salt = '$gsu&56';
        $date = date("Ym");
        dd(md5($date . $salt));

        $data=[
            'forwarding_address' => 'c.stein@body-attack.com',
            'sender_address' => 'noreply@body-attack.de',
            'subject' => 'SSL'
            //'bcc' => 'c.stein@body-attack.com',
        ];

        Mail::raw('TEST', function ($message) use ($data) {

            $message->to($data['forwarding_address']);
            $message->subject($data['subject']);
            $message->from($data['sender_address']);
            $message->replyTo($data['sender_address']);
        });

        */

        return view('app.dashboard')->with(['settings' => $settings]);

    }

    public function login()
    {
        return view('login');
    }


}
