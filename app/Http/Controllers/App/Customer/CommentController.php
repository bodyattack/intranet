<?php

namespace App\Http\Controllers\App\Customer;

use App\Http\Controllers\Controller;
use App\Customer;


class CommentController extends Controller
{


    public function _validate()
    {
        $attributes = request()->validate([
            'comment' => 'required|not_in:0',
        ]);

        return $attributes;
    }


    public function index(Customer $customer)
    {
        $comments = $customer->getParentComments();


        #dd($comments[1]->getLastChild($comments[1]->children_rec));

       # dd($comments[1]->children_rec[0]->latest()->first());

        return view('app.customer.comment.index', compact('customer', 'comments'));
    }



    public function store(Customer $customer)
    {

        $attributes = $this->_validate();

        $customer->storeComment($attributes['comment'], $this->user->id);

        return redirect()
            ->route('app.customer.comment.index', [$customer])
            ->with('success', 'Bemerkung erstellt!');
    }


}
