<?php

namespace App\Http\Controllers\App\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Customer;


class CustomerController extends Controller
{

    public function _filterValidate()
    {
        $rules = [
            'filter' => 'sometimes|nullable|numeric',
            'filter_reset' => 'sometimes|nullable|numeric',
            'filter_word' => 'sometimes|nullable|regex:/^[\w]+$/',
            'filter_row' => 'sometimes|nullable|required_with:filter_word,filter|string|regex:/^[\w]+$/',
            'filter_order' => 'sometimes|nullable|required_with:filter|string|in:desc,asc',
        ];

        $attributes = [
            'filter' => 'Filter',
            'filter_reset' => 'Filter löschen',
            'filter_word' => 'Suchbegriff',
            'filter_row' => 'Suchbereich',
            'filter_order' => 'Sortierung',
        ];


        $attributes = $this->validate(request(), $rules, array(), $attributes);

        return $attributes;
    }

    public function filterQuery($filter_attributes, $paginate=true)
    {

        if (isset($filter_attributes['filter_reset'])) {
            request()->session()->forget('customer_filter');
        }

        $query = Customer::with('settings');

        if (isset($filter_attributes['filter'])) {

            request()->session()->put('customer_filter', $filter_attributes);

            if (isset($filter_attributes['filter_word']) && $filter_attributes['filter_row'] != 1) {

                $query->where($filter_attributes['filter_row'], 'LIKE', '%' . $filter_attributes['filter_word'] . '%');
                if($filter_attributes['filter_row'] == 'firstname') {
                    $query->orWhere('lastname', 'LIKE', '%' . $filter_attributes['filter_word'] . '%');
                }
            }

            $query->orderBy($filter_attributes['filter_row'], $filter_attributes['filter_order']);
        }

        if(!$paginate) return $query->get();

        return $query->paginate($this->elements_per_page, ['*'], 'c_page');

    }


    public function index()
    {

        $filter_attributes = $this->_filterValidate();
        if (empty($filter_attributes) && request()->session()->has('customer_filter')) {
            $filter_attributes = request()->session()->get('customer_filter');
        }

        $customers = $this->filterQuery($filter_attributes);

        return view('app.customer.index')->with(['customer' => new Customer, 'customers' => $customers, 'filter_attributes' => $filter_attributes]);

    }

    function edit(Customer $customer) {
        return view('app.customer.edit')->with(['customer' => $customer]);
    }


    function direct_edit(Customer $customer) {
        return view('app.customer.direct.edit')->with(['customer' => $customer]);
    }

    function direct_address(Customer $customer) {
        return view('app.customer.direct.address')->with(['customer' => $customer]);
    }

    public function destroy(Customer $customer)
    {
        $customer->delete();
        return redirect(route('app.customer.index'))->withSuccess($customer->presentation_short.' erfolgreich gelöscht');
    }


    function store() {

        $attributes = request()->validate([
            'erp_kdnr' => 'sometimes|nullable|int',
            'gender' => 'sometimes|nullable|string|in:male,female,div',
            'title' => 'sometimes|nullable|string',
            'firstname' => 'required|string|max:128',
            'lastname' => 'required|string|max:128',
            'email' => 'required|string|email|max:128|unique:customers',
            'phone' => 'sometimes|nullable|string',
            'dob' => 'sometimes|nullable|date|date_format:Y-m-d',
            'password' => 'required|confirmed|min:6',
        ]);

        if(!empty($attributes['password'])) {
            $attributes['password'] = Hash::make($attributes['password']);
        }

        $customer = Customer::create($attributes);
        

        return redirect(route('app.customer.index'))->withSuccess($customer->presentation_short.' erfolgreich angelegt');
    }


    function update(Customer $customer) {

        $attributes = request()->validate([
            'erp_kdnr' => 'sometimes|nullable|int',
            'gender' => 'sometimes|nullable|string|in:male,female,div',
            'title' => 'sometimes|nullable|string',
            'firstname' => 'required|string|max:128',
            'lastname' => 'required|string|max:128',
            'email' => 'required|string|email|max:128|unique:customers,email,'.$customer->id,
            'phone' => 'sometimes|nullable|string',
            'dob' => 'sometimes|nullable|date|date_format:Y-m-d',
            'password' => 'sometimes|nullable|string',
        ]);

        $customer->erp_kdnr = $attributes['erp_kdnr'];
        $customer->gender = $attributes['gender'];
        $customer->title = $attributes['title'];
        $customer->firstname = $attributes['firstname'];
        $customer->lastname = $attributes['lastname'];
        $customer->email = $attributes['email'];
        $customer->phone = $attributes['phone'];
        $customer->dob = $attributes['dob'];

        if(!empty($attributes['password'])) $customer->password = Hash::make($attributes['password']);

        $customer->update();

        return back()->withSuccess($customer->presentation_short.' erfolgreich bearbeitet');

    }


    public function toggleActive(Customer $customer) {

        $customer->update(['active' => !$customer->active]);
        return back()->withSuccess($customer->presentation_short.' wurde '.(!$customer->active?'deaktiviert':'aktiviert'));
    }

    
}
