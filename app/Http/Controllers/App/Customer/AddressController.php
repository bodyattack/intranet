<?php

namespace App\Http\Controllers\App\Customer;

use App\Http\Controllers\Controller;
use App\Customer;
use App\Country;
use App\CustomerAddress AS Address;

class AddressController extends Controller
{


    public function _validate()
    {
        $attributes = request()->validate([
            'address' => 'required|max:255',
            'additional' => 'nullable|string|max:255',
            'type' => 'required|string|max:255',
            'zip' => 'required|max:255',
            'city' => 'required|max:255',
            'primary' => 'sometimes|nullable|required|numeric',
            'country_id' => 'required|numeric',
            'customer_id' => 'required|numeric',
        ]);

        return $attributes;
    }


    public function index(Customer $customer)
    {
        $address = $customer->address()->paginate($this->elements_per_page, ['*'], 'a_page');

        return view('app.customer.address.index', compact('customer', 'address'));
    }

    public function create(Customer $customer)
    {
        $countries = Country::all();

        return view('app.customer.address.create', compact('customer', 'countries'));
    }

    public function store(Customer $customer, Address $address)
    {
        $address = $address->create($this->_validate());

        return redirect()
            ->route('app.customer.address.index', ['address' => $address, 'customer' => $customer])
            ->with('success', $address->presentation_short.' erfolgreich erstellt');
    }


    public function edit(Customer $customer, Address $address)
    {
        return view('app.customer.address.edit', compact('customer', 'address'));
    }

    public function update(Customer $customer, Address $address)
    {
        $address->update($this->_validate());

        return back()->withSuccess($address->presentation_short.' bearbeitet erfolgreich bearbeitet');
    }

    public function togglePrimary(Customer $customer, Address $address)
    {
        $address_array = $customer->address->where('type', $address->type);
        foreach ($address_array AS $address_object) {
            $address_object->update(['primary' => 0]);
        }
        $address->update(['primary' => 1]);

        return back()->withSuccess($address->presentation_short.' erfolgreich primär gesetzt');
    }


    public function destroy(Customer $customer, Address $address)
    {
        $address->delete();

        return redirect()
            ->route('app.customer.address.index', [$customer])
            ->with('success', 'Adresse entfernt!');
    }
}
