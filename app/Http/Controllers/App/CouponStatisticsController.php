<?php

namespace App\Http\Controllers\App;

use App\Coupon;
use App\CouponCode;
use App\Http\Controllers\Controller;
use App\TempOrder;
use Illuminate\Support\Facades\DB;

class CouponStatisticsController extends Controller
{


    public function _filterValidate()
    {
        $rules = [
            'filter' => 'sometimes|nullable|integer',
            'coupon' => 'sometimes|nullable|numeric',
            'code' => 'sometimes|nullable|string',
            'datefrom_filter' => 'sometimes|nullable|date|date_format:Y-m-d',
            'dateto_filter' => 'sometimes|nullable|date|date_format:Y-m-d',
        ];

        $attributes = [
            'filter' => 'Filter',
            'coupon' => 'Gutschein',
            'code' => 'Gutscheincode',
            'datefrom_filter' => 'Datum von',
            'dateto_filter' => 'Datum bis',
        ];

        return $this->validate(request(), $rules, array(), $attributes);
    }


    public function filterQuery($filter_attributes)
    {

        $couponCodes = [];
        if (isset($filter_attributes['filter'])) {
            $coupon_id = 0;

            if (isset($filter_attributes['code']) && !empty(trim($filter_attributes['code']))) {
                // find coupon id by code
                $couponCodes = CouponCode::with(['coupon', 'orders']);
                $code = trim($filter_attributes['code']);
                $codes = $couponCodes->whereHas('coupon', function ($query) use ($code) {
                    $query->where('code', 'like', '%' . $code . '%');
                });
                $coupon_id = $codes->first()->coupon_id;
            }

            if (isset($filter_attributes['coupon']) && !empty(trim($filter_attributes['coupon']))) {
                $coupon_id = $filter_attributes['coupon'];
            }


            if (!empty($coupon_id)) {
                $couponCodes = CouponCode::with(['coupon', 'orders']);
                $couponCodes->whereHas('coupon', function ($query) use ($coupon_id) {
                    $query->where('id', $coupon_id);
                });

                if (!empty($filter_attributes['datefrom_filter']) && !empty($filter_attributes['dateto_filter'])) {
                    $couponCodes->whereHas('orders', function ($query) use ($filter_attributes) {
                        $query->whereDate('datum', '>=', $filter_attributes['datefrom_filter']);
                        $query->whereDate('datum', '<=', $filter_attributes['dateto_filter']);
                    });
                } elseif (!empty($filter_attributes['datefrom_filter'])) {
                    $couponCodes->whereHas('orders', function ($query) use ($filter_attributes) {
                        $query->whereDate('datum', '>=', $filter_attributes['datefrom_filter']);
                    });
                } elseif (!empty($filter_attributes['dateto_filter'])) {
                    $couponCodes->whereHas('orders', function ($query) use ($filter_attributes) {
                        $query->whereDate('datum', '<=', $filter_attributes['dateto_filter']);
                    });
                }

                $couponCodes->whereHas('orders');

                // Off, because the results are in the relations and these are many
                #if(!$paginate) return $couponCodes->get();
                #$couponCodes = $couponCodes->paginate($this->elements_per_page);

                $couponCodes = $couponCodes->get();

            }

        }

        return $couponCodes;
    }


    public function index()
    {

        $filter_attributes = $this->_filterValidate();

        $couponCodes = $this->filterQuery($filter_attributes);

        return view('app.coupon.statistics')->with(['couponCodes' => $couponCodes, 'filter_attributes' => $filter_attributes]);
    }


    public function show(Coupon $coupon)
    {
        return view('app.coupon.edit')->with(['coupon' => $coupon]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function details(Coupon $coupon)
    {
        return view('app.coupon.details')->with(['coupon' => $coupon]);
    }



    public function exportCsv()
    {

        $filter_attributes = $this->_filterValidate();

        if(!empty($filter_attributes['coupon'])) {

            $fileName = 'Gutschein_Auswertung_' . date('Y-m-d') . '.csv';
            $couponCodes = $this->filterQuery($filter_attributes);

            if($couponCodes->isNotEmpty()) {

                $headers = array(
                    "Content-Encoding" => "UTF-8",
                    "Content-type" => "text/csv; charset=UTF-8",
                    "Content-Disposition" => "attachment; filename=$fileName",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );

                $columns = array('Code', 'Bestelldatum', 'Best.-Nr.: Shop', 'Best.-Nr.: Intranet', 'Kunde', 'Ort', 'Zahlart', 'Artikel', 'Warenwert', 'Nachlass in Euro', 'Nachlass in Prozent', 'Gutschein Art');

                $callback = function () use ($couponCodes, $columns) {

                    $couopn_arten[1] = 'Gutschrift';
                    $couopn_arten[2] = 'Prozent';
                    $couopn_arten[3] = 'Gratisprodukt';
                    $couopn_arten[4] = 'Versandkostenfrei';

                    $file = fopen('php://output', 'w');
                    fputcsv($file, $columns, ';', '"');

                    foreach ($couponCodes as $code) {

                        $coupon = $code->coupon;
                        $orders = $code->orders;

                        foreach ($orders as $order) {

                            $preis_vorher = $order->gesamtsumme + $order->warenkorb_nachlass;
                            $rabatt = round((($order->warenkorb_nachlass * 100) / $preis_vorher), 2);
                            $artikel_count = count($order->warenkorb_string);

                            $row['code'] = $code->code;
                            $row['datum'] = $order->datum;
                            $row['transaction_key'] = $order->transaction_key;
                            $row['intranet_id'] = $order->intranet_id;
                            $row['kunde'] = $order->kunde['firstname'] . ' ' . $order->kunde['lastname'];
                            $row['ort'] = $order->kunde['zip'] . ' ' . $order->kunde['city'];
                            $row['zahlungsart'] = $order->zahlungsart;
                            $row['artikel'] = $artikel_count;
                            $row['betrag'] = str_replace('.', ',', $order->gesamtsumme / 100);
                            $row['nachlass'] = str_replace('.', ',', $order->warenkorb_nachlass / 100);
                            $row['rabatt'] = str_replace('.', ',', $rabatt);
                            $row['art'] = $couopn_arten[$coupon->art];

                            $row = array_map(function ($cell) {
                                return mb_convert_encoding($cell, 'UTF-16LE', 'UTF-8');
                            }, $row);

                            fputcsv($file, $row, ';', '"');
                        }
                    }

                    fclose($file);
                };

                return response()->stream($callback, 200, $headers);
            }
        }

        return back()->withDanger('Keine Daten vorhanden');
    }



}
