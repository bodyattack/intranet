<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\Presentable;

class CustomerAddress extends Model
{


    public $presentation_short = 'Adresse';
    public $presentation_long = 'Adressen';

    protected $guarded=['id'];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

}
