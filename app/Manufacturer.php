<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use App\Filters\Filterable;

class Manufacturer extends Model
{
    use Sortable;
    use Filterable;

    protected $guarded=['id'];

    public $presentation_short = 'Hersteller';
    public $presentation_long = 'Hersteller';

    public $sortable = [
        'id',
        'name',
        'pos',
        'description',
        'created_at',
        'updated_at'
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
