<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded=['id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'created_by')->withDefault([
            'name' => 'Gelöschter Nutzer',
        ]);
    }

    public function userEdit()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }


    public function child()
    {
        return $this->hasMany('App\Comment', 'parent_id');
    }


    public function children_rec()
    {
        return $this->child()->with('children_rec');
    }


    public function parent()
    {
        return $this->belongsTo('App\Comment','id');
    }

    public function parent_rec()
    {
        return $this->parent()->with('parent_rec');
    }

}
