<?php

namespace App;

use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;
use App\Presenters\Presentable;
use Kyslik\ColumnSortable\Sortable;

class PayonePayments extends Model
{

    use Sortable;
    use Filterable;

    protected $guarded=['id'];

    public $accountnumber=[
        'AMZ' => '1373001', // Amazon Shop BASN
        'PPE' => '1374001', // Paypal
        'V' => '1374600', // Kreditkarte
        'M' => '1374600', // Kreditkarte
        'PDT' => '1374600', // Paydirekt
        'PNT' => '1374600', // Sofort Überweisung
        'NULL' => 'ohne Bezug',
        # 'V' => '1374300', // Kreditkarte ALT - Anpassung ML 22.04.2022
        # 'M' => '1374300', // Kreditkarte ALT - Anpassung ML 22.04.2022
        # 'PDT' => '1374700', // Paydirekt ALT - Anpassung ML 22.04.2022
        # 'PNT' => '1374701', // Sofort Überweisung ALT - Anpassung ML 22.04.2022
    ];

    public $sortable = [
        'booking_date',
        'created_at',
        'amount',
        'currency',
        'ERP_Kundenr',
        'Rechnungsnummern',
        'custom_orders_id',
        'txid',
        'clearingsubtype'
    ];

    public function tempOrder()
    {
        return $this->belongsTo('App\TempOrder');
    }

    public static function getAccountnumber() {
        $objekt = new self;
        return $objekt->accountnumber;
    }
}
