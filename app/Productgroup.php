<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productgroup extends Model
{
    protected $guarded=['id'];

    public $presentation_short = 'Produktgruppe';
    public $presentation_long = 'Produktgruppen';


    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
