<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IrBillItem extends Model
{
    protected $guarded=['id'];

    public $presentation_short = 'Rechnungsdokument';
    public $presentation_long = 'Rechnungsdokumente';

    public function bill() {
        return $this->hasOne('App\Bill');
    }


}
