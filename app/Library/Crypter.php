<?php

namespace App\Library;

use Illuminate\Support\Facades\Crypt;

/*
| The data will be encrypted using the algorithm specified in your config/app.php file,
| and the key will be the APP_KEY value in your .env file.
*/

class Crypter
{
    /**
     * @param string $path
     * @param bool   $returnAsString
     */
    public static function encryptFile(string $path, bool $returnAsString = false)
    {
        if (!is_file($path)) {
            throw new \InvalidArgumentException('File ist not available!');
        }

        if ($returnAsString) {
            return self::encryptString(file_get_contents($path));
        }

        file_put_contents($path, self::encryptString(file_get_contents($path)));
    }

    /**
     * @param string $path
     * @param bool   $returnAsString
     */
    public static function decryptFile(string $path, bool $returnAsString = false)
    {
        if (!is_file($path)) {
            throw new \InvalidArgumentException('File ist not available!');
        }

        if ($returnAsString) {
            return self::decryptString(file_get_contents($path));
        }

        file_put_contents($path, self::decryptString(file_get_contents($path)));
    }

    /**
     * @param string $data
     *
     * @return string
     */
    public static function encryptString(string $data): string
    {
        return Crypt::encrypt($data);
    }

    /**
     * @param string $data
     *
     * @return string
     */
    public static function decryptString(string $data): string
    {
        return Crypt::decrypt($data);
    }
}
