<?php

namespace App\Library;

use App\Comment;
use Illuminate\Support\Str;

trait ExportCsv
{


    public function csv()
    {

        $users = User::all();

        // Header
        $head_row = array();
        $head_row[] = 'Anrede';
        $head_row[] = 'Vorname';
        $head_row[] = 'Nachname';
        $head_row[] = 'Email';

        // Daten
        $colum_rows = array();
        $i = 0;
        foreach ($users AS $user) {
            $colum_rows[$i][] = $user->gender;
            $colum_rows[$i][] = $user->name;
            $colum_rows[$i][] = $user->surname;
            $colum_rows[$i][] = $user->email;

            $i++;
        }

        self::getCsv($head_row, $colum_rows, date('Y-m-d') . '_user.csv');
    }


    public static function getCsv($columnNames, $rows, $fileName = 'file.csv')
    {

        header('Content-Type: text/x-csv');
        header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Disposition: attachment; filename=' . $fileName);
        header('Pragma: no-cache');

        $output = "";
        $output .= implode(";", $columnNames) . "\n";

        foreach ($rows as $row) {
            $output .= implode(";", $row) . "\n";
        }

        echo $output;

    }


}
