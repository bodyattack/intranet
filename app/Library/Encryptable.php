<?php

namespace App\Library;

use Illuminate\Support\Facades\Crypt;

/*
| Apply the trait to the model, and define a array property called
| $encryptable whith column names whose data should be encrypted.
|
| The data will be encrypted using the algorithm specified in your config/app.php file,
| and the key will be the APP_KEY value in your .env file.
|
| class Patient extends Model
| {
|    use Encryptable;
|
|     protected $encryptable = [
|         'blood_type',
|         'medical_conditions',
|         'allergies',
|         'emergency_contact_id',
|     ];
| }
*/

trait Encryptable
{
    /**
     * Decrypt attribute.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if (!empty($value) && in_array($key, $this->encryptable)) {
            try {
                return Crypt::decrypt($value);
            } catch (\Exception $e) {
                return $value;
            }
        }

        return $value;
    }

    /**
     * Encrypt attribute.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        if (!empty($value) && in_array($key, $this->encryptable)) {
            $value = Crypt::encrypt($value);
        }

        return parent::setAttribute($key, $value);
    }

    /**
     * @return array
     */
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();

        foreach ($this->encryptable as $key) {
            if (isset($attributes[$key])) {
                try {
                    $attr = Crypt::decrypt($attributes[$key]);
                } catch (\Exception $e) {
                    $attr = $attributes[$key];
                }
                $attributes[$key] = $attr;
            }
        }

        return $attributes;
    }
}
