<?php

namespace App\Library;

use App\Image;
use Illuminate\Support\Str;

trait Images
{

    private function getRelatedName() {
        return Str::of(get_class($this))->afterLast('\\');
    }

    private function checkEmpty($images) {
        return $images->isEmpty();
    }

    public function getImages() {
        $images = Image::with('user')->where('related', $this->getRelatedName())->where('foreign_id', $this->id)->orderByDesc('id')->get();
        return $this->checkEmpty($images) ? [] : $images;
    }


    public function storeImage($name, $url, $pos=0) {

        Comment::create([
            'name' => $name,
            'url' => $url,
            'pos' => $pos,
            'related' => $this->getRelatedName(),
            'foreign_id' => $this->id,
        ]);

    }


}
