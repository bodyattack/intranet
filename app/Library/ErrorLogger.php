<?php

namespace App\Library;

use Illuminate\Support\Facades\Log;

trait ErrorLogger
{

    public function logError($message, $channel='daily') {

        $this->log($message, 'error', $channel);
        
    }

    public function logInfo($message, $channel='daily') {

        $this->log($message, 'info', $channel);

    }


    private function log($message_multi_line, $type, $channel) {

        $message="\n\r";
        $message .= implode("\n\r", $message_multi_line);
        $message.="\n\r";
        $message.="######################################################";

        if(env('APP_DEBUG')) echo $message;

        Log::channel($channel)->$type($message);


    }



    private function logAlt($message_multi_line, $type, $channel) {



        $message="\n\r";
        if($type=='info') {
            $message .= implode("\n\r", $message_multi_line);
            $message.="\n\r";
            $message.="######################################################";
            Log::channel($channel)->info($message);
        }
        else {
            $message .= implode("\n\r", $message_multi_line);
            $message.="\n\r";
            $message.="######################################################";
            Log::channel($channel)->error($message);
        }


    }
    
}
