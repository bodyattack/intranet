<?php

namespace App\Library;

use App\Comment;
use Illuminate\Support\Str;

trait Comments
{


    private function getRelatedName() {
        return Str::of(get_class($this))->afterLast('\\');
    }

    private function checkEmpty($comments) {
        return $comments->isEmpty();
    }

    private static function checkEmptyStatic($comments) {
        return $comments->isEmpty();
    }


    public static function getCommentsStatic($foreign_id, $related) {
        $comments = Comment::where('related',$related)->where('foreign_id', $foreign_id)->get();
        return self::checkEmptyStatic($comments) ? [] : $comments;
    }

    public static function getUserCommentsStatic($foreign_id, $user_id, $related) {
        $comments = Comment::where('related',$related)->where('foreign_id', $foreign_id)->where('created_by', $user_id)->get();
        return self::checkEmptyStatic($comments) ? [] : $comments;
    }



    public function getCommentsLastChild() {
        $comments = Comment::with('user')->where('related', $this->getRelatedName())->where('foreign_id', $this->id)->orderByDesc('id')->get();
        return $this->checkEmpty($comments) ? [] : $comments;
    }

    public function getComments() {
        $comments = Comment::with('user')->where('related', $this->getRelatedName())->where('foreign_id', $this->id)->orderByDesc('id')->get();
        return $this->checkEmpty($comments) ? [] : $comments;
    }

    public function getParentComments() {
        $comments = Comment::with('user')->whereNull('parent_id')->where('related', $this->getRelatedName())->where('foreign_id', $this->id)->orderByDesc('id')->get();
        return $this->checkEmpty($comments) ? [] : $comments;
    }

    public function getCommentsFromUser($user_id) {
        $comments =  Comment::with('user')->where('related', $this->getRelatedName())->where('foreign_id', $this->id)->where('created_by', $user_id)->orderByDesc('id')->get();
        return $this->checkEmpty($comments) ? [] : $comments;
    }


    public function storeComment($text, $user_id) {

        Comment::create([
            'comment' => $text,
            'related' => $this->getRelatedName(),
            'foreign_id' => $this->id,
            'created_by' => $user_id,
        ]);

    }


}
