<?php

namespace App\Library;

use ConnectionException;
use Illuminate\Support\Str;
use PhpImap\Mailbox;
use Illuminate\Support\Facades\Mail;


class ImapController
{

    use ErrorLogger;

    protected static $logChannel = 'imap';

    public $forwarding_address = 'it@body-attack.de';   // Default, override by Cron BillController
    public $sender_address = 'noreply@body-attack.de';  // Default, override by Cron BillController

    protected $mailbox;
    protected $mail_ids = [];
    protected $emails= [];
    protected $attachment_errors= [];


    public function connect($conf, $user, $pass)
    {

        if(!@\imap_open($conf, $user, $pass, 0, 0, [])) {
            $this->logError(['IMAP Error: Postfach '.$conf.', Verbindung nicht möglich!'], self::$logChannel);
            return false;
        }

        try {
            $this->mailbox = new Mailbox($conf, $user, $pass);
            return true;
        }
        catch (ConnectionException $e) {
            $this->logError(['IMAP Error: '.$e->getMessage()], self::$logChannel);
        }
        catch (Exception $e) {
            $this->logError(['General Error: '.$e->getMessage()], self::$logChannel);
        }

    }


    public function checkMails($type='UNSEEN')
    {
        try {
            $this->mail_ids = $this->mailbox->searchMailbox($type);
        }
        catch (ConnectionException $e) {
            $this->logError(['IMAP Error: '.$e->getMessage()], self::$logChannel);
        }
        catch (Exception $e) {
            $this->logError(['General Error: '.$e->getMessage()], self::$logChannel);
        }

        if(!empty($this->mail_ids)) return $this->mail_ids;

        return [];
    }

    public function validateAttachments($attachments)
    {
        $unsets=[];
        foreach ($attachments as $key => $attachment) {

            $type = $this->getAttachmentType($attachment);

            if($type != 'pdf') {
                $unsets[]=$key;
            }
            else {
                if($this->checkAttachmentSize($attachment)) {
                    $this->setError('Anhang ist größer 19,5 MB!');
                }
            }

        }

        foreach ($unsets as $unset) {
            unset($attachments[$unset]);
        }

        return $attachments;
    }


    public function checkAttachmentSize($attachments)
    {
        if($attachments->sizeInBytes>20447232) {
            return true;
        }
        return false;
    }

    public function getAttachmentType($attachment)
    {
        return Str::afterLast(strtolower($attachment->name), '.');
    }


    public function setError($error) {
        $this->attachment_errors[]=$error;
    }

    public function getError() {
        return $this->attachment_errors;
    }

    public function getMailIds() {

            return $this->mail_ids;

    }


    public function deleteMailById($mail_id) {

            return $this->deleteMail($mail_id);

    }



    public function checkRequiredFolders() : void {

        $folders = $this->getFolders();

        if (!(in_array('Erledigt', $folders))) {
            $this->mailbox->createMailbox('INBOX/Erledigt');
        }
        if (!(in_array('Error', $folders))) {
            $this->mailbox->createMailbox('INBOX/Error');
        }

    }


    public function getFolders($pattern='INBOX*') {

        $folders_full = $this->mailbox->getMailboxes($pattern);
        $key= 'shortpath';

        return array_map(function($v) use ($key) {
            return is_object($v) ? $v->$key : substr($v[$key], 6);
        }, $folders_full);

    }

    public function getMails($markUnread=true) {

        foreach ($this->mail_ids as $mail_id) {
            $this->emails[$mail_id] = $this->mailbox->getMail(
                $mail_id, // ID of the email, you want to get
                $markUnread // Do NOT mark emails as seen (optional)
            );
        }

        return $this->emails;
    }


    public function getMailById($mail_id, $markUnread=true) {

        try {
            $email = $this->mailbox->getMail(
            $mail_id, // ID of the email, you want to get
            $markUnread // Do NOT mark emails as seen (optional)
            );
        }

        catch (ConnectionException $e) {
            $this->logError(['IMAP Error: '.$e->getMessage()], self::$logChannel);
        }
        catch (Exception $e) {
            $this->logError(['General Error: '.$e->getMessage()], self::$logChannel);
        }

        return $email;

    }


    public function forwardMailById($email, $attachment=[], $type='error') {

        $LE  = "\r\n";
        $preSubject="";
        $messege_body = "\n "."Siehe Anlage";

        if($type=='error') {
            $error_text="";
            foreach ($this->attachment_errors As $error) {
                $error_text.="- ".$error. $LE . $LE;
            }
            $preSubject = "Rechnungskontrolle - ";
            $preBody = "Folgende Nachricht konnte nicht automatisch verarbeitet werden:" . $LE . $LE .
                $error_text .
                "###########################################################" . $LE . $LE .
                "Erhalten von: " . $email->fromAddress . $LE . $LE .
                "Erhalten am: " . $email->date . $LE . $LE .
                "Betreff: " . $email->subject . $LE . $LE .
                "###########################################################" . $LE . $LE . $LE;
            $messege_body = "\n$preBody.$email->textPlain";

            if(!Str::contains($this->sender_address, '@')) $this->sender_address = 'rechnung@body-attack.de';
        }

        try {

            $data=[
                'email_object' => $email,
                'forwarding_address' => $this->forwarding_address,
                'sender_address' => $this->sender_address,
                'subject' => $preSubject.$email->subject,
                'bcc' => 'c.stein@body-attack.com',
                'attachment' => $attachment,
                ];

            if($type=='error') {
                $data['cc'] = 'm.langhof@body-attack.com';
            }

            Mail::raw($messege_body, function ($message) use ($data) {

                $message->to($data['forwarding_address']);
                $message->subject($data['subject']);
                $message->from($data['sender_address']);
                $message->replyTo($data['sender_address']);
                $message->bcc($data['bcc']);
                if(isset($data['cc']) && !empty(trim($data['cc'])))  {
                    $message->cc($data['cc']);
                }

                $email = $data['email_object'];

                if(empty($data['attachment'])) {
                    if ($email->hasAttachments()) {
                        $attachments = $email->getAttachments();
                        foreach ($attachments as $attachment) {
                            $message->attachData($attachment->getContents(), $attachment->name);
                        }
                    }
                }
                else {
                    $message->attachData($data['attachment']->getContents(), $data['attachment']->name);
                }

            });

        }

        catch (ConnectionException $e) {
                $this->logError(['IMAP Error: '.$e->getMessage()], self::$logChannel);
            }
        catch (Exception $e) {
            $this->logError(['General Error: '.$e->getMessage()], self::$logChannel);
        }

    }


    public function moveToFolderByID($mail_id, $folder='Erledigt') {

        try {
            $this->mailbox->moveMail($mail_id, $folder);
        }
        catch (ConnectionException $e) {
            $this->logError(['IMAP Error: '.$e->getMessage()], self::$logChannel);
        }
        catch (Exception $e) {
            $this->logError(['General Error: '.$e->getMessage()], self::$logChannel);
        }

    }


    public function getTextByID($mail_id) {

        try {
            echo 'from-name: '.(string) (isset($this->emails[$mail_id]->fromName) ? $this->emails[$mail_id]->fromName : $this->emails[$mail_id]->fromAddress)."\n";
            echo 'from-email: '.(string) $this->emails[$mail_id]->fromAddress."\n";
            echo 'to: '.(string) $this->emails[$mail_id]->toString."\n";
            echo 'subject: '.(string) $this->emails[$mail_id]->subject."\n";
            echo 'message_id: '.(string) $this->emails[$mail_id]->messageId."\n";
        }
        catch (ConnectionException $e) {
            $this->logError(['IMAP Error: '.$e->getMessage()], self::$logChannel);
        }
        catch (Exception $e) {
            $this->logError(['General Error: '.$e->getMessage()], self::$logChannel);
        }

    }

    public function getSubjectByID($mail_id) {

        try {
            return (string) $this->emails[$mail_id]->subject;
        }
        catch (ConnectionException $e) {
            $this->logError(['IMAP Error: '.$e->getMessage()], self::$logChannel);
        }
        catch (Exception $e) {
            $this->logError(['General Error: '.$e->getMessage()], self::$logChannel);
        }

    }

    public function getSenderByID($mail_id) {

        try {
            return (string) $this->emails[$mail_id]->fromAddress;
        }
        catch (ConnectionException $e) {
            $this->logError(['IMAP Error: '.$e->getMessage()], self::$logChannel);
        }
        catch (Exception $e) {
            $this->logError(['General Error: '.$e->getMessage()], self::$logChannel);
        }

    }


}
