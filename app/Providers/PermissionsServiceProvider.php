<?php

namespace App\Providers;

use App\Permission;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class PermissionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {


        if (app()->environment('local') && Schema::hasTable('permissions'))
        {
            Permission::get()->map(function($permission){
                Gate::define($permission->slug, function($user) use ($permission){
                    return $user->hasPermission($permission);
                });
            });
        }
        /*
        else {
            Permission::get()->map(function($permission){
                Gate::define($permission->slug, function($user) use ($permission){
                    return $user->hasPermission($permission);
                });
            });
        }
        */

        Blade::directive('area', function ($area){
            return "<?php if(auth()->check() && auth()->user()->hasArea({$area})) : ?>";
        });

        Blade::directive('endarea', function (){
            return "<?php endif; ?>";
        });

        Blade::directive('end', function (){
            return "<?php endif; ?>";
        });

        Blade::directive('permission', function ($permission){
            return "<?php if(auth()->check() && auth()->user()->hasPermission({$permission})) : ?>";
        });

        Blade::directive('endpermission', function (){
            return "<?php endif; ?>";
        });

        Blade::directive('anypermission', function ($param){
            $permissions = explode('|', $param);
            $allowed = 0;
            foreach($permissions as $permission) {
                if(auth()->user()->hasPermission($permission)) {
                    $allowed = 1;
                    break;
                }
            }
            return "<?php if(auth()->check() && {$allowed} ) : ?>";
        });

        Blade::directive('anyarea', function ($param){
            $areas = explode('|', $param);
            $allowed = 0;
            foreach($areas as $area) {
                if(auth()->user()->hasArea($area)) {
                    $allowed = 1;
                    break;
                }
            }
            return "<?php if(auth()->check() && {$allowed} ) : ?>";
        });
    }
}
