<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // money
        Blade::directive('money', function ($money) {
            return "<?php echo number_format(str_replace(',', '.', $money), 2, ',', '.').' €'; ?>";
        });

        // money_space
        Blade::directive('money_space', function ($money) {
            return "<?php echo number_format(str_replace(',', '.', $money), 2, ',', ' ').' €'; ?>";
        });

        // date
        Blade::directive('date', function ($date) {
            return "<?php echo ( empty($date)  ? '' : date('d.m.Y', strtotime($date)) ); ?>";
        });

        // date
        Blade::directive('dateDB', function ($date) {
            return "<?php echo ( empty($date)  ? '' : date('Y-m-d', strtotime($date)) ); ?>";
        });

        // date
        Blade::directive('datetimeDB', function ($date) {
            return "<?php echo ( empty($date)  ? '' : date('Y-m-d H:i:s', strtotime($date)) ); ?>";
        });

        // datetimee
        Blade::directive('datetime', function ($date) {
            return "<?php echo ( empty($date)  ? '' : date('d.m.Y - H:i', strtotime($date)) ); ?>";
        });

        // head icons
        Blade::directive('headicons', function ($arguments) {

            $array = explode(',',str_replace("'", "", $arguments));
            if(count($array) == 2 ) list($anz, $type) = $array;
            else {
                list($anz) = $array;
                $type = "";
            }

            return "<?php echo '<th style=\'vertical-align: inherit;\' class=\'head-icons-{$anz}\'> <i class=\'fas {$type} float-right\'></i> </th>'; ?>";
        });

    }
}
