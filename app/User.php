<?php

namespace App;

use App\Notifications\ResetUserPassword;
use App\Notifications\UserWelcome;
use App\Department;
use App\Http\Controllers\Control\Permissions\HasPermissionsTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasPermissionsTrait;


    protected $guarded=['id'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function fullName()
    {
        $name = $this->name;
        if(!empty($this->surname)) $name .= ' '.$this->surname;

        return $name;
    }


    public function level()
    {
        return $this->belongsTo('App\Level');
    }


    public function departments() {
        return $this->belongsToMany(Department::class,'users_departments');
    }

    public function hasDepartment($department) {

        if(is_array($department)) $department = $department[0];
        if(!$department instanceof  Department) {
            $department = Department::where('name', $department)->first();
        }
        return (bool) $this->departments()->where('id', $department->id)->count();
    }



    public function departmentsLeader() {

        // ask only connected departments
        #$array = $this->departments->where('user_id', $this->id)->pluck('slug')->toArray();

        // all departments, even department if not connected to a user
        $array = $this->hasMany('App\Department', 'user_id', 'id')->pluck('slug')->toArray();
        $erg=[];
        foreach ($array AS $value) {
            $erg[$value]=1;
        }
        return $erg;
    }

    public function getDepartmentsBySlug() {
        $array = $this->departments()->pluck('slug')->toArray();
        $erg=[];
        foreach ($array AS $value) {
            $erg[$value]=1;
        }
        return $erg;
    }

    public function auditorForDepartments()
    {
        return $this->belongsToMany('App\Department', 'ir_department_releaseusers', 'user_id', 'department_id');
    }

    public function auditorAndRelatedDepartments()
    {
         return collect([$this->auditorForDepartments, $this->departments])->collapse()->unique('id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetUserPassword($token));
    }


    public function sendWelcomeNotification()
    {
        $this->notify(new UserWelcome());
    }


}
