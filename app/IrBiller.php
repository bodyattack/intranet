<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IrBiller extends Model
{
    protected $guarded=['id'];

    public $presentation_short = 'Rechnungssteller';
    public $presentation_long = 'Rechnungssteller';

    public function distributor() {
        return $this->belongsToMany('App\Department', 'ir_billers_departments', 'biller_id', 'department_id');
    }

    public function hasDistributor($department) {
        return (bool) $this->distributor()->where('id', $department->id)->count();
    }

    public function costcenter() {
        return $this->hasOne('App\IrCostcenter', 'id', 'costcenter_id');
    }


    // static!  all billers with distributor
    public static function withDistributor()
    {
        return  DB::table('ir_billers')
            ->join('ir_billers_departments', 'ir_billers.id', '=', 'ir_billers_departments.biller_id')
            ->select('ir_billers.*')
            ->groupBy('ir_billers_departments.biller_id', 'ir_billers.id')
            ->orderBy('ir_billers.name')
            ->get();
    }
}
