<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mandant extends Model
{

    protected $guarded=['id'];

    protected $table = "mandanten";

    public $presentation_short = 'Mandant';
    public $presentation_long = 'Mandanten';

    public function mailbox() {
        return $this->hasOne('App\IrMailbox');
    }

    public function costcenter() {
        return $this->hasOne('App\IrCostcenter');
    }
}
