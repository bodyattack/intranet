<?php

namespace App;

use App\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;
use App\Library\Comments;
use Kyslik\ColumnSortable\Sortable;

class IrBill extends Model
{
    use Sortable;
    use Filterable;
    use Comments;

    protected $guarded=['id'];

    public $presentation_short = 'Rechnung';
    public $presentation_long = 'Rechnungen';

    public $sortable = [
        'id',
        'status',
        'payment_method',
        'bill_number',
        'amount',
        'created_at',
        'due_date'
    ];

    public function items() {
        return $this->hasMany('App\IrBillItem', 'bill_id');
    }


    public function mandant()
    {
        return $this->belongsTo('App\Mandant');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function costcenter()
    {
        return $this->belongsTo('App\IrCostcenter');
    }


    public function biller() {
        return $this->hasOne('App\IrBiller', 'id','biller_id');
    }


    public function mailbox() {
        return $this->hasOne('App\IrMailbox', 'id', 'mailbox_id');
    }


}


