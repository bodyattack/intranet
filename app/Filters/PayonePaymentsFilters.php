<?php
namespace App\Filters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PayonePaymentsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function amount($term)
    {
        return $this->builder->where('payone_payments.amount', '=', "$term");
    }

    public function custom_orders_id($term)
    {
        return $this->builder->where('payone_payments.custom_orders_id', '=', $term);
    }

    public function txid($term)
    {
        return $this->builder->where('payone_payments.txid', '=', "$term");
    }

    public function ERP_Kundenr($term)
    {
        return $this->builder->where('payone_payments.ERP_Kundenr', '=', "$term");
    }

    public function currency($term)
    {
        return $this->builder->where('payone_payments.currency', '=', "$term");
    }

    public function clearingsubtype($term)
    {
        return $this->builder->where('payone_payments.clearingsubtype', '=', "$term");
    }

    public function Rechnungsnummern($term)
    {
        return $this->builder->where('payone_payments.Rechnungsnummern', 'LIKE', "%$term%");
    }

    public function date_from($term)
    {
        return $this->builder->whereDate('payone_payments.booking_date', '>=', $term);
    }

    public function date_to($term)
    {
        return $this->builder->whereDate('payone_payments.booking_date', '<=', $term);
    }



    public function _filterValidate()
    {

        $rules = [
            'amount' => 'sometimes|nullable|numeric',
            'custom_orders_id' => 'sometimes|nullable|int',
            'txid' => 'sometimes|nullable|int',
            'Rechnungsnummern' => 'sometimes|nullable|string',
            'ERP_Kundenr' => 'sometimes|nullable|string',
            'currency' => 'sometimes|nullable|string',
            'clearingsubtype' => 'sometimes|nullable|string',
            'date_from' => 'sometimes|nullable|date|date_format:Y-m-d',  // booking_date
            'date_to' => 'sometimes|nullable|date|date_format:Y-m-d',
        ];

        $attributes = [
            'amount' => 'Betrag',
            'custom_orders_id' => 'Bestellnummer',
            'txid' => 'TXID',
            'Rechnungsnummern' => 'Rechnungsnummer',
            'ERP_Kundenr' => 'Kundennummer',
            'currency' => 'Währung',
            'clearingsubtype' => 'Kontonummer',
            'due_date_from' => 'Kaufdatum von',
            'due_date_to' => 'Kaufdatum bis',
        ];

        return Validator::validate($this->filters(), $rules, array(), $attributes);

    }
}
