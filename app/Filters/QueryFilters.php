<?php

namespace App\Filters;

use App\IrFilter as Filter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class QueryFilters
{
    protected $related_class = '';
    protected $filters = [];

    public    $default_filters = [];
    protected $force_override_filter = 0;

    protected $request;
    protected $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->related_class = $this->getRelatedName();
    }

    private function getRelatedName() {
        return Str::afterLast(get_class($this), '\\');
    }

    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        $filters = $this->filters();

        foreach ($filters as $name => $value) {
            if ( ! method_exists($this, $name)) {
                continue;
            }
            if (strlen($value)) {
                $this->$name($value);
            }
        }

        return $this->builder;
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    public function getFilters()
    {
        return $this->filters;
    }

    public function setDefaultFilters($filters, $force_override_filter = 0)
    {
        $this->default_filters = $filters;
        $this->force_override_filter = $force_override_filter;

    }

    public function filters()
    {
        // Filters by request
        $this->filters = $this->request->all();

        // Filters override by defaults
        if( (empty($this->filters) OR $this->force_override_filter) AND !empty($this->default_filters)) {
            $this->filters = $this->default_filters;
        }

        // Filters override by DB
        if(isset($this->filters['filter_own']) && !empty($this->filters['filter_own'])) {
            $this->filters = $this->prepareOwnFilter($this->filters['filter_own']);
        }

        return $this->filters;
    }


    private function prepareOwnFilter($filter_id)
    {
        $user_filter = json_decode(Filter::where('id', $filter_id)->where('user_id', Auth::id())->where('page', $this->related_class)->first()->filter, true);

        foreach ($user_filter as $key => $value) {
            $filter_attributes[$key]=$value;
            if(preg_match('/(date)/', $key)) {
                // date param on filter manipulate given month / year to current
                $filter_attributes[$key]=preg_replace('/(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)$/',date('Y').'-'.date('m').'-$3', $value);
            };
        }

        $filter_attributes['filter_own']=$filter_id;

        return $filter_attributes;

    }
}