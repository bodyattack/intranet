<?php

namespace App\Filters;

use App\Http\Controllers\Controller;
use App\IrFilter as Filter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SavedFilterController extends Controller
{

    public function store(Request $request)
    {

        $params = $request->all(['name', 'filter', 'related']);

        Filter::create([
            'user_id' => Auth::user()->id,
            'name' => $params['name'],
            'page' => $params['related'],
            'global' => '0',
            'filter' => $params['filter'],
        ]);

        return 'store';

    }

    public function store_success() {
        return redirect(route('app.invoicerelease.bills.index'))->withSuccess('Filter erfolgreich angelegt.');
    }

    public function update_success() {
        return redirect(route('app.invoicerelease.bills.index'))->withSuccess('Filter erfolgreich bearbeitet.');
    }


    public function update(Request $request, Filter $filter)
    {
        $params = $request->all(['name', 'filter']);

        $filter->update([
            'name' => $params['name'],
            'filter' => $params['filter'],
        ]);

        return 'update';
    }

    public function destroy(Filter $filter)
    {
        $filter->delete();
        return redirect(route('app.invoicerelease.bills.index'))->withSuccess('Filter erfolgreich gelöscht.');
    }
}
