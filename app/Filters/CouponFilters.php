<?php
namespace App\Filters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CouponFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
        $this->_filterValidate();
    }


    public function kunde($term) {
        return $this->builder->where('temp_orders.kunde', 'LIKE', "%$term%");
    }

    public function transfer($term) {
        return $this->builder->where('temp_orders.transfer', '=', $term);
    }

    public function number($term) {
        return $this->builder->where('temp_orders.TxId', '=', "$term")
            ->orWhere('temp_orders.intranet_id', '=', "$term")
            ->orWhere('temp_orders.transaction_key', '=', "$term");
    }

    public function date_from($term)
    {
        return $this->builder->whereDate('temp_orders.datum', '>=', $term);
    }

    public function date_to($term)
    {
        return $this->builder->whereDate('temp_orders.datum', '<=', $term);
    }


    public function _filterValidate()
    {
        $rules = [
            'filter' => 'sometimes|nullable|numeric',
            'number' => 'sometimes|nullable|string',
            'kunde' => 'sometimes|nullable|string',
            'transfer' => 'sometimes|nullable|string|in:0,1,2,-1',
            'date_from' => 'sometimes|nullable|date|date_format:Y-m-d',
            'date_to' => 'sometimes|nullable|date|date_format:Y-m-d',
        ];

        $attributes = [
            'filter' => 'Filter',
            'number' => 'Auftragsnummer',
            'kunde' => 'Kunde',
            'transfer' => 'Transfer Status',
            'date_from' => 'Datum von',
            'date_to' => 'Datum bis',
        ];

        return Validator::validate($this->filters(), $rules, array(), $attributes);
    }


}