<?php
namespace App\Filters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class IrBillFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function department($term)
    {
        return $this->builder->where('ir_bills.department_id', '=', "$term");
    }

    public function mandant($term)
    {
        return $this->builder->where('ir_bills.mandant_id', $term);
    }

    public function biller($term)
    {
        return $this->builder->where('ir_bills.biller_id', '=', "$term");
    }

    public function payment($term)
    {
        return $this->builder->where('ir_bills.payment_method', '=', "$term");
    }

    public function process_number($term)
    {
        return $this->builder->where('ir_bills.process_number', 'LIKE', "%$term%");
    }

    public function bill_number($term)
    {
        return $this->builder->where('ir_bills.bill_number', 'LIKE', "%$term%");
    }

    public function due_date_from($term)
    {
        return $this->builder->whereDate('ir_bills.due_date', '>=', $term);
    }

    public function due_date_to($term)
    {
        return $this->builder->whereDate('ir_bills.due_date', '<=', $term);
    }

    public function date_from($term)
    {
        return $this->builder->whereDate('ir_bills.date', '>=', $term);
    }

    public function date_to($term)
    {
        return $this->builder->whereDate('ir_bills.date', '<=', $term);
    }

    public function status($term)
    {
        if ($term == 'unsettled') {
            $yesterday = date("Y-m-d", strtotime('-1 days'));
            return $this->builder->whereNotIn('status', ['bezahlt'])->whereDate('ir_bills.due_date', '<', $yesterday);
        }
        if ($term == 'transit') {
            return $this->builder->whereRaw('(MONTH(ir_bills.date) <> MONTH(ir_bills.delivery_date) AND ir_bills.date > ir_bills.delivery_date) ');
        }
        if ($term == 'swimm') {
            return $this->builder->whereRaw('(MONTH(ir_bills.date) <> MONTH(ir_bills.delivery_date) AND ir_bills.date < ir_bills.delivery_date)');
        }
        if ($term == 'skonto') {
            return $this->builder->whereHas('biller', function ($query) use ($term) {
                $query->whereRaw('( ir_billers.discount_days <> 0 AND DATE(NOW()) < DATE_ADD(ir_bills.date, INTERVAL ir_billers.discount_days DAY))');
            });
        }
        return $this->builder->where('ir_bills.status', $term);
    }

    public function _filterValidate()
    {

        $rules = [
            'filter' => 'sometimes|nullable|numeric',
            'filter_own' => 'sometimes|nullable|int',
            'status' => 'sometimes|nullable|string',
            'mandant' => 'sometimes|nullable|int',
            'biller' => 'sometimes|nullable|int',
            'process_number' => 'sometimes|nullable|string',
            'bill_number' => 'sometimes|nullable|string',
            'department' => 'sometimes|nullable|int',
            'payment' => 'sometimes|nullable|string',
            'date_from' => 'sometimes|nullable|date|date_format:Y-m-d',
            'date_to' => 'sometimes|nullable|date|date_format:Y-m-d',
            'due_date_from' => 'sometimes|nullable|date|date_format:Y-m-d',
            'due_date_to' => 'sometimes|nullable|date|date_format:Y-m-d',
        ];

        $attributes = [
            'filter' => 'Filter',
            'filter_own' => 'Individueller Filter',
            'status' => 'Status',
            'process_number' => 'Vorgangsnummer',
            'bill_number' => 'Rechnungsnummer',
            'mandant' => 'Mandant',
            'biller' => 'Rechnungsteller',
            'department' => 'Abteilung',
            'payment' => 'Zahlart',
            'datepart' => 'Zeitraum nach ist',
            'date_from' => 'Rechnungsdatum von',
            'date_to' => 'Rechnungsdatum bis',
            'due_date_from' => 'Fälligkeitsdatum von',
            'due_date_to' => 'Fälligkeitsdatum bis',
        ];

        return Validator::validate($this->filters(), $rules, array(), $attributes);

    }
}
