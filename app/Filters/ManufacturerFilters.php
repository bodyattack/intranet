<?php
namespace App\Filters;
use App\Manufacturer;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ManufacturerFilters extends QueryFilters
{
    protected $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function name($term) {
        return $this->builder->where('manufacturers.name', 'LIKE', "%$term%");
    }

}