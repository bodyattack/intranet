<?php


namespace App\Externals\Gurado;


class PreAuthorization extends AbstractRequest
{

    public function authorization($voucher, $external='', $amount=1000 ) {

        $params = [
            'amount' => $amount,
            'returnParameters' => [
                #'externalPreauthorizeTransactionNumber' => $external,
                'shop_order_id' => $external,
            ],
        ];

        return $this->sendRequest($params, 'PUT', 'vouchers/'.$voucher.'/authorize');

    }

    public function redeem($external='xxx') {

        return $this->sendRequest([], 'PUT', 'vouchers/preauthorize/ext-'.$external.'/redeem');

    }

    public function release($external='xxx') {

        return $this->sendRequest([], 'PUT', 'vouchers/preauthorize/ext-'.$external.'/release');

    }




}