<?php


namespace App\Externals\Gurado;


class Vouchers extends AbstractRequest
{

    public function listVouchers() {

        $params = [
            'pageSize' => '10',
            'page' => '1',
        ];

        return $this->sendRequest($params, 'GET', 'vouchers');

    }

    public function getVoucherInformation($voucher) {

        return $this->sendRequest([], 'GET', 'vouchers/'.$voucher);

    }


    public function getVoucherTransactions($voucher) {

        return $this->sendRequest([], 'GET', 'vouchers/'.$voucher.'/transactions');

    }

    public function rechargeVoucher($voucher, $amount) {

        $params = [
            'amount' => $amount,
        ];

        return $this->sendRequest($params, 'PUT', 'vouchers/'.$voucher.'/recharge');

    }

    public function redeemVoucher($voucher, $data) {

        $params = [
            'amount' => $data['amount'],
            'transactionDate' => date("Y-m-d H:i:s"),
            'returnParameters' =>  [
                'kassierernummer' => $data['kassierernummer'],
                'bonnummer' => $data['bonnummer']
            ]
        ];

        $this->sendRequest($params, 'PUT', 'vouchers/'.$voucher.'/redeem');

    }

    public function create($amount=10) {

        $params = [
            'amount' => $amount,
            'externalProductSku' => '123',
            ];

        return $this->sendRequest($params, 'POST', 'vouchers');

    }
}