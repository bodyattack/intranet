<?php

namespace App\Externals\Gurado;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;


abstract class AbstractRequest
{

    protected $CONSUMER_KEY = '';
    protected $CONSUMER_SECRET = '';

    protected $api_url = 'https://api.gurado.de/v1.0/';
    private $client;

    public $status_code = 0;
    public $error_message  = 0;

    public function __construct()
    {

        $this->CONSUMER_KEY = env('GURADO_CONSUMER_KEY');
        $this->CONSUMER_SECRET = env('GURADO_CONSUMER_SECRET');

        $this->client = new Client([
            'base_uri' => $this->api_url,
            'headers'  => [
                'X-GURADO-CONSUMER-KEY' => $this->CONSUMER_KEY,
                'X-GURADO-CONSUMER-SECRET' => $this->CONSUMER_SECRET,
                'content-type' => 'application/x-www-form-urlencoded',
                'Accept' => 'application/json'],
        ]);

    }

    protected function sendRequest(array $params, $method = 'post', $route = '')
    {

        try {
            return $this->parseResponse(
                $this->client->{$method}($route, ['form_params' => $params])
            );

        } catch (ClientException $e) {

            $response = $e->getResponse();
            $data['method'] = $method;
            $data['entpoint'] = $this->api_url.$route;
            $data['params'] = $params;
            $data['response'] = json_decode($response->getBody()->getContents(), true);
            $data['status'] = $response->getStatusCode();

            if($data['status'] == 404 || $data['status'] == 400) {
                return $data;
            }

            $this->logError($data, 'Api Call');

            return false;
        }
    }


    private function parseResponse($response)
    {

        if($response->getStatusCode() == 200) {
            if (implode($response->getHeader('Content-Type')) == 'text/plain; charset=UTF-8') {
                return $this->parseTextResponse($response);
            }

            return json_decode($response->getBody(), true);
        }

        return false;

    }


    private function parseTextResponse($response)
    {
        $responseArray = array();
        $explode = explode("\n", $response->getBody());

        foreach ($explode as $e) {
            $keyValue = explode('=', $e);
            if (trim($keyValue[0]) != '') {
                if (count($keyValue) == 2) {
                    $responseArray[$keyValue[0]] = trim($keyValue[1]);
                } else {
                    $key = $keyValue[0];
                    unset($keyValue[0]);
                    $value = implode('=', $keyValue);
                    $responseArray[$key] = $value;
                }
            }
        }

        if(env('APP_DEBUG')=='true') {
            $context = 'RESPONSE:';
            $this->logError($responseArray, $context);
        }

        if ($responseArray['status'] == 'ERROR') {
            $this->logError($responseArray, 'Payone returned an error');
        }

        return $responseArray;
    }



    protected function logError($message, $context="")
    {

        if(is_array($message)) $message = print_r($message, TRUE);
        if(is_array($context)) $context = print_r($context, TRUE);

        $text="\r\n"."##############################################";
        if(!empty($context))  $text.="\r\n".$context;
        $text.="\r\n".$message;

        $this->Log($text, 'Error');
    }

    protected function Log($message, $type)
    {
        if(env('APP_DEBUG')=='true' AND $type=='Error') {
            echo $type. ' - '.$message;
        }
        Log::channel('api')->error($type.'-LOG Gurado : '.$message);
    }



}