<?php


namespace App\Externals\Gurado;


class VoucherTransactions extends AbstractRequest
{


    public static function listAll() {

        $params = [
            'pageSize' => '10',
            'page' => '1',
        ];

        return self::sendRequest($params, 'GET', 'transactions');

    }

    public function get($code) {

        $params = [
            'pageSize' => '10',
            'page' => '1',
        ];

        return $this->sendRequest($params, 'GET', 'transactions/'.$code.'/transactions');

    }


}