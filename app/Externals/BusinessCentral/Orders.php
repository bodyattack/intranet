<?php

namespace App\Externals\BusinessCentral;

class Orders extends AbstractRequest
{

    public function create($data) {

        $response = $this->makeApiRequest('InboundCreateInboundOrder_TWC', 'POST', ['SOAPAction' => '#POST', 'Content-Type' => 'application/xml'], $data);

        if ($response["status"] != 200) return $response["status"]."~".$response["text"];

    }
}

/**
<s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'>
    <s11:Body>
        <ns1:CreateSalesOrder xmlns:ns1='urn:microsoft-dynamics-schemas/codeunit/InboundCreateInboundOrder_TWC'>
            <ns1:inboundCreateInbOrder>
 *
 *
                <Header_General>
                    <Document_No></Document_No>
                    <Order_Date>2023-10-02</Order_Date>
                    <Currency_Code></Currency_Code>
                    <Shipping_Agent_Code>DHL</Shipping_Agent_Code>
                    <Shipping_Agent_Service_Code>STANDARD</Shipping_Agent_Service_Code>
                    <External_Document_No>Review_20231006</External_Document_No>
                    <Payment_Method_Code>Rechnung</Payment_Method_Code>
                    <Prices_Incl_VAT>true</Prices_Incl_VAT>
                </Header_General>
 *
 *
                <Header_Sell_To_Customer>
                    <Contact_No>K00000001</Contact_No>
                    <Name>Michael Wendler</Name>
                    <Name_2></Name_2>
                    <First_Name>Michael</First_Name>
                    <Surname>Wendler</Surname>
                    <Address>Preußenweg 10</Address>
                    <Address_2></Address_2>
                    <Post_Code>49076</Post_Code>
                    <City>Osnabrück</City>
                    <Country_Code>DE</Country_Code>
                    <Template_Code></Template_Code>
                    <E_Mail_Address>dzumstrull@tso.de</E_Mail_Address>
                </Header_Sell_To_Customer>
 *
 *
                <Header_Bill_To_Customer>
                </Header_Bill_To_Customer>
 *
 *
                <Header_Ship_To_Customer>
                    <Name>Olga Bohlen</Name>
                    <Name_2></Name_2>
                    <First_Name>Laura</First_Name>
                    <Surname>Wendler</Surname>
                    <Address>Preußenweg 10a</Address>
                    <Address_2>1. Stockwerk</Address_2>
                    <Post_Code>49076</Post_Code>
                    <City>Osnabrück</City>
                    <Country_Code>DE</Country_Code>
                    <ShipmentType></ShipmentType>
                </Header_Ship_To_Customer>
 *
 *
                <Header_Payment>
                    <TX_ID></TX_ID>
                    <TX_Code></TX_Code>
                    <TX_Amount></TX_Amount>
                    <PmtTransactionAmount></PmtTransactionAmount>
                </Header_Payment>
 *
 *
                <Sales_Lines>
 *
                    <Sales_Line>
                        <Line_No>10000</Line_No>
                        <Type>Item</Type>
                        <No>10025</No>
                        <Description>Serious Mass (2,72kg-Strawberry) von Optimum Nutrition Inhalt: 2,72kg Geschmack: Strawberry</Description>
                        <Description_2></Description_2>
                        <Quantity>1</Quantity>
                        <Line_Amount_Incl_VAT>249.50</Line_Amount_Incl_VAT>
                        <Unit_Price>249.50</Unit_Price>
                    </Sales_Line>
 *
                </Sales_Lines>
 *
 *
                <Comment_Lines>
 *
                    <Comment_Line>
                        <Comment>Auftragsbemerkung Zeile 1</Comment>
                    </Comment_Line>
                    <Comment_Line>
                        <Comment>Auftragsbemerkung Zeile 2</Comment>
                    </Comment_Line>
 *
                </Comment_Lines>
 *
 *
 *
            </ns1:inboundCreateInbOrder>
        </ns1:CreateSalesOrder>
    </s11:Body>
</s11:Envelope>

 **/