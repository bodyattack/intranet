<?php

namespace App\Externals\BusinessCentral;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;

class AbstractRequest
{
    private $client_id;
    private $client_secret;
    private $api_url;
    private $token_file = __DIR__ . '/token_data.json';


    public function __construct()
    {

        $this->client_id = env('MS_BC_CLIENT_ID');
        $this->client_secret = env('MS_BC_CLIENT_SECRET');
        $this->scope = env('MS_BC_SCOPE');
        $this->token_url = env('MS_BC_TOKEN_URL');
        $this->api_url = env('MS_BC_API_URL');

    }

    public function getToken()
    {

        $tokenData = $this->readTokenData();

        // Überprüfe, ob Token-Daten vorhanden und gültig sind
        if ($tokenData && isset($tokenData['access_token']) && isset($tokenData['expires']) && $tokenData['expires'] > time()) {
            return $tokenData['access_token'];
        }

        try {
            $client = new Client();

            $post_data = [
                'grant_type' => 'client_credentials',
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'scope' => $this->scope,
            ];

            $response = $client->post($this->token_url, [
                'form_params' => $post_data,
            ]);

            $http_status = $response->getStatusCode();
            if ($http_status !== 200) {
                $this->logError("Fehler bei der Token-Anfrage. HTTP-Statuscode: " . $http_status);
                return null;
            }

            $response_data = json_decode($response->getBody(), true);

            // Speichere Token-Daten in der Datei
            $this->saveTokenData([
                'access_token' => $response_data['access_token'],
                'expires' => time() + $response_data['expires_in'],
            ]);

            return $response_data['access_token'];

        } catch (RequestException $e) {
            $this->logError("Fehler bei der Token-Anfrage: " . $e->getMessage());
            return null;
        }
    }

    public function makeApiRequest($endpoint, $method = 'GET', $headers = [], $body = null)
    {

        try {
            $client = new Client();

            $url = $this->api_url . $endpoint;

            $options = [
                'headers' => $headers + ['Authorization' => 'Bearer ' . $this->getToken()],
                'body' => $body,
            ];

            $response = $client->request($method, $url, $options);

            $responseArray["status"] = $response->getStatusCode();
            $responseArray["text"] = $this->convertSoapResponse($response->getBody()->getContents());

            return $responseArray;

        } catch (RequestException $e) {

            $responseBody = "";

            // Zugriff auf die vollständige Antwort
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseBody = $response->getBody()->getContents();

                // Speichere die vollständige Antwort in einer Variable
                $this->logError("Meldung: " . $responseBody);
            }
            else  $this->logError("Fehler bei API-Anfrage: " . $e->getMessage());

            $responseBodyTemp = substr($responseBody, strpos($responseBody, '<faultstring') + 30, strlen($responseBody));
            $responseBody = substr($responseBodyTemp, 0, 167);

            $responseArray["status"] = '';
            $responseArray["text"] = $responseBody;

            return $responseArray;
        }
    }


    private function convertSoapResponse($data)
    {
        $soap_string = preg_replace("/(<\/?)(\w+):([^>]*>)/", '$1$2$3', $data);
        $soap_string = simplexml_load_string($soap_string);
        $json = json_encode($soap_string);

        return json_decode($json, true);
    }


    private function saveTokenData($data)
    {

        file_put_contents($this->token_file, json_encode($data));
    }

    private function readTokenData()
    {
        if (file_exists($this->token_file)) {
            $data = file_get_contents($this->token_file);
            return json_decode($data, true);
        }

        return null;
    }


    private function logError($message)
    {
        $developMode = env('APP_DEBUG'); // 'true' für Entwicklungsmodus, 'false' für Produktionsmodus

        if ($developMode === 'true') {
            // Im Entwicklungsmodus direkt im Browser ausgeben
            echo $message . '<br>';
        } else {
            // Im Produktionsmodus in eine Log-Datei schreiben
            Log::channel('api')->error('ERROR-LOG MS-BC : '.$message);
        }
    }

}
