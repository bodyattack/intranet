<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Externals\Gurado\Vouchers;
use App\Externals\Gurado\PreAuthorization;

class Gurado extends Model
{

    private $code = '';
    private $externalId = '';
    private $amount = '';


    public function initJSON($data) {

        $respose =[];
        $array = json_decode($data, true);

        foreach ($array AS $data) {
            $temp = new self;
            $temp->code = $data['code'];
            $temp->externalId = $data['externalId'];
            $temp->amount = $data['amount'];
            $respose[] = $temp;
        }

        return $respose;

    }


    public function redemByOrder() {

        $preAuthorization = new PreAuthorization();
        $response = $preAuthorization->redeem($this->externalId);

        if(isset($response['messageStatus']) && $response['messageStatus'] == 'SUCCESS') {
            return true;
        }
        else {
            $data['kassierernummer'] = '99';
            $data['bonnummer'] = '99';
            $data['amount'] = $this->amount;
            $voucher = new Vouchers();
            $response = $voucher->redeemVoucher($this->code, $data);
            if(isset($response['messageStatus']) && $response['messageStatus'] == 'SUCCESS') {
                return true;
            }
        }

        return false;

    }


}
