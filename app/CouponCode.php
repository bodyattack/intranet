<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponCode extends Model
{
    protected $guarded=['id'];

    public $presentation_short = 'Gutscheincode';
    public $presentation_long = 'Gutscheincods';

    public function isActive()
    {
        $coupon = $this->coupon;

        if(empty($coupon->active)) return false;

        return true;

    }

    public function orders() {
        return $this->belongsToMany(TempOrder::class,'coupon_codes_orders', 'coupon_code_id', 'order_id')->withTimestamps();
    }


    public function coupon() {
        return $this->belongsTo('App\Coupon');
    }


}
