<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Presenters\Presentable;

class CustomerSetting extends Model
{

    protected $guarded=['id'];

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
