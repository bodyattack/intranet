<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seal extends Model
{
    protected $guarded=['id'];

    public $presentation_short = 'Siegel';
    public $presentation_long = 'Siegel';


    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
