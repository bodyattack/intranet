<?php

namespace App;

use App\Library\Comments;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    use Comments;
    
    public $presentation_short = 'Kunde';
    public $presentation_long = 'Kunden';


    protected $guarded = ['id'];


    public function settings()
    {
        return $this->hasOne('App\CustomerSetting')->withDefault();
    }


    public function notes()
    {
        return $this->hasMany('App\CustomerNote');
    }

    public function address()
    {
        return $this->hasMany('App\CustomerAddress');
    }

    public function hasAddress(): bool
    {
        return !empty($this->address->address) && !empty($this->address->zip) && !empty($this->address->city) && !empty($this->address->country_id);
    }


    public function invoices()
    {
        return $this->hasMany('App\CustomerInvoice');
    }

}
