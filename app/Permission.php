<?php

namespace App;

use App\Area;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    protected $guarded=['id'];

    public function areas() {
        return $this->belongsToMany(Area::class,'areas_permissions');
    }

    public function area() {
        return $this->belongsTo(Area::class);
    }

}
