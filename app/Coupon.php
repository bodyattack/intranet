<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $guarded=['id'];

    public $presentation_short = 'Gutschein';
    public $presentation_long = 'Gutscheine';


    public function isValid($einloesungen='')
    {

        if(empty($this->active)) return false;

        $date_now = new \DateTime();
        if(!empty($this->date_from)) {
            $date_from = new \DateTime($this->date_from);
            if ($date_now < $date_from) return false;
        }

        if(!empty($this->date_to)) {
            $date_to = new \DateTime($this->date_to);
            if ($date_now > $date_to) return false;
        }

        if(!empty($this->use_max)) {
            // Berechne Einlösungen aus DB, wenn Wert nicht übergeben.
            if($einloesungen==='') {
                $einloesungen = $this->uses();
            }
            if ($einloesungen >= $this->uses_max() ) return false;
        }

        return true;

    }

    public function uses() {
        return array_sum($this->codes()->withCount('orders')->pluck('orders_count')->toArray());
    }

    public function uses_max() {
        return $this->use_max * $this->multi;
    }

    public function codes() {
        return $this->hasMany('App\CouponCode');
    }

    public function unusedCodes() {
        return $this->hasMany('App\CouponCode')
            ->leftJoin('coupon_codes_orders', 'coupon_codes.id', '=', 'coupon_codes_orders.coupon_code_id')
            ->whereNull('coupon_codes_orders.coupon_code_id')
            ->select('coupon_codes.*'); // ensure that you only select columns from coupon_codes
    }

    public function userCreate()
    {
        return $this->belongsTo('App\User', 'created_by')->withDefault([
            'name' => 'Gelöschter Nutzer',
        ]);
    }

    public function userUpdate()
    {
        return $this->belongsTo('App\User', 'updated_by')->withDefault([
            'name' => 'Gelöschter Nutzer',
        ]);
    }


}
