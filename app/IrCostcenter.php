<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IrCostcenter extends Model
{
    protected $guarded=['id'];

    public $presentation_short = 'Kostenstelle';
    public $presentation_long = 'Kostenstellen';

    public function mandant()
    {
        return $this->belongsTo('App\Mandant');
    }


    public function scopeMandant (Builder $query, $name) {
        return $query->whereHas('mandant', function ($q) use ($name) {
            $q->where('name', $name);
        });
    }
}



