<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allergen extends Model
{
    protected $guarded=['id'];

    public $presentation_short = 'Allergen';
    public $presentation_long = 'Allergene';


    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
