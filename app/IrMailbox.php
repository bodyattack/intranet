<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IrMailbox extends Model
{
    protected $guarded=['id'];

    protected $table = "ir_mailboxes";

    public $presentation_short = 'Postfach';
    public $presentation_long = 'Postfächer';


    public function mandant()
    {
        return $this->belongsTo('App\Mandant');
    }

}
