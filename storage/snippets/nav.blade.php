<ul class="navbar-menu float-right">
    <li class="has-dropdown float-right">
        <a class="drop-toggle" href="#"><em class="ti ti-settings"></em> Einstellungen</a>
        <ul class="navbar-dropdown">
            <li class="has-dropdown">
                <a href="{{ route('aof.user.get') }}" class="drop-toggle">Control Nutzer </a>
                <ul class="navbar-dropdown">
                    <li><a href="{{ route('aof.register.get') }}">Control Nutzer erstellen</a></li>
                </ul>
            </li>
        </ul>
    </li>
</ul>