<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

// Login area
Route::namespace('App\Auth')
    ->group(function () {

    // Authentication Routes
    Route::get('/', 'LoginController@showLoginForm')->name('start');
    Route::get('login', 'LoginController@showLoginForm')->name('login');

    Route::post('login', 'LoginController@login')->name('login.submit');
    Route::post('logout', 'LoginController@logout')->name('logout');

    // Password Reset Routes
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');

    // Registration Routes
    # Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    # Route::post('register', 'RegisterController@register');

    // Email Verification Routes
    # Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
    # Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
    # Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
});


// User is logged in
Route::middleware('auth')->group(function () {
    Route::patch('/comment/{comment}', 'CommentController@update')->name('comment.update');
    Route::delete('/comment/{comment}', 'CommentController@destroy')->name('comment.destroy');
});

Route::middleware('auth')->name('ajax.')->group(function () {
    Route::post('/savedFilters/store', '\App\Filters\SavedFilterController@store')->name('savedFilters.store');
    Route::get('/savedFilters/store_success', '\App\Filters\SavedFilterController@store_success')->name('savedFilters.store_success');
    Route::patch('/savedFilters/update/{filter}', '\App\Filters\SavedFilterController@update')->name('savedFilters.update');
    Route::get('/savedFilters/update_success', '\App\Filters\SavedFilterController@update_success')->name('savedFilters.update_success');
    Route::delete('/savedFilters/destroy/{filter}', '\App\Filters\SavedFilterController@destroy')->name('savedFilters.destroy');
});


Route::namespace('App')
    ->middleware('auth')
    ->name('app.')
    ->group(function () {

    Route::get('/dashboard', 'HomeController@index')->name('dashboard')->middleware('auth');

    Route::get('/amazon', 'AmazonController@index')->name('amazon.index');

    Route::get('/users', 'UserController@index')->name('user.index');
    Route::get('/users/request', 'UserController@request')->name('user.request');
    Route::get('/user/password', 'UserController@showPasswortChangeForm')->name('user.showPasswortChangeForm');
    Route::post('/user/{user}/password', 'UserController@storePasswortChange')->name('user.storePasswortChange');

    Route::resources([
            '/seal' => 'SealController',
            '/allergen' => 'AllergenController',
            '/coupons/area/coupon' => 'CouponController',
            '/productgroup' => 'ProductgroupController',
            '/manufacturer' => 'ManufacturerController',
            '/temporder' => 'TempOrderController',
        ]
    );

    Route::get('/temporder/dashboard/search/', 'TempOrderController@dashboardSearch')->name('temporder.dashboardSearch');
    Route::get('/temporder/{temporder}/address/', 'TempOrderController@getOrderAddress')->name('temporder.getOrderAddress');
    Route::post('/temporder/{temporder}/address/', 'TempOrderController@setOrderAddress')->name('temporder.setOrderAddress');
    Route::get('/temporder/{temporder}/warenkorb/', 'TempOrderController@getOrderWarenkorb')->name('temporder.getOrderWarenkorb');
    Route::post('/temporder/{temporder}/warenkorb/', 'TempOrderController@setOrderWarenkorb')->name('temporder.setOrderWarenkorb');
    Route::get('/temporder/{temporder}/retransmit/', 'TempOrderController@orderRetransmit')->name('temporder.orderRetransmit');
    Route::get('/temporder/{temporder}/retransmitNew/', 'TempOrderController@orderRetransmitNew')->name('temporder.orderRetransmitNew');
    Route::get('/temporder/{temporder}/activetoggle/cron/', 'TempOrderController@toggleCron')->name('temporder.toggleCron');

    Route::get('/coupons/area/statistics/', 'CouponStatisticsController@index')->name('coupon.statistics');
    Route::get('/coupons/area/statistics/csv', 'CouponStatisticsController@exportCsv')->name('coupon.exportCsv');

    Route::get('/coupons/area/coupon/activetoggle/{coupon}', 'CouponController@toggleActive')->name('coupon.toggleActive');
    Route::get('/coupons/area/coupon/details/{coupon}', 'CouponController@details')->name('coupon.details');
    Route::get('/coupons/area/coupon/details/{coupon}/csv', 'CouponController@codesCsv')->name('coupon.codesCsv');

    Route::get('/reviews', 'ReviewsController@index')->name('reviews.index');
    Route::get('/reviews/show', 'ReviewsController@show')->name('reviews.show');
    Route::delete('/reviews/destroy', 'ReviewsController@destroy')->name('reviews.destroy');
    Route::post('/reviews', 'ReviewsController@store')->name('reviews.store');

    Route::get('/topCategories', 'TopCategoriesController@index')->name('topCategories.index');
    Route::get('/topCategories/show/{topCategorie}', 'TopCategoriesController@show')->name('topCategories.show');
    Route::get('/topCategories/create', 'TopCategoriesController@create')->name('topCategories.create');
    Route::post('/topCategories', 'TopCategoriesController@store')->name('topCategories.store');
    Route::patch('/topCategories/{topCategorie}', 'TopCategoriesController@update')->name('topCategories.update');
    Route::delete('/topCategories/destroy/{topCategorie}', 'TopCategoriesController@destroy')->name('topCategories.destroy');
    Route::get('/topCategories/activetoggle/{topCategorie}', 'TopCategoriesController@toggleActive')->name('topCategories.toggleActive');


    Route::get('/topCategoriesEng', 'TopCategoriesEngController@index')->name('topCategoriesEng.index');
    Route::get('/topCategoriesEng/show/{topCategorie}', 'TopCategoriesEngController@show')->name('topCategoriesEng.show');
    Route::get('/topCategoriesEng/create', 'TopCategoriesEngController@create')->name('topCategoriesEng.create');
    Route::post('/topCategoriesEng', 'TopCategoriesEngController@store')->name('topCategoriesEng.store');
    Route::patch('/topCategoriesEng/{topCategorie}', 'TopCategoriesEngController@update')->name('topCategoriesEng.update');
    Route::delete('/topCategoriesEng/destroy/{topCategorie}', 'TopCategoriesEngController@destroy')->name('topCategoriesEng.destroy');
    Route::get('/topCategoriesEng/activetoggle/{topCategorie}', 'TopCategoriesEngController@toggleActive')->name('topCategoriesEng.toggleActive');

    Route::get('/gurado', 'GuradoController@index')->name('gurado.index');
    Route::get('/gurado/show', 'GuradoController@show')->name('gurado.show');
    Route::post('/gurado', 'GuradoController@store')->name('gurado.store');

    Route::get('/seal/activetoggle/{seal}', 'SealController@toggleActive')->name('seal.toggleActive');
    Route::get('/allergen/activetoggle/{allergen}', 'AllergenController@toggleActive')->name('allergen.toggleActive');
    Route::get('/productgroup/activetoggle/{productgroup}', 'ProductgroupController@toggleActive')->name('productgroup.toggleActive');
    Route::get('/manufacturer/activetoggle/{manufacturer}', 'ManufacturerController@toggleActive')->name('manufacturer.toggleActive');
    Route::post('/manufacturer/updateOrder', 'ManufacturerController@updateOrder')->name('manufacturer.updateOrder');

        // InvoiceRelease
    Route::namespace('InvoiceRelease')
        ->group(function () {

        Route::resource('/invoicerelease/costcenter', 'CostcenterController', ['as' => 'invoicerelease']);
        Route::get('/invoicerelease/costcenter/activetoggle/{costcenter}', 'CostcenterController@toggleActive')->name('invoicerelease.costcenter.toggleActive');

        Route::resource('/invoicerelease/bills', 'BillController', ['as' => 'invoicerelease']);
        Route::resource('/invoicerelease/biller', 'BillerController', ['as' => 'invoicerelease']);
        Route::get('/invoicerelease/ajax/', 'BillerController@showAjax')->name('invoicerelease.ajax');
        Route::get('/invoicerelease/ajax/{biller}', 'BillerController@showAjax')->name('invoicerelease.ajax.biller');

        Route::get('/invoicerelease/csv/bills', 'BillController@csv')->name('invoicerelease.bills.csv');
        Route::post('/invoicerelease/biller/csv', 'BillerController@csv')->name('invoicerelease.biller.csv');
        Route::get('/invoicerelease/bills/pdf/{filename}', 'BillItemController@loadFile')->name('invoicerelease.bills.pdf');

        Route::get('/invoicerelease/transactions/calendar', 'TransactionsController@calendar')->name('invoicerelease.transactions.calendar');
        Route::get('/invoicerelease/transactions/approval', 'TransactionsController@approval')->name('invoicerelease.transactions.approval');
        Route::get('/invoicerelease/transactions/unsettled', 'TransactionsController@unsettled')->name('invoicerelease.transactions.unsettled');
        Route::get('/invoicerelease/transactions/due_yesterday', 'TransactionsController@due_yesterday')->name('invoicerelease.transactions.due_yesterday');
        Route::post('/invoicerelease/transactions/status', 'TransactionsController@setStatus')->name('invoicerelease.transactions.status');
        Route::post('/invoicerelease/transactions/bills', 'TransactionsController@getBillsByDate')->name('invoicerelease.transactions.getBillsByDate');

        Route::post('/invoicerelease/upload/bills/store', 'BillItemController@upload')->name('invoicerelease.bills.upload');
        Route::get('/invoicerelease/upload/bills/remove', 'BillItemController@removeFiles')->name('invoicerelease.bills.remove-files');

        Route::get('/info/biller/subjects', 'BillerController@match_subjects')->name('invoicerelease.biller.match_subjects');

        Route::get('/invoicerelease/payone/bills', 'PayonePaymentController@csv')->name('invoicerelease.payone.csv');
        Route::get('/invoicerelease/payone/bills_gurado', 'PayonePaymentController@csv_gurado')->name('invoicerelease.payone.csv_gurado');
        Route::get('/invoicerelease/payone', 'PayonePaymentController@index')->name('invoicerelease.payone');
        Route::get('/invoicerelease/payone/import', 'PayonePaymentController@importShow')->name('invoicerelease.payone.importShow');
        Route::post('/invoicerelease/payone/import', 'PayonePaymentController@import')->name('invoicerelease.payone.import');

 });

    // Customers
    Route::namespace('Customer')
        ->group(function () {
            Route::resource('/customer', 'CustomerController');
            Route::get('/customer/direct/{customer}', 'CustomerController@direct_edit')->name('customer.direct.edit');
            Route::get('/customer/direct/{customer}/address', 'CustomerController@direct_edit')->name('customer.direct.address');
            Route::get('/customer/activetoggle/{customer}', 'CustomerController@toggleActive')->name('customer.toggleActive');

            // address
            Route::get('/customer/{customer}/address', 'AddressController@index')->name('customer.address.index');
            Route::get('/customer/{customer}/address/create', 'AddressController@edit')->name('customer.address.create');
            Route::get('/customer/{customer}/address/{address}', 'AddressController@edit')->name('customer.address.edit');
            Route::patch('/customer/{customer}/address/{address}', 'AddressController@update')->name('customer.address.update');
            Route::post('/customer/{customer}/address', 'AddressController@store')->name('customer.address.store');
            Route::delete('/customer/{customer}/address/{address}', 'AddressController@destroy')->name('customer.address.destroy');
            Route::get('/customer/{customer}/address/{address}/togglePrimary', 'AddressController@togglePrimary')->name('customer.address.togglePrimary');

            // settings
            Route::get('/customer/{customer}/settings/create', 'SettingsController@create')->name('customer.settings.create');
            Route::post('/customer/{customer}/settings', 'SettingsController@store')->name('customer.settings.store');
            Route::get('/customer/{customer}/settings/edit', 'SettingsController@edit')->name('customer.settings.edit');
            Route::patch('/customer/{customer}/settings', 'SettingsController@update')->name('customer.settings.update');
            Route::delete('/customer/{customer}/settings', 'SettingsController@destroy')->name('customer.settings.destroy');

            // notes
            Route::get('/customer/{customer}/comment', 'CommentController@index')->name('customer.comment.index');
            Route::post('/customer/{customer}/comment', 'CommentController@store')->name('customer.comment.store');
            Route::delete('/customer/{customer}/comment/{comment}', 'CommentController@destroy')->name('customer.comment.destroy');

            // invoices
            #Route::get('/customer/{customer}/invoices', 'InvoiceController@index')->name('customer.invoices.index');

            // Orders
            #Route::get('/customer/{customer}/orders', 'OrderController@index')->name('customer.invoices.index');

        });


    // Customers
        /*
    Route::namespace('Product')
        ->group(function () {
            Route::resource('/product', 'ProductController');
            Route::get('/product/direct/{product}', 'ProductController@direct_edit')->name('product.direct.edit');
            Route::get('/product/activetoggle/{product}', 'ProductController@toggleActive')->name('product.toggleActive');

            // settings
            Route::get('/product/{product}/settings/create', 'SettingsController@create')->name('product.settings.create');
            Route::post('/product/{product}/settings', 'SettingsController@store')->name('product.settings.store');
            Route::get('/product/{product}/settings/edit', 'SettingsController@edit')->name('product.settings.edit');
            Route::patch('/product/{product}/settings', 'SettingsController@update')->name('product.settings.update');
            Route::delete('/product/{product}/settings', 'SettingsController@destroy')->name('product.settings.destroy');

            // notes
            Route::get('/product/{product}/comment', 'CommentController@index')->name('product.comment.index');
            Route::post('/product/{product}/comment', 'CommentController@store')->name('product.comment.store');
            Route::delete('/product/{product}/comment/{comment}', 'CommentController@destroy')->name('product.comment.destroy');

            // images
            Route::get('/product/{product}/images', 'ImageController@index')->name('product.images.index');

            // seals
            Route::get('/product/{product}/seals', 'SealsController@index')->name('product.seals.index');

            // allergens
            Route::get('/product/{product}/allergens', 'AllergensController@index')->name('product.allergens.index');

        });
        */

 });



