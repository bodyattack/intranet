<?php

/*
|--------------------------------------------------------------------------
| Cron & Listener Routes
|--------------------------------------------------------------------------
*/

// Login area
Route::namespace('Cron')
    ->name('cron.')
    ->group(function () {

        #Route::get('/csv', 'CronController@loadCSV')->name('cron.csv');

        Route::get('/invoicerelease/mails', 'InvoiceRelease\BillController@loadFromMailbox');

        Route::get('/orders/push', 'TempOrdersController@pushOrders');
        Route::get('/orders/getDetails', 'TempOrdersController@getOrderDetails');
        Route::get('/orders/resetCron', 'TempOrdersController@resetCronStatus');

    });
