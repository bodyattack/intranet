<?php

/*
|--------------------------------------------------------------------------
| Control Routes
|--------------------------------------------------------------------------
*/


// Login area
Route::namespace('Control\Auth')
    ->name('control.')
    ->group(function () {

    Route::get('/', 'LoginBackendUserController@showLoginForm')->name('login.index');
    Route::post('/login', 'LoginBackendUserController@login')->name('login.submit');
    Route::post('/logout', 'LoginBackendUserController@logout')->name('logout.submit');

    Route::get('/password/reset', 'ForgotBackendPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email', 'ForgotBackendPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset/{token}', 'ResetBackendPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset', 'ResetBackendPasswordController@reset')->name('password.update');

});


// User is logged in
Route::middleware(['auth:backend_user'])
    ->namespace('Control')
    ->name('control.')
    ->group(function () {

    Route::get('/dashboard', 'HomeController@index')->name('dashboard');


    Route::get('/admin', 'BackenduserController@showUsers')->name('backenduser.index');
    Route::delete('/admin/{user}/delete', 'BackenduserController@deleteUser')->name('backenduser.delete');
    Route::get('/admin/passwort', 'BackenduserController@showPasswortChangeForm')->name('backenduser.showPasswortChangeForm');
    Route::post('/admin/{user}/passwort', 'BackenduserController@storePasswortChange')->name('backenduser.storePasswortChange');
    Route::post('admin/register', 'Auth\RegisterBackendUserController@registerFromBackend')->name('backenduser.store');
    Route::get('admin/register', 'Auth\RegisterBackendUserController@showRegistrationForm')->name('backenduser.create');

    Route::get('/users', 'UserController@index')->name('user.index');
    Route::get('/users/create', 'UserController@create')->name('user.create');
    Route::post('/users/csv', 'UserController@csv')->name('user.csv');
    Route::post('/users', 'UserController@store')->name('user.store');
    Route::get('/users/{user}', 'UserController@show')->name('user.show');
    Route::patch('/users/{user}', 'UserController@update')->name('user.update');
    Route::delete('/users/{user}/delete', 'UserController@delete')->name('user.delete');
    Route::get('/users/activetoggle/{user}', 'UserController@toggleIsActive')->name('user.isActiveToggle');

    // Login as User
    Route::get( 'users/switch/start/{id}', 'UserController@user_switch_start')->name('user.switch.start');


    Route::get('/users/{user}/departments/assing', 'UserController@departmentAssingShow')->name('user.department.assing.show');
    Route::get('/users/{user}/departments/{department}/assing', 'UserController@departmentAssingStore')->name('user.department.assing');
    Route::get('/users/{user}/areas/assing', 'UserController@areaAssingShow')->name('user.area.assing.show');
    Route::get('/users/{user}/areas/{area}/assing', 'UserController@areaAssingStore')->name('user.area.assing');

    Route::get('/users/{user}/areas/{area}', 'UserController@permissionAssingShow')->name('user.permission.assing.show');
    Route::get('/users/{user}/permissions/{permission}/assing', 'UserController@permissionAssingStore')->name('user.permission.assing');

    Route::get('/areas', 'AreaController@index')->name('areas.index');
    Route::get('/areas/create', 'AreaController@create')->name('areas.create');
    Route::get('/areas/{area}', 'AreaController@show')->name('areas.show');
    Route::post('/areas', 'AreaController@store')->name('areas.store');
    Route::patch('/areas/{area}', 'AreaController@update')->name('areas.update');
    Route::delete('/areas/{area}/delete', 'AreaController@delete')->name('areas.delete');

    Route::get('/areas/{area}/assing', 'AreaController@showToAssing')->name('areas.assing');
    Route::get('/areas/{area}/permissions/{permission}/assing', 'AreaController@permissionAssing')->name('areas.permissionAssing');

    Route::get('/permissions', 'PermissionsController@index')->name('permissions.index');
    Route::get('/permissions/create', 'PermissionsController@create')->name('permissions.create');
    Route::get('/permissions/{permission}', 'PermissionsController@show')->name('permissions.show');
    Route::post('/permissions', 'PermissionsController@store')->name('permissions.store');
    Route::patch('/permissions/{permission}', 'PermissionsController@update')->name('permissions.update');
    Route::delete('/permissions/{permission}/delete', 'PermissionsController@delete')->name('permissions.delete');

    Route::resources([
            '/department' => 'DepartmentController',
            '/level' => 'LevelController',
            '/mandant' => 'MandantController',
            '/apiaccount' => 'ApiAccountController',
            '/mailbox' => 'MailboxController',
            '/country' => 'CountryController',
            '/seal' => 'SealController',
            '/allergen' => 'AllergenController',
        ]
    );
    Route::get('/country/activetoggle/{country}', 'CountryController@toggleActive')->name('country.toggleActive');
    Route::get('/seal/activetoggle/{seal}', 'SealController@toggleActive')->name('seal.toggleActive');
    Route::get('/allergen/activetoggle/{allergen}', 'AllergenController@toggleActive')->name('allergen.toggleActive');
    Route::get('/mailbox/activetoggle/{mailbox}', 'MailboxController@toggleActive')->name('mailbox.toggleActive');

    Route::get('/invoicerelease', 'InvoiceRelease\HomeController@index')->name('invoicerelease.index');

    // CSV und sonstige nachträgliche Verarbeitung / Importe
    Route::post('/invoicerelease/upload/biller', 'InvoiceRelease\HomeController@uploadBiller')->name('invoicerelease.upload_biller');
    #Route::get('/temp/create/coupons', 'InvoiceRelease\HomeController@createCoupons');
        
});

// Back to Admin
Route::namespace('Control')
    ->middleware('auth')
    ->name('control.')
    ->group(function () {
        // Back to Admin
        Route::get( 'users/switch/stop', 'UserController@user_switch_stop' )->name('user.switch.stop');
});
