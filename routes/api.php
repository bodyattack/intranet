<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

*/


// v1
Route::prefix('v1')
    ->namespace('Api')
    ->group(function () {
        Route::get('/', 'BaseController@default');
    }
);

Route::middleware(['auth.credentials.data'])
    ->prefix('v1')
    ->namespace('Api\v1')
    ->name('api.v1.')
    ->group(function () {

        # Nur POST / PATCH Routen, da auth im jason Content übergeben werden muss!

        Route::post('temp/orders', 'TempOrdersController@store');
        Route::post('temp/orders/{id}/destroy', 'TempOrdersController@destroy');

        Route::post('gurado', 'GuradoCodeController@create');

        /*
        Route::post('customers/{customer_id}/orders', 'CustomerController@orders');
        Route::post('customers/{customer_id}/orders/{order_id}', 'CustomerController@order');

        Route::post('customers/{customer_id}/addresses', 'AddressController@store');
        # Route::put('customers/{customer_id}/addresses', 'AddressController@update'); // TODO
        Route::post('customers/{customer_id}/addresses/{address}/destroy', 'AddressController@destroy');

        Route::post('articles/{erp_number}', 'ArticleController@show');
        Route::post('customer', 'CustomerController@show');
        */

        }
    );

Route::middleware(['auth.token.get'])
    ->prefix('v1')
    ->namespace('Api\v1')
    ->name('api.v1.')
    ->group(function () {

        # Nur GET Routen, da token im Querry übergeben werden muss!

        Route::get('countries', 'CountryController@index');
        Route::get('coupons/{coupon}', 'CouponCodeController@show');

        Route::get('gurado/{gurado}', 'GuradoCodeController@show');
        Route::get('gurado/{gurado}/authorize', 'GuradoCodeController@authorization');
        Route::get('gurado/{external}/release', 'GuradoCodeController@release');

        /*
        Route::get('customers/verify', 'CustomerController@verify');
        Route::get('customers/{customer_id}/addresses', 'AddressController@index');
        Route::get('customers/{customer_id}/addresses/{address}', 'AddressController@show');
        */

        }
    );

/*

API GET TOKEN
$salt = '$gsu&56';
$date = date("Ym");
$md5 = md5($date . $salt);

*/
