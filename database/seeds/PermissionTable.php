<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class PermissionTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permission = new Permission();
        $permission->slug = 'rechnung-weiterleiten';
        $permission->name = 'Rechnung weiterleiten';
        $permission->description = 'Hier könnte eiine Beschreibung stehen';
        $permission->area_id = '1';
        $permission->save();

        $permission = new Permission();
        $permission->slug = 'rechnung-erstellen';
        $permission->name = 'Rechnung erstellen';
        $permission->area_id = '1';
        $permission->save();

        $permission = new Permission();
        $permission->slug = 'rechnung-loeschen';
        $permission->name = 'Rechnung löschen';
        $permission->area_id = '1';
        $permission->save();

        $permission = new Permission();
        $permission->slug = 'rechnungssteller-erstellen';
        $permission->name = 'Rechnungssteller erstellen';
        $permission->area_id = '1';
        $permission->save();

        $permission = new Permission();
        $permission->slug = 'kostenstellen-anlegen';
        $permission->name = 'Kostenstellen anlegen';
        $permission->area_id = '1';
        $permission->save();

        $permission = new Permission();
        $permission->slug = 'rechnungskontrolle-zahlungsverkehr';
        $permission->name = 'Zugang Rechnungskontrolle Zahlungsverkehr';
        $permission->area_id = '1';
        $permission->save();

        $permission = new Permission();
        $permission->slug = 'backendnutzer-bearbeiten';
        $permission->name = 'Backendnutzer bearbeiten';
        $permission->area_id = '0';
        $permission->save();

    }
}
