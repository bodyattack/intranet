<?php

use App\Allergen;
use Illuminate\Database\Seeder;

class AllergensTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allergens = array(
            array('name_en' => 'Weizen' ,'name' => 'Weizen', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Milch' ,'name' => 'Milch', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Nuss' ,'name' => 'Nuss', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            );

        $i=0;
        foreach ($allergens AS $allergen) {

            $icon = 'icon-'.++$i.'_off.png';

            $oAllergen = new Allergen();
            $oAllergen->name = $allergen['name'];
            $oAllergen->name_en = $allergen['name_en'];
            $oAllergen->icon = $icon;
            $oAllergen->filter_link = $allergen['filter_link'];
            $oAllergen->filter_link_en = $allergen['filter_link_en'];
            $oAllergen->active = $allergen['active'];
            $oAllergen->save();
        }
        

    }

}
