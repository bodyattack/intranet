<?php

use App\Mandant;
use Illuminate\Database\Seeder;

class MandantTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $mandant = new Mandant();
        $mandant->name = 'Body Attack Sports Nutrition';
        $mandant->short_name = 'BASN';
        $mandant->datev_email = '8a26447d-7795-4d3e-a082-f8c39499e977@uploadmail.datev.de';
        $mandant->save();

        $mandant = new Mandant();
        $mandant->name = 'My Supps';
        $mandant->short_name = 'MYS';
        $mandant->datev_email = '2fe022e3-c3f6-45fb-9fdf-33ec875d5d51@uploadmail.datev.de';
        $mandant->save();

        $mandant = new Mandant();
        $mandant->name = 'Body Attack Franchise';
        $mandant->short_name = 'BAF';
        $mandant->datev_email = 'd3588ce4-60c6-4cfb-ae3a-583efa1e5cf4@uploadmail.datev.de';
        $mandant->save();

    }
}
