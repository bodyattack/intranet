<?php

use App\Department;
use App\IrBiller;
use Illuminate\Database\Seeder;

class BillerTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $ir_billers = array(
            array('id' => '1','name' => '1&1','due_days' => '0','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'rechnung@1und1.de','match_subject' => '1und1','costcenter_id' => '1','created_at' => '2020-08-11 14:35:41','updated_at' => '2020-08-11 14:35:41'),
            array('id' => '2','name' => 'Immobilien Scout 24','due_days' => '7','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'service@immobilienscout24.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-11 15:20:28','updated_at' => '2020-08-11 15:20:28'),
            array('id' => '3','name' => 'inline Kurierdienst GmbH','due_days' => '7','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@inline-kurier.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 08:23:53','updated_at' => '2020-08-12 08:23:53'),
            array('id' => '4','name' => 'RWR REMONDIS GmbH & Co. KG','due_days' => '7','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@r-w-r.de','match_subject' => NULL,'costcenter_id' => '63','created_at' => '2020-08-12 08:35:03','updated_at' => '2020-08-12 08:35:03'),
            array('id' => '5','name' => 'FSL Ladenbau GmbH','due_days' => '8','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@fsl-ladenbau.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 09:12:46','updated_at' => '2020-08-12 09:20:47'),
            array('id' => '6','name' => 'CONCAS GmbH','due_days' => '14','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@concas.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 09:20:22','updated_at' => '2020-08-12 09:20:22'),
            array('id' => '7','name' => 'Werbegemeinschaft Gänsemarkt Passage e.','due_days' => '14','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@gaensemarkt-passage.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 09:21:22','updated_at' => '2020-08-12 09:21:22'),
            array('id' => '8','name' => 'CLONIE Lichtwerbung','due_days' => '14','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'anfrage@clonie.de','match_subject' => NULL,'costcenter_id' => '1','created_at' => '2020-08-12 09:28:20','updated_at' => '2020-08-12 09:28:20'),
            array('id' => '9','name' => 'Gebäudereinigung Wolff GmbH','due_days' => '13','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@wolffgeb.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 09:44:56','updated_at' => '2020-08-12 09:44:56'),
            array('id' => '10','name' => 'Gero Hermanns Steuerberater','due_days' => '14','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'service@gerohermanns.de','match_subject' => NULL,'costcenter_id' => '1','created_at' => '2020-08-12 09:55:34','updated_at' => '2020-08-12 09:55:34'),
            array('id' => '11','name' => 'VKF Renzel GmbH','due_days' => '30','discount_days' => '10','discount' => '2.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@vkf-renzel.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 09:59:57','updated_at' => '2020-08-12 09:59:57'),
            array('id' => '12','name' => 'Flyeralarm GmbH','due_days' => '7','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@flyeralarm.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 10:04:42','updated_at' => '2020-08-12 10:04:42'),
            array('id' => '13','name' => 'Concardis GmbH','due_days' => '7','discount_days' => '0','discount' => '0.00','payment' => 'Einzug','currency' => 'EUR','match_email' => 'service@concardis.com','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 10:09:22','updated_at' => '2020-08-12 10:09:22'),
            array('id' => '14','name' => 'REA Card GmbH','due_days' => '4','discount_days' => '0','discount' => '0.00','payment' => 'Einzug','currency' => 'EUR','match_email' => 'Kundenbetreuung@rea-card.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 10:12:46','updated_at' => '2020-08-12 10:12:46'),
            array('id' => '15','name' => 'Oswald F. Groth','due_days' => '9','discount_days' => '0','discount' => '0.00','payment' => 'Einzug','currency' => 'EUR','match_email' => 'office@groth-versicherungen.de','match_subject' => NULL,'costcenter_id' => '1','created_at' => '2020-08-12 11:19:03','updated_at' => '2020-08-12 11:19:03'),
            array('id' => '16','name' => 'KAYA Glas- & Gebäudereinigung','due_days' => '14','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@kaya-reinigung.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 11:23:46','updated_at' => '2020-08-12 11:23:46'),
            array('id' => '17','name' => 'Arena Hostel Hamburg','due_days' => '7','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'kontakt@arenahostel.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 13:54:40','updated_at' => '2020-08-12 13:54:40'),
            array('id' => '18','name' => 'IG Franken Gebäudereinigung GmbH','due_days' => '10','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@franken.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 14:12:33','updated_at' => '2020-08-12 14:12:33'),
            array('id' => '19','name' => 'DOKOM Gesellschaft f. Telekommunikation','due_days' => '8','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@dokom21.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-12 14:38:57','updated_at' => '2020-08-12 14:38:57'),
            array('id' => '20','name' => 'E.ON Energie Deutschland GmbH','due_days' => '14','discount_days' => '0','discount' => '0.00','payment' => 'Einzug','currency' => 'EUR','match_email' => 'gewerbekunden@eon.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-18 06:56:44','updated_at' => '2020-08-18 06:56:44'),
            array('id' => '21','name' => 'Friedrich Hofmann Betriebsgesellschaft','due_days' => '14','discount_days' => '0','discount' => '0.00','payment' => 'Einzug','currency' => 'EUR','match_email' => 'gewerbekunden@hofmann-denkt.com','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-18 06:59:25','updated_at' => '2020-08-18 06:59:25'),
            array('id' => '22','name' => 'Gesundheitszeugnis.de','due_days' => '7','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@gesundheitszeugnis-hamburg.com','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-18 07:01:33','updated_at' => '2020-08-18 07:01:33'),
            array('id' => '23','name' => 'Heinz Glas & Gebäudereinigung','due_days' => '7','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'info@heinz-gebaeudereinigung.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-18 07:03:51','updated_at' => '2020-08-18 07:03:51'),
            array('id' => '24','name' => 'Elbtal-Dienstleistungen','due_days' => '14','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'elbtaldienstleistung@gmx.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-18 07:07:01','updated_at' => '2020-08-18 07:07:01'),
            array('id' => '25','name' => 'Berolina Schriftbild Nord GmbH','due_days' => '8','discount_days' => '0','discount' => '0.00','payment' => 'Rechnung','currency' => 'EUR','match_email' => 'hamburg@berolina.de','match_subject' => NULL,'costcenter_id' => '44','created_at' => '2020-08-18 07:09:25','updated_at' => '2020-08-18 07:09:25')
        );

        $biller = new IrBiller();
        $biller->name = 'Anona';
        $biller->costcenter_id = '2';
        $biller->match_email = 'rechnung@anona.de';
        $biller->match_subject = 'Anona';
        $biller->save();
        $biller->distributor()->attach(App\Department::inRandomOrder()->first());


        if(env('APP_DEBUG')=='true') {

            $biller = new IrBiller();
            $biller->name = 'Hetzner';
            $biller->costcenter_id = '1';
            $biller->match_email = 'rechnung@hetzner.de';
            $biller->match_subject = 'Hetzner';
            $biller->save();
            $biller->distributor()->attach(App\Department::inRandomOrder()->first());

            foreach ($ir_billers AS $ir_biller) {
                $biller = new IrBiller();
                $biller->name = $ir_biller['name'];
                $biller->due_days = $ir_biller['due_days'];
                $biller->discount_days = $ir_biller['discount_days'];
                $biller->discount = $ir_biller['discount'];
                $biller->payment = $ir_biller['payment'];
                $biller->currency = $ir_biller['currency'];
                $biller->match_email = $ir_biller['match_email'];
                $biller->match_subject = $ir_biller['match_subject'];
                $biller->costcenter_id = $ir_biller['costcenter_id'];
                $biller->save();
            }

        }

        else {

            foreach ($ir_billers AS $ir_biller) {
                $biller = new IrBiller();
                $biller->name = $ir_biller['name'];
                $biller->due_days = $ir_biller['due_days'];
                $biller->discount_days = $ir_biller['discount_days'];
                $biller->discount = $ir_biller['discount'];
                $biller->payment = $ir_biller['payment'];
                $biller->currency = $ir_biller['currency'];
                $biller->match_email = $ir_biller['match_email'];
                $biller->match_subject = $ir_biller['match_subject'];
                $biller->costcenter_id = $ir_biller['costcenter_id'];
                $biller->save();
            }

        }

    }

}
