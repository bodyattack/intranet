<?php

use App\Seal;
use Illuminate\Database\Seeder;

class SealsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seals = array(
            array('name_en' => 'Made in Germany' ,'name' => 'Made in Germany', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'CarnoSyn®' ,'name' => 'CarnoSyn®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Optipep®' ,'name' => 'Optipep®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Creapure®' ,'name' => 'Creapure®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'CarnipureTM' ,'name' => 'CarnipureTM', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'CFM' ,'name' => 'CFM', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'DLG Gold quality seal' ,'name' => 'DLG Gold', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'DLG Silver quality seal' ,'name' => 'DLG Silber', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Organic quality seal' ,'name' => 'BIO-Siegel', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'DE-ECO quality seal' ,'name' => 'DE-ÖKO', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Aspartame-free' ,'name' => 'Aspartamfrei', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Without pork' ,'name' => 'Ohne Schwein', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Without Sweeteners' ,'name' => 'Ohne Süßstoff', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => '2:1:1 BCAA' ,'name' => '2:1:1 BCAA', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Free Form BCAA' ,'name' => 'Free Form BCAA', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'ClarinolTM' ,'name' => 'ClarinolTM', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Maxi-Caps' ,'name' => 'Maxi-Caps', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'BIOPERINE®' ,'name' => 'BIOPERINE®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'VitaCholine®' ,'name' => 'VitaCholine®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Caffeine free' ,'name' => 'Koffeinfrei', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Oxab' ,'name' => 'Oxab', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'GMO free' ,'name' => 'GMO free', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'DRcaps®' ,'name' => 'DRcaps®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '0'),
            array('name_en' => 'DRcaps®' ,'name' => 'DRcaps®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '0'),
            array('name_en' => 'DRcaps®' ,'name' => 'DRcaps®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '0'),
            array('name_en' => 'DRcaps®' ,'name' => 'DRcaps®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'AGMAPURE®' ,'name' => 'AGMAPURE®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Vcaps®' ,'name' => 'Vcaps®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Licaps®' ,'name' => 'Licaps®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Suntheanine®' ,'name' => 'Suntheanine®', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Vegetarian' ,'name' => 'Vegetarisch', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Without preservatives' ,'name' => 'Ohne Konservierungsstoffe', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Without aroma' ,'name' => 'Ohne Aromen', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Sugar-free' ,'name' => 'Zuckerfrei', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Low in sugar' ,'name' => 'Zuckerarm', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Fat-free' ,'name' => 'Fettfrei', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Low in fat' ,'name' => 'Fettarm', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Rich in protein' ,'name' => 'Proteinreich', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Rich in fiber' ,'name' => 'Ballaststoff-reich', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Soya-free' ,'name' => 'Sojafrei', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Halal' ,'name' => 'Halal', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '0'),
            array('name_en' => 'Halal' ,'name' => 'Halal', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'DMA pure' ,'name' => 'DMA pure', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Studies' ,'name' => 'Durch Studien belegt', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'ATP TOR' ,'name' => 'ATP TOR', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'CREAZ' ,'name' => 'CREAZ', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'CREAZ' ,'name' => 'CREAZ', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '0'),
            array('name_en' => 'Low Carb' ,'name' => 'Low Carb', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Citrusyn' ,'name' => 'Citrusyn', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => '#femline' ,'name' => '#femline', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Without dyes' ,'name' => 'Ohne Farbstoffe', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'ARGIZ' ,'name' => 'ARGIZ', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'ATPure' ,'name' => 'ATPure', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Glycopump' ,'name' => 'Glycopump', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Without palm oil' ,'name' => 'Ohne Palmöl', 'icon' => '', 'filter_link' => '', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Vegan' ,'name' => 'Glycopump', 'icon' => 'icon-vegan_off.svg', 'filter_link' => 'vegane-produkte.html', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Cologne list' ,'name' => 'Kölner Liste', 'icon' => 'icon-koelnerliste_off.svg', 'filter_link' => 'koelner-liste.html', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Glutenfree' ,'name' => 'Glutenfrei', 'icon' => 'icon-glutenfrei_off.svg', 'filter_link' => 'glutenfreie-produkte.html', 'filter_link_en' => '', 'active' => '1'),
            array('name_en' => 'Lactose-free' ,'name' => 'Laktosefrei', 'icon' => 'icon-laktosefrei_off.svg', 'filter_link' => 'laktosefreie-produkte.html', 'filter_link_en' => '', 'active' => '1'),
        );

        $i=0;
        foreach ($seals AS $seal) {

            $icon = 'icon-'.++$i.'_off.png';

            $oSeal = new Seal();
            $oSeal->name = $seal['name'];
            $oSeal->name_en = $seal['name_en'];
            $oSeal->icon = $icon;
            $oSeal->filter_link = $seal['filter_link'];
            $oSeal->filter_link_en = $seal['filter_link_en'];
            $oSeal->active = $seal['active'];
            $oSeal->save();
        }
        

    }

}
