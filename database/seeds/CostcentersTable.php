<?php

use Illuminate\Database\Seeder;
use App\IrCostcenter;

class CostcentersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $values = ['9999','13000','14000','12000','11000','19000','14020','15000','15010','11020','9997','14020','14021','14022','14013','12020','12021','12022','12023','12024','12010','12030','13050','13051','13052','13053','15020','11011','11030','11031','11032','11033','11034','11035','11036','11037','11038','13030','19000','13040','13041','13042','13043','13010','9998','13020','11010','9990','10000','14011','14012','14010','21110','21102','21101','21105','21108','21104','21000','21100','21109','21103','21106','21112','21111','21090','50000','13012','13013','13014','13020','13021','13022','13023','13044','13045','19001','19002','19020','19003','19004','17000','17001','17002','15021','15022','15023','15024','15025','15026','15027','15028','15029','15030','15031','14001','15011','60000','71102','71103','71105','71106','71108','71109','71111','71112','71113','71114','71115','71116','71117','71118','71119','71120','71121','71122','71123','40000','80000','12040','14014','19005','13046','13047','12048','80001','12032','12031','0'];
        $names = ['SONSTIGES','Budde','Dorit','Sellmer','Tore','Ackmann','Andrea','Krüger','AB','Aussendienst','Automaten','BuHa','BuHa Inkasso','Buha Gebühren','Einkauf','Etiketten','Etiketten Schachteln und Displays','Etiketten Produktfolien','Etiketten Labore und Behörden','Etiketten Diverses','Export','Grafik','IT','IT-Internetkosten','IT-Hard- und Software','IT-Wartung und Leasing','Lager','LEH','Marketing','Marketing Mailing B2B','Marketing Werbung B2B','Marketing Werbemittel B2B','Marketing Mailing B2C','Marketing Werbung B2C','Marketing Werbemittel B2C','MaFo','Marketing Sonstiges','Ökos','Personal','PR','Online-Werbung','PR Social Media','PR E-Mailmarketing','Premium-Stores','Sicherheit','PS Marketing','Vertrieb','Verwaltung','Geschäftsführung','EK-Dienstleistungen','EK-Fracht','EK-Kostenstelle','PS Alstertal','PS Bergedorf','PS Billstedt','PS Bochum','PS Darmstadt','PS Düsseldorf','PS Eidelstedt','PS Eppendorf','PS Frankfurt Skyline','PS Harburg','PS Köln','PS Leipzig','PS Mageburg','PS Norderstedt','FIBO','GL Hein','GL Ebeling','GL Neumann','PS-Marketing','PS-Einarbeitung','PS-Recruiting','PS-Schulungen','PR-Maßnahmen','PR Tools-Abos','Azubis','Schulungen Mitarbeiter','Schulungen Geschäftsführung','Recruiting','Marketing','Sponsoring allgemein','Sponsoring (fest)',' Sponsoring (var)','Lager - Verpackungen','Lager - Zeitarbeit','Lager - Sicherheit','Lager - Speditionen','Lager - Versanddienstleister','Lager - Bursped','Lager - Auslagerungen','Lager - Investitionen','Lager -  Zweitzustellungen','Lager - Entsorgung','Lager-Iran','Rechtschutz','Sensorik','Rückrufe Produkte','Pers. B-Marzahn','Pers. HH-Meile','Pers. Frankfurt II','Pers. Nürnberg','Pers. Schnelsen','Pers. B-Gesund','Pers. Erlangen','Pers. HH-Gänse','Pers. Hannover','Pers. Karlsruhe','Pers. Dortmund','Pers. Kiel','Pers. Neumünster','Pers. Dresden','Pers. Ulm','Pers. Bielefeld','Pers. Köln II','Pers. Hannover EAG','Pers. Lübeck','Jabuvit','LVS','Markenrechte','Produktentwicklung','Betriebsveranstaltungen','Affiliate Marketing','Remarketing','Amazon Ads','ERP SQL-Umstellung','Freelancer','Übersetzungen','OHNE'];

        foreach ( $names as $key => $name) {
            $mailbox = new IrCostcenter();
            $mailbox->name = $name;
            $mailbox->value = $values[$key];
            $mailbox->mandant_id = 1;
            $mailbox->save();

        }

    }
}
