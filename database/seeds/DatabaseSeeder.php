<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BackendUserTable::class);
        $this->call(PermissionTable::class);
        $this->call(AreaTable::class);
        $this->call(UserTable::class);
        $this->call(DepartmentTable::class);
        $this->call(MandantTable::class);
        $this->call(MailboxesTable::class);
        $this->call(CostcentersTable::class);
        $this->call(BillerTable::class);
        $this->call(BillTable::class);
        $this->call(LevelTable::class);
        $this->call(CustomerTable::class);
        $this->call(SealsTable::class);
        $this->call(AllergensTable::class);
        $this->call(ProductTable::class);
        $this->call(TempOrderTable::class);
        $this->call(CouponTable::class);
    }
}
