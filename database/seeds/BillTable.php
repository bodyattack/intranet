<?php

use App\IrBill;
use Illuminate\Database\Seeder;

class BillTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if(env('APP_DEBUG')=='true') {
            factory(App\IrBill::class, 500)->create();
        }

    }

}
