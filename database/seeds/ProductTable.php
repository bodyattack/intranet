<?php

use App\Product;
use App\Manufacturer;
use App\Productgroup;
use Illuminate\Database\Seeder;

class ProductTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');


        DB::table('manufacturers')->truncate();
        DB::table('productgroups')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        // Hersteller
        $manufactures = array(
            array('name' => 'BodyAttack', 'description' => 'Eigene Herstellung'),
            array('name' => 'Ultimate Nutrition', 'description' => 'Ein Text'),
            array('name' => 'CLIF Bar', 'description' => 'Ein Text'),
            array('name' => 'Oh Yeah', 'description' => 'Ein Text'),
            array('name' => 'My Supps', 'description' => 'Ein Text'),
            array('name' => 'Oat King', 'description' => 'Ein Text'),
            array('name' => 'SmartShake', 'description' => 'Ein Text'),
            array('name' => 'JabuVit', 'description' => 'Ein Text'),
        );

        foreach ( $manufactures as $element) {
            $manufacture = new Manufacturer();
            $manufacture->name = $element['name'];
            $manufacture->description = $element['description'];
            $manufacture->save();
        }

        // Produktgruppen
        $productgroups = array(
            array('name' => 'Riegel'),
            array('name' => 'Protein'),
            array('name' => 'Fertigdrink'),
            array('name' => 'Textil'),
        );

        foreach ( $productgroups as $element) {
            $productgroup = new Productgroup();
            $productgroup->name = $element['name'];
            $productgroup->save();
        }

        // Produkte
        factory(Product::class, 33)->create()->each(
            function ($product) {

                // TODO - Verknüpfung!
                /*
                // seals
                $product->seals()->attach(rand(0,40));
                if(rand(0,10) % 2==0) {
                    $product->seals()->sync(rand(0,40));
                    if(rand(0,10) % 2==0) {
                        $product->seals()->sync(rand(0,40));
                    }
                }
                */
            }
        );



    }
}
