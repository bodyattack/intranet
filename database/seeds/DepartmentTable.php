<?php

use App\Permission;
use App\Department;
use App\ApiAccount;
use App\User;
use App\Area;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DepartmentTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user_herms = User::where('name','Herms')->first();
        $user_christian = User::where('name','Christian')->first();

        $department = new Department();
        $department->name = 'IT';
        $department->slug = Str::slug($department->name);
        $department->save();
        $department->invoiceApprovers()->attach($user_herms);
        $department->invoiceApprovers()->attach($user_christian);

        $department = new Department();
        $department->name = 'Buchhaltung';
        $department->slug = Str::slug($department->name);
        $department->short_name = 'BuHa';
        $department->save();
        $department->invoiceApprovers()->attach(App\User::inRandomOrder()->first());

        $department = new Department();
        $department->name = 'Premium-Stores';
        $department->slug = Str::slug($department->name);
        $department->short_name = '';
        $department->save();
        $department->invoiceApprovers()->attach(App\User::inRandomOrder()->first());

        $department = new Department();
        $department->name = 'Grafik';
        $department->slug = Str::slug($department->name);
        $department->short_name = '';
        $department->save();
        $department->invoiceApprovers()->attach(App\User::inRandomOrder()->first());

        $department = new Department();
        $department->name = 'Einkauf';
        $department->slug = Str::slug($department->name);
        $department->short_name = '';
        $department->save();
        $department->invoiceApprovers()->attach(App\User::inRandomOrder()->first());

        $department = new Department();
        $department->name = 'Marketing';
        $department->slug = Str::slug($department->name);
        $department->short_name = '';
        $department->save();
        $department->invoiceApprovers()->attach(App\User::inRandomOrder()->first());

        $department = new Department();
        $department->name = 'Personal+Verwaltung';
        $department->slug = Str::slug($department->name);
        $department->short_name = 'Perso';
        $department->save();
        $department->invoiceApprovers()->attach(App\User::inRandomOrder()->first());

        $department = new Department();
        $department->name = 'Lager';
        $department->slug = Str::slug($department->name);
        $department->save();
        $department->invoiceApprovers()->attach(App\User::inRandomOrder()->first());

        $department = new Department();
        $department->name = 'Export';
        $department->slug = Str::slug($department->name);
        $department->save();
        $department->invoiceApprovers()->attach(App\User::inRandomOrder()->first());

        $department = new Department();
        $department->name = 'Vertrieb';
        $department->slug = Str::slug($department->name);
        $department->save();
        $department->invoiceApprovers()->attach(App\User::inRandomOrder()->first());

        $department = new Department();
        $department->name = 'Geschäftsführung';
        $department->slug = Str::slug($department->name);
        $department->short_name = 'GF';
        $department->save();
        $department->invoiceApprovers()->attach(App\User::inRandomOrder()->first());


        // Berechtigungen
        $permission_weiterleiten = Permission::where('slug','rechnung-weiterleiten')->first();
        $permission_erstellen = Permission::where('slug','rechnung-erstellen')->first();
        $permission_loeschen = Permission::where('slug','rechnung-loeschen')->first();
        $permission_rechnungssteller = Permission::where('slug','rechnungssteller-erstellen')->first();
        $permission_kostenstellen = Permission::where('slug','kostenstellen-anlegen')->first();
        $permission_zahlungskallender = Permission::where('slug','rechnungskontrolle-zahlungsverkehr')->first();


        // User Buchhaltung anlegen!
        $area = Area::where('slug','rechnungskontrolle')->first();
        $department = Department::where('name','Buchhaltung')->first();

        $user_christian->departments()->attach('1');
        $user_christian->departments()->attach('2');

        $user_christian->permissions()->attach($permission_weiterleiten);
        $user_christian->permissions()->attach($permission_erstellen);
        $user_christian->permissions()->attach($permission_loeschen);
        $user_christian->permissions()->attach($permission_kostenstellen);
        $user_christian->permissions()->attach($permission_rechnungssteller);
        $user_christian->permissions()->attach($permission_zahlungskallender);

        $apiaccount = new ApiAccount();
        $apiaccount->user_id = $user_christian->id;
        $apiaccount->username = Str::slug($user_christian->fullName());
        $apiaccount->key = 'IzBiOKuBviK4Ctiw5qTc5lQDUFIRuAoaqQObG5JzrPi78tUn';
        $apiaccount->save();


        $user = new User();
        $user->name = 'Tatjana';
        $user->surname = 'Golinski';
        $user->email = 't.golinski@body-attack.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('!Sophia0306');
        $user->remember_token = Str::random(10);
        $user->level_id = '3';
        $user->save();
        $user->areas()->attach($area);
        $user->departments()->attach($department);

        $user = new User();
        $user->name = 'Melanie';
        $user->surname = 'Lange';
        $user->email = 'm.lange@body-attack.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('!Ml2019ba');
        $user->remember_token = Str::random(10);
        $user->level_id = '3';
        $user->save();
        $user->areas()->attach($area);
        $user->departments()->attach($department);

        $user = new User();
        $user->name = 'Stephanie';
        $user->surname = 'Kowalski';
        $user->email = 's.kowalski@body-attack.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('Laura2020!');
        $user->remember_token = Str::random(10);
        $user->level_id = '3';
        $user->save();
        $user->departments()->attach($department);

        $user = new User();
        $user->name = 'Anja';
        $user->surname = 'Schittek';
        $user->email = 'a.schittek@body-attack.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('Juni01%');
        $user->remember_token = Str::random(10);
        $user->level_id = '3';
        $user->save();
        $user->departments()->attach($department);

        $user = new User();
        $user->name = 'Angelina';
        $user->surname = 'Sommerfeld';
        $user->email = 'a.sommerfeld@body-attack.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('Angi,2644');
        $user->remember_token = Str::random(10);
        $user->level_id = '3';
        $user->save();
        $user->areas()->attach($area);
        $user->departments()->attach($department);
        $user->permissions()->attach($permission_weiterleiten);
        $user->permissions()->attach($permission_erstellen);
        $user->permissions()->attach($permission_loeschen);
        $user->permissions()->attach($permission_kostenstellen);
        $user->permissions()->attach($permission_rechnungssteller);
        $user->permissions()->attach($permission_zahlungskallender);
        
        $user = new User();
        $user->name = 'Johanna';
        $user->surname = 'Bätjer';
        $user->email = 'j.baetjer@body-attack.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('Jb_2020');
        $user->remember_token = Str::random(10);
        $user->level_id = '3';
        $user->save();
        $user->areas()->attach($area);
        $user->departments()->attach($department);
        $user->permissions()->attach($permission_weiterleiten);
        $user->permissions()->attach($permission_erstellen);
        $user->permissions()->attach($permission_loeschen);
        $user->permissions()->attach($permission_kostenstellen);
        $user->permissions()->attach($permission_rechnungssteller);
        $user->permissions()->attach($permission_zahlungskallender);
        

        $user = new User();
        $user->name = 'Lucie';
        $user->surname = 'Leschly Sörensen';
        $user->email = 'l.soerensen@body-attack.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('Larsi0611');
        $user->remember_token = Str::random(10);
        $user->level_id = '3';
        $user->save();
        $user->areas()->attach($area);
        $user->departments()->attach($department);


    }
}
