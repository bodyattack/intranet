<?php

use App\Coupon;
use App\CouponCode;
use App\TempOrder;
use Illuminate\Database\Seeder;

class CouponTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Coupon::class, 32)->create()->each(
            function ($coupon) {
                return $this->generateCodes($coupon);
            }
        );

        // ein Coupon 751 mal eingeöst
        factory(Coupon::class, 1)->create()->each(
            function ($coupon) {
                $coupon->multi = 1;
                $coupon->use_max = 751;
                $couponCode = $this->generateCodes($coupon);

                $orders = TempOrder::offset(500)->limit(750)->get();
                foreach ($orders AS $order) {
                    $couponCode->orders()->attach($order);
                    $order->gutschein = $couponCode->code;
                    $order->update();
                }
            }
        );

        // ein Coupon mit 2500 multi codes
        factory(Coupon::class, 1)->create()->each(
            function ($coupon) {
                $coupon->multi = 2500;
                $coupon->use_max = 1;
                $this->generateCodes($coupon);
            }
        );

    }

    public function generateCodes($coupon)
    {

        $tempOrder1 = null;
        $tempOrder2 = null;

        $multi = intval($coupon->multi);
        $rand = rand(1999, 4599);

        if($multi==1) {

            $code_start = 'ONCE-';
            $string_first = strtoupper(dechex($rand));
            $code_extension_first = str_pad($string_first, 4, "X", STR_PAD_LEFT);
            $string_last = strtoupper(dechex(rand(999, 9999)));
            $code_extension_last = str_pad($string_last, 4, "X", STR_PAD_LEFT);

            $couponCode = new CouponCode();
            $couponCode->code = $code_start.$code_extension_first.'-'.$code_extension_last;

            $coupon->codes()->save($couponCode);

            // Gutscheine verknüpfen
            if(rand(0,99)%2==0) {

                $id = rand(1,500);
                $tempOrder1 = TempOrder::where('id', $id)->first();
                if(rand(0,99)%2==0)  {
                    $id2 = rand(500,1000);
                    if($id=!$id2) {
                        $tempOrder2 = TempOrder::where('id', $id2)->first();
                    }
                }

                if(!empty($tempOrder1)) {
                    $couponCode->orders()->sync($tempOrder1);
                    $tempOrder1->gutschein = $couponCode->code;
                    $tempOrder1->update();
                }
                if(!empty($tempOrder2)) {
                    $couponCode->orders()->sync($tempOrder2);
                    $tempOrder2->gutschein = $couponCode->code;
                    $tempOrder2->update();
                }
            }

        }
        else {

            for($i=0; $i<$multi; $i++) {

                $tempOrder1 = null;
                $tempOrder2 = null;

                $code_start = 'MANY-';
                $string_first = strtoupper(dechex($rand+$i));
                $code_extension_first = str_pad($string_first, 4, "X", STR_PAD_LEFT);
                $string_last = strtoupper(dechex(rand(999, 9999)));
                $code_extension_last = str_pad($string_last, 4, "X", STR_PAD_LEFT);

                $couponCode = new CouponCode();
                $couponCode->code = $code_start.$code_extension_first.'-'.$code_extension_last;

                $coupon->codes()->save($couponCode);

                // Gutscheine verknüpfen
                if(rand(0,99)%3==0) {

                    $id = rand(1,500);
                    $tempOrder1 = TempOrder::where('id', $id)->first();
                    if(rand(0,99)%2==0)  {
                        $id2 = rand(500,1000);
                        if($id=!$id2) {
                            $tempOrder2 = TempOrder::where('id', $id2)->first();
                        }
                    }

                    if(!empty($tempOrder1)) {
                        $couponCode->orders()->sync($tempOrder1);
                        $tempOrder1->gutschein = $couponCode->code;
                        $tempOrder1->update();
                    }
                    if(!empty($tempOrder2)) {
                        $couponCode->orders()->sync($tempOrder2);
                        $tempOrder2->gutschein = $couponCode->code;
                        $tempOrder2->update();
                    }
                }

            }

        }

        return $couponCode;
    }
}
