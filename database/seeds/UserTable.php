<?php

use App\Department;
use Illuminate\Database\Seeder;
use App\User;
use App\Area;
use App\Customer;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rechungskontrolle = Area::where('slug','rechnungskontrolle')->first();
        $gutscheine = Area::where('slug','gutscheine')->first();

        $user = new User();
        $user->name = 'Christian';
        $user->surname = 'Stein';
        $user->email = 'c.stein@body-attack.de';
        $user->email_verified_at = now();
        $user->password = bcrypt('123456');
        $user->remember_token = Str::random(10);
        $user->level_id = '1';
        $user->api_key = 'BA0k915omViRmvKVBveewHyFBvJ3snpW2aPNXY7v3oBZT8QA';
        $user->save();
        $user->areas()->attach($rechungskontrolle);
        $user->areas()->attach($gutscheine);

        $user = new User();
        $user->name = 'Herms';
        $user->surname = 'Gundlach';
        $user->email = 'h.gundlach@my-supps.de';
        $user->email_verified_at = now();
        $user->password = bcrypt('hg2020#');
        $user->remember_token = Str::random(10);
        $user->level_id = '1';
        $user->save();

        $user = new User();
        $user->name = 'Philip';
        $user->surname = 'Eggerstedt';
        $user->email = 'p.eggerstedt@body-attack.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('pe2012ba');
        $user->remember_token = Str::random(10);
        $user->level_id = '1';
        $user->save();

        if(env('APP_DEBUG')=='true') {
            factory(User::class, 5)->create();
        }

    }
}
