<?php

use Illuminate\Database\Seeder;
use App\Area;
use App\Permission;

class AreaTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //RoleTableSeeder.php
        $rechnungsfreigabe_role = new Area();
        $rechnungsfreigabe_role->name = 'Rechnungskontrolle';
        $rechnungsfreigabe_role->slug = 'rechnungskontrolle';
        $rechnungsfreigabe_role->save();

        $gutscheine_role = new Area();
        $gutscheine_role->name = 'Gutscheine';
        $gutscheine_role->slug = 'gutscheine';
        $gutscheine_role->save();

        $permission = Permission::where('slug','rechnung-weiterleiten')->first();
        $rechnungsfreigabe_role->permissions()->attach($permission);        
        
        $permission = Permission::where('slug','rechnung-erstellen')->first();
        $rechnungsfreigabe_role->permissions()->attach($permission);

        $permission = Permission::where('slug','rechnung-loeschen')->first();
        $rechnungsfreigabe_role->permissions()->attach($permission);

        $permission = Permission::where('slug','rechnungssteller-erstellen')->first();
        $rechnungsfreigabe_role->permissions()->attach($permission);

        $permission = Permission::where('slug','kostenstellen-anlegen')->first();
        $rechnungsfreigabe_role->permissions()->attach($permission);

        $permission = Permission::where('slug','rechnungskontrolle-zahlungsverkehr')->first();
        $rechnungsfreigabe_role->permissions()->attach($permission);

    }
}
