<?php

use App\Level;
use Illuminate\Database\Seeder;

class LevelTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $position = new Level();
        $position->name = 'Praktikant';
        $position->slug = 'praktikant';
        $position->save();

        $position = new Level();
        $position->name = 'Mitarbeiter';
        $position->slug = 'mitarbeiter';
        $position->save();

        $position = new Level();
        $position->name = 'Zentrale';
        $position->slug = 'zenrale';
        $position->save();

        $position = new Level();
        $position->name = 'Shop';
        $position->slug = 'shop';
        $position->save();

        $position = new Level();
        $position->name = 'Lager';
        $position->slug = 'lager';
        $position->save();


    }
}
