<?php

use App\TempOrder;
use Illuminate\Database\Seeder;

class TempOrderTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TempOrder::class, 1500)->create();

    }
}
