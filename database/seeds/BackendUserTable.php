<?php

use App\BackendUser;
use Illuminate\Database\Seeder;

class BackendUserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $ba_user = new BackendUser();
        $ba_user->name = 'Christian Stein';
        $ba_user->email = 'c.stein@body-attack.de';
        $ba_user->password = bcrypt('123456');
        $ba_user->save();

        $ba_user = new BackendUser();
        $ba_user->name = 'Herms Gundlach';
        $ba_user->email = 'h.gundlach@my-supps.de';
        $ba_user->password = bcrypt('passwort');
        $ba_user->save();

        $ba_user = new BackendUser();
        $ba_user->name = 'Theis Klussmeier';
        $ba_user->email = 't.klussmeier@body-attack.de';
        $ba_user->password = bcrypt('passwort');
        $ba_user->save();

        $ba_user = new BackendUser();
        $ba_user->name = 'Fabian Gill';
        $ba_user->email = 'f.gill@body-attack.de';
        $ba_user->password = bcrypt('passwort');
        $ba_user->save();

        $ba_user = new BackendUser();
        $ba_user->name = 'Markus Ackerle';
        $ba_user->email = 'm.aeckerle@body-attack.de';
        $ba_user->password = bcrypt('passwort');
        $ba_user->save();


        $ba_user = new BackendUser();
        $ba_user->name = 'Patrick Trumpjahn';
        $ba_user->email = 'p.trumpjahn@body-attack.de';
        $ba_user->password = bcrypt('passwort');
        $ba_user->save();

    }
}
