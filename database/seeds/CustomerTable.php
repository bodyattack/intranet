<?php

use App\Customer;
use App\Country;
use Illuminate\Database\Seeder;

class CustomerTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('customers')->truncate();
        DB::table('countries')->truncate();
        DB::table('customer_addresses')->truncate();
        DB::table('customer_settings')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $countries = array(
            array('countries_id' => '81','countries_name' => 'Germany','countries_name_de' => 'Deutschland','countries_iso_code_2' => 'DE','countries_iso_code_3' => 'DEU','address_format_id' => '1','versand_kosten' => '4.95','versandkostenfrei' => '60','aktiv' => '1'),
            array('countries_id' => '14','countries_name' => 'Austria','countries_name_de' => 'Österreich','countries_iso_code_2' => 'AT','countries_iso_code_3' => 'AUT','address_format_id' => '1','versand_kosten' => '7.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '21','countries_name' => 'Belgium','countries_name_de' => 'Belgien','countries_iso_code_2' => 'BE','countries_iso_code_3' => 'BEL','address_format_id' => '1','versand_kosten' => '9.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '57','countries_name' => 'Denmark','countries_name_de' => 'Dänemark','countries_iso_code_2' => 'DK','countries_iso_code_3' => 'DNK','address_format_id' => '1','versand_kosten' => '9.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '72','countries_name' => 'Finland','countries_name_de' => 'Finnland','countries_iso_code_2' => 'FI','countries_iso_code_3' => 'FIN','address_format_id' => '1','versand_kosten' => '11.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '73','countries_name' => 'France','countries_name_de' => 'Frankreich','countries_iso_code_2' => 'FR','countries_iso_code_3' => 'FRA','address_format_id' => '1','versand_kosten' => '14.99','versandkostenfrei' => '150','aktiv' => '1'),
            array('countries_id' => '84','countries_name' => 'Greece','countries_name_de' => 'Griechenland','countries_iso_code_2' => 'GR','countries_iso_code_3' => 'GRC','address_format_id' => '1','versand_kosten' => '14.00','versandkostenfrei' => '99','aktiv' => '0'),
            array('countries_id' => '103','countries_name' => 'Ireland','countries_name_de' => 'Irland','countries_iso_code_2' => 'IE','countries_iso_code_3' => 'IRL','address_format_id' => '1','versand_kosten' => '11.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '105','countries_name' => 'Italy','countries_name_de' => 'Italien','countries_iso_code_2' => 'IT','countries_iso_code_3' => 'ITA','address_format_id' => '1','versand_kosten' => '11.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '122','countries_name' => 'Liechtenstein','countries_name_de' => 'Liechtenstein','countries_iso_code_2' => 'LI','countries_iso_code_3' => 'LIE','address_format_id' => '1','versand_kosten' => '16.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '124','countries_name' => 'Luxembourg','countries_name_de' => 'Luxemburg','countries_iso_code_2' => 'LU','countries_iso_code_3' => 'LUX','address_format_id' => '1','versand_kosten' => '9.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '150','countries_name' => 'Netherlands','countries_name_de' => 'Niederlande','countries_iso_code_2' => 'NL','countries_iso_code_3' => 'NLD','address_format_id' => '1','versand_kosten' => '10.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '160','countries_name' => 'Norway','countries_name_de' => 'Norwegen','countries_iso_code_2' => 'NO','countries_iso_code_3' => 'NOR','address_format_id' => '1','versand_kosten' => '16.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '195','countries_name' => 'Spain','countries_name_de' => 'Spanien','countries_iso_code_2' => 'ES','countries_iso_code_3' => 'ESP','address_format_id' => '3','versand_kosten' => '11.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '203','countries_name' => 'Sweden','countries_name_de' => 'Schweden','countries_iso_code_2' => 'SE','countries_iso_code_3' => 'SWE','address_format_id' => '1','versand_kosten' => '11.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '204','countries_name' => 'Switzerland','countries_name_de' => 'Schweiz','countries_iso_code_2' => 'CH','countries_iso_code_3' => 'CHE','address_format_id' => '1','versand_kosten' => '16.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '222','countries_name' => 'United Kingdom','countries_name_de' => 'Großbritannien','countries_iso_code_2' => 'GB','countries_iso_code_3' => 'GBR','address_format_id' => '1','versand_kosten' => '11.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '223','countries_name' => 'Bulgaria','countries_name_de' => 'Bulgarien','countries_iso_code_2' => 'BG','countries_iso_code_3' => 'BG','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '224','countries_name' => 'Estonia','countries_name_de' => 'Estland','countries_iso_code_2' => 'EE','countries_iso_code_3' => 'EE','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '225','countries_name' => 'Latvia','countries_name_de' => 'Lettland','countries_iso_code_2' => 'LV','countries_iso_code_3' => 'LV','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '226','countries_name' => 'Lithuania','countries_name_de' => 'Litauen','countries_iso_code_2' => 'LT','countries_iso_code_3' => 'LT','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '227','countries_name' => 'Malta','countries_name_de' => 'Malta','countries_iso_code_2' => 'MT','countries_iso_code_3' => 'MT','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '228','countries_name' => 'Monaco','countries_name_de' => 'Monaco','countries_iso_code_2' => 'MC','countries_iso_code_3' => 'MC','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '229','countries_name' => 'Poland','countries_name_de' => 'Polen','countries_iso_code_2' => 'PL','countries_iso_code_3' => 'PL','address_format_id' => '1','versand_kosten' => '9.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '230','countries_name' => 'Portugal','countries_name_de' => 'Portugal','countries_iso_code_2' => 'PT','countries_iso_code_3' => 'PT','address_format_id' => '1','versand_kosten' => '11.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '231','countries_name' => 'Romania','countries_name_de' => 'Rumänien','countries_iso_code_2' => 'RO','countries_iso_code_3' => 'RO','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '232','countries_name' => 'San Marino','countries_name_de' => 'San Marino','countries_iso_code_2' => 'SM','countries_iso_code_3' => 'SM','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '233','countries_name' => 'Slovakia','countries_name_de' => 'Slovakei','countries_iso_code_2' => 'SK','countries_iso_code_3' => 'SK','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '234','countries_name' => 'Slovenia','countries_name_de' => 'Slowenien','countries_iso_code_2' => 'SI','countries_iso_code_3' => 'SI','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '235','countries_name' => 'Czech Republic','countries_name_de' => 'Tschechien','countries_iso_code_2' => 'CZ','countries_iso_code_3' => 'CZ','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '236','countries_name' => 'Hungary','countries_name_de' => 'Ungarn','countries_iso_code_2' => 'HU','countries_iso_code_3' => 'HU','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '1'),
            array('countries_id' => '237','countries_name' => 'Cyprus','countries_name_de' => 'Zypern','countries_iso_code_2' => 'CY','countries_iso_code_3' => 'CY','address_format_id' => '1','versand_kosten' => '13.99','versandkostenfrei' => '99','aktiv' => '0')
        );

        foreach ( $countries as $element) {
            $country = new Country();
            $country->name = $element['countries_name'];
            $country->name_de = $element['countries_name_de'];
            $country->code = $element['countries_iso_code_2'];
            $country->shipping = $element['versand_kosten'];
            $country->shipping_free_limit = $element['versandkostenfrei'];
            $country->active = $element['aktiv'];
            $country->save();
        }


        if(env('APP_DEBUG')=='true') {
            factory(Customer::class, 50)->create()->each(
                function ($customer) {
                    // address
                    factory(App\CustomerAddress::class)->create([
                        'customer_id' => $customer->id,
                    ]);
                    // customer settings
                    factory(App\CustomerSetting::class)->create([
                        'customer_id' => $customer->id,
                    ]);
                }
            );

        }


    }
}
