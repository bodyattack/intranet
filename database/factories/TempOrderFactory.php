<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\TempOrder::class, function (Faker $faker) {

    $date = $faker->dateTimeThisMonth;
    $versand_kosten = $faker->randomElement([9.99, 0, 4.99, 14.99]);

    $warenkoerbe= [
        '[{"menge":"9","staffel":"","product_id":"3472","variante":"Marzipan","einzelpreis":"4.7922"},{"menge":"1","staffel":"0","product_id":"2139","variante":"Apricot-Yoghurt","einzelpreis":"47.99"},{"menge":"1","staffel":"0","product_id":"2139","variante":"Banana","einzelpreis":"47.99"},{"menge":"1","staffel":"","product_id":"2734","variante":"","einzelpreis":"7.99"}]',
        '[{"menge":"9","staffel":"","product_id":"1027","variante":"Marzipan","einzelpreis":"4.7922"},{"menge":"1","staffel":"0","product_id":"574","variante":"Apricot-Yoghurt","einzelpreis":"47.99"},{"menge":"1","staffel":"0","product_id":"2139","variante":"Banana","einzelpreis":"47.99"},{"menge":"1","staffel":"","product_id":"1511","variante":"","einzelpreis":"18.99"}]',
        '[{"menge":12,"staffel":"0","product_id":"4090","variante":"","einzelpreis":0.19083333333333333},{"menge":"1","staffel":"","product_id":"4145","variante":"Banana,Banana Cream,Black","einzelpreis":"69.99"}]',
        '[{"menge":"1","staffel":"","product_id":"4366","variante":"Creamy","einzelpreis":"9.99"}]',
        '[{"menge":15,"staffel":"0","product_id":"3814","variante":"Blueberry","einzelpreis":0.12333333333333334}]',
        '[{"menge":"1","staffel":"","product_id":"4366","variante":"Creamy","einzelpreis":"9.99"},{"menge":2,"staffel":"0","product_id":"2139","variante":"Cookies n Cream","einzelpreis":"49.99"},{"menge":"1","staffel":"0","product_id":"2139","variante":"Chocolate","einzelpreis":"49.99"},{"menge":"1","staffel":"0","product_id":"2139","variante":"Vanilla","einzelpreis":"49.99"}]',
    ];

    $warkorb_string = $faker->randomElement($warenkoerbe);
    $warenkorb = json_decode($warkorb_string, true);

    $gesamtpreis = $versand_kosten;
    foreach ($warenkorb AS $key => $item) {
        $menge = intval($item['menge']);
        $preis = floatval($item['einzelpreis']);
        $gesamtpreis += $menge * $preis;

    }

    $kunden= [
        '{"salutation":"m","firstname":"Herms","lastname":"Gundlach","company":"","street":"Breslauer Str. 17","zip":"25474","city":"Ellerbk","country":"DE","email":"gundlach@genion.de","telephonenumber":"041013007600","ip":"77.3.255.121","shipping_firstname":"Herms","shipping_lastname":"Herms","shipping_street":"Breslauer Str. 17","shipping_zip":"25474","shipping_city":"Ellerbk","shipping_country":"DE","gast":"","lsalutation":"m","lfirstname":"","llastname":"","lcompany":"","lstreet":"","lzip":"","lcity":"","lcountry":"DE","ltelephonenumber":"","customer_id":"","birthday":"1972-08-29","hash":"7921961ab2506a02eae9739a6ce31b3a"}',
        '{"salutation":"","firstname":"Christian","lastname":"Stein","company":null,"street":"ESpachstr. 1","zip":"79111","city":"Freiburg","state":"Baden-W\u00fcrttemberg","country":"DE","telephonenumber":null,"email":"c.stein@body-attack.com","ip":"77.3.255.121","lsalutation":"","lfirstname":"","llastname":"","lcompany":"","lstreet":"","lzip":"","lcity":"","lcountry":"","ltelephonenumber":"","kundenid":""}',
        '{"salutation":"","firstname":"Karl","lastname":"K\u00fcfer","company":"Karl K\u00fcfer","street":"Matzleinsdorferplatz 9999","zip":"1050","city":"Wien","country":"AT","telephonenumber":"+4319999999","email":"c.stein@body-attack.de","ip":"85.222.213.244","lsalutation":"","lfirstname":"","llastname":"","lcompany":"","lstreet":"","lzip":"","lcity":"","lcountry":"","ltelephonenumber":"","kundenid":""}',
        '{"salutation":"","firstname":"Max","lastname":"Mustermann","company":"Max Mustermann","street":"Sch\u00fctzstra\u00dfe 123","zip":"80939","city":"M\u00fcnchen","country":"DE","telephonenumber":"+491731112222","email":"t.klussmeier@body-attack.com","ip":"77.8.191.16","lsalutation":"","lfirstname":"","llastname":"","lcompany":"","lstreet":"","lzip":"","lcity":"","lcountry":"","ltelephonenumber":"","kundenid":""}',
        '{"salutation":"","firstname":"Elisabeth","lastname":"Harrison","company":"Elisabeth Harrison","street":"4973 Primrose Lane","zip":"SE1 2BY","city":"London","country":"GB","telephonenumber":"+44774999888","email":"t.klussmeier@body-attack.com","ip":"77.8.191.16","lsalutation":"","lfirstname":"","llastname":"","lcompany":"","lstreet":"","lzip":"","lcity":"","lcountry":"","ltelephonenumber":"","kundenid":""}',
        '{"salutation":"","firstname":"Frank","lastname":"Tester","company":null,"street":"Schnackenburg-Allee 217-223","zip":"22525","city":"Hamburg","state":null,"country":"DE","telephonenumber":null,"email":"paypalsandbox@body-attack.de","ip":"95.116.134.101","lsalutation":"","lfirstname":"","llastname":"","lcompany":"","lstreet":"","lzip":"","lcity":"","lcountry":"","ltelephonenumber":"","kundenid":""}',
        '{"salutation":"m","firstname":"Theis","lastname":"Klussmeier","company":"6. Stock","street":"Schnackenburgallee 217-223","zip":"22525","city":"Hamburg","country":"AT","email":"t.klussmeier@body-attack.com","telephonenumber":"0404600360171","ip":"95.116.134.101","gast":"","lsalutation":"","lfirstname":"","llastname":"","lcompany":"","lstreet":"","lzip":"","lcity":"","lcountry":"DE","ltelephonenumber":"","customer_id":"1888","birthday":"1970-01-12","hash":"ad01736001499e11142de3c3d2a520d1"}',
    ];

    $zufall = rand(0,99);
    $gesamtsumme = intval($gesamtpreis*100);
    $nachlass =  ($zufall%3==0 ? 0 : ($gesamtsumme/100)*rand(5,20));

    return [
        'datum' => $date,
        'verifydate' => $date,
        'warenkorb_string' =>  json_decode($warkorb_string),
        'warenkorb_nachlass' => $nachlass,
        'gesamtsumme' => $gesamtsumme-$nachlass,
        'versandkosten' => intval($versand_kosten*100),
        'gutschein' => '',
        'zahlungsart' => $faker->randomElement(['Paypal', 'Klarna', 'Kreditkarte', 'Giropay', 'Überweisung']),
        'kunde' => json_decode($faker->randomElement($kunden)),
        'TxId' => strval(rand(10000000, 99999999)),
        'lieferlandabweichung' => 0,
        'intranet_id' => '',
        'transaction_key' => str_pad(strtoupper(dechex(rand(10000, 100000))), 8, "X", STR_PAD_LEFT),
        'userid_payone' => strval(rand(10000000, 99999999)),
        'status' => rand(1, 2),
        'transfer' => intval(rand(1, 99) % 2),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
