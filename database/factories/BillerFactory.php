<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\IrBiller::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->company,
        'costcenter_id' => rand(0,100),
        'match_email' => $faker->companyEmail,
        'match_subject' => $faker->title,
    ];
});

