<?php

use Faker\Generator as Faker;

$factory->define(App\CustomerAddress::class, function (Faker $faker, $params) {
    return [
        'address' => $faker->streetAddress,
        'zip' => $faker->postcode,
        'city' => $faker->city,
        'country_id' => 1, // DE
        'customer_id' => $params['customer_id'],
    ];
});
