<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Product::class, function (Faker $faker) {

    return [
        'erp_nr' => null,
        'productgroup_id' => 1,
        'manufacturer_id' => 1,
        'name' => $faker->name,
        'variation' => '',
        'quantity' => 50,
        'measure' => 'g',
        'packaging_unit' => 12,
        'ean' => $faker->ean8,
        'ansi' => '',
    ];

});

