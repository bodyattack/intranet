<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\IrBill::class, function (Faker $faker) {

    $release = ( rand(0, 100)%2==0?true:false );

    $date = $faker->dateTimeThisMonth;
    $delivery_date = $faker->dateTimeThisMonth;
    if(rand(0, 210)%3==0) {
        $date = $faker->dateTimeBetween('last month', '+'.rand(1,18).' days');
        $delivery_date = $faker->dateTimeBetween('last month', '+'.rand(14,31).' days');
    }

    return [
        'bill_number' => $faker->creditCardNumber,
        'date' => $date,
        'due_date' =>  date('Y-m-d', strtotime($date->format("Y-m-d")  . ' +  14 days')),
        'delivery_date' => ( $release ? date('Y-m-d', strtotime($date->format("Y-m-d") . ' -  '.rand(2,14).' days')): $delivery_date ),
        'release_date' => ( $release ? date('Y-m-d', strtotime($date->format("Y-m-d") . ' +  '.rand(5,10).' days')): null ),
        'payment_date' =>  ( $release ? date('Y-m-d', strtotime($date->format("Y-m-d")  . ' +  '.rand(9,21).' days')): null ),
        'status' =>  ( $release ? 'bezahlt' : $faker->randomElement(['not_assigned', 'neu', 'vorab_freigabe', 'freigabe', 'reklamieren', 'bezahlen', 'blocken', 'bezahlt']) ),
        'payment_method' => $faker->randomElement(['Rechnung', 'Einzug', 'Kreditkarte', 'PayPal', 'Bar', 'Vorkasse', 'Amazon', 'Verrechnung', 'Intern']),
        'currency' => $faker->randomElement(['EUR', 'GBP', 'USD', 'CHF', 'AUD']),
        'amount' => $faker->randomFloat(2, 1, 5000),
        'deposit' => rand(0, 1),
        'biller_id' => rand(1, 21),
        'department_id' =>  rand(1, 11),
        'costcenter_id' => (rand(0, 9)%3==0?'3':rand(1, 110)),
        'mailbox_id' => rand(1, 2),
        'mandant_id' => rand(1, 3),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
