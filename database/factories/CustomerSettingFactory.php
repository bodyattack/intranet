<?php

use Faker\Generator as Faker;

$factory->define(App\CustomerSetting::class, function (Faker $faker, $params) {
    return [
        'newsletter' => 1,
        'customer_id' => $params['customer_id'],
    ];
});
