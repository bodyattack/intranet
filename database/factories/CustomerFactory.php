<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Customer::class, function (Faker $faker) {

    $gender = $faker->randomElement(['male', 'female']);

    return [
        'erp_kdnr' => null,
        'gender' => $gender,
        'firstname' => $faker->name($gender),
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'email_verified_at' => now(),
        'password' => '$2y$10$WLbv.8XgEwLUwxMgGpYUSOrRuTRLFvS5bcaVu0iWYJxVMxseJ/Ywm', // password
    ];
});

