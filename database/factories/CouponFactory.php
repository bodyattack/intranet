<?php


$factory->define(App\Coupon::class, function () {

    $zufall1 = rand(0,99);
    $zufall2 = rand(0,99);

    return [
        'value' => floatval(rand(0,50)),
        'art' => rand(1,4),
        'cheaper_check' => rand(0,1),
        'use_max' => ( $zufall1%2==0 ? ($zufall2%2?0:1) : rand(2, 10) ),
        'multi' => ( $zufall2%2==0 ? 1 : rand(5, 25) ),
        'active' => 1, // DE
        'created_by' => 1, // DE
    ];
});
