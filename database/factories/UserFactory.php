<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'level_id' => (rand() % 2) ? 1 : 2,
        'password' => '$2y$10$WLbv.8XgEwLUwxMgGpYUSOrRuTRLFvS5bcaVu0iWYJxVMxseJ/Ywm', // password
        'remember_token' => Str::random(10),
    ];
});

