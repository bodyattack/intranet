<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIrMailboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ir_mailboxes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('conf');
            $table->string('user');
            $table->string('pass');
            $table->tinyInteger('tool_id')->default(1);
            $table->unsignedBigInteger('mandant_id')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->timestamps();

            $table->foreign('mandant_id')->references('id')->on('mandanten')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ir_mailboxes');
    }
}
