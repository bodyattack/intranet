<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldBillsTables extends Migration

{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('ir_bills', function (Blueprint $table) {
            $table->unsignedInteger('mandant_id')->nullable()->after('mailbox_id');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('ir_bills', function (Blueprint $table) {
            $table->dropColumn('mandant_id');
        });

    }
}
