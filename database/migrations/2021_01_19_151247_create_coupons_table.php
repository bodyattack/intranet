<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('art');
            $table->float('value')->nullable();
            $table->string('code')->nullable();
            $table->smallInteger('use_count')->nullable();
            $table->tinyInteger('cheaper_check')->default(1);
            $table->integer('use_max')->nullable();
            $table->text('description')->nullable();
            $table->text('description_private')->nullable();
            $table->string('shops')->nullable();
            $table->dateTime('date_from')->nullable();
            $table->dateTime('date_to')->nullable();
            $table->float('min_order_value')->nullable();
            $table->float('max_order_value')->nullable();
            $table->string('mandatory_products')->nullable();
            $table->string('free_products')->nullable();
            $table->string('manufacturers')->nullable();
            $table->string('free_shipping_countries')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
