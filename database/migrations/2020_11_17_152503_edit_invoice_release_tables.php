<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditInvoiceReleaseTables extends Migration

{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('ir_bills', function (Blueprint $table) {
            $table->renameColumn('number', 'process_number');
            $table->text('bill_number')->nullable()->before('date');
            $table->tinyInteger('onward_debit')->default(0); // Weiterbelastung
        });

        Schema::table('ir_billers', function (Blueprint $table) {
            $table->text('creditor_number')->nullable()->after('name');
        });

        Schema::table('ir_costcenters', function (Blueprint $table) {
            $table->tinyInteger('active')->default(1);
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('ir_bills', function (Blueprint $table) {
            $table->renameColumn('process_number', 'number');
            $table->dropColumn('bill_number');
            $table->dropColumn('onward_debit');
        });

        Schema::table('ir_billers', function (Blueprint $table) {
            $table->dropColumn('creditor_number');
        });

        Schema::table('ir_costcenters', function (Blueprint $table) {
            $table->dropColumn('active');
        });

    }
}
