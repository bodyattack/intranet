<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIrBillersDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ir_billers_departments', function (Blueprint $table) {
            $table->unsignedBigInteger('biller_id');
            $table->unsignedBigInteger('department_id');

            //SETTING THE PRIMARY KEYS
            $table->primary(['biller_id','department_id']);
        });

        Schema::table('ir_billers_departments', function (Blueprint $table) {
            $table->foreign('biller_id')->references('id')->on('ir_billers')->onDelete('cascade');
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ir_billers_departments');
    }
}
