<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayonePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('payone_payments', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedBigInteger('temp_order_id')->nullable();

            // Payone Impot CSV Data
            $table->bigInteger('aid')->nullable();
            $table->bigInteger('txid')->nullable();
            $table->string('reference')->nullable();
            $table->bigInteger('userid')->nullable();
            $table->bigInteger('customerid')->nullable();
            $table->dateTime('create_time')->nullable();
            $table->date('booking_date')->nullable();
            $table->date('document_date')->nullable();
            $table->string('document_reference')->nullable();
            $table->string('param')->nullable();
            $table->string('event')->nullable();
            $table->string('clearingtype')->nullable();
            $table->string('clearingsubtype')->nullable();
            $table->decimal('amount')->nullable();
            $table->string('currency')->nullable();
            // Intranet Impot Data
            $table->bigInteger('ERP_Kundenr')->nullable();
            $table->bigInteger('custom_orders_id')->nullable();
            $table->string('Rechnungsnummern')->nullable();
            $table->timestamps();

            $table->index('txid');

            $table->foreign('temp_order_id')->references('id')->on('temp_orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('payone_payments');
    }
}
