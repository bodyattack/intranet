<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTempOrders1Tables extends Migration

{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('temp_orders', function (Blueprint $table) {
            $table->integer('warenkorb_nachlass')->nullable()->after('warenkorb_string');
            $table->integer('versandkosten')->nullable()->after('gesamtsumme');
            $table->dropColumn('warenkorb');
            $table->dropColumn('warenkorb_preise');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('temp_orders', function (Blueprint $table) {
            $table->dropColumn('warenkorb_nachlass');
            $table->dropColumn('versandkosten');
            $table->text('warenkorb');
            $table->text('warenkorb_preise')->nullable()->after('warenkorb_string');
        });

    }
}
