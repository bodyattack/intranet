<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCodesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_codes_orders', function (Blueprint $table) {
            $table->bigInteger('coupon_code_id')->unsigned();
            $table->bigInteger('order_id')->unsigned();
            $table->timestamps();

            //SETTING THE PRIMARY KEYS
            $table->primary(['coupon_code_id','order_id']);

            $table->foreign('coupon_code_id')->references('id')->on('coupon_codes')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('temp_orders')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_codes_orders');
    }
}
