<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsAllergensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_allergens', function (Blueprint $table) {
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('allergen_id')->unsigned();

            //SETTING THE PRIMARY KEYS
            $table->primary(['product_id','allergen_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_allergens');
    }
}
