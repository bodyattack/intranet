<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('country_id')->nullable();
            $table->string('address');
            $table->string('additional')->nullable();
            $table->string('zip');
            $table->string('city');
            $table->enum('type', ['global', 'invoice', 'delivery'])->default('global');
            $table->tinyInteger('primary')->default(0);
            $table->timestamps();

            $table->index(['id','customer_id']);

        });

        Schema::table('customer_addresses', function (Blueprint $table) {
            // Entfernt, da Kundendatensätze gelöscht würden, wenn ein Land versehendlich gelöscht wird
            #$table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_addresses');
    }
}
