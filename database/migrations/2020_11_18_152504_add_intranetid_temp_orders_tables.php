<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntranetidTempOrdersTables extends Migration

{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('temp_orders', function (Blueprint $table) {
            $table->string('intranet_id')->nullable()->after('transaction_key');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('temp_orders', function (Blueprint $table) {
            $table->dropColumn('intranet_id');
        });

    }
}
