<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('parent')->nullable();
            $table->bigInteger('erp_nr')->nullable();
            $table->bigInteger('productgroup_id')->nullable();
            $table->bigInteger('manufacturer_id')->nullable();
            $table->string('name')->nullable();
            $table->string('variation')->nullable();
            $table->string('quantity')->nullable(); // Menge
            $table->string('packaging_unit')->nullable(); // VPE
            $table->string('measure')->nullable(); // Maßeinheit
            $table->string('ean')->nullable();
            $table->string('ansi')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->timestamps();

            // Bilder

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
