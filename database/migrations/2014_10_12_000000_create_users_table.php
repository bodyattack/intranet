<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('level_id')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->string('name');
            $table->string('surname')->nullable();
            $table->string('email')->unique();
            $table->string('erp_name')->nullable();
            $table->string('tel')->nullable();
            $table->string('password');
            $table->string('last_login_ip')->nullable();
            $table->datetime('last_login_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
