<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTempOrdersTables extends Migration

{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('temp_orders', function (Blueprint $table) {
            $table->string('transaction_key')->nullable()->after('userid_payone');
            $table->text('warenkorb_preise')->nullable()->after('warenkorb');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('temp_orders', function (Blueprint $table) {
            $table->dropColumn('transaction_key');
            $table->dropColumn('warenkorb_preise');
        });

    }
}
