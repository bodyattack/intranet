<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIrBillItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ir_bill_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('address')->nullable();
            $table->integer('pages')->default(1);
            $table->string('filename');
            $table->unsignedBigInteger('bill_id')->index();
            $table->timestamps();

            // Entfernt, da PDF Datei nicht automatisch gelöscht würde
            #$table->foreign('bill_id')->references('id')->on('ir_bills')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ir_bill_items');
    }
}
