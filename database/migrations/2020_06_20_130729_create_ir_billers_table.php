<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIrBillersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ir_billers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('due_days')->default(0);
            $table->integer('discount_days')->default(0);
            $table->decimal('discount', 4, 2)->default(0);
            $table->enum('payment', ['Rechnung', 'Einzug', 'Kreditkarte', 'PayPal', 'Bar', 'Vorkasse', 'Amazon', 'Verrechnung', 'Intern'])->default('Rechnung');
            $table->enum('currency', ['EUR', 'GBP', 'USD', 'CHF', 'AUD'])->default('EUR');
            $table->string('match_email')->nullable();
            $table->string('match_subject')->nullable();
            $table->string('costcenter_id')->nullable();
            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ir_billers');
    }
}
