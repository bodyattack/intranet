<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCodesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('coupon_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('coupon_id');
            $table->string('code')->unique();
            $table->timestamps();

            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
        });

        Schema::table('coupons', function (Blueprint $table) {
            $table->dropColumn('code');
            $table->dropColumn('use_count');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('coupon_codes');

        Schema::table('coupons', function (Blueprint $table) {
            $table->string('code')->nullable()->after('id');
            $table->smallInteger('use_count')->nullable()->after('use_max');
        });
    }
}
