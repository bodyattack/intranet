<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_orders', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->dateTime('datum')->nullable();
            $table->dateTime('verifydate')->nullable();
            $table->text('warenkorb');
            $table->string('gutschein')->nullable();
            $table->integer('gesamtsumme');
            $table->string('zahlungsart');
            $table->text('kunde');
            $table->integer('TxId')->nullable();
            $table->string('userid_payone')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('transfer')->default(0);
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_orders');
    }
}
