<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCouponsTables extends Migration

{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('coupons', function (Blueprint $table) {
            $table->string('free_articles')->nullable()->after('free_products');
            $table->smallInteger('multi')->nullable()->after('cheaper_check');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('coupons', function (Blueprint $table) {
            $table->dropColumn('free_articles');
            $table->dropColumn('multi');
        });

    }
}
