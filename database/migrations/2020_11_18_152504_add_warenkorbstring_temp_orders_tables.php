<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWarenkorbstringTempOrdersTables extends Migration

{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('temp_orders', function (Blueprint $table) {
            $table->text('warenkorb_string')->nullable()->after('warenkorb');
            $table->tinyInteger('lieferlandabweichung')->default(0)->after('kunde');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('temp_orders', function (Blueprint $table) {
            $table->dropColumn('warenkorb_string');
            $table->dropColumn('lieferlandabweichung');
        });

    }
}
