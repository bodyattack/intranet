<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldOrdersTables extends Migration

{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('temp_orders', function (Blueprint $table) {
            $table->string('gurado')->nullable()->after('gutschein');
            $table->smallInteger('cron_processing')->nullable()->default(0)->after('transfer');
            $table->smallInteger('intranet_orders_data_replay_count')->nullable()->default(0)->after('transfer');  //um testbestellungen ohne bezug nicht ewig neu abzufragen
            $table->text('intranet_api_error')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('temp_orders', function (Blueprint $table) {
            $table->dropColumn('gurado');
            $table->dropColumn('cron_processing');
            $table->dropColumn('intranet_orders_data_replay_count');
            $table->dropColumn('intranet_api_error');
        });

    }
}
