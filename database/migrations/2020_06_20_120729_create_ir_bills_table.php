<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIrBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ir_bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('number')->nullable();
            $table->text('comment')->nullable();
            $table->dateTime('date')->nullable();  // Rechnungsdatum
            $table->dateTime('due_date')->nullable(); // Fälligkeit
            $table->dateTime('delivery_date')->nullable(); //Lieferdatum
            $table->dateTime('release_date')->nullable(); // Freigabedateum
            $table->dateTime('payment_date')->nullable(); // Zahldatm
            $table->decimal('amount', 10, 2)->default('0');
            $table->enum('status', ['not_assigned', 'neu', 'vorab_freigabe', 'freigabe', 'reklamieren', 'bezahlen', 'blocken', 'bezahlt'])->default('neu');
            $table->enum('payment_method', ['Rechnung', 'Einzug', 'Kreditkarte', 'PayPal', 'Bar', 'Vorkasse', 'Amazon', 'Verrechnung', 'Intern'])->default('Rechnung');
            #$table->string('floating')->default(0); // errechnet aus Datum
            $table->enum('currency', ['EUR', 'GBP', 'USD', 'CHF', 'AUD'])->default('EUR');
            $table->tinyInteger('deposit')->default(0); // Anzahlung
            $table->unsignedInteger('biller_id')->nullable();
            $table->unsignedInteger('department_id')->nullable();
            $table->unsignedInteger('costcenter_id')->nullable();
            $table->unsignedInteger('mailbox_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ir_bills');
    }
}
