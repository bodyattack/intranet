<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwörter müssen aus mindestens acht Zeichen bestehen und mit der Bestätigung übereinstimmen.',
    'reset' => 'Das Passwort wurde zurück gesetzt!',
    'sent' => 'Wir haben Ihnen einen Passwort zurücksetzen Link per E-Mail zugeschickt!',
    'token' => 'Die Passwort zurücksetzen URL ist ungültig!',
    'user' => "Wir können keinen Benutzer mit dieser E-Mail-Adresse finden.",

];
