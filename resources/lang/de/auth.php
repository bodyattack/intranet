<?php

$name = 'Name';
$email = 'E-Mail Adresse';
$passwort = 'Passwort';
$passwort_confirm = 'Passwort wiederholen';
$send = 'Absenden';
$cancel = 'Abbrechen';

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ihre Eingaben führen zu keinem gültigen Nutzerkonto.',
    'throttle' => 'Zu viele Fehlversuche! Bitte versuchen Sie es in :seconds Sekunden.',

    'login' => [
        'card_header' => 'Bitte melden Sie sich an',
        'name' => $name,
        'email' => $email,
        'password' => $passwort,
        'cancel' => $cancel,
        'login' => 'Anmelden',
        'forget' => 'Passwort vergessen?',
        'remember' => 'Eingeloggt bleiben',
        ],

    'reset_email' => [
        'card_header' => 'Passwort zurücksetzen!',
        'email' => $email,
        'sent' => $send ,
        'cancel' => $cancel,
        ],

    'reset' => [
        'card_header' => 'Passwort neu vergeben!',
        'email' => $email,
        'sent' => $send ,
        'cancel' => $cancel,
        'password' => $passwort,
        'password_confirm' => $passwort_confirm,
        ],

    'register' => [
        'card_header' => 'Bitte geben Sie Ihre Daten ein',
        'tel' => 'Telefon',
        'unit' => 'Abteilung',
        'name' => $name,
        'surname' => 'Nachname',
        'email' => $email,
        'password' => $passwort,
        'password_confirm' => 'Passwort wiederholen',
        'sent' => $send ,
        'cancel' => $cancel,
        'register' => 'Registrieren',
        'save' => 'Speichern',
        'ebene1' => [
            'ebene2a' => 'The.',
            'ebene2b' => 'The.',
        ],
    ],

];
