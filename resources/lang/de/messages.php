<?php

return [
    'thanks' => 'Vielen Dank',
    'hello' => 'Hallo!',
    'success' => 'Erfolgreich',
    'regards' => 'Viele Grüße',
    'error' => 'Fehler',
    'email_rights' => 'Alle Rechte vorbehalten',
];

