<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Gültigeration Language Lines
    |--------------------------------------------------------------------------
    |
    | Das following language lines contain das default error messages used by
    | das gültigerator class. Some of dasse rules have multiple versions such
    | as das size rules. Feel free to tweak each of dasse messages here.
    |
    */

    'accepted' => ':Attribute muss akzeptiert werden.',
    'active_url' => ':Attribute ist keine gültige URL.',
    'after' => ':Attribute muss ein Datum nach :date sein.',
    'after_or_equal' => ':Attribute muss ein Datum nach oder gleich :date sein.',
    'alpha' => ':Attribute darf nur ziffern enthalten.',
    'alpha_dash' => ':Attribute darf nur ziffern, nummern, punkte und unterstriche enthalten.',
    'alpha_num' => ':Attribute darf nur ziffern und nummern enthalten.',
    'array' => ':Attribute muss ein array sein.',
    'before' => ':Attribute muss ein Datum vor :date. sein',
    'before_or_equal' => ':Attribute muss ein Datum vor oder gleich :date sein.',
    'between' => [
        'numeric' => ':Attribute muss zwischen :min und :max.',
        'file' => ':Attribute muss zwischen :min und :max kilobytes.',
        'string' => ':Attribute muss zwischen :min und :max zeichen.',
        'array' => ':Attribute muss have between :min und :max items.',
    ],
    'boolean' => ':Attribute muss true oder false sein.',
    'confirmed' => ':Attribute Bestätigung stimmt nicht überein.',
    'date' => ':Attribute ist kein gültiges Datum.',
    'date_equals' => ':Attribute muss ein Datum gleich :date. sein',
    'date_format' => ':Attribute entspricht nicht dem Format :format.',
    'different' => ':Attribute und :other muss unterschiedlich sein.',
    'digits' => ':Attribute muss zwischen :digits sein.',
    'digits_between' => ':Attribute muss zwischen :min und :max digits.',
    'dimensions' => ':Attribute ungültige Dimensionen.',
    'distinct' => ':Attribute hat einen doppelten Wert.',
    'email' => ':Attribute muss eine gültige E-Mail Adresse sein.',
    'ends_with' => ':Attribute muss enden mit :values',
    'exists' => 'Das ausgewählte :Attribute ungültig.',
    'file' => ':Attribute muss eine Datei sein.',
    'filled' => ':Attribute muss einen Wert haben.',
    'gt' => [
        'numeric' => ':Attribute muss größer sein als :value.',
        'file' => ':Attribute muss größer sein als :value kilobytes.',
        'string' => ':Attribute muss größer sein als :value zeichen.',
        'array' => ':Attribute muss mehr als :value Teile haben.',
    ],
    'gte' => [
        'numeric' => ':Attribute muss größer oder gleich :value sein.',
        'file' => ':Attribute muss größer oder gleich :value kilobytes sein.',
        'string' => ':Attribute muss größer oder gleich :value zeichen sein.',
        'array' => ':Attribute muss :value 1 oder mehr Elemente haben.',
    ],
    'image' => ':Attribute muss ein Bild sein.',
    'in' => 'Das ausgewählte :Attribute ungültig.',
    'in_array' => ':Attribute fehlt in :other.',
    'integer' => ':Attribute muss eine Zahl sein.',
    'ip' => ':Attribute muss eine gültige IP address.',
    'ipv4' => ':Attribute muss eine gültige IPv4 address.',
    'ipv6' => ':Attribute muss eine gültige IPv6 address.',
    'json' => ':Attribute muss eine gültige JSON string.',
    'lt' => [
        'numeric' => ':Attribute muss weniger als :value.',
        'file' => ':Attribute muss weniger kilobytes :value haben.',
        'string' => ':Attribute muss weniger :value zeichen haben.',
        'array' => ':Attribute muss weniger haben als :value.',
    ],
    'lte' => [
        'numeric' => ':Attribute muss kleiner oder gleich :value sein.',
        'file' => ':Attribute muss kleiner oder gleich :value kilobytes sein.',
        'string' => ':Attribute muss kleiner oder gleich :value zeichen sein.',
        'array' => ':Attribute darf nicht mehr als :value Einträge haben.',
    ],
    'max' => [
        'numeric' => ':Attribute darf nicht größer sein als :max.',
        'file' => ':Attribute darf nicht größer sein als :max kilobytes.',
        'string' => ':Attribute darf nicht größer sein als :max zeichen.',
        'array' => ':Attribute darf nicht mehr als :max Teile haben.',
    ],
    'mimes' => ':Attribute muss ein Dateityp von: :values.',
    'mimetypes' => ':Attribute muss ein Dateityp von: :values.',
    'min' => [
        'numeric' => ':Attribute muss größer sein als :min.',
        'file' => ':Attribute muss größer sein als :min kilobytes.',
        'string' => ':Attribute muss länger :min Zeichen sein.',
        'array' => ':Attribute muss mehr einträge haben als :min.',
    ],
    'not_in' => 'Das ausgewählte :Attribute fehlt.',
    'not_regex' => ':Attribute Format ist ungültig.',
    'numeric' => ':Attribute muss nummerisch sein.',
    'present' => ':Attribute muss vorhanden sein.',
    'regex' => ':Attribute format ist ungültig.',
    'required' => ':Attribute ist ein Pflichtfeld.',
    'required_if' => ':Attribute ist ein Pflichtfeld wenn :other ist :value.',
    'required_unless' => ':Attribute ist ein Pflichtfeld wenn nicht :other ist in :values.',
    'required_with' => ':Attribute ist ein Pflichtfeld wenn :values vorhanden ist.',
    'required_with_all' => ':Attribute ist ein Pflichtfeld wenn :values vorhanden.',
    'required_without' => ':Attribute ist ein Pflichtfeld wenn :values nicht vorhanden.',
    'required_without_all' => ':Attribute ist ein Pflichtfeld wenn kein :values vorhanden.',
    'same' => ':Attribute und :other muss match.',
    'size' => [
        'numeric' => ':Attribute muss :size.',
        'file' => ':Attribute muss :size kilobytes.',
        'string' => ':Attribute muss :size zeichen.',
        'array' => ':Attribute muss :size Zeichen haben.',
    ],
    'starts_with' => ':Attribute muss starten mit einem der folgenden Werte: :values',
    'string' => ':Attribute muss ein string.',
    'timezone' => ':Attribute muss ein gültiger zone.',
    'unique' => ':Attribute ist bereits vergeben.',
    'uploaded' => ':Attribute konnte nich geladen werden.',
    'url' => ':Attribute format ist ungültig.',
    'uuid' => ':Attribute muss ein gültiger UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Gültigeration Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom gültigeration messages for attributes using das
    | convention "attribute.rule" to name das lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Gültigeration Attributes
    |--------------------------------------------------------------------------
    |
    | Das following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
