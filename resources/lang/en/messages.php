<?php

return [
    'thanks' => 'Vielen Dank',
    'hello' => 'Hello!',
    'success' => 'Erfolgreich',
    'regards' => 'Regards',
    'error' => 'Fehler',
    'email_rights' => 'Alle Rechte vorbehalten',
];

