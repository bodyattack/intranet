@component('mail::message')
# Willkommen

Sie **haben** *sich* angemeldet

@component('mail::button', ['url' => 'https://www.google.de', 'color' => 'BA'])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
