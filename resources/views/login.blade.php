
<!DOCTYPE html>
<html lang="zxx" class="js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Site Title  -->
    <title>Intranet - Login</title>
    <!-- Vendor Bundle CSS -->

    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>
    <script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <style>
        .is-invalid {
            border: 1px solid #e3342f;
        }
    </style>

</head>

<body class="page-ath">

<div class="page-ath-wrap">
    <div class="page-ath-content">
        <div class="page-ath-header">
            <a href="./" class="page-ath-logo"><img src="{{ asset('img/BA_logo_23_rot.png') }}" class="logo-signin" style="margin-left: -40px" alt="logo"></a>
        </div>
        <div class="page-ath-form">
            <h2 class="page-ath-heading">Login <small>{{ config('app.name') }}</small></h2>
            <form id="login-form" method="POST" action="{{ route('login.submit') }}">
                @csrf
                <div class="input-item">
                    <input type="text" id="email" name="email" placeholder="E-Mailadresse" class="input-bordered  {{ $errors->any() ? ' is-invalid' : '' }}">
                </div>
                <div class="input-item">
                    <input type="password" id="password" name="password" placeholder="Passwort" class="input-bordered {{ $errors->any() ? ' is-invalid' : '' }}">
                </div>
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="d-flex justify-content-between align-items-center">
                    <div class="input-item text-left"></div>
                    <div>
                        <a href="password/reset">Passwort zurücksetzen</a>
                        <div class="gaps-2x"></div>
                    </div>
                </div>

                <button class="btn btn-primary btn-block" id="login-button">
                    <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                    Login
                </button>

            </form>
        </div>
        <div class="page-ath-footer">
            <ul class="footer-links">
                <li><a href="https://www.body-attack.de/datenschutz.html" target="_blank">Datenschutzerklärung</a></li>
                <li><a href="https://www.body-attack.de/impressum.html" target="_blank">Impressum</a></li>
                <li><a href="{{ route('control.login.index') }}" >Admin</a></li>
                <li>&copy; Body Attack Sports Nutrition GmbH & Co. KG</li>
            </ul>
        </div>
    </div>
    <div class="page-ath-gfx"></div>
</div>


<script>
    (function() {

        document.getElementById("login-button").addEventListener ("click", login, false);

        function login() {

            console.log('test');

            var login_form =  $('#login-form');
            var login_button =  $('#login-button');

            login_button.attr('disabled', true);
            login_button.children().removeClass('d-none');

            login_form.submit();

        }

    })();

</script>


@if(Session::has('danger'))
    <!-- Modal Centered -->
    <div class="modal fade" id="modal-centered" tabindex="-1">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered">
            <div class="modal-content">
                <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body">
                    <h3 class="popup-title">Hinweis</h3>

                    <p id="alert-text"></p>

                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>

    <script>
        $(document).ready(function() {

            var danger ='<?php echo Session::get('danger');?>';
            var data = danger.split('|');
            var text = '';

            switch (data[0]) {
                case "error_verified":
                    text = "<br><strong>Bitte bestätigen Sie zunächst Ihre E-Mail Adresse!</strong>";
                    break;
                case "success_reset":
                    text = '<br>Ihr Passwort wurde zurückgesetzt, Sie können sich mit dem neu vergebenen Passwort anmelden!';
                    break;
                case "clear":
                    text = '<br>Ihr Account wurde gelöscht!';
                    break;
                case "token_expired":
                    text = '<br>Sicherheitshinweis: Ihr Validierungs-Token ist abgelaufen, Bitte führen Sie die Aktion erneut aus.';

                    break;
                default:
                    text = 'Es ist ein Fehler aufgetreten, bitte kommen Sie zu einem späteren Zeitpunkt wieder!';
            }

            $('#alert-text').html(text);
            $('#modal-centered').modal('show');

        });

    </script>

@endif

</body>
</html>
