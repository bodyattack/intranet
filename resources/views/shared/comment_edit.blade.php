<?php
function getLastChild($comment) {
    $comment_temp = $comment;
    if(!$comment->children_rec->isEmpty()) {
        $comment_temp = getLastChild($comment->children_rec[0]);
    }
    return $comment_temp;
}
?>
@empty(!$comments)
    @if($edit)
        <div class="container mb-4" >
            <div class="row">
                @foreach($comments AS $comment)
                    <?php
                    $update = false;
                    $update_text ='';
                    if($comment->updated_by){
                        $update = true;
                        $update_text = '<p>Frühere Version:<br>';
                        $update_text.= $comment->userEdit->fullName().' <br>';
                        $update_text.= date('d.m.Y H:i', strtotime($comment->updated_at)).'</p>';
                    }
                    if(!$comment->children_rec->isEmpty()) {
                        $comment = getLastChild($comment);
                    }

                    ?>
                    <div class="col-md-12 mt-1 p-2" style="border:1px solid #e4d3d5;">

                        <form method="POST" id='form_{{$comment->id}}' action="{{ route('comment.update', ['comment' => $comment->id]) }}">
                            @method('PATCH')
                            @csrf

                            <div class="row">

                                <div class="col-md-9" style="border-right:2px dotted #f99a93;">
                                <textarea id="comment" type="text" class="form-control p-1" name="comment">{{ $comment->comment }} {{ $comment->id }}</textarea>
                                </div>
                                <div class="col-md-2">
                                    <div class="h-auto">
                                        <p><small>Erstellt:</small></p>
                                    </div>
                                    <div class="h-auto">
                                        <h5 style="font-size: 14px;" class="media-heading">{{ $comment->user->fullName() }}</h5>
                                    </div>
                                    <div class="h-auto pt-1">
                                        <p><small>@datetime($comment->created_at)</small>
                                        @if($update)
                                            <i data-toggle="tooltip" data-html="true" title="" data-original-title="{{ $update_text }}" class="fas fa-info-circle pl-2 pt-2"></i>
                                        @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-1 text-center ">
                                    <div class="{{ $update ? '' : 'h-50' }}" style=" {{ $update ? 'height: 33%; ' : '' }} display: flex; align-items: center; justify-content: center;">
                                        <i title="Änderung an Bemerkung speichern" onClick="$('#form_{{$comment->id}}').submit();" class=" far fa-save text-primary" style="cursor: pointer;"></i>
                                    </div>
                                    <div class="{{ $update ? '' : 'h-50' }}" style=" {{ $update ? 'height: 33%; ' : '' }} display: flex; align-items: center; justify-content: center;">
                                    <a title="Bemerkung löschen" class="modal-delete" style="" href="#"
                                           data-form-action="{{route('comment.destroy', ['comment' => $comment->id])}}"
                                           data-form-info="Bemerkung von '{{ $comment->user->fullName() }} ' endgültig löschen?">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </div>
                                    @if($update)
                                    <div style="height: 33%; display: flex; align-items: center; justify-content: center;">
                                        <i  title="Zeige ältere Versionen an"  onClick="$('#form').submit();" class="fas fa-history text-primary" style="cursor: pointer; font-size: 14px;"></i>
                                    </div>
                                    @endif
                                </div>

                            </div>

                        </form>
                    </div>
                @endforeach

            </div>
        </div>
    @else
        <div class="container mb-4" >
            <div class="row">
                @foreach($comments AS $comment)
                    <?php
                    $update = false;
                    if($comment->updated_by){
                        $update = true;
                    }
                    ?>
                    <div class="col-md-12 mt-1 p-2" style="border:1px solid #e4d3d5;">
                        <div class="row">
                            <div class="col-md-9" style="border-right:2px dotted #f99a93;">
                                <p>{{ $comment->comment }}</p> </div>
                            <div class="col-md-3">
                                <div class="h-auto">
                                    <p><small>Erstellt:</small></p>
                                </div>
                                <div class="h-auto">
                                    <h5 style="font-size: 14px;" class="media-heading">{{ $comment->user->fullName() }}</h5>
                                </div>
                                <div class="h-auto pt-1">
                                    <p><small>@datetime($comment->created_at)</small>
                                        @if($update)
                                            <?php
                                            $icon_text = '<p>Überarbeitet:<br>';
                                            $icon_text.= $comment->userEdit->fullName().' <br>';
                                            $icon_text.= date('d.m.Y H:i', strtotime($comment->created_at)).'</p>';
                                            ?>
                                            <i data-toggle="tooltip" data-html="true" title="" data-original-title="{{ $icon_text }}" class="fas fa-info-circle pl-2 pt-2"></i>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                @endforeach

            </div>
        </div>
    @endif
@endempty

