@php( $edit = (isset($edit) ? $edit : 0 ) )
@empty(!$comments)
    <div class="container mb-4" >
        <div class="row">

            @foreach($comments AS $comment)
                <div class="col-md-12 mt-1 p-2" style="border:1px solid #e4d3d5;">
                    <div class="row">
                        <div class="col-md-9" style="border-right:2px dotted #f99a93;">
                            <p>{!!  strip_tags($comment->comment, ['br', 'p', 'a', 'b', 'i', 'u', 'k']) !!} </p> </div>
                        <div class="col-md-{{ $edit ? '2' : '3' }}">
                            <div class="h-auto">
                                <p><small>Erstellt:</small></p>
                            </div>
                            <div class="h-auto">
                                <h5 style="font-size: 14px;" class="media-heading">{{ $comment->user->fullName() }}</h5>
                            </div>
                            <div class="h-auto pt-1">
                                <p><small>@datetime($comment->created_at)</small></p>
                            </div>
                        </div>
                        @if($edit)
                            <div class="col-md-1 text-center ">
                                <div class="h-100" style="display: flex; align-items: center; justify-content: center;">
                                    <a title="Bemerkung löschen" class="modal-delete" style="" href="#"
                                       data-form-action="{{route('comment.destroy', ['comment' => $comment->id])}}"
                                       data-form-info="Bemerkung von '{{ $comment->user->fullName() }} ' endgültig löschen?">
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
            @endforeach

        </div>
    </div>
@endempty

