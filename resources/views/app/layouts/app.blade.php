@include('app.layouts.head')
<div id="app">

    @include('app.layouts.nav')


    <main class="py-4">
        @include('app.layouts.errors')
        @yield('content')
    </main>

</div>

@include('app.layouts.foot')

@include('app.layouts.modal.delete')
@include('app.layouts.modal.large')

</body>
</html>
