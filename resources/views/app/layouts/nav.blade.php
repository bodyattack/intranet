<div class="topbar-wrap" style="margin-bottom: 1px">

    <div class="topbar is-sticky">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">

                <ul class="topbar-nav d-lg-none mr-3">
                    <li class="topbar-nav-item relative">
                        <a class="toggle-nav" href="#">
                            <div class="toggle-icon">
                                <span class="toggle-line"></span>
                                <span class="toggle-line"></span>
                                <span class="toggle-line"></span>
                                <span class="toggle-line"></span>
                            </div>
                        </a>
                    </li><!-- .topbar-nav-item -->
                </ul><!-- .topbar-nav -->

                <a class="topbar-logo w-50" href="/dashboard">
                    <img style="margin-left: -20px;"  src="{{ asset('img/BA_logo_23_weiss.png') }}" alt="logo">&nbsp;&nbsp;<span class='d-none d-md-inline ml-3 navbar-brand' style="color: #fff">&nbsp;&nbsp;{{ config('app.name') }} @if( env('APP_DEBUG')=='true') | TESTSYSTEM @endif </span>
                </a>
                @auth()
                    <ul class="topbar-nav">
                        <li class="topbar-nav-item relative">
                            <span class="user-welcome  d-lg-inline-block">Willkommen, {{ Auth::user()->name }}!</span>
                        </li><!-- .topbar-nav-item -->
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i title=" {{ __('Logout') }}" class="fas fa-sign-out-alt"></i>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul><!-- .topbar-nav -->
                @endauth
            </div>
        </div><!-- .container -->
    </div><!-- .topbar -->


    <div class="navbar">
        <div class="container">

            <div class="navbar-mobile">
                <div class="container">
                    <ul class="topbar-nav d-lg-none">
                        <li class="topbar-nav-item relative">
                        </li><!-- .topbar-nav-item -->
                    </ul><!-- .topbar-nav -->
                </div><!-- .container -->
            </div><!-- .navbar -->

            <div class="navbar-innr">

                @auth()
                <ul class="navbar-menu">
                    <li><a href="/dashboard"><em class="ti ti-layout-grid2"></em>Dashboard</a></li>
                    <li class="has-dropdown">
                        <a  class="drop-toggle" href="{{ route('app.customer.index') }}"><em class="ti ti-user"></em>Unternehmen</a>
                        <ul class="navbar-dropdown">
                            @if(env('APP_ENV')=='local')
                            <li><a href="{{ route('app.customer.index') }}">Kunden</a></li>
                            @endif
                            <li><a href="{{ route('app.temporder.index') }}">Bestellungen</a></li>
                        </ul>
                    </li>

                    <li class="has-dropdown">
                        <a class="drop-toggle" href="#"><em class="ti ti-direction-alt"></em>Verwaltung</a>
                        <ul class="navbar-dropdown">
                            @if(env('APP_ENV')=='local')
                            <li><a href="#">Artikel</a></li>
                            <li><a href="{{ route('app.manufacturer.index') }}">Hersteller</a></li>
                            <li><a href="{{ route('app.productgroup.index') }}">Produktgruppen</a></li>
                            <li><a href="{{ route('app.seal.index') }}">Siegel</a></li>
                            <li><a href="{{ route('app.allergen.index') }}">Allergene</a></li>
                            @endif
                            <li><a href="{{ route('app.topCategories.index') }}">Top-Kategorien</a></li>
                            <li><a href="{{ route('app.topCategoriesEng.index') }}">Top-Kategorien (ENG)</a></li>
                            <li><a href="{{ route('app.reviews.index') }}">Bewertungen</a></li>
                            @if(array_key_exists('gutscheine', $entry['areas']))
                                <li class="has-dropdown">
                                    <a class="nav-link drop-toggle" href="{{ route('app.coupon.index') }}">Gutscheine</a>
                                    <ul class="navbar-dropdown">
                                        <li ><a href="{{ route('app.gurado.index') }}">Gurado</a></li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </li>

                    <li class="has-dropdown">
                        <a class="drop-toggle" href="#"><em class="ti ti-star"></em>Tools</a>
                        <ul class="navbar-dropdown">
                            @if(array_key_exists('rechnungskontrolle', $entry['areas']))
                                <?php
                                $rechnungskontrolle_optionen=0;
                                $rechnungskontrolle_zahlungsverkehr=0;
                                $rechnungskontrolle_erstellen=0;
                                $rechnungskontrolle_anlegen=0;
                                if(array_key_exists('rechnungskontrolle-zahlungsverkehr', $entry['permissions'])) {
                                    $rechnungskontrolle_optionen=1;
                                    $rechnungskontrolle_zahlungsverkehr=1;
                                }
                                if(array_key_exists('rechnungssteller-erstellen', $entry['permissions'])) {
                                    $rechnungskontrolle_optionen=1;
                                    $rechnungskontrolle_erstellen=1;
                                }
                                if(array_key_exists('kostenstellen-anlegen', $entry['permissions'])) {
                                    $rechnungskontrolle_optionen=1;
                                    $rechnungskontrolle_anlegen=1;
                                }

                                ?>
                                <li class="{{ $rechnungskontrolle_optionen ? 'has-dropdown' : ''  }}">
                                    <a  class="nav-link {{ $rechnungskontrolle_optionen ? ' drop-toggle' : '' }}"  href="{{ route('app.invoicerelease.bills.index') }}">Rechnungskontrolle</a>
                                    @if($rechnungskontrolle_optionen)
                                    <ul class="navbar-dropdown">
                                        <li class="d-none d-md-block d-lg-none"><a href="{{ route('app.invoicerelease.transactions.calendar') }}">Rechnungskontrolle</a></li>
                                        @if($rechnungskontrolle_zahlungsverkehr)
                                            <li class="has-dropdown">
                                                <a class="nav-link drop-toggle" href="{{ route('app.invoicerelease.transactions.calendar') }}">Zahlungsverkehr</a>
                                                <ul class="navbar-dropdown">
                                                    <li><a href="{{ route('app.invoicerelease.payone') }}">PayONE</a></li>
                                                </ul>
                                            </li>
                                        @endif
                                        @if($rechnungskontrolle_erstellen)
                                            <li><a href="{{ route('app.invoicerelease.biller.index') }}">Rechnungssteller</a></li>
                                        @endif
                                        @if($rechnungskontrolle_anlegen)
                                            <li><a href="{{ route('app.invoicerelease.costcenter.index') }}">Kostenstellen</a></li>
                                        @endif
                                    </ul>
                                    @endif
                                </li>
                            @endif
                        </ul>
                    </li>
                    <li class="has-dropdown">
                        <a class="drop-toggle" href="#"><em class="ti ti-info-alt"></em>Infos</a>
                        <ul class="navbar-dropdown">
                            <li><a href="{{ route('app.invoicerelease.biller.match_subjects') }}">Rechnung intern weiterleiten </a></li>
                        </ul>
                    </li>
                    @if(env('APP_ENV')=='local')
                    <li class="">
                        <a class="drop-toggle" href="{{ route('app.amazon.index') }}"><em class="ti ti-pulse"></em>Amazon</a>
                    </li>
                    @endif
                </ul>
                <ul class="navbar-menu float-lg-right">
                    <li class="has-dropdown mr-2">
                        <a class="drop-toggle" href="#"><em class="ti ti-settings"></em>Einstellungen</a>
                        <ul class="navbar-dropdown">
                            <li><a href="{{ route('app.user.showPasswortChangeForm') }}">Passwort ändern</a></li>
                        </ul>
                    </li>
                </ul>
                @endauth

            </div><!-- .navbar-innr -->
        </div><!-- .container -->
    </div><!-- .navbar -->

</div><!-- .topbar-wrap -->