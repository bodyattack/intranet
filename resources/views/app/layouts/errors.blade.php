@if ($errors->any())
    <div class="container mb-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    <ul>
                        @foreach($errors->all() AS $error)
                            <li>{!! $error !!}</li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif


@if(Session::has('warning'))
    <div class="container mb-1">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-warning alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <ul>
                        <strong>{!! Session::get('warning') !!}</strong>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif

@if(Session::has('success'))
    <div class="container mb-3">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <ul>
                        <strong>{!! Session::get('success') !!}</strong>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif

@if(Session::has('danger'))
    <div class="container mb-3">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <ul>
                        <strong>{!! Session::get('danger') !!}</strong>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert" style="display: none;">
            <button onclick="javascript:$('.alert-danger').slideUp('slow');" type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="alert-danger-text"></div>
            </div>
        </div>
    </div>
</div>
