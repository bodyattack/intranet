@extends('app.layouts.app')
<?php
$urlQuerry='';
if(strpos(url()->full(), '?')) {
$urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
}
?>
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Sie {{ empty($customer->id) ? ' registrieren' : 'bearbeiten' }} einen {{ $customer->presentation_long }}</div>

                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12">
                                @include('app.customer.subnavi')

                                @empty(!$customer->id)
                                <form method="POST" action="{{ route('app.customer.update', ['customer' => $customer->id]) }}">
                                @method('PATCH')
                                @else
                                <form method="POST" action="{{ route('app.customer.store') }}">
                                    @endempty

                                    @csrf
                                    <input type="hidden" name="urlQuerry" value="{{ $urlQuerry }}">

                                    <table class="table table-vertical">
                                        <tbody>

                                        @include('app.customer.form')

                                        </tbody>
                                    </table>

                                    <div class="form-group mb-0">
                                        <div class="">
                                            <a class="btn btn-primary" href="{{ route('app.customer.index', [old('urlQuerry', $urlQuerry)]) }}" role="button">Zurück</a>
                                            <button type="submit" name="submit-type" value="stay" class="btn btn-primary">Speichern</button>
                                            <button type="button" title="{{ $customer->presentation_short }} löschen" class="btn btn-danger float-right modal-delete"  href="#"
                                                    data-form-action="{{route('app.customer.destroy', ['customer'=>$customer->id])}}"
                                                    data-form-info="Wollen Sie den Kunden '{{ $customer->lastname }}' wirklich endgültig löschen?">Löschen
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
        @endsection


        @section('footer_scripts')
            <script type="text/javascript">

                function removedate(type) {
                    $('#'+type).val('');
                    $('#'+type+'_dummy').val('');
                }

                $(function() {

                    $("#dob_dummy").datepicker({
                        prevText: '&#x3c;zurück', prevStatus: '',
                        prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                        nextText: 'Vor&#x3e;', nextStatus: '',
                        nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                        currentText: 'heute', currentStatus: '',
                        todayText: 'heute', todayStatus: '',
                        clearText: '-', clearStatus: '',
                        closeText: 'schließen', closeStatus: '',
                        monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                        monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                        dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                        dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                        dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                        altField : "#dob",
                        altFormat : "yy-mm-dd",
                        dateFormat : "dd.mm.yy"
                    }, $.datepicker.regional['de']);
                });


            </script>
@stop