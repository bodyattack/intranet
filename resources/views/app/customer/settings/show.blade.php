@extends('app.layouts.app')

@section('content')
@include('app.customers.title')

@if ($customer->settings->id)
<form method="POST" action="{{ route('app.customers.settings.destroy', [$customer]) }}">
    @csrf @method('DELETE')
    <a class="btn btn-primary" href="{{ route('app.customers.index') }}" role="button">Zurück</a>
    <a class="btn btn-primary" href="{{ route('app.customers.settings.edit', [$customer]) }}"
        role="button">Bearbeiten</a>
    <button class="btn btn-danger" onclick="return window.confirm('Wirklich löschen?');">Löschen</button>
</form>
@else
<a class="btn btn-primary" href="{{ route('app.customers.index') }}" role="button">Zurück</a>
<a class="btn btn-primary" href="{{ route('app.customers.settings.create', [$customer]) }}"
    role="button">Einstellungen eintragen</a>
@endif

@include('app.customers.subnavi')

@if ($customer->settings->id)
<table class="table table-vertical">
    <tbody>
        <tr>
            <th class="w-25">Newsletter:</th>
            <td>{{ $customer->settings->present()->newsletter }}</td>
        </tr>
        <tr>
            <th>Einzelverbindungs-Nachweis:</th>
            <td>{{ $customer->settings->present()->evn }}</td>
        </tr>
        <tr>
            <th>Einzelverbidnungs-Nachweis kompakt:</th>
            <td>{{ $customer->settings->present()->evn_small }}</td>
        </tr>
        <tr>
            <th>Digitale Postmarke:</th>
            <td>{{ $customer->settings->present()->digital_stamp }}</td>
        </tr>
        <tr>
            <th>Angelegt am:</th>
            <td>{{ $customer->settings->present()->created_at }}</td>
        </tr>
        <tr>
            <th>Verändert am:</th>
            <td>{{ $customer->settings->present()->updated_at }}</td>
        </tr>
    </tbody>
</table>
@else
<p>Keine Einstellungen vorhanden</p>
@endif
@endsection