@extends('app.layouts.app')

@section('content')
@include('app.customers.title')
<form method="POST" action="{{ route('app.customers.settings.update', [$customer]) }}" autocomplete="off">
    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
    @csrf @method('PUT')
    <div>
        <a class="btn btn-primary" href="{{ route('app.customers.settings.show', [$customer]) }}"
            role="button">Zurück</a>
        <button type="submit" class="btn btn-primary">Speichern</button>
    </div>
    @include('app.customers.subnavi')
    @include('app.customers.settings.form')
</form>
@endsection