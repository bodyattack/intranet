@extends('app.layouts.app')
<?php
$urlQuerry='';
if(strpos(url()->full(), '?')) {
    $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
}
if($urlQuerry=='' & strpos(url()->previous(), '?')) {
    $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
}
?>
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Bemerkungen zum Kunden</div>

                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12">
                                @include('app.customer.subnavi')

                                @include('shared.comment', ['edit' => 1])

                                <form method="POST" action="{{ route('app.customer.comment.store', ['customer' => $customer->id]) }}">

                                    @csrf
                                    <input type="hidden" name="urlQuerry" value="{{ $urlQuerry }}">

                                    <table class="table table-vertical">
                                        <tbody>

                                        <div class="form-group">
                                            <label for="comment">Neu erstellen</label>
                                            <textarea name="comment" required class="form-control" id="comment">{{ old('comment') }}</textarea>
                                        </div>

                                        </tbody>
                                    </table>

                                    <div class="form-group mb-0">
                                        <div class="">
                                            <a class="btn btn-primary" href="{{ route('app.customer.index', [old('urlQuerry', $urlQuerry)]) }}" role="button">Zurück</a>
                                            <button type="submit" name="submit-type" value="stay" class="btn btn-primary">Speichern</button>
                                        </div>
                                    </div>

                                </form>


                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>




@endsection