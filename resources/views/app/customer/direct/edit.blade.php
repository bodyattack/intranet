@extends('app.layouts.app')
<?php
$searchable_select=0; // ergänze die class "selectpicker" select tag um  filtern zu können
if(!isset($previousUrlQuerry)) {
    $previousUrlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
}
?>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Sie {{ empty($customer->id) ? ' registrieren' : 'bearbeiten' }} einen {{ $customer->presentation_long }}</div>

                    <div class="card-body">

                        @empty(!$customer->id)
                            <form method="POST" action="{{ route('app.customer.update', ['customer' => $customer->id]) }}">
                                @method('PATCH')
                                @else
                                    <form method="POST" action="{{ route('app.customer.store') }}">
                                        @endempty

                                        @csrf
                                        <input id="filterQuerry" type="hidden" name="urlQuerry" value="{{ old('urlQuerry', $previousUrlQuerry) }}">



                                        <div class="form-group row" >
                                            <label for="gender" class="col-md-4 col-form-label text-md-right">Geschlecht</label>

                                            <div class="col-md-6">
                                                <div class="input-with-label">
                                                    <div class="select-wrapper">
                                                        <select class="input-bordered" name="gender" id="gender">
                                                            <option value="div" {{ old('gender', $customer->gender) == 'div' ? 'selected="selected"' : '' }}>Divers</option>
                                                            <option value="male" {{ old('gender', $customer->gender) == 'male' ? 'selected="selected"' : '' }}>Mann</option>
                                                            <option value="female" {{ old('gender', $customer->gender) == 'female' ? 'selected="selected"' : '' }}>Frau</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                @error('gender')
                                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                                @enderror
                                            </div>

                                        </div>


                                        <div class="form-group row">
                                            <label for="title" class="col-md-4 col-form-label text-md-right">Titel</label>

                                            <div class="col-md-6">
                                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title', $customer->title) }}" >

                                                @if ($errors->has('title'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="firstname" class="col-md-4 col-form-label text-md-right">Vorname</label>

                                            <div class="col-md-6">
                                                <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname', $customer->firstname) }}" required autofocus>

                                                @if ($errors->has('firstname'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="lastname" class="col-md-4 col-form-label text-md-right">Nachname</label>

                                            <div class="col-md-6">
                                                <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname', $customer->lastname) }}" required>

                                                @if ($errors->has('lastname'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">@lang('auth.register.email')</label>

                                            <div class="col-md-6">
                                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $customer->email) }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="phone" class="col-md-4 col-form-label text-md-right">Telefon</label>

                                            <div class="col-md-6">
                                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone', $customer->phone) }}" >

                                                @if ($errors->has('phone'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="dob_dummy" class="col-md-4 col-form-label text-md-right">Geburtsdatum</label>
                                            <div class="col-md-6">
                                                <?php
                                                $dob_dummy= old('dob_dummy', (!empty($customer->dob)? $customer->dob :''));
                                                $dob= old('dob', $customer->dob);
                                                ?>
                                                <i data-toggle="tooltip" data-html="true" onclick="removedate('dob')" data-original-title="Datum entfernen"  style="right: 20px; font-size: 20px; position: absolute; margin: 10px; color: #e69b9b;" class="fas fa-times"></i>
                                                <input style="background: white" readonly autocomplete="off" id="dob_dummy" type="text" class="form-control{{ $errors->has('dob_dummy') ? ' is-invalid' : '' }}" name="dob_dummy"  value="@date($dob_dummy)">
                                                <input id="dob" type="hidden" name="dob" value="@dateDB($dob)">

                                                @if ($errors->has('dob'))
                                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('amount') }}</strong>
                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="erp_kdnr" class="col-md-4 col-form-label text-md-right">ERP-Kd.-Nr.</label>

                                            <div class="col-md-6">
                                                <input id="erp_kdnr" type="text" class="form-control{{ $errors->has('erp_kdnr') ? ' is-invalid' : '' }}" name="erp_kdnr" value="{{ old('erp_kdnr', $customer->erp_kdnr) }}" >

                                                @if ($errors->has('erp_kdnr'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('erp_kdnr') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>



                                        <div class="form-group row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">@lang('auth.register.password')</label>

                                            <div class="col-md-6">
                                                <input placeholder="{{ !empty($customer->id) ? 'Wird durch Eingabe überschrieben' : '' }}"  id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        @empty($customer->id)
                                            <div class="form-group row">
                                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@lang('auth.register.password_confirm')</label>

                                                <div class="col-md-6">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                                                </div>
                                            </div>
                                        @endempty

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit"  name="submit-type" value="stay" class="btn btn-primary">@lang('auth.register.save')</button>
                                                <a style="float: right;" href="{{ route('app.customer.index', old('urlQuerry', $previousUrlQuerry)) }}" value="back" class="btn btn-primary"> {{ __('Zurück') }}</a>
                                            </div>
                                        </div>

                                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer_scripts')
    <script type="text/javascript">

        function removedate(type) {
            $('#'+type).val('');
            $('#'+type+'_dummy').val('');
        }

        $(function() {

            $("#dob_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#dob",
                altFormat : "yy-mm-dd",
                dateFormat : "dd.mm.yy"
            }, $.datepicker.regional['de']);
        });


    </script>
@stop