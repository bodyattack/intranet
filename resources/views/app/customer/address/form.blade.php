<div class="form-group">
    <label for="type">Verwendungsart</label>
    <select required name="type" class="form-control" id="type">
        @foreach (['global' => 'Stammadresse', 'invoice' => 'Rechnungsadresse', 'delivery' => 'Lieferadresse'] as $key => $type)
            <option value="{{ $key }}" @if($address->type == $key || old('type') == $key) selected @endif >{{ $type }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="address">Anschrift</label>
    <input name="address" required class="form-control" id="address"
        value="{{ old('address', $address->address) }}">
</div>
<div class="form-group">
    <label for="additional">Zusatzfeld</label>
    <input name="additional" class="form-control" id="additional"
        value="{{ old('additional', $address->additional) }}">
</div>
<div class="form-group">
    <label for="zip">PLZ</label>
    <input name="zip" required class="form-control" id="zip" value="{{ old('zip', $address->zip) }}">
</div>
<div class="form-group">
    <label for="city">Ort</label>
    <input name="city" required class="form-control" id="city" value="{{ old('city', $address->city) }}">
</div>
<div class="form-group">
    <label for="country_id">Land</label>
    <select required name="country_id" class="form-control" id="country_id">
        <option selected disabled>-- Bitte wählen --</option>
        @foreach (App\Country::orderBy('name')->get() as $country)
        <option value="{{ $country->id }}" @if($address->country_id == $country->id || old('country_id') == $country->id) selected @endif >{{ $country->name }}</option>
        @endforeach
    </select>
</div>
