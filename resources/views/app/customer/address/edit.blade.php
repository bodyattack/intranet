@extends('app.layouts.app')
<?php
$urlQuerry='';
if(strpos(url()->full(), '?')) {
    $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
}
if($urlQuerry=='' & strpos(url()->previous(), '?')) {
    $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
}
?>
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Sie {{ empty($address->id) ? ' erstellen' : 'bearbeiten' }} eine {{ $customer->presentation_long }} Adresse</div>

                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12">
                                @include('app.customer.subnavi')

                                @empty(!$address->id)
                                <form method="POST" action="{{ route('app.customer.address.update', ['customer' => $customer->id, 'address' => $address->id, ]) }}">
                                @method('PATCH')
                                @else
                                <form method="POST" action="{{ route('app.customer.address.store', ['customer' => $customer->id, 'address' => $address->id, ]) }}">

                                    @endempty
                                    @csrf

                                    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                                    <input type="hidden" name="urlQuerry" value="{{ $urlQuerry }}">


                                    <table class="table table-vertical">
                                        <tbody>

                                        @include('app.customer.address.form')

                                        </tbody>
                                    </table>

                                    <div class="form-group mb-0">
                                        <div class="">
                                            <a class="btn btn-primary" href="{{ route('app.customer.address.index', ['customer' => $customer->id, old('urlQuerry', $urlQuerry)]) }}" role="button">Zurück</a>
                                            <button type="submit" name="submit-type" value="stay" class="btn btn-primary">Speichern</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
