@extends('app.layouts.app')

@section('content')

<form method="POST" action="{{ route('app.customer.address.store', [$customer]) }}" autocomplete="off">
    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
    @csrf
    <div>
        <a class="btn btn-primary" href="{{ route('app.customer.address.show', [$customer]) }}"
            role="button">Zurück</a>
        <button type="submit" class="btn btn-primary">Speichern</button>
    </div>
    @include('app.customers.subnavi')
    @include('app.customers.address.form')
</form>
@endsection