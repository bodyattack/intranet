
<ul class="nav nav-tabs ml-0 mb-4">
    <li class="nav-item">
        <a class="nav-link @if(in_array(Route::currentRouteName(), ['app.customer.show', 'app.customer.edit'])) active @endif"
           href="{{ route('app.customer.edit', ['customer' => $customer, old('urlQuerry', $urlQuerry)]) }}">Allgemein</a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'address')) active @endif"
           href="{{ route('app.customer.address.index', ['customer' => $customer, old('urlQuerry', $urlQuerry)]) }}">Adressen</a>
    </li>

    <!--
    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'settings')) active @endif"
           href="{{ route('app.customer.settings.edit', ['customer' => $customer, old('urlQuerry', $urlQuerry)]) }}">Bestellungen</a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'settings')) active @endif"
           href="{{ route('app.customer.settings.edit', ['customer' => $customer, old('urlQuerry', $urlQuerry)]) }}">Rechnungen</a>
    </li>


    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'settings')) active @endif"
           href="{{ route('app.customer.settings.edit', ['customer' => $customer, old('urlQuerry', $urlQuerry)]) }}">Einstellungen</a>
    </li>

    --->

    <li class="nav-item">
        <a class="nav-link disabled " style="color: #b5c1cc;" href="#">Bestellungen</a>
    </li>

    <li class="nav-item">
        <a class="nav-link disabled "  style="color: #b5c1cc;"  href="#">Rechnungen</a>
    </li>

    <li class="nav-item">
        <a class="nav-link disabled "  style="color: #b5c1cc;"  href="#">Einstellungen</a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'notes')) active @endif"
           href="{{ route('app.customer.comment.index', ['customer' => $customer, old('urlQuerry', $urlQuerry)]) }}">Notizen</a>
    </li>


</ul>
