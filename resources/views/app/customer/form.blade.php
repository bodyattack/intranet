
<div class="form-group">
    <label for="erp_kdnr">ERP-Kd.-Nr.</label>
    <input id="erp_kdnr" type="text" class="form-control{{ $errors->has('erp_kdnr') ? ' is-invalid' : '' }}" name="erp_kdnr" value="{{ old('erp_kdnr', $customer->erp_kdnr) }}" >
</div>

<div class="form-group">
    <label for="gender">Anrede</label>
    <select required name="gender" class="form-control" id="gender">
        <option selected disabled>-- Bitte wählen --</option>
        @foreach (['female' => 'Frau', 'male' => 'Herr', 'div' => 'Divers'] as $key=>$value)
        <option value="{{ $key }}" @if($customer->gender == $key || old('gender') == $key) selected @endif>{{ $value }}
        </option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="title">Titel</label>
    <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title', $customer->title) }}" >
</div>

<div class="form-group">
    <label for="firstname">Vorname</label>
    <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname', $customer->firstname) }}" required autofocus>
</div>


<div class="form-group">
    <label for="lastname">Nachname</label>
    <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname', $customer->lastname) }}" required>
</div>


<div class="form-group">
    <label for="email">E-Mail</label>
    <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $customer->email) }}" required>
</div>

<div class="form-group">
    <label for="phone">Telefon</label>
    <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone', $customer->phone) }}" >
</div>

<div class="form-group">
    <label for="dob">Geburtsdatum</label>
    <?php
    $dob_dummy= old('dob_dummy', (!empty($customer->dob)? $customer->dob :''));
    $dob= old('dob', $customer->dob);
    ?>
    <i data-toggle="tooltip" data-html="true" onclick="removedate('dob')" data-original-title="Datum entfernen"  style="right: 0px; font-size: 22px; position: absolute; margin: 38px; color: #e69b9b;" class="fas fa-times"></i>
    <input style="background: white" readonly autocomplete="off" id="dob_dummy" type="text" class="form-control{{ $errors->has('dob_dummy') ? ' is-invalid' : '' }}" name="dob_dummy"  value="@date($dob_dummy)">
    <input id="dob" type="hidden" name="dob" value="@dateDB($dob)">
</div>


<div class="form-group">
    <label for="password">Passwort</label>
    <input placeholder="{{ !empty($customer->id) ? 'Wird durch Eingabe überschrieben' : '' }}"  id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >
</div>

@empty($customer->id)
    <div class="form-group">
        <label for="password_confirmation">Passwort Bestätigen</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
    </div>
@endempty
