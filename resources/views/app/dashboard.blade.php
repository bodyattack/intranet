@extends('app.layouts.app')


@section('content')

    <div class="container">

        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Dashboard</h4>
                        </div>

                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">

                        <div class="card mt-2">
                            <div class="card-header">Aufträge B2C</div>

                            <div class="card-body">

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped mt-2 mb-3">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>Heute neu</th>
                                            <th>Letzten 7 Tage</th>
                                            <th>Nicht Übertragen</th>
                                            <th>Übertragungsfehler</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr id="">
                                            <?php
                                            $open = $settings['temp_orders'][0]->open ?? NULL;
                                            $error = $settings['temp_orders'][0]->error ?? NULL;
                                            ?>
                                            <td>{{ $settings['temp_orders'][0]->today ?? 'n.a' }}</td>
                                            <td>{{ $settings['temp_orders'][0]->week ?? 'n.a' }}</td>
                                            <td>{{ $open + $error }}</td>
                                            <td>{{ $error ?? 'n.a' }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <form class="mb-0" id="submit-form" method="get" action="{{ route('app.temporder.dashboardSearch') }}">
                                    <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                                    @csrf
                                    <div class="input-group">
                                        <input id="quest" name="quest" type="text" class="form-control" placeholder="Suchbegriff" value="">
                                        <select  name="art" id="art" style="max-width: 170px;  cursor: pointer; color:#495057; border: 1px solid #ced4da; padding-top: 8px; border-radius: 0px; height: 37px !important;">
                                            @foreach (['number' => 'In Auftragsnummern', 'kunde' => 'In Kundendaten'] as $key=>$value)
                                                <option value="{{ $key }}">{{ $value }} </option>
                                            @endforeach
                                        </select>
                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Suchen</button>
                                    </div>
                                </form>


                            </div>
                        </div>


                        @if(!empty($settings['bills']))
                        <div class="card mt-2">
                            <div class="card-header">Rechnungskontrolle</div>

                            <div class="card-body">

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped mt-2 mb-3">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>Wartet auf Freigabe</th>
                                            <th>Zurückgestellt</th>
                                            <th>Mit Rückfragen</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr id="">
                                            <td><a style="color: black" href="{{ route('app.invoicerelease.bills.index') }}?filter=1&status=neu">{{ $settings['bills'][0]->neu ?? '' }} <i class="fas fa-link fa-sm "></i></a></td>
                                            <td><a style="color: black" href="{{ route('app.invoicerelease.bills.index') }}?filter=1&status=blocken">{{ $settings['bills'][0]->blocken ?? '' }} <i class="fas fa-link fa-sm "></i></a></td>
                                            <td><a style="color: black" href="{{ route('app.invoicerelease.bills.index') }}?filter=1&status=reklamieren">{{ $settings['bills'][0]->inquiry ?? '' }} <i class="fas fa-link fa-sm "></i></a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        @endif


                    </div>
                </div>

            </div>
        </div>



    </div>

@endsection

