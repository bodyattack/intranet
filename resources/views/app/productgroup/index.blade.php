@extends('app.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $productgroup->presentation_long }}</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="{{ $productgroup->presentation_short }} neu anlegen" style="float: right;" href="{{ route('app.productgroup.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">

                                <thead class="thead-dark">
                                <tr>
                                    <th>Name</th>
                                    <th>Erzeugt am</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $productgroups AS $productgroup )
                                    <tr id="">
                                        {{--
                                       <td>
                                           <a title="{{ $manufacturer->presentation_short }}  {{ $manufacturer->active ? 'deaktivieren' : 'aktivieren' }}" class="mr-2"  href="{{ route('app.manufacturer.toggleActive', $manufacturer->id) }}" role="button"><i class="fa fa-circle {{ $manufacturer->active ? 'text-success' : 'text-secondary' }}"></i></a>
                                       </td>
                                       --}}
                                        <td>{{ $productgroup->name }} </td>
                                        <td>@date($productgroup->created_at)</td>
                                        <td>
                                            <a title="{{ $productgroup->presentation_short }} bearbeiten" class="mr-2" href="{{ route('app.productgroup.show', ['productgroup'=>$productgroup->id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="{{ $productgroup->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die {{ $productgroup->presentation_short }} '{{ $productgroup->name }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('app.productgroup.destroy', ['productgroup'=>$productgroup->id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $productgroup->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

@endsection
