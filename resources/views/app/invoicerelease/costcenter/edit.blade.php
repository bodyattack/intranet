@extends('app.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> {{ $costcenter->presentation_short }} {{ empty($costcenter->id) ? ' erstellen' : 'bearbeiten' }} </div>

                    <div class="card-body">

                        @empty(!$costcenter->id)
                            <form id="submit-form" method="POST" action="{{ route('app.invoicerelease.costcenter.update', ['costcenter' => $costcenter->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('app.invoicerelease.costcenter.store') }}">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Bezeichnung</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $costcenter->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                

                        <div class="form-group row">
                            <label for="value" class="col-md-4 col-form-label text-md-right">Wert</label>

                            <div class="col-md-6">
                                <input id="value" type="text" class="form-control{{ $errors->has('value') ? ' is-invalid' : '' }}" name="value" value="{{ old('value', $costcenter->value) }}" >

                                @if ($errors->has('value'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="mandant_id" class="col-md-4 col-form-label text-md-right">Mandant</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="input-bordered" name="mandant_id" id="mandant_id">
                                            <option value="0">Bitte wählen</option>
                                            @foreach(\App\Mandant::all() AS $mandant)
                                                <option value="{{$mandant->id}}" {{ $costcenter->mandant_id == $mandant->id  || old('mandant_id')== $mandant->id ? 'selected="selected"' : '' }} >{{$mandant->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @error('mandant_id')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                        </div>


                                <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click">Speichern</button>
                                <a style="float: right;" href="{{ route('app.invoicerelease.costcenter.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection