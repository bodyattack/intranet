@extends('app.layouts.app')

@section('content')

    <div class="container">

        <div class="card content-area">
            <div class="card-innr">

                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Kostenstellen</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="Neue {{ $costcenter->presentation_short }} anlegen" style="float: right;" href="{{ route('app.invoicerelease.costcenter.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <form id="submit-form" method="get" action="{{ route('app.invoicerelease.costcenter.index') }}">
                            <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                            @csrf
                            <div class="input-group">
                                <input id="filter_word" name="filter_word" type="text" class="form-control" placeholder="Suchbegriff" value="{{ old('filter_word') ?? $filter_attributes['filter_word'] ?? '' }}">
                                <div class="input-group-btn" >
                                    <div class="input-group-append">
                                        <select  name="filter_row" id="filter_row" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="name" {{ old('filter_row') == 'name' || ( isset($filter_attributes['filter_row']) && $filter_attributes['filter_row'] == 'name' ) ? 'selected="selected"' : '' }}>in Bezeichnung</option>
                                            <option value="value" {{ old('filter_row') == 'value' || ( isset($filter_attributes['filter_row']) && $filter_attributes['filter_row'] == 'value' ) ? 'selected="selected"' : '' }}>in Wert</option>
                                         </select>
                                        <select  name="filter_mandant" id="filter_mandant" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="" >Mandant</option>
                                            @foreach(\App\Mandant::all() AS $mandant)
                                                <option value="{{$mandant->id}}" {{ isset($filter_attributes['filter_mandant']) && $filter_attributes['filter_mandant'] == $mandant->id  || old('mandant_id') == $mandant->id ? 'selected="selected"' : '' }} >{{$mandant->name}}</option>
                                            @endforeach
                                        </select>
                                        <select  name="filter_order" id="filter_order" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="desc" {{ old('filter_order') == 'desc' || ( isset($filter_attributes['filter_order']) && $filter_attributes['filter_order'] == 'desc' ) ? 'selected="selected"' : '' }}>Absteigend</option>
                                            <option value="asc"  {{ old('filter_order') == 'asc' || ( isset($filter_attributes['filter_order']) && $filter_attributes['filter_order'] == 'asc' ) ? 'selected="selected"' : '' }} >Aufsteigend</option>
                                        </select>
                                        <select  name="filter_active" id="filter_active" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="1" {{ old('filter_active') == '1' || ( isset($filter_attributes['filter_active']) && $filter_attributes['filter_active'] == '1' ) ? 'selected="selected"' : '' }}>Aktive</option>
                                            <option value="-1" {{ old('filter_active') == '-1' || ( isset($filter_attributes['filter_active']) && $filter_attributes['filter_active'] == '-1' ) ? 'selected="selected"' : '' }}>Alle</option>
                                            <option value="0"  {{ old('filter_active') == '0' || ( isset($filter_attributes['filter_active']) && $filter_attributes['filter_active'] == '0' ) ? 'selected="selected"' : '' }} >Inaktive</option>
                                        </select>
                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                                        <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.invoicerelease.costcenter.index' ) }}"><i class="fas fa-redo"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>

                    <div class="col-md-12">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">
                                <thead class="thead-dark">
                                <tr>
                                    @headicons('1')
                                    <th>Bezeichnung</th>
                                    <th>Wert</th>
                                    <th>Mandant</th>
                                    <th>Erstellt</th>
                                    @headicons('1', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $costcenters AS $costcenter )
                                    <tr id="">
                                        <td>
                                            <a title="{{ $costcenter->presentation_short }} {{ $costcenter->active ? 'deaktivieren' : 'aktivieren' }}" class="mr-2"  href="{{ route('app.invoicerelease.costcenter.toggleActive', $costcenter->id) }}" role="button"><i class="fa fa-circle {{ $costcenter->active ? 'text-success' : 'text-secondary' }}"></i></a>
                                        </td>
                                        <td>{{ $costcenter->name }} </td>
                                        <td>{{ $costcenter->value }} </td>
                                        <td>{{ $costcenter->mandant->name }} </td>
                                        <td>@date($costcenter->created_at)</td>
                                        <td>
                                            <a title="{{ $costcenter->presentation_short }} bearbeiten" class="mr-2" href="{{ route('app.invoicerelease.costcenter.edit', ['costcenter'=>$costcenter->id] ) }}"><i class="far fa-edit"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $costcenter->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>

                <div class="row justify-content-md-center">
                    {{ $costcenters->withQueryString()->links() }}
                </div>

            </div>
        </div>
    </div>

@endsection