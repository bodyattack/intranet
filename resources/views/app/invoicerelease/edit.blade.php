@extends('app.layouts.app')
<?php
$userBuchhaltung = 0;
if(array_key_exists('buchhaltung', $entry['departments'])){
    $userBuchhaltung = 1;
    $searchable_select = 1;
}
$userAbteilungsleiter = 0;
// Abteilungsleiter
if(!empty($entry['leader'])){
    $userAbteilungsleiter = 1;
}
if(!isset($urlQuerry)) {
    $urlQuerry = '';
    $previous = URL::previous();
    if($pos = strpos($previous, '?')) {
        $urlQuerry = substr($previous, $pos+1);
    }
}
?>
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">{{ $bill->presentation_short }} bearbeiten</div>

                    <div class="card-body">

                        <div class="row">

                            <div class="col-6 border-right">
                                @include('app.invoicerelease._pdf')
                            </div>

                            <div class="col-6">
                                <h5>Angaben</h5>

                                <form id="submit-form" method="POST" action="{{ route('app.invoicerelease.bills.update', ['bill' => $bill->id]) }}">
                                    @method('PATCH')
                                    @csrf

                                    <input id="filterQuerry" type="hidden" name="urlQuerry" value="{{ old('urlQuerry', $urlQuerry) }}">
                                    <input id="userBuchhaltung" type="hidden" name="userBuchhaltung" value="{{ $userBuchhaltung }}">
                                    <input id="userAbteilungsleiter" type="hidden" name="userAbteilungsleiter" value="{{ $userAbteilungsleiter }}">

                                    @include('app.invoicerelease._form')

                                     <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" id="submit-button" name="submit-type" value="stay" class="btn btn-primary prevent-double-click">
                                                <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                                Speichern
                                            </button>
                                            <a style="float: right;" href="{{ route('app.invoicerelease.bills.index', old('urlQuerry', $urlQuerry)) }}" value="back" class="btn btn-primary">Zurück</a>
                                        </div>
                                    </div>

                                    @empty(!$comments = $bill->getComments())
                                        <hr class="mt-4">
                                        <h5>Bemerkungen & Verlauf</h5>
                                        @include('shared.comment', ['edit' => false])
                                    @endempty

                                </form>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection