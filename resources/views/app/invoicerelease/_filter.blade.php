<form id="submit-form" method="get" action="{{ route('app.invoicerelease.bills.index') }}">
    @csrf
    <input id="filter" name="filter" type="hidden" class="form-control" value="1" >

    <div class="input-group">
        <select class="form-control" name="status" id="status" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; height: 37px !important;">
            <option value="" >Jeder Status</option>
            <option value="not_assigned" {{ ( isset($filter['status']) && $filter['status'] == 'not_assigned') || ('not_assigned' == old('status')) ? 'selected="selected"' : '' }} >Nicht zugeordnet</option>
            <option value="neu"  {{ ( isset($filter['status']) && $filter['status'] == 'neu') || ('neu' == old('status')) ? 'selected="selected"' : '' }} >Wartet auf Freigabe</option>
            <option value="vorab_freigabe" {{ ( isset($filter['status']) && $filter['status'] == 'vorab_freigabe') || ('vorab_freigabe' == old('status')) ? 'selected="selected"' : '' }}  >Vorab-Freigegeben</option>
            <option value="freigabe" {{ ( isset($filter['status']) && $filter['status'] == 'freigabe') || ('freigabe' == old('status')) ? 'selected="selected"' : '' }}  >Freigegeben</option>
            <option value="bezahlt" {{ ( isset($filter['status']) && $filter['status'] == 'bezahlt') || ('bezahlt' == old('status')) ? 'selected="selected"' : '' }} >Bezahlt</option>
            <option value="bezahlen"  {{ ( isset($filter['status']) && $filter['status'] == 'bezahlen') || ('bezahlen' == old('status')) ? 'selected="selected"' : '' }} >Bezahlen</option>
            <option value="blocken"  {{ ( isset($filter['status']) && $filter['status'] == 'blocken') || ('blocken' == old('status')) ? 'selected="selected"' : '' }} >Zurückstellen / Bezahlung zurückhalten</option>
            <option value="reklamieren"  {{ ( isset($filter['status']) && $filter['status'] == 'reklamieren') || ('reklamieren' == old('status')) ? 'selected="selected"' : '' }} >Reklamiert</option>
            <option value="swimm"  {{ ( isset($filter['status']) && $filter['status'] == 'swimm') || ('swimm' == old('status')) ? 'selected="selected"' : '' }} >Schwimmende Ware</option>
            <option value="transit"  {{ ( isset($filter['status']) && $filter['status'] == 'transit') || ('transit' == old('status')) ? 'selected="selected"' : '' }} >Ware in Transit</option>
            <option value="skonto"  {{ ( isset($filter['status']) && $filter['status'] == 'skonto') || ('skonto' == old('status')) ? 'selected="selected"' : '' }} >Skonto möglich</option>
            <option value="unsettled"  {{ ( isset($filter['status']) && $filter['status'] == 'unsettled') || ('unsettled' == old('status')) ? 'selected="selected"' : '' }} >Überfällig</option>
        </select>

        <div class="input-group-btn" style="flex: content" >
            <div class="input-group-append">
                @php($private_filters = \App\IrFilter::orderBy('name')->where('page', 'IrBillFilters')->where('user_id', Auth::user()->id)->get())
                <select  name="filter_own" id="filter_own" style="color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;
                    @if($private_filters->isEmpty()) " disabled="disabled" >
                        <option value="">Es sind keine individuellen Filter vorhanden</option>
                    @else cursor: pointer;">
                        <option value="">Bestehenden Filter laden</option>
                        @foreach($private_filters AS $private_filter)
                            {{--  <option value="{{$private_filter->id}}"  {{ ( isset($filter['filter_own']) && $filter['filter_own'] == $private_filter->id) || ($private_filter->id == old('filter_own')) ? 'selected="selected"' : '' }}  >{{$private_filter->name}}</option> --}}
                            <option value="{{$private_filter->id}}" >{{$private_filter->name}}</option>
                        @endforeach
                    @endif
                </select>
                <input id="bill_number" style="border-radius: 0px 0px 0px 0px; border-left: 0px;" name="bill_number" type="text" class="form-control" placeholder="Rechnungsnummer" value="{{ old('bill_number') ?? $filter['bill_number'] ?? '' }}">
                <input id="process_number" style="border-radius: 0px 0px 0px 0px;"  name="process_number" type="text" class="form-control" placeholder="Vorgangsnummer" value="{{ old('process_number') ?? $filter['process_number'] ?? '' }}">
                <span id="toggel-span" title="Weitere Filteroptionen" onclick="$('#hidden-filter').toggle('', function() { var span = $('#toggel-span').children().first(); if(span.hasClass('fa-chevron-down')) { span.removeClass('fa-chevron-down'); span.addClass('fa-chevron-up'); } else { span.removeClass('fa-chevron-up'); span.addClass('fa-chevron-down'); } });"  style="border: 1px solid #ced4da; border-radius: 0px; border-left: 0px; border-right: 0px; line-height: 35px; min-width: 45px; text-align: center; color: #707377; cursor: pointer;"  role="button" href="#"><i class="fas fa-chevron-down"></i></span>
                <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.invoicerelease.bills.index' ) }}"><i class="fas fa-redo"></i></a>
            </div>
        </div>
    </div>

    <div id="hidden-filter" class="border pb-3" style="display: none">

        <div class="col-12 mt-3">
            <div class="row">
                <div class="col-6">
                    <div class="row">
                        <label for="mandant" class="col-md-4 col-form-label text-md-right">Mandant</label>
                        <div class="col-md-8">
                            <div class="input-with-label">
                                <div class="select-wrapper">
                                    <select class="input-bordered" name="mandant" id="mandant">
                                        <option value="">Bitte wählen</option>
                                        @foreach(\App\Mandant::all() AS $mandant)
                                            <option value="{{$mandant->id}}"   {{ ( isset($filter['mandant']) && $filter['mandant'] == $mandant->id) || ($mandant->id == old('mandant')) ? 'selected="selected"' : '' }}  >{{$mandant->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row">
                        <label for="biller" class="col-md-4 col-form-label text-md-right">Rechnungssteller</label>
                        <div class="col-md-8 ">
                            <div class="input-with-label">
                                <div class="select-wrapper">
                                    <select class="selectpicker input-bordered form-control" name="biller" id="biller"  data-actions-box="true">
                                        <option value="">Bitte wählen</option>
                                        @foreach(\App\IrBiller::withDistributor() AS $biller)
                                            <option value="{{$biller->id}}"  {{ ( isset($filter['biller']) && $filter['biller'] == $biller->id) || ($biller->id == old('biller')) ? 'selected="selected"' : '' }}  >{{$biller->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 mt-3">
            <div class="row">
                <div class="col-6">
                    <div class="row">
                        <label for="department" class="col-md-4 col-form-label text-md-right">Freigabe Abteilung</label>
                        <div class="col-md-8">
                        <div class="input-with-label">
                            <div class="select-wrapper">
                                <select class="input-bordered" name="department" id="department">
                                    <option value="">Bitte wählen</option>
                                    @foreach($departments AS $department)
                                        <option value="{{$department->id}}" {{ ( isset($filter['department']) && $filter['department'] == $department->id) || ($department->id == old('department')) ? 'selected="selected"' : '' }}  >{{$department->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row">
                        <label for="payment" class="col-md-4 col-form-label text-md-right">Zahlart</label>
                        <div class="col-md-8">
                            <div class="input-with-label">
                                <div class="select-wrapper">
                                    <select class="input-bordered" name="payment" id="payment">
                                        <option value="" {{ ( isset($filter['payment']) && $filter['payment'] == '0') || ('0' == old('payment')) ? 'selected="selected"' : '' }} >Bitte wählen</option>
                                        <option value="Rechnung" {{ ( isset($filter['payment']) && $filter['payment'] == 'Rechnung') || ('Rechnung' == old('payment')) ? 'selected="selected"' : '' }} >Rechnung</option>
                                        <option value="Einzug"{{ ( isset($filter['payment']) && $filter['payment'] == 'Einzug') || ('Einzug' == old('payment')) ? 'selected="selected"' : '' }}  {{ old('payment', '') == 'Einzug' ? 'selected="selected"' : '' }}>Einzug</option>
                                        <option value="Kreditkarte" {{ ( isset($filter['payment']) && $filter['payment'] == 'Kreditkarte') || ('Kreditkarte' == old('payment')) ? 'selected="selected"' : '' }} >Kreditkarte</option>
                                        <option value="PayPal" {{ ( isset($filter['payment']) && $filter['payment'] == 'PayPal') || ('PayPal' == old('payment')) ? 'selected="selected"' : '' }} >PayPal</option>
                                        <option value="Bar" {{ ( isset($filter['payment']) && $filter['payment'] == 'Bar') || ('Bar' == old('payment')) ? 'selected="selected"' : '' }} >Bar</option>
                                        <option value="Vorkasse" {{ ( isset($filter['payment']) && $filter['payment'] == 'Vorkasse') || ('Vorkasse' == old('payment')) ? 'selected="selected"' : '' }} >Vorkasse</option>
                                        <option value="Amazon" {{ ( isset($filter['payment']) && $filter['payment'] == 'Amazon') || ('Amazon' == old('payment')) ? 'selected="selected"' : '' }} >Amazon</option>
                                        <option value="Verrechnung" {{ ( isset($filter['payment']) && $filter['payment'] == 'Verrechnung') || ('Verrechnung' == old('payment')) ? 'selected="selected"' : '' }} >Verrechnung</option>
                                        <option value="Intern" {{ ( isset($filter['payment']) && $filter['payment'] == 'Intern') || ('Intern' == old('payment')) ? 'selected="selected"' : '' }} >Intern</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 mt-3">
            <div class="row">
                <div class="col-6">
                    <div class="row">
                        <label class="col-md-4 col-form-label text-md-right">Rechnungsdatum von</label>
                        <div class="col-md-3">
                            <?php
                            $date_from = ( isset($filter['date_from']) ? $filter['date_from'] : old('date_from') );
                            $date_to = ( isset($filter['date_to']) ? $filter['date_to'] : old('date_to') );
                            ?>
                                <i data-toggle="tooltip" data-html="true" onclick="removedate('date_from')" data-original-title="Datum entfernen"  class="remove-date right-15 fas fa-times"></i>
                            <input style="background: white" readonly autocomplete="off" id="date_from_dummy" type="text" class="form-control" name=" " value="@empty(!$date_from) @date($date_from) @endempty">
                            <input id="date_from" type="hidden" name="date_from" value="{{ $date_from }}">
                        </div>
                        <label class="col-md-2 col-form-label text-md-right">
                            <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Beim speichern eines Filters mit Zeitraum, wird vom Datum jeweils nur der Tag berücksichtigt.<br> Monat und Jahr werden bei künftigen Abfragen mit aktuellen Datum ersetzt." class="fas fa-info-circle"></i>
                            Bis</label>
                        <div class="col-md-3">
                            <i data-toggle="tooltip" data-html="true" onclick="removedate('date_to')" data-original-title="Datum entfernen"  class="remove-date right-15 fas fa-times"></i>
                            <input style="background: white" readonly autocomplete="off" id="date_to_dummy" type="text" class="form-control" name="" value="@empty(!$date_to) @date($date_to) @endempty">
                            <input id="date_to" type="hidden" name="date_to" value="{{ $date_to }}">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row">
                        <label class="col-md-4 col-form-label text-md-right">Fälligkeitsdatum von</label>
                        <div class="col-md-3">
                            <?php
                            $due_date_from = ( isset($filter['due_date_from']) ? $filter['due_date_from'] : old('due_date_from') );
                            $due_date_to = ( isset($filter['due_date_to']) ? $filter['due_date_to'] : old('due_date_to') );
                            ?>
                            <i data-toggle="tooltip" data-html="true" onclick="removedate('due_date_from')" data-original-title="Datum entfernen"  class="remove-date right-15 fas fa-times"></i>
                            <input style="background: white" readonly autocomplete="off" id="due_date_from_dummy" type="text" class="form-control" name=" " value="@empty(!$due_date_from) @date($due_date_from) @endempty">
                            <input id="due_date_from" type="hidden" name="due_date_from" value="{{ $due_date_from }}">
                        </div>
                        <label class="col-md-2 col-form-label text-md-right">
                            <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Beim speichern eines Filters mit Zeitraum, wird vom Datum jeweils nur der Tag berücksichtigt.<br> Monat und Jahr werden bei künftigen Abfragen mit aktuellen Datum ersetzt." class="fas fa-info-circle"></i>
                            Bis</label>
                        <div class="col-md-3">
                            <i data-toggle="tooltip" data-html="true" onclick="removedate('due_date_to')" data-original-title="Datum entfernen"  class="remove-date right-15 fas fa-times"></i>
                            <input style="background: white" readonly autocomplete="off" id="due_date_to_dummy" type="text" class="form-control" name="" value="@empty(!$due_date_to) @date($due_date_to) @endempty">
                            <input id="due_date_to" type="hidden" name="due_date_to" value="{{ $due_date_to }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="col-12 mt-4">
            <div class="form-group row mb-0 ">

                <?php
                $user_name = '';
                $user_id = 0;
                $user_titel = 'speichern';
                if( isset($filter['filter_own']) && $filter['filter_own'] != 0 ) {
                    $user = App\IrFilter::orderBy('name')->where('id', $filter['filter_own'])->get();
                    $user_name = $user[0]->name;
                    $user_id = $user[0]->id;
                    $user_titel = 'bearbeiten';
                }
                ?>

                <label for="name" class="col-md-2 col-form-label text-md-right">Filter {{$user_titel}}</label>
                <div class="col-md-6">
                    <input id="related" name="related" type="hidden" value="IrBillFilters">
                    <input id="name" type="text" class="form-control" placeholder="Hier eine Bezeichnung vergeben" name="name" value="{{ $user_name }}" >
                    <span id="error_name" style="color:#e3342f; font-size: 80%; display: none;">Sie müssen eine Bezeichnung vergeben!</span>
                </div>
                <div class="col-md-4" style="padding-right: 45px;">
                    <div class="row">
                        @empty(!$user_name)
                        <div class="col-md-6">
                         <button  style="line-height: 18px;" type="button" data-form-info="Soll {{$user_name}} wirklich gelöscht werden?" data-form-action="{{route('ajax.savedFilters.destroy', $user_id)}}" class="btn btn-secondary btn-search modal-delete" >Löschen</button>
                        </div>
                        @endempty
                        <div class="col-md-6" style="float: right;">
                            @empty(!$user_name)
                                <button style="line-height: 18px;" type="button" id="filterAction" data-form-method="PATCH" data-form-action="{{ route('ajax.savedFilters.update', ['filter' => $user_id]) }}" class="btn btn-secondary btn-search" >Speichern</button>
                            @else
                                <button style="line-height: 18px;" type="button" id="filterAction" data-form-method="POST" data-form-action="{{ route('ajax.savedFilters.store') }}" class="btn btn-secondary btn-search" >Speichern</button>
                            @endempty
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</form>

@section('footer_scripts')
    <script type="text/javascript">

        $('#filterAction').click(function(){

            var formArray = $('#submit-form').serializeArray();
            var array_new = {};
            for (let i=0; i<formArray.length; i++) {
                var key = formArray[i].name
                array_new[key] = formArray[i].value;
            }

            var formJson = JSON.stringify(array_new);
            var form_action = $(this).attr('data-form-action');
            var form_method = $(this).attr('data-form-method');
            var name_objekt = $('#name');
            var related_objekt = $('#related');
            var name = name_objekt.val();
            var related = related_objekt.val();


            name_objekt.removeClass('is-invalid');
            $('#error_name').hide();
            if(name=='') {
                name_objekt.addClass('is-invalid');
                $('#error_name').show();
                return true;
            }

            $.ajax({
                url: form_action,
                type: form_method,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "filter": formJson,
                    "name": name,
                    "related": related
                },
                success: function(res) {
                    if(res=='update') window.document.location.href = "{{ route('ajax.savedFilters.update_success') }}";
                    if(res=='store') window.document.location.href = "{{ route('ajax.savedFilters.store_success') }}";
                }
            });
        });

        $(function() {

            $("#date_from_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#date_from",
                altFormat : "yy-mm-dd",
                dateFormat : "dd.mm.yy",
                changeMonth: true,
                changeYear: true,
                defaultDate: "@empty(!$date_from)@date($date_from)@endempty"
            }, $.datepicker.regional['de']);

            $("#date_to_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#date_to",
                altFormat : "yy-mm-dd",
                dateFormat : "dd.mm.yy",
                changeMonth: true,
                changeYear: true,
                defaultDate: "@empty(!$date_to)@date($date_to)@endempty"
            }, $.datepicker.regional['de']);

            $("#due_date_from_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#due_date_from",
                altFormat : "yy-mm-dd",
                dateFormat : "dd.mm.yy",
                changeMonth: true,
                changeYear: true,
                defaultDate: "@empty(!$due_date_from)@date($due_date_from)@endempty"
            }, $.datepicker.regional['de']);

            $("#due_date_to_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#due_date_to",
                altFormat : "yy-mm-dd",
                dateFormat : "dd.mm.yy",
                changeMonth: true,
                changeYear: true,
                defaultDate: "@empty(!$due_date_to)@date($due_date_to)@endempty"
            }, $.datepicker.regional['de']);

        });


    </script>
@stop