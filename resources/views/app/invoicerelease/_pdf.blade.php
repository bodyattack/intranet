@if(!$bill->items->isEmpty())
<h5>Original Dokument
    <a target="_blank" href="{{ route('app.invoicerelease.bills.pdf', $bill->items[0]->filename) }}" class="float-right" title="In eigenem Tab ansehen"><em class="ti ti-layers"></em></a>
</h5>
<object data="{{ route('app.invoicerelease.bills.pdf', $bill->items[0]->filename) }}" type="application/pdf"
        style="width:100%;height:1000px;">Dokument konnte nicht gefunden werden!</a>
</object>
@else
    <h5>Original Dokument</h5>
    <p>Es existiert keine Rechnungs-Datei zu diesem Datensatz</p>
@endif