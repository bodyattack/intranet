@section('container-class') container-fluid @endsection
@extends('app.layouts.app')
<?php
if(!isset($urlQuerry)) {
    $urlQuerry = '';
    $previous = URL::previous();
    if($pos = strpos($previous, '?')) {
        $urlQuerry = substr($previous, $pos+1);
    }
}
?>
@section('content')

    <div class="@yield('container-class', 'container')">

        <div class="card content-area">
                <div class="card-innr">

                    <div class="card-head">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="card-title card-title-lg">Rechnungskontrolle</h4>
                            </div>

                            <div class="col-md-6">
                                @if(array_key_exists('rechnung-erstellen', $entry['permissions']))
                                <a title="Neue {{ $bill->presentation_short }} anlegen" style="float: right;" href="{{ route('app.invoicerelease.bills.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                                @endif
                            </div>

                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            @include('app.invoicerelease._filter')
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped mt-2 mb-3">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th style='width: 68px; padding-left: 15px;'>@sortablelink('status', 'Info')</th>
                                        <th>@sortablelink('mandant.name', 'Mandant')</th>
                                        <th>@sortablelink('biller.name', 'Rechnungssteller')</th>
                                        <th>@sortablelink('bill_number', 'Rechnungsnummer')</th>
                                        <th>@sortablelink('department.name', 'Freigabeabteilung')</th>
                                        <th>@sortablelink('costcenter.value', 'Kostenstelle')</th>
                                        <th class="text-right">@sortablelink('amount', 'Betrag')</th>
                                        <th>@sortablelink('created_at', 'Empfangen am')</th>
                                        <th>@sortablelink('due_date', 'Fällig am')</th>
                                        @headicons('3', 'fa-cogs')
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ( $bills AS $bill )
                                        <?php
                                        $status_text='Eingelesen';
                                        $status_color='#fff';
                                        switch ($bill->status) {

                                            case 'not_assigned' :  $status_text='Nicht zugeordnet'; $status_color='#000'; break;
                                            case 'neu' :  $status_text='Wartet auf Freigabe'; $status_color='#fbee26'; break;
                                            case 'vorab_freigabe' :  $status_text='Vorab-Freigegeben'; $status_color='#169525'; break;
                                            case 'freigabe' :  $status_text='Freigegeben'; $status_color='#22f039'; break;
                                            case 'bezahlt' :  $status_text='Bezahlt'; $status_color='#2256f0;'; break;
                                            case 'bezahlen' :  $status_text='Kann bezahlt werden'; $status_color='#f09622'; break;
                                            case 'blocken' :  $status_text='Bezahlung zurückhalten'; $status_color='#f02222'; break;
                                            case 'reklamieren' :  $status_text='Reklamiert'; $status_color='#9716c6'; break;
                                            default : $status_text='Fehler'; $status_color='#000'; break;
                                        }
                                        $swimm_text = '';
                                        $transit_text = '';
                                        $discount_text = '';
                                        if(!empty($bill->delivery_date) && !empty($bill->date)) {
                                            $month_date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $bill->date)->month;
                                            $month_delivery_date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $bill->delivery_date)->month;
                                            if($month_date!=$month_delivery_date && $bill->date < $bill->delivery_date) {
                                                $swimm_text = '<i title="Schwimmende Ware" style="color: #01acff" class="fas fa-fish"></i>';
                                            }
                                            if($month_date!=$month_delivery_date && $bill->date > $bill->delivery_date) {
                                                $transit_text = '<i title="Ware in Transit" style="color: #ce622d" class="fas fa-dolly-flatbed"></i>';
                                            }
                                        }
                                        if($bill->biller!=NULL && $bill->biller_id != 0 && !empty($bill->date) && !empty($discount_days = $bill->biller->discount_days)) {
                                            $now = Carbon\Carbon::now();
                                            $discount_date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $bill->date)->addDays($discount_days);
                                            if($now<$discount_date)  {
                                                $discount_text = '<i title="Skonto in Höhe von '.$bill->discount_value.'% möglich" style="color: #f2610b" class="fas fa-euro-sign"></i>';
                                            }
                                        }
                                        ?>
                                        <tr id="">
                                            <td>
                                                <i title="{{ $status_text }}" style="color: {{ $status_color }}" class="fa fa-circle mr-1"></i>
                                                {!! $swimm_text !!}
                                                {!! $transit_text !!}
                                                {!! $discount_text !!}
                                            </td>
                                            <td>{{ ( empty($bill->mandant) ? '' : $bill->mandant->short_name) }}</td>
                                            <td>
                                                <a style="color:unset; display: block;" title="Rechnung bearbeiten" class="mr-2" href="{{ route('app.invoicerelease.bills.edit', ['bill'=>$bill->id] ) }}"> {{ ( empty($bill->biller) ? '' :  $bill->biller->name ) }}</a>
                                            </td>
                                            <td>{{ $bill->bill_number }}</td>
                                            <td>{{ ( empty($bill->department) ? '' :  $bill->department->name ) }}
                                            <td>{{ ( empty($bill->costcenter) ? '' :  $bill->costcenter->value )  }}
                                                {{--
                                                @empty(!$comments = \App\Library\Comments::getCommentsStatic($bill->id, 'IrBill'))
                                                <i style="font-size: 15px; line-height: inherit; float: right; color: #828282;" data-toggle="tooltip" data-html="true"  data-original-title="{{count($comments)}} Kommentare vorhanden!" class="far fa-comment"></i>
                                                @endempty
                                                --}}
                                            </td>
                                            <td class="text-right" style="white-space: nowrap; ">@money($bill->amount)</td>
                                            <td>@date($bill->created_at)</td>
                                            <td>@date($bill->due_date)</td>
                                            <td>
                                                @if(array_key_exists('rechnung-loeschen', $entry['permissions']))
                                                    <a title="Rechnung löschen" class="ml-3 mr-1 float-right modal-delete"  href="#"
                                                       data-form-info="Wollen Sie die Rechnung '{{ ( empty($bill->biller) ? '' :  $bill->biller->name )  }}' wirklich endgültig löschen?"
                                                       data-form-action="{{route('app.invoicerelease.bills.destroy', ['bill'=>$bill->id, $urlQuerry])}}">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                @endif
                                                <a title="Rechnung bearbeiten" class="ml-2 mr-1 float-right" href="{{ route('app.invoicerelease.bills.edit', ['bill'=>$bill->id] ) }}"><i class="far fa-edit"></i></a>                                            </td>
                                        </tr>

                                    @empty
                                        <tr id=""><td colspan="100%">Keine {{ $bill->presentation_long }} vorhanden</td></tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        {{ $bills->withQueryString()->links() }}
                    </div>

                    @if(array_key_exists('rechnungen-exportieren', $entry['permissions']))
                    <div class="row">
                        <div class="col-md-12 ">
                            <a style="min-width: 220px; line-height: unset; padding: 0px; border-radius: 0px; line-height: 35px;"  class="btn btn-secondary "  role="button" href="{{ route('app.invoicerelease.bills.csv', $filter) }}">Daten als CSV exportieren</a>
                         </div>
                    </div>
                    @endif

            </div>
        </div>
    </div>


@endsection
