@section('container-class') container-fluid @endsection
@extends('app.invoicerelease.transactions.index')
@section('transactions')
    <div id="calendar" class="tab-pane mt-3 active">
        <div class="row">

            <div class="col-md-12">
                @include('app.invoicerelease.transactions._billsTableWithOptions')
            </div>
        </div>

        <div class="row justify-content-md-center">
            {{ $bills->withQueryString()->links() }}
        </div>

    </div>
    </div>

@endsection

