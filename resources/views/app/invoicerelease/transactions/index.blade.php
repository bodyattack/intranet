@php($calendar=1)
@php($searchable_select=1)
@extends('app.layouts.app')

@section('content')
    <style>
        .input-bordered {
            color: #f563d1  !important;
            border-radius: 4px;
            border: 1px solid #d2dde9;
            padding: 0px 15px;
            line-height: initial;
            font-size: initial;
            color: rgba(73, 84, 99, 0.7);
            transition: all .4s;
            background: initial;
            height: initial !important;
        }
    </style>
    <div class="@yield('container-class', 'container')">

        <div class="card content-area">
                <div class="card-innr">

                    <div class="card-head">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="card-title card-title-lg">Zahlungsverkehr</h4>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 mt-3">

                            <ul class="nav nav-tabs-line">
                                <li class="nav-item">
                                    <a class="nav-link @if(strpos(Route::currentRouteName(), 'calendar')) active @endif" data-toggle="" data-href="#calendar" href="{{ route('app.invoicerelease.transactions.calendar') }}">Zahlungskalender</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(strpos(Route::currentRouteName(), 'approval')) active @endif"  data-toggle="" data-href="#not_released" href="{{ route('app.invoicerelease.transactions.approval') }}">Nicht freigegeben</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(strpos(Route::currentRouteName(), 'unsettled')) active @endif"  data-toggle="" data-href="#open_previous" href="{{ route('app.invoicerelease.transactions.unsettled') }}">Überfällig</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                @yield('transactions')
                            </div>

                        </div>
                    </div>

                </div>
        </div>

    </div>

@endsection

@section('footer_scripts_extra')
    <script type="text/javascript">

        function loadBills(e) {

            $(e).css('background-color', '#eee');

            var biller_filter = $('#biller_filter').val();
            var department_filter = $('#department_filter').val();
            var mandant_filter = $('#mandant_filter').val();

            var date_year = $(e).attr('data-year');
            var date_month = $(e).attr('data-month');
            var date_day = $(e).attr('data-day');
            var order_by = $(e).attr('order_by');
            var row = $(e).attr('data-row');

            var date = date_year+'-'+date_month+'-'+date_day;

            if ($('#details').length) {
                var details =  $('#details');
                var date_details = details.attr('data-date');
                $('#cell_date_'+date_details).css('background-color', '#fff');
                details.remove();

                if(date_details==date) return true;
            }

            $.ajax({
                url: '{{ route('app.invoicerelease.transactions.getBillsByDate') }}',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "date_month" : date_month,
                    "date_year" : date_year,
                    "date_day" : date_day,
                    "biller_filter" : biller_filter,
                    "department_filter" : department_filter,
                    "mandant_filter" : mandant_filter,
                    "order_by" : order_by,
                },
                success: function(data) {
                    if(data!='') {
                        $('#' + row).append(
                            '<div id="details" data-date="'+date+'" style="display: none; border: 8px solid #eee; border-top: none;" class="col-md-12">'+
                               '<div class="row pb-3"><div style="margin-bottom: -8px; background-color: #eee;" class="col-md-12"></div></div>'+ data +
                            '</div>'
                        )
                        $('#details').show('slow');
                    }
                }
            });

        }

    </script>
@stop