<div class="table-responsive">
    <table class="table table-bordered table-striped mt-2 mb-3">
        <thead class="thead-dark">
        <tr>
            <th>Fällig am</th>
            <th>Empfangen am</th>
            <th>Mandant</th>
            <th>Rechnungssteller</th>
            <th>Rechnungsnummer</th>
            <th>Freigabeabteilung</th>
            <th>Zahlart</th>
            <th class="text-right" style="min-width: 115px;">Betrag</th>
            <th>Status</th>
            <th>Aktionen
                <i style="font-size: 18px; float: right; margin-right: 5px; margin-top:3px" data-toggle="tooltip" data-html="true" title="Beim ändern einer Aktion wird dies unmittelbar als neuer Status gespeichert!" data-original-title="Hinweis: Durch verändern einer Aktion wird diese unmittelbar als neuer Status gespeichert!" class="fas fa-info-circle"></i>
            </th>
            @headicons('1', 'fa-eye')
        </tr>
        </thead>
        <tbody>
        @forelse ( $bills AS $bill )
            <?php
            switch ($bill->status) {

                case 'not_assigned' :  $status_text='Nicht zugeordnet'; $status_color='#000'; break;
                case 'neu' :  $status_text='Wartet auf Freigabe'; $status_color='#000'; break;
                case 'vorab_freigabe' :  $status_text='Vorab-Freigegeben'; $status_color='#000'; break;
                case 'freigabe' :  $status_text='Freigegeben'; $status_color='#000'; break;
                case 'bezahlt' :  $status_text='Bezahlt'; $status_color='#2fb986 ; font-weight: bolder;'; break;
                case 'bezahlen' :  $status_text='Kann bezahlt werden'; $status_color='#000'; break;
                case 'blocken' :  $status_text='Bezahlung zurückhalten'; $status_color='#000'; break;
                case 'reklamieren' :  $status_text='Reklamiert'; $status_color='#000'; break;
                default : $status_text='Fehler'; $status_color='#000'; break;
            }
            ?>
            <tr id="">
                <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $bill->due_date)->format('d.m.Y') }}</td>
                <td class="text-right">@date($bill->created_at)</td>
                <td>{{ $bill->mandant }}</td>
                <td>{{ $bill->biller }}</td>
                <td>{{ $bill->bill_number }}</td>
                <td>{{ $bill->department }}</td>
                <td>{{ $bill->payment_method }} </td>
                <td class="text-right">@money($bill->amount)</td>
                <td  id="status_{{$bill->id}}" style="color: {{ $status_color }};">{{ $status_text }}</td>
                <td>
                    <select class="input-bordered aktion" name="aktion" data-id="{{$bill->id}}">
                        <option value="0">Bitte wählen</option>
                        <option value="blocken">Zahlung zurückstellen</option>
                        <option value="reklamieren" >Rechnung reklamieren</option>
                        <option value="bezahlt" >Als bezahlt markieren</option>
                        <!-- <option value="bezahlen" {{ $bill->status== 'bezahlen' ? 'selected="selected"' : '' }}>Zahlung einleiten</option> -->
                        <!-- <option value="blocken" {{ $bill->status== 'blocken' ? 'selected="selected"' : '' }}>Zahlung zurückstellen</option> -->
                        <!-- <option value="reklamieren" {{ $bill->status== 'reklamieren' ? 'selected="selected"' : '' }}>Rechnung reklamieren</option> -->
                        <!-- <option value="bezahlt" {{ $bill->status== 'bezahlt' ? 'selected="selected"' : '' }}>Als bezahlt markieren</option> -->
                    </select>
                </td>
                <td><a target="_blank" title="Rechnung einsehen" href="{{ route('app.invoicerelease.bills.edit', ['bill'=>$bill->id] ) }}"><i class="far fa-eye"></i></a>  </td>
            </tr>
        @empty
            <tr id=""><td colspan="100%">Keine {{ $bill->presentation_long }} vorhanden</td></tr>
        @endforelse
        </tbody>
    </table>
</div>

<script type="text/javascript">

    $('.aktion').on('change', function () {

        var status = $(this).val();
        var bill_id = $(this).attr('data-id');
        var element = $(this);
        var element_status = $('#status_'+bill_id);

        var status_text = '';
        switch (status) {
            case 'not_assigned' :  status_text='Nicht zugeordnet';  break;
            case 'neu' :  status_text='Wartet auf Freigabe';  break;
            case 'vorab_freigabe' :  status_text='Vorab-Freigegeben'; break;
            case 'freigabe' :  status_text='Freigegeben';  break;
            case 'bezahlt' :  status_text='Bezahlt';  break;
            case 'bezahlen' :  status_text='Kann bezahlt werden';  break;
            case 'blocken' :  status_text='Bezahlung zurückhalten';  break;
            case 'reklamieren' :  status_text='Reklamiert'; break;
            default : status_text='Fehler';  break;
        }

        $.ajax({
            url: '{{ route('app.invoicerelease.transactions.status') }}',
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "status" : status,
                "id" : bill_id,
            },
            success: function(data) {
                var color_first = 'initial';
                var color_last = '#e81515';
                if(data) {
                    color_first = '#54ff5e';
                    color_last = 'initial';
                }
                else {
                    element.css('color', '#fff');
                }
                element.css('background', color_first);
                element_status.html(status_text);
                setTimeout(function(){
                    element.css('background', color_last);
                }, 750);
            }
        });

    })

</script>