@section('container-class') container-fluid @endsection
@extends('app.invoicerelease.transactions.index')
@section('transactions')

        <div id="calendar" class="tab-pane mt-3 active">
            <div class="row">
                <div class="col-md-12">

                    <form id="submit-form" method="get" action="{{ route('app.invoicerelease.transactions.calendar') }}">
                        <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                        @csrf

                        <div class="input-group">
                            <a title="Vorherigen Monat anzeigen"  style=" border: 1px solid #ced4da; border-right: 0px; border-radius: 4px 0px 0px 4px; line-height: 35px; min-width: 35px; text-align: center;" href="{{ route('app.invoicerelease.transactions.calendar', ['date_month' => $calendar_settings['last_month'], 'date_year' => $calendar_settings['last_year'], 'biller_filter' => $filter_attributes['biller_filter'] ?? '', 'department_filter' => $filter_attributes['department_filter'] ?? '', 'mandant_filter' => $filter_attributes['mandant_filter'] ?? ''  ]) }}"><i class="fas fa-angle-left"></i></a>
                            <select class="form-control" name="date_month" id="date_month" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; height: 37px !important;">
                                <?php
                                $months=['empty', 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober',  'November',  'Dezember'];
                                for ($i=1; $i<=12; $i++) {
                                    echo '<option value="'.$i.'"'.  ( (isset($filter_attributes['date_month']) && $filter_attributes['date_month'] == $i)  ? "selected='selected'" : "" ) .'>'.$months[$i].'</option>';
                                }
                                ?>
                            </select>
                            <select class="form-control" name="date_year" id="date_year" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; height: 37px !important;">
                                <?php
                                for ($i=($calendar_settings['year']-10); $i<=($calendar_settings['year']+1); $i++) {
                                   echo '<option value="'.$i.'"'.  ( (isset($filter_attributes['date_year']) && $filter_attributes['date_year'] == $i) ? "selected='selected'" : "" ) .'>'.$i.'</option>';
                                }
                                ?>
                            </select>
                            <a title="Nächsten Monat anzeigen"  style=" border: 1px solid #ced4da; border-left: 0px;line-height: 35px; min-width: 37px; text-align: center;" href="{{ route('app.invoicerelease.transactions.calendar', ['date_month' => $calendar_settings['next_month'], 'date_year' => $calendar_settings['next_year'], 'biller_filter' => $filter_attributes['biller_filter'] ?? '', 'department_filter' => $filter_attributes['department_filter'] ?? '', 'mandant_filter' => $filter_attributes['mandant_filter'] ?? ''  ] ) }}"><i class="fas fa-angle-right"></i></a>
                            <div class="input-group-append" style="flex: content; z-index: 0;">
                                <select style="color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;" class="selectpicker form-control" name="biller_filter" id="biller_filter"  data-actions-box="true">
                                    <option value="0">Alle Rechnungssteller</option>
                                    @foreach(\App\IrBiller::all() AS $biller)
                                        <option value="{{$biller->id}}"  {{ ( isset($filter_attributes['biller_filter']) && $filter_attributes['biller_filter'] == $biller->id) || ($biller->id == old('biller_filter')) ? 'selected="selected"' : '' }}  >{{$biller->name}}</option>
                                    @endforeach
                                </select>
                                <select style="color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;" class="form-control" name="department_filter" id="department_filter">
                                    <option value="0">Alle Abteilungen</option>
                                    @foreach(\App\Department::all() AS $department)
                                        <option value="{{$department->id}}" {{ ( isset($filter_attributes['department_filter']) && $filter_attributes['department_filter'] == $department->id) || ($department->id == old('department_filter')) ? 'selected="selected"' : '' }}  >{{$department->name}}</option>
                                    @endforeach
                                </select>
                                <select style="color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;" class="form-control" name="mandant_filter" id="mandant_filter">
                                    <option value="0">Alle Mandanten</option>
                                    @foreach(\App\Mandant::all() AS $mandant)
                                        <option value="{{$mandant->id}}"   {{ ( isset($filter_attributes['mandant_filter']) && $filter_attributes['mandant_filter'] == $mandant->id) || ($mandant->id == old('mandant_filter')) ? 'selected="selected"' : '' }}  >{{$mandant->name}}</option>
                                    @endforeach
                                </select>
                                <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                                <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.invoicerelease.transactions.calendar' ) }}"><i class="fas fa-redo"></i></a>
                            </div>
                        </div>

                    </form>


                    <!-- Calendar -->
                    <div class="calendar p-2 mt-4">
                        <ol class="day-names list-unstyled">
                            <div class="row">
                                <li class="font-weight-bold text-uppercase">Montag</li>
                                <li class="font-weight-bold text-uppercase">Dienstag</li>
                                <li class="font-weight-bold text-uppercase">Mittwoch</li>
                                <li class="font-weight-bold text-uppercase">Donnerstag</li>
                                <li class="font-weight-bold text-uppercase">Freitag</li>
                                <li class="font-weight-bold text-uppercase">Samstag</li>
                                <li class="font-weight-bold text-uppercase">Sonntag</li>
                                <li class="font-weight-bold text-uppercase">Kalenderwoche</li>
                            </div>
                        </ol>

                        <ol class="days list-unstyled">
                        <?php
                        $day_count = 0;

                        $month_offen=0;
                        $month_bezahlt=0;
                        $month_offen_anona=0;

                        for($i=0; $i<$calendar_settings['cells_sum_total']; $i++) {

                            $start_of_week = 0;
                            $ende_of_week = 0;
                            $tag_offen=0;
                            $tag_bezahlt=0;
                            $today = 0;
                            $offset=0;

                            if($i==0 || $i%$calendar_settings['cells_per_week']==0 ) {
                                $start_of_week=1;
                            }
                            if(($day_count+1) == $calendar_settings['day'] && $calendar_settings['month']==$calendar_settings['month_real'] && $calendar_settings['year']==$calendar_settings['year_real']) $today=1;
                            if($i!=0 && $i%$calendar_settings['cells_per_week']==($calendar_settings['cells_per_week']-1)) $ende_of_week=1;

                            if($i<$calendar_settings['cells_offset_start'] || $i>=$calendar_settings['cells_sum_total']-$calendar_settings['cells_offset_end']) {
                                $offset=1;
                            }
                            elseif(!$offset && !$ende_of_week && $day_count<$calendar_settings['last_of_month']) {
                                $day_count++;
                            }

                            $calender_cell='';
                            if(!(env('APP_DEBUG') && 0)) {  // set on 1 to show index of cell

                                if($start_of_week) {
                                    $week_offen=0;
                                    $week_bezahlt=0;
                                    $week_offen_anona=0;

                                   # $full_date = $calendar_settings['year'].'-'.str_pad($calendar_settings['month'], 2, 0, STR_PAD_LEFT).'-'.str_pad($day_count, 2, 0, STR_PAD_LEFT);
                                    $full_date = $calendar_settings['year'].'-'.$calendar_settings['month'].'-'.$day_count;
                                    $kw =  \Carbon\Carbon::parse(date($full_date))->week;

                                    $calender_cell.='<div class="row" id="kw_'.$kw.'">';
                                }

                                $string_content_cell = '';
                                if(array_key_exists($day_count, $calendar_settings['data'])) {

                                    $week_offen_anona+=$calendar_settings['data'][$day_count]['offen_anona'];

                                    foreach ($calendar_settings['data'][$day_count]['methods'] AS $key => $method) {

                                        $tag_offen+=$method['offen'];
                                        $tag_bezahlt+=$method['bezahlt'];
                                        $title='Offen: '.$method['offen'].'€ / Bezahlt: '.$method['bezahlt'].'€';
                                        $display_sum=$method['offen'];
                                        $css='text-secondary';

                                        if($method['offen']==0) {
                                            $css='text-success';
                                            $display_sum=$method['bezahlt'];
                                        }

                                        $string_content_cell.='<div style="width: 100%; display: block;">'.$key;
                                        $string_content_cell.='<span class="float-right">';
                                        $string_content_cell.='<span class="'.$css.'" style="cursor:help;" title="'.$title.'">'.number_format(str_replace(',', '.', $display_sum), 2, ',', ' ').'</span>';
                                        $string_content_cell.='</span>';
                                        $string_content_cell.='</div>';

                                    }
                                }

                                if($offset || $ende_of_week) {
                                    if($ende_of_week) {

                                        $month_offen+=$week_offen;
                                        $month_bezahlt+=$week_bezahlt;
                                        $month_offen_anona+=$week_offen-$week_offen_anona;

                                        $calender_cell.='<li>';
                                        $calender_cell.='<div style="height: 100%; position: relative;">';
                                        $calender_cell.='<div class="date">';
                                        $calender_cell.='<button style="padding: 0px; min-width:unset; width:30px; float:right; margin-right:5px; color:#fd49d0; cursor: default" type="button" class="btn btn-circle" >'.$kw.' </button>';
                                        $calender_cell.='</div>';
                                        $calender_cell.='<div style="position: absolute; bottom:5px; left:5px; width: 95%;">';
                                        $calender_cell.='<div style="width: 100%;">';
                                        $calender_cell.='<span class="text-right d-block">Woche offen:</span>';
                                        $calender_cell.='<span class="text-right d-block font-bold text-secondary">'.number_format(str_replace(',', '.', $week_offen), 2, ',', ' ').'</span>';
                                        $calender_cell.='</div>';
                                        $calender_cell.='<div style="width: 100%;">';
                                        $calender_cell.='<span class="text-right d-block">Ohne Anona:</span>';
                                        $calender_cell.='<span class="text-right d-block font-bold text-danger">'.number_format(str_replace(',', '.', $week_offen-$week_offen_anona), 2, ',', ' ').'</span>';
                                        $calender_cell.='</div>';
                                        $calender_cell.='<div style="width: 100%;" class="mt-2">';
                                        $calender_cell.='<span class="text-right d-block">Woche bezahlt:</span>';
                                        $calender_cell.='<span class="text-right d-block font-bold text-info">'.number_format(str_replace(',', '.', $week_bezahlt), 2, ',', ' ').'</span>';
                                        $calender_cell.='</div>';
                                        $calender_cell.='</div>';
                                        $calender_cell.='</div>';
                                        $calender_cell.='</li>';
                                    }
                                    else {
                                        $calender_cell.='<li class="outside"></li>';
                                    }
                                }
                                else {
                                    $calender_cell.='<li id="cell_date_'.$calendar_settings['year'].'-'.$calendar_settings['month'].'-'.$day_count.'" onclick="loadBills(this)" style="cursor:pointer" class="pb-1" order_by="ir_billers.name|ir_bills.amount" data-year="'.$calendar_settings['year'].'"  data-month="'.$calendar_settings['month'].'"  data-day="'.$day_count.'" data-row="kw_'.$kw.'">';
                                    $calender_cell.='<div style="height: 100%; padding-bottom: 3.5em;">';
                                    $calender_cell.='<div class="date">';
                                    $calender_cell.='<button style="padding: 0px; min-width:unset; width:30px; '.($today?' border:0px; color: #fff !important;  background-color: #d73448 !important;':'').'" class="btn '.($today?'text-danger':'text-muted').' btn-circle">'.($offset?'':$day_count).' </button>';
                                    $calender_cell.='</div>';
                                    $calender_cell.='<div class="content">';
                                    $calender_cell.=$string_content_cell;
                                    $calender_cell.='</div>'; // Ende Content
                                    $calender_cell.='</div>';
                                    $calender_cell.='<div class="content-sum" style="margin-top: -3.5em;">';
                                    if($tag_offen) {
                                        $calender_cell.='<div style="font-weight: bolder" >Gesamt offen:<span class="float-right"><span class="text-secondary">'.number_format(str_replace(',', '.', $tag_offen), 2, ',', ' ').'</span></span></div>';
                                    }
                                    else {
                                        $calender_cell.='<div style="font-weight: bolder" >&nbsp;<span class="float-right"><span class="text-secondary">&nbsp;</span></span></div>';
                                    }
                                    $calender_cell.='<div style="width: 100%; display: block; font-weight: bolder" >Gesamt bezahlt:<span class="float-right"><span class="text-success">'.number_format(str_replace(',', '.', $tag_bezahlt), 2, ',', ' ').'</span></span></div>';
                                    $calender_cell.='</div>';
                                    $calender_cell.='</li>';

                                    $week_offen+=$tag_offen;
                                    $week_bezahlt+=$tag_bezahlt;
                                }

                                if($ende_of_week) $calender_cell.='</div>';
                            }

                            // debug
                            else {
                                if($start_of_week) $calender_cell.='<div class="row">';
                                if($offset || $ende_of_week) {
                                    if($offset ) {
                                        $calender_cell.='<li class="outside">';
                                        $calender_cell.='<div class="date">'.(empty($offset)?$day_count:'').'</div>';
                                        $calender_cell.='<div class="content">';
                                        $calender_cell.='<span style="width: 100%; display: block;" >mod:<span class="float-right">'.($i!=0?$i%$calendar_settings['cells_per_week']:'').'</span></span>';
                                        $calender_cell.='<span style="width: 100%; display: block;" >cells_per_week:<span class="float-right">'.$calendar_settings['cells_per_week'].'</span></span>';
                                        $calender_cell.='<span style="width: 100%; display: block;" >index:<span class="float-right">'.$i.'</span></span>';
                                        $calender_cell.='<span style="width: 100%; display: block;" >daycount:<span class="float-right">'.$day_count.'</span></span>';
                                        $calender_cell.='<span style="width: 100%; display: block;" >start_of_week:<span class="float-right">'.$start_of_week.'</span></span>';
                                        $calender_cell.='<span style="width: 100%; display: block;" >ende_of_week:<span class="float-right">'.$ende_of_week.'</span></span>';
                                        $calender_cell.='<span style="width: 100%; display: block;" >offset:<span class="float-right">'.$offset.'</span></span>';
                                        $calender_cell.='</div>';
                                        $calender_cell.='</li>';
                                    }
                                    if($ende_of_week && $day_count<$calendar_settings['last_of_month']) {
                                        $calender_cell.='<li class="">KW</li>';
                                    }
                                }
                                else {
                                    $calender_cell.='<li>';
                                    $calender_cell.='<div class="date">'.(empty($offset)?$day_count:'').'</div>';
                                    $calender_cell.='<div class="content">';
                                    $calender_cell.='<span style="width: 100%; display: block;" >mod:<span class="float-right">'.($i!=0?$i%$calendar_settings['cells_per_week']:'').'</span></span>';
                                    $calender_cell.='<span style="width: 100%; display: block;" >cells_per_week:<span class="float-right">'.$calendar_settings['cells_per_week'].'</span></span>';
                                    $calender_cell.='<span style="width: 100%; display: block;" >index:<span class="float-right">'.$i.'</span></span>';
                                    $calender_cell.='<span style="width: 100%; display: block;" >daycount:<span class="float-right">'.$day_count.'</span></span>';
                                    $calender_cell.='<span style="width: 100%; display: block;" >start_of_week:<span class="float-right">'.$start_of_week.'</span></span>';
                                    $calender_cell.='<span style="width: 100%; display: block;" >ende_of_week:<span class="float-right">'.$ende_of_week.'</span></span>';
                                    $calender_cell.='<span style="width: 100%; display: block;" >offset:<span class="float-right">'.$offset.'</span></span>';
                                    $calender_cell.='</div>';
                                    $calender_cell.='</li>';
                                }

                                if($ende_of_week) $calender_cell.='</div>';
                            }
                            echo $calender_cell;
                        }
                        ?>
                        </ol>

                    </div>
                </div>
                </div>
            </div>


            <div class="row mt-3">
                <div class="col-md-6">
                    <div style="width: 100%;">
                        <span class="text-left d-block">Gesamt offen:</span>
                        <span class="text-left d-block font-bold text-secondary"><?php echo number_format(str_replace(',', '.', $total_open['open']), 2, ',', ' '); ?></span>
                    </div>
                    <div style="width: 100%;">
                        <span class="text-left d-block">Ohne Anona:</span>
                        <span class="text-left d-block font-bold text-danger"><?php echo number_format(str_replace(',', '.', $total_open['open_anona']), 2, ',', ' '); ?></span>
                    </div>
                    <div style="width: 100%;" class="mt-2">
                        <span class="text-left d-block">Überfällig:</span>
                        <span class="text-left d-block font-bold text-danger"><?php echo number_format(str_replace(',', '.', $total_open['unsettled']), 2, ',', ' '); ?></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div style="width: 100%;">
                        <span class="text-right d-block">Monat offen:</span>
                        <span class="text-right d-block font-bold text-secondary"><?php echo number_format(str_replace(',', '.', $month_offen), 2, ',', ' '); ?></span>
                    </div>
                    <div style="width: 100%;">
                        <span class="text-right d-block">Ohne Anona:</span>
                        <span class="text-right d-block font-bold text-danger"><?php echo number_format(str_replace(',', '.', $month_offen_anona), 2, ',', ' '); ?></span>
                    </div>
                    <div style="width: 100%;" class="mt-2">
                        <span class="text-right d-block">Monat bezahlt:</span>
                        <span class="text-right d-block font-bold text-info"><?php echo number_format(str_replace(',', '.', $month_bezahlt), 2, ',', ' '); ?></span>
                    </div>
                </div>
            </div>



        </div>

@endsection
