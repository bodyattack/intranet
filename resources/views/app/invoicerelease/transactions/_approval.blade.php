@section('container-class') container-fluid @endsection
@extends('app.invoicerelease.transactions.index')
@section('transactions')

    <div id="calendar" class="tab-pane mt-3 active">
        <div class="row">

            <div class="col-md-12">

                <form id="submit-form" method="get" action="{{ route('app.invoicerelease.transactions.approval') }}">
                    <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                    @csrf

                    <div class="input-group">
                        <select class="form-control" name="status" id="status" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; height: 37px !important;">
                            <option value="" >Jeder Status</option>
                            <option value="not_assigned" {{ ( isset($filter['status']) && $filter['status'] == 'not_assigned') || ('not_assigned' == old('status')) ? 'selected="selected"' : '' }} >Nicht zugeordnet</option>
                            <option value="neu"  {{ ( isset($filter['status']) && $filter['status'] == 'neu') || ('neu' == old('status')) ? 'selected="selected"' : '' }} >Wartet auf Freigabe</option>
                            <option value="vorab_freigabe" {{ ( isset($filter['status']) && $filter['status'] == 'vorab_freigabe') || ('vorab_freigabe' == old('status')) ? 'selected="selected"' : '' }}  >Vorab-Freigegeben</option>
                            <option value="freigabe" {{ ( isset($filter['status']) && $filter['status'] == 'freigabe') || ('freigabe' == old('status')) ? 'selected="selected"' : '' }}  >Freigegeben</option>
                            <option value="bezahlt" {{ ( isset($filter['status']) && $filter['status'] == 'bezahlt') || ('bezahlt' == old('status')) ? 'selected="selected"' : '' }} >Bezahlt</option>
                            <option value="bezahlen"  {{ ( isset($filter['status']) && $filter['status'] == 'bezahlen') || ('bezahlen' == old('status')) ? 'selected="selected"' : '' }} >Bezahlen</option>
                            <option value="blocken"  {{ ( isset($filter['status']) && $filter['status'] == 'blocken') || ('blocken' == old('status')) ? 'selected="selected"' : '' }} >Bezahlung zurückhalten</option>
                            <option value="reklamieren"  {{ ( isset($filter['status']) && $filter['status'] == 'reklamieren') || ('reklamieren' == old('status')) ? 'selected="selected"' : '' }} >Reklamiert</option>
                            <option value="swimm"  {{ ( isset($filter['status']) && $filter['status'] == 'swimm') || ('swimm' == old('status')) ? 'selected="selected"' : '' }} >Schwimmende Ware</option>
                            <option value="transit"  {{ ( isset($filter['status']) && $filter['status'] == 'transit') || ('transit' == old('status')) ? 'selected="selected"' : '' }} >Ware in Transit</option>
                            <option value="skonto"  {{ ( isset($filter['status']) && $filter['status'] == 'skonto') || ('skonto' == old('status')) ? 'selected="selected"' : '' }} >Skonto möglich</option>
                            <option value="unsettled"  {{ ( isset($filter['status']) && $filter['status'] == 'unsettled') || ('unsettled' == old('status')) ? 'selected="selected"' : '' }} >Überfällig</option>
                        </select>
                        <div class="input-group-append" style="flex: content; z-index: 1;">
                            <select style="color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;" class="selectpicker form-control" name="biller" id="biller"  data-actions-box="true">
                                <option value="">Alle Rechnungssteller</option>
                                @foreach(\App\IrBiller::all() AS $biller)
                                    <option value="{{$biller->id}}"  {{ ( isset($filter['biller']) && $filter['biller'] == $biller->id) || ($biller->id == old('biller')) ? 'selected="selected"' : '' }}  >{{$biller->name}}</option>
                                @endforeach
                            </select>
                            <select style="color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;" class="form-control" name="department" id="department">
                                <option value="">Alle Abteilungen</option>
                                @foreach(\App\Department::all() AS $department)
                                    <option value="{{$department->id}}" {{ ( isset($filter['department']) && $filter['department'] == $department->id) || ($department->id == old('department')) ? 'selected="selected"' : '' }}  >{{$department->name}}</option>
                                @endforeach
                            </select>
                            <select style="color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;" class="form-control" name="mandant" id="mandant">
                                <option value="">Alle Mandanten</option>
                                @foreach(\App\Mandant::all() AS $mandant)
                                    <option value="{{$mandant->id}}"   {{ ( isset($filter['mandant']) && $filter['mandant'] == $mandant->id) || ($mandant->id == old('mandant')) ? 'selected="selected"' : '' }}  >{{$mandant->name}}</option>
                                @endforeach
                            </select>
                            <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                            <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.invoicerelease.transactions.approval' ) }}"><i class="fas fa-redo"></i></a>
                        </div>
                    </div>

                </form>

                @include('app.invoicerelease.transactions._billsTableWithOptionsSortable')

            </div>
        </div>

        <div class="row justify-content-md-center">
            {{ $bills->withQueryString()->links() }}
        </div>

    </div>
    </div>

@endsection
