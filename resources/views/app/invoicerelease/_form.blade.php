<?php
$swimm_text = '';
$transit_text = '';
if(!empty($bill->delivery_date) && !empty($bill->date)) {
    $month_date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $bill->date)->month;
    $month_delivery_date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $bill->delivery_date)->month;
    if($month_date!=$month_delivery_date && $bill->date < $bill->delivery_date) {
        $swimm_text = '<span title="Schwimmende Ware"><i style="color: #01acff" class="fas fa-fish"></i>&nbsp;<i style="color: #01acff" class="fas fa-fish"></i></span>';
    }
    if($month_date!=$month_delivery_date && $bill->date > $bill->delivery_date) {
        $transit_text = '<span title="Ware in Transit"><i style="color: #ce622d" class="fas fa-dolly-flatbed"></i></span>';
    }
}
?>
<input id="created_at" type="hidden" name="created_at" value="{{ $bill->created_at }}">

<div class="row">
    <div class="col-md-12">

        @if(!$userBuchhaltung)
        <div class="row">
            <div class="col-md-4 text-md-right">Mandant:</div>
            <div class="col-md-2">{{ ( empty($bill->mandant) ? '' : $bill->mandant->short_name) }}</div>
            <div class="col-md-2 text-md-right">Rechnungssteller:</div>
            <div class="col-md-4">{{ (empty(!$bill->biller) ? $bill->biller->name : '') }}</div>
        </div>

        <div class="row">
            <div class="col-md-4 text-md-right">Kostenstelle:</div>
            <div class="col-md-2">{{ (empty(!$bill->costcenter) ? $bill->costcenter->name : '') }}</div>
            <div class="col-md-2 text-md-right">Zahlart:</div>
            <div class="col-md-4 ">{{$bill->payment_method_method}}</div>
        </div>

        <div class="row">
            <div class="col-md-4 text-md-right">Eingangsdatum:</div>
            <div class="col-md-2">@date($bill->created_at) </div>
            <div class="col-md-2 text-md-right">Hinweis:</div>
            <div class="col-md-4 "> {!! $swimm_text . $transit_text !!} </div>
        </div>

        <input id="department_id" type="hidden" name="department_id" value="{{ $bill->department_id }}">
        <input id="costcenter_id" type="hidden" name="costcenter_id" value="{{ $bill->costcenter_id }}">
        <input id="payment_method" type="hidden" name="payment_method" value="{{ $bill->payment_method }}">

        @else
        <div class="row">
            <div class="col-md-4 text-md-right">Mandant:</div>
            <div class="col-md-2">{{ ( empty($bill->mandant) ? '' : $bill->mandant->short_name) }}</div>
            <div class="col-md-2 text-md-right">Eingangsdatum:</div>
            <div class="col-md-4">@date($bill->created_at)</div>
        </div>
        <div class="row">
            <div class="col-md-4 text-md-right">Hinweis:</div>
            <div class="col-md-8 ">
                <?php
                if($bill->biller!=NULL && $bill->biller_id != 0 && !empty($bill->date) && !empty($discount_days = $bill->biller->discount_days)) {
                    $now = Carbon\Carbon::now();
                    $discount_date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $bill->date)->addDays($discount_days);
                    if($now<$discount_date)  {
                       echo '<span title="Skonto in Höhe von '.$bill->biller->discount.'% möglich"><i style="color: #f2610b" class="fas fa-euro-sign"></i><i style="color: #f2610b" class="fas fa-euro-sign"></i></span>';
                    }
                }
                ?>
                {!! $swimm_text . $transit_text !!}
            </div>
        </div>

        @endif

    </div>
</div>

<hr>

@if($userBuchhaltung)
    <div class="form-group row">

        <label for="biller_id" class="col-md-4 col-form-label text-md-right">Rechnungssteller</label>
        <div class="col-md-6">
            <div class="input-with-label">
                <div class="select-wrapper">
                    <select  class="input-bordered form-control" name="biller_id" id="biller_id"  data-actions-box="false">
                        <option value="0">Bitte wählen</option>
                        <?php
                        $biller_biller_id = $bill->biller_id;
                        if($bill->biller!=NULL && $biller_biller_id!=0) {
                            $biller_biller_id = $bill->biller->id;
                        }
                        ?>
                        @foreach(\App\IrBiller::with('distributor')->orderBy('name')->get() AS $biller)
                           <option value="{{$biller->id}}" {{ old('biller_id', $biller_biller_id) == $biller->id ? 'selected="selected"' : '' }} >{{$biller->name}} {{ ($biller->distributor->isEmpty()?'(Freigabe-Abteilung fehlt!)':'') }}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            @error('biller_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="department_id" class="col-md-4 col-form-label text-md-right">Abteilung / Zuteilen</label>

        <div class="col-md-6">
            <div class="input-with-label">
                <div class="select-wrapper">
                    <select class="input-bordered" name="department_id" id="department_id">
                        <option value="0">Bitte wählen</option>
                        @foreach(\App\Department::withInvoiceApprovers() AS $department)
                            <option value="{{$department->id}}" {{ old('department_id', $bill->department_id) == $department->id ? 'selected="selected"' : '' }} >{{$department->name}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            @error('department_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="costcenter_id" class="col-md-4 col-form-label text-md-right">Kostenstelle</label>

        <div class="col-md-6">
            <div class="input-with-label">
                <div class="select-wrapper">
                    <select class="selectpicker input-bordered form-control"  name="costcenter_id" id="costcenter_id" data-actions-box="true">
                        <option value="0">Bitte wählen</option>
                        @foreach(\App\IrCostcenter::orderBy('name')->get() AS $costcenter)
                            <option value="{{$costcenter->id}}" {{ old('costcenter_id', $bill->costcenter_id) == $costcenter->id ? 'selected="selected"' : '' }} >{{$costcenter->name}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            @error('costcenter_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="payment_method" class="col-md-4 col-form-label text-md-right">Zahlart</label>

        <div class="col-md-6">
            <div class="input-with-label">
                <div class="select-wrapper">
                    <select class="input-bordered" name="payment_method" id="payment_method">
                        <option value="0" {{ old('payment_method', $bill->payment_method) == '0' ? 'selected="selected"' : '' }}>Bitte wählen</option>
                        <option value="Amazon" {{ old('payment_method', $bill->payment_method) == 'Amazon' ? 'selected="selected"' : '' }}>Amazon</option>
                        <option value="Bar" {{ old('payment_method', $bill->payment_method) == 'Bar' ? 'selected="selected"' : '' }}>Bar</option>
                        <option value="Einzug" {{ old('payment_method', $bill->payment_method) == 'Einzug' ? 'selected="selected"' : '' }}>Einzug</option>
                        <option value="Intern" {{ old('payment_method', $bill->payment_method) == 'Intern' ? 'selected="selected"' : '' }}>Intern</option>
                        <option value="Kreditkarte" {{ old('payment_method', $bill->payment_method) == 'Kreditkarte' ? 'selected="selected"' : '' }}>Kreditkarte</option>
                        <option value="PayPal" {{ old('payment_method', $bill->payment_method) == 'PayPal' ? 'selected="selected"' : '' }}>PayPal</option>
                        <option value="Rechnung" {{ old('payment_method', $bill->payment_method) == 'Rechnung' ? 'selected="selected"' : '' }}>Rechnung</option>
                        <option value="Verrechnung" {{ old('payment_method', $bill->payment_method) == 'Verrechnung' ? 'selected="selected"' : '' }}>Verrechnung</option>
                        <option value="Vorkasse" {{ old('payment_method', $bill->payment_method) == 'Vorkasse' ? 'selected="selected"' : '' }}>Vorkasse</option>
                    </select>
                </div>
            </div>
            @error('payment_method')
            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
            @enderror
        </div>
    </div>
@endif

<div class="form-group row">

    <label for="amount" class="col-md-4 col-form-label text-md-right">Betrag / Währung / Weiterberechnung</label>

    <div class="col-md-6  input-group">
        <input onclick="$(this).select();" name="amount" id="amount" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" value="{{ old('amount', $bill->amount) }}">
        <div class="input-group-append">
            <select @if(!$userBuchhaltung) disabled @endif name="currency" id="currency" style="{{ $userBuchhaltung ? 'color:#495057;' : '' }} border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                <option value="EUR" {{ old('currency', $bill->currency) == 'EUR' ? 'selected="selected"' : '' }}>EUR</option>
                <option value="GBP" {{ old('currency', $bill->currency) == 'GBP' ? 'selected="selected"' : '' }}>GBP</option>
                <option value="USD" {{ old('currency', $bill->currency) == 'USD' ? 'selected="selected"' : '' }}>USD</option>
                <option value="CHF" {{ old('currency', $bill->currency) == 'CHF' ? 'selected="selected"' : '' }}>CHF</option>
                <option value="AUD" {{ old('currency', $bill->currency) == 'AUD' ? 'selected="selected"' : '' }}>AUD</option>
            </select>
        </div>
        <div class="input-group-append">
            <select name="onward_debit" id="onward_debit" style="color:#495057; border: 1px solid #ced4da; border-radius: 0px 4px 4px 0px; height: 37px !important;">
                <option value="0" {{ old('onward_debit', $bill->onward_debit) == '0' ? 'selected="selected"' : '' }}>Keine</option>
                <option value="1" {{ old('onward_debit', $bill->onward_debit) == '1' ? 'selected="selected"' : '' }}>Weiterbelastet</option>
            </select>
        </div>
        <!--
        <div class="input-group-append">
            <select  name="deposit " id="deposit" style="color:#495057; border: 1px solid #ced4da; border-radius: 0px 4px 4px 0px; height: 37px !important;">
                <option value="0" {{ old('deposit', $bill->deposit) == '0' ? 'selected="selected"' : '' }}>Vollbezahlt</option>
                <option value="1" {{ old('deposit', $bill->deposit) == '1' ? 'selected="selected"' : '' }}>Anzahlung</option>
            </select>
        </div>
        --->
        @if ($errors->has('amount'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('amount') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group row">
    <label for="date_dummy" class="col-md-4 col-form-label text-md-right">Rechnungsdatum</label>
    <div class="col-md-6">
        <?php
        $date_dummy = old('date_dummy', (!empty($bill->date)? $bill->date :''));
        $date = old('date', $bill->date);
        ?>
            <i data-toggle="tooltip" data-html="true" onclick="removedate('date')" data-original-title="Datum entfernen"  style="right: 20px; font-size: 20px; position: absolute; margin: 10px; color: #e69b9b;" class="fas fa-times"></i>
        <input style="background: white" autocomplete="off" id="date_dummy" type="text"  data-target="date" class="datefield form-control{{ $errors->has('date_dummy') ? ' is-invalid' : '' }}" name="date_dummy" value="@date($date_dummy)">
        <input id="date" type="hidden" name="date" value="@dateDB($date)">

        @if ($errors->has('date'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('amount') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="due_date_dummy" class="col-md-4 col-form-label text-md-right">Fälligkeitsdatum</label>
    <div class="col-md-6">
        <?php
        $due_date_dummy= old('due_date_dummy', (!empty($bill->due_date)? $bill->due_date :''));
        $due_date= old('due_date', $bill->due_date);
        ?>
            <i data-toggle="tooltip" data-html="true" onclick="removedate('due_date')" data-original-title="Datum entfernen"  style="right: 20px; font-size: 20px; position: absolute; margin: 10px; color: #e69b9b;" class="fas fa-times"></i>
        <input style="background: white" autocomplete="off" id="due_date_dummy" type="text"  data-target="due_date" class="datefield form-control{{ $errors->has('due_date_dummy') ? ' is-invalid' : '' }}" name="due_date_dummy"  value="@date($due_date_dummy)">
        <input id="due_date" type="hidden" name="due_date" value="@dateDB($due_date)">

        @if ($errors->has('due_date'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('amount') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="delivery_date_dummy" class="col-md-4 col-form-label text-md-right">Lieferdatum</label>
    <div class="col-md-6">
        <?php
        $delivery_date_dummy= old('delivery_date_dummy', (!empty($bill->delivery_date)? $bill->delivery_date :''));
        $delivery_date= old('delivery_date', $bill->delivery_date);
        ?>
            <i data-toggle="tooltip" data-html="true" onclick="removedate('delivery_date')" data-original-title="Datum entfernen"  style="right: 20px; font-size: 20px; position: absolute; margin: 10px; color: #e69b9b;" class="fas fa-times"></i>
        <input style="background: white" autocomplete="off" id="delivery_date_dummy" type="text"  data-target="delivery_date" class="datefield form-control{{ $errors->has('delivery_date_dummy') ? ' is-invalid' : '' }}" name="delivery_date_dummy"  value="@date($delivery_date_dummy)">
        <input id="delivery_date" type="hidden" name="delivery_date" value="@dateDB($delivery_date)">

        @if ($errors->has('delivery_date'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('amount') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="bill_number" class="col-md-4 col-form-label text-md-right">Rechnungsnummer</label>

    <div class="col-md-6">
        <input id="bill_number" type="text" class="form-control{{ $errors->has('bill_number') ? ' is-invalid' : '' }}" name="bill_number" value="{{ old('bill_number', $bill->bill_number) }}">

        @if ($errors->has('bill_number'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('bill_number') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="process_number" class="col-md-4 col-form-label text-md-right">Vorgangsnummer</label>

    <div class="col-md-6">
        <input id="process_number" type="text" class="form-control{{ $errors->has('process_number') ? ' is-invalid' : '' }}" name="process_number" value="{{ old('process_number', $bill->process_number) }}">

        @if ($errors->has('process_number'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('process_number') }}</strong>
            </span>
        @endif
    </div>
</div>


<div id="options" class="form-group row" style="display: none">
    <label for="status" class="col-md-4 col-form-label text-md-right">Reklamation Textbausteine</label>

    <div class="col-md-6">
        <div class="input-with-label">
            <div class="select-wrapper">
                <select onchange="pushOptionsToComment(this)"  class="input-bordered" name="status" id="status">
                    <option value="0">Mustertext für Bemerkung wählen?</option>
                    <option value="kostestelle">Kostenstelle falsch</option>
                    <option value="betrag" >Rechnungsbetrag nicht korrekt</option>
                    <option value="doppelt">Rechnung doppelt</option>
                    <option value="rechnungsteller">Rechnungssteller falsch</option>
                    <option value="andere">Keiner der genannten</option>
                </select>
            </div>
        </div>
        @error('status')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

</div>



<div class="form-group row">
    <label for="comment" class="col-md-4 col-form-label text-md-right">Bemerkung</label>

    <div class="col-md-6">
        <textarea rows="2" id="comment" type="text" class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}" name="comment">{{ old('comment', $bill->comment) }}</textarea>
        @if ($errors->has('comment'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('comment') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group row">
    <label for="status" class="col-md-4 col-form-label text-md-right">Aktion</label>

    <div class="col-md-6">
        <div class="input-with-label">
            <div class="select-wrapper">
                <select  onchange="displayOptions(this)" class="input-bordered" name="status" id="status">
                    @if($userBuchhaltung)
                        <option value="save" {{ old('status', $bill->status) == 'save' ? 'selected="selected"' : '' }}>Speichern</option>
                        <option value="neu" {{ old('status', $bill->status) == 'neu' ? 'selected="selected"' : '' }}>Erneut zur Verteilung</option>
                        <option value="vorab_freigabe" {{ old('status', $bill->status) == 'vorab_freigabe' ? 'selected="selected"' : '' }}>Vorab-Freigeben</option>
                        @if(array_key_exists('rg-reklamation-buha', $entry['permissions']))
                            <option value="reklamieren" {{ old('status', $bill->status) == 'reklamieren' ? 'selected="selected"' : '' }}>Reklamieren</option>
                        @endif
                        <option value="blocken" {{ old('status', $bill->status) == 'blocken' ? 'selected="selected"' : '' }}>Zurückstellen</option>
                        <option value="bezahlen" {{ old('status', $bill->status) == 'bezahlen' ? 'selected="selected"' : '' }}>Bezahlen</option>
                        <option value="bezahlt" {{ old('status', $bill->status) == 'bezahlt' ? 'selected="selected"' : '' }}>Bezahlt</option>
                        <option style="color: red;" value="destroy" {{ old('status', $bill->status) == 'destroy' ? 'selected="selected"' : '' }}>Rechnung Löschen</option>

                        @if($userAbteilungsleiter)
                            <option value="freigabe" {{ old('status', $bill->status) == 'freigabe' ? 'selected="selected"' : '' }}>Freigeben</option>
                        @endif
                    @else
                        <option value="0" >Bitte wählen</option>
                        @if($userAbteilungsleiter)
                            <option value="freigabe" {{ old('status', $bill->status) == 'freigabe' ? 'selected="selected"' : '' }}>Freigeben</option>
                        @else
                            <option value="vorab_freigabe" {{ old('status', $bill->status) == 'vorab_freigabe' ? 'selected="selected"' : '' }}>Vorab-Freigeben</option>
                        @endif
                        <option value="reklamieren" {{ old('status', $bill->status) == 'reklamieren' ? 'selected="selected"' : '' }}>Reklamieren</option>
                        <option value="blocken" {{ old('status', $bill->status) == 'blocken' ? 'selected="selected"' : '' }}>Zurückstellen</option>
                        <option value="save" {{ old('status', $bill->status) == 'save' ? 'selected="selected"' : '' }}>Speichern ohne Statuswechsel</option>
                    @endif
                </select>
            </div>
        </div>
        @error('status')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

</div>

@section('footer_scripts')
    <script type="text/javascript">

        $('.datefield').on('click', function(){
            $(this).select();
        });

        $('.datefield').on('keyup', function(e){

           var date = $(this).val();
           var target = $(this).data('target');
           var date_lenght = date.length;

            var new_date = '00.00.0000';
            var new_date_target = '2000-99-99';
            var change = false;

           if(date_lenght == 8 && date.match(/^(0[1-9]|[12]\d|3[01])(0[1-9]|1[0-2])\d{4}$/) != null) {

               var day = date.substring(0, 2);
               var month = date.substring(2, 4);
               var year = date.substring(4);

               new_date_target = year + '-' + month + '-' + day;
               new_date = day + '.' + month + '.' + year;

               change = true;

           }
           else if(date_lenght >= 8 && e.key != 'Tab')  {
               change = true;
           }

           if(change) {
               $(this).val(new_date);
               $('#' + target).val(new_date_target);
           }

        });


        Date.prototype.addDays = function(days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        }


        $('#biller_id, #date_dummy').change(function(){

            // Felder holen
            var invoice_created_at = $("#created_at");
            var invoice_date = $("#date");
            var due_date = $("#due_date");
            var due_date_dummy = $("#due_date_dummy");

            // Rechnungssteller, existiert oder wurde gewählt
            var biller_id = $("#biller_id").find(':selected').val();

            // Wenn kein Rechnungssteller vorhanden, Fälligkeit sofort
            if(biller_id =='' ) {
                var date = invoice_date.val()
                var date = new Date(date);
                var date_new = date.addDays(0);

                // Fälligkeits datum bearbeiten
                due_date_dummy.val(date_new.toLocaleDateString('de', {
                    month: '2-digit',
                    day: '2-digit',
                    year: 'numeric',
                }));
                due_date.val(date_new.getUTCFullYear() + '-' + ('00' + (date_new.getUTCMonth() + 1)).slice(-2) + '-' + ('00' + date_new.getUTCDate()).slice(-2));

            }
            else {
                // Fälligkeit neu rechnen anhand Eingang oder Rechnungsdatum
                $.ajax({
                    url: '{{  route('app.invoicerelease.ajax') }}/'+biller_id,
                    type: 'GET',
                    success: function(data) {

                        var date = invoice_date.val()
                        if(date =='' ) {
                            var date = invoice_created_at.val()
                        }

                        biller = JSON.parse(data);

                        // Fälligkeit anpassen anhand Basisdatum
                        var biller_due_days = biller[0].due_days;
                        var date = new Date(date);
                        var date_new = date.addDays(biller_due_days);

                        // Restliche Vorgabe Werte des Rechnungstellers bearbeiten
                        $("#payment_method").val(biller[0].payment);
                        if(biller[0].distributor.length > 0) {
                            $("#department_id").val(biller[0].distributor[0].id);
                        }
                        $("#currency").val(biller[0].currency);

                        $('select[name=costcenter_id]').val(biller[0].costcenter_id);

                        // Fälligkeits datum bearbeiten
                        due_date_dummy.val(date_new.toLocaleDateString('de', {
                            month: '2-digit',
                            day: '2-digit',
                            year: 'numeric',
                        }));
                        due_date.val(date_new.getUTCFullYear() + '-' + ('00' + (date_new.getUTCMonth() + 1)).slice(-2) + '-' + ('00' + date_new.getUTCDate()).slice(-2));

                        // Auswahl / Anzeige der nun gewählten Kostenstelle aktualisieren
                        $('.selectpicker').selectpicker('refresh')

                    }
                });

            }

        });

        function removedate(type) {
            $('#'+type).val('');
            $('#'+type+'_dummy').val('');
        }

        function displayOptions(e) {
            $('#options').hide();
            $('#comment').html('');
            if(e.value=='reklamieren') {
                $('#options').show();
            }
        }

        function pushOptionsToComment(e) {
            var text = '';
            switch(e.value) {
                case 'andere' :
                case '0'      : $('#comment').attr('placeholder', 'Bitte einen genauen Grund in diesem Feld eingeben!');
                                break;
                case 'rechnungsteller'  : text = 'Der Rechnungssteller ist nicht der richtige';
                                          break;
                case 'kostestelle'      : text = 'Die Kostenstelle stimmt nicht, bitte anpassen wie folgt: ';
                                          break;
                case 'betrag'           : text = 'Der in Rechnung gestellte Betrag ist nicht korrekt';
                                          break;
                case 'doppelt'          : text = 'Diese Rechnung ist schon vorhanden / wurde bereits bezahlt';
                                          break;
            }

            $('#comment').html(text);

            //console.log(e.options[e.selectedIndex].text);
        }

        $(function() {

            $("#delivery_date_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#delivery_date",
                altFormat : "yy-mm-dd",
                dateFormat : "dd.mm.yy"
            }, $.datepicker.regional['de']);

            $("#date_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#date",
                altFormat : "yy-mm-dd",
                dateFormat : "dd.mm.yy"
            }, $.datepicker.regional['de']);

            $("#due_date_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#due_date",
                altFormat : "yy-mm-dd",
                dateFormat : "dd.mm.yy"
            }, $.datepicker.regional['de']);
        });


    </script>
@stop