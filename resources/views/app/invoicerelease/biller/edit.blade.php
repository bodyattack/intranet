@extends('app.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">{{ $biller->presentation_short }} {{ empty($biller->id) ? ' erstellen' : 'bearbeiten' }}</div>

                    <div class="card-body">

                        @empty(!$biller->id)
                            <form id="submit-form" method="POST" action="{{ route('app.invoicerelease.biller.update', ['biller' => $biller->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('app.invoicerelease.biller.store') }}">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Bezeichnung</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $biller->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="creditor_number" class="col-md-4 col-form-label text-md-right">Kreditoren Nummer</label>

                            <div class="col-md-6">
                                <input id="creditor_number" type="text" class="form-control{{ $errors->has('creditor_number') ? ' is-invalid' : '' }}" name="creditor_number" value="{{ old('creditor_number', $biller->creditor_number) }}" >

                                @if ($errors->has('creditor_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('creditor_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="costcenter_id" class="col-md-4 col-form-label text-md-right">Kostenstelle</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="input-bordered" name="costcenter_id" id="costcenter_id">
                                            <option value="0">Bitte wählen</option>
                                            @foreach(\App\IrCostcenter::orderBy('name')->get() AS $costcenter)
                                                <option value="{{$costcenter->id}}" {{ $biller->costcenter_id == $costcenter->id || old('costcenter_id') == $costcenter->id ? 'selected="selected"' : '' }} >{{$costcenter->name}} ({{$costcenter->value}}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                @error('costcenter_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="department_id" class="col-md-4 col-form-label text-md-right">Freigabe Abteilung</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="input-bordered" name="department_id" id="department_id">
                                            <option value="0">Bitte wählen</option>
                                            <?php
                                            $biller_department_id = 0;
                                            if($biller->id!=0 && count($biller->distributor)) $biller_department_id = $biller->distributor[0]->id;

                                            ?>
                                            @foreach(\App\Department::withInvoiceApprovers() AS $department)
                                            <option value="{{$department->id}}" {{ $biller_department_id == $department->id || old('department_id') == $department->id ? 'selected="selected"' : '' }} >{{$department->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                @error('department_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="payment" class="col-md-4 col-form-label text-md-right">Zahlart</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="input-bordered" name="payment" id="payment">
                                            <option value="0" {{ $biller->payment == '0' || old('payment') == '0' ? 'selected="selected"' : '' }}>Bitte wählen</option>
                                            <option value="Rechnung" {{ $biller->payment == 'Rechnung' || old('payment') == 'Rechnung' ? 'selected="selected"' : '' }}>Rechnung</option>
                                            <option value="Einzug" {{ $biller->payment == 'Einzug' || old('payment') == 'Einzug' ? 'selected="selected"' : '' }}>Einzug</option>
                                            <option value="Kreditkarte" {{ $biller->payment == 'Kreditkarte' || old('payment') == 'Kreditkarte' ? 'selected="selected"' : '' }}>Kreditkarte</option>
                                            <option value="PayPal" {{ $biller->payment == 'PayPal' || old('payment') == 'PayPal' ? 'selected="selected"' : '' }}>PayPal</option>
                                            <option value="Bar" {{ $biller->payment == 'Bar' || old('payment') == 'Bar' ? 'selected="selected"' : '' }}>Bar</option>
                                            <option value="Vorkasse" {{ $biller->payment == 'Vorkasse' || old('payment') == 'Vorkasse' ? 'selected="selected"' : '' }}>Vorkasse</option>
                                            <option value="Amazon" {{ $biller->payment == 'Amazon' || old('payment') == 'Amazon' ? 'selected="selected"' : '' }}>Amazon</option>
                                            <option value="Verrechnung" {{ $biller->payment == 'Verrechnung' || old('payment') == 'Verrechnung' ? 'selected="selected"' : '' }}>Verrechnung</option>
                                            <option value="Intern" {{ $biller->payment == 'Intern' || old('payment') == 'Intern' ? 'selected="selected"' : '' }}>Intern</option>
                                        </select>
                                    </div>
                                </div>
                                @error('payment')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="due_days" class="col-md-4 col-form-label text-md-right">Zahlungsziel (Tage Vorlauf)</label>

                            <div class="col-md-6">
                                <input id="due_days" type="text" class="form-control{{ $errors->has('due_days') ? ' is-invalid' : '' }}" name="due_days" value="{{ old('due_days', $biller->due_days) ?? '14' }}">

                                @if ($errors->has('due_days'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('due_days') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="discount_days" class="col-md-4 col-form-label text-md-right">Skonto (Tage verfügbar)</label>

                            <div class="col-md-6">
                                <input id="discount_days" type="text" class="form-control{{ $errors->has('discount_days') ? ' is-invalid' : '' }}" name="discount_days" value="{{ old('discount_days', $biller->discount_days) ?? '0' }}">

                                @if ($errors->has('discount_days'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('discount_days') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="discount" class="col-md-4 col-form-label text-md-right">Skonto (Summe in %)</label>

                            <div class="col-md-6">
                                <input id="discount" type="text" class="form-control{{ $errors->has('discount') ? ' is-invalid' : '' }}" name="discount" value="{{ old('discount', $biller->discount) ?? '0' }}">

                                @if ($errors->has('discount'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('discount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                                <hr>

                        <div class="form-group row">
                            <label for="match_subject" class="col-md-4 col-form-label text-md-right">Zuordnungs Betreff</label>

                            <div class="col-md-6">
                                <input id="match_subject" type="text" class="form-control{{ $errors->has('match_subject') ? ' is-invalid' : '' }}" name="match_subject" value="{{ old('match_subject', $biller->match_subject) }}"  >

                                @if ($errors->has('match_subject'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('match_subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="match_email" class="col-md-4 col-form-label text-md-right">Zuordnungs E-Mail</label>

                            <div class="col-md-6">
                                <input  id="match_email" type="text" class="form-control{{ $errors->has('match_email') ? ' is-invalid' : '' }}" name="match_email" value="{{ old('match_email', $biller->match_email) }}"  >

                                @if ($errors->has('match_email'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('match_email') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>


                         <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="submit-button" name="submit-type" value="stay" class="btn btn-primary prevent-double-click">
                                    <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                    Speichern
                                </button>
                                <a style="float: right;" href="{{ route('app.invoicerelease.biller.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection