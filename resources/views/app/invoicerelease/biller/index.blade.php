@extends('app.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $biller->presentation_long }}</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="{{ $biller->presentation_short }} neu anlegen" style="float: right;" href="{{ route('app.invoicerelease.biller.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">

                        <form id="submit-form" method="get" action="{{ route('app.invoicerelease.biller.index') }}">
                            <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                            @csrf
                            <div class="input-group">
                                <input id="filter_word" name="filter_word" type="text" class="form-control" placeholder="Suchbegriff" value="{{ old('filter_word') ?? $filter_attributes['filter_word'] ?? '' }}">
                                <div class="input-group-btn" >
                                    <div class="input-group-append">
                                        <select  name="filter_row" id="filter_row" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="name" {{ old('filter_row') == 'name' || ( isset($filter_attributes['filter_row']) && $filter_attributes['filter_row'] == 'name' ) ? 'selected="selected"' : '' }}>in Bezeichnung</option>
                                            <option value="payment" {{ old('filter_row') == 'payment' || ( isset($filter_attributes['filter_row']) && $filter_attributes['filter_row'] == 'payment' ) ? 'selected="selected"' : '' }}>in Zahlart</option>
                                         </select>
                                        <select  name="filter_costcenter" id="filter_costcenter" style="max-width: 175px; cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="" >Kostenstelle</option>
                                            @foreach(\App\IrCostcenter::orderBy('name')->get() AS $costcenter)
                                                <option value="{{$costcenter->id}}" {{ isset($filter_attributes['filter_costcenter']) && $filter_attributes['filter_costcenter'] == $costcenter->id  || old('filter_costcenter') == $costcenter->id  ? 'selected="selected"' : '' }} >{{$costcenter->name}}</option>
                                            @endforeach
                                        </select>
                                        {{--
                                        <select  name="filter_payment" id="filter_payment" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="" {{ ( isset($filter_attributes['filter_payment']) && $filter_attributes['filter_payment'] == '' ) || old('payment') == '' ? 'selected="selected"' : '' }}>Zahlart alle</option>
                                            <option value="Rechnung" {{ ( isset($filter_attributes['filter_payment']) && $filter_attributes['filter_payment'] == 'Rechnung' ) || old('payment') == 'Rechnung' ? 'selected="selected"' : '' }}>Rechnung</option>
                                            <option value="Einzug" {{ ( isset($filter_attributes['filter_payment']) && $filter_attributes['filter_payment'] == 'Einzug' ) || old('payment') == 'Einzug' ? 'selected="selected"' : '' }}>Einzug</option>
                                            <option value="Kreditkarte" {{ ( isset($filter_attributes['filter_payment']) && $filter_attributes['filter_payment'] == 'Kreditkarte' ) || old('payment') == 'Kreditkarte' ? 'selected="selected"' : '' }}>Kreditkarte</option>
                                            <option value="PayPal" {{ ( isset($filter_attributes['filter_payment']) && $filter_attributes['filter_payment'] == 'PayPal' ) || old('payment') == 'PayPal' ? 'selected="selected"' : '' }}>PayPal</option>
                                            <option value="Bar" {{ ( isset($filter_attributes['filter_payment']) && $filter_attributes['filter_payment'] == 'Bar' ) || old('payment') == 'Bar' ? 'selected="selected"' : '' }}>Bar</option>
                                            <option value="Vorkasse" {{ ( isset($filter_attributes['filter_payment']) && $filter_attributes['filter_payment'] == 'Vorkasse' ) || old('payment') == 'Vorkasse' ? 'selected="selected"' : '' }}>Vorkasse</option>
                                            <option value="Amazon" {{ ( isset($filter_attributes['filter_payment']) && $filter_attributes['filter_payment'] == 'Amazon' ) || old('payment') == 'Amazon' ? 'selected="selected"' : '' }}>Amazon</option>
                                            <option value="Verrechnung" {{ ( isset($filter_attributes['filter_payment']) && $filter_attributes['filter_payment'] == 'Verrechnung' ) || old('payment') == 'Verrechnung' ? 'selected="selected"' : '' }}>Verrechnung</option>
                                            <option value="Intern" {{ ( isset($filter_attributes['filter_payment']) && $filter_attributes['filter_payment'] == 'Intern' ) || old('payment') == 'Intern' ? 'selected="selected"' : '' }}>Intern</option>
                                        </select>
                                        --}}
                                        <select  name="filter_skonto" id="filter_skonto" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="" {{ old('filter_skonto') == '' || ( isset($filter_attributes['filter_skonto']) && $filter_attributes['filter_skonto'] == '' ) ? 'selected="selected"' : '' }}>Skonto Alle</option>
                                            <option value="1"  {{ old('filter_skonto') == '1' || ( isset($filter_attributes['filter_skonto']) && $filter_attributes['filter_skonto'] == '1' ) ? 'selected="selected"' : '' }} >Mit Skonto</option>
                                            <option value="0" {{ old('filter_skonto') == '0' || ( isset($filter_attributes['filter_skonto']) && $filter_attributes['filter_skonto'] == '0' ) ? 'selected="selected"' : '' }}>Ohne Skonto</option>
                                        </select>
                                        <select  name="filter_order" id="filter_order" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="desc" {{ old('filter_order') == 'desc' || ( isset($filter_attributes['filter_order']) && $filter_attributes['filter_order'] == 'desc' ) ? 'selected="selected"' : '' }}>Absteigend</option>
                                            <option value="asc"  {{ old('filter_order') == 'asc' || ( isset($filter_attributes['filter_order']) && $filter_attributes['filter_order'] == 'asc' ) ? 'selected="selected"' : '' }} >Aufsteigend</option>
                                        </select>
                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                                        <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.invoicerelease.biller.index' ) }}"><i class="fas fa-redo"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>


                    <div class="col-md-12">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Bezeichnung</th>
                                    <th>Zahlart</th>
                                    <th>Kostenstelle</th>
                                    <th>Skonto</th>
                                    <th>Freigabeabeilung</th>
                                    <th>Erzeugt am</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $billers AS $biller )
                                    <tr id="">
                                        <td>{{ $biller->name }} </td>
                                        <td>{{ $biller->payment }} </td>
                                        <td>{{ $biller->costcenter->name }} </td>
                                        <td>{{ (!empty($biller->discount_days) ? $biller->discount.'%, bis '.$biller->discount_days.' Tage' : '-' ) }} </td>
                                        <td>{{ (!$biller->distributor->isEmpty() ? $biller->distributor[0]->name : '!!! fehlt !!!' ) }} </td>
                                        <td>@date($biller->created_at)</td>
                                        <td>
                                            <a title="{{ $biller->presentation_short }} bearbeiten" class="mr-2" href="{{ route('app.invoicerelease.biller.show', ['biller'=>$biller->id] ) }}"><i class="far fa-edit"></i></a>
                                            @if($biller->id != 85)  <!-- ANONA Live --->
                                            <a title="{{ $biller->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die {{ $biller->presentation_short }} '{{ $biller->name }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('app.invoicerelease.biller.destroy', ['biller'=>$biller->id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $biller->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-md-center">
                    {{ $billers->withQueryString()->links() }}
                </div>

                <div class="row mt-3">
                    <div class="col-md-12 ">
                        @php($colums='"name","due_days","discount_days","discount","payment","currency","match_email","match_subject","costcenter_id","created_at"')
                        @php($headers='"Name","Zahlungsziel-Tage","Skonto-Tage","Skonto","Zahlweise","Waehrung","E-Mail Zuordnung","Betreff Zuordnung","ID Kostenstelle","Erzeugt"')
                        <button onclick="generateCSV({  action: '{{route("app.invoicerelease.biller.csv")}}', columns: '[{{$colums}}]', headers: '[{{$headers}}]', filename: '{{ $biller->presentation_short }}.csv',  })" class="btn btn-primary">Exportiere Daten als CSV</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

