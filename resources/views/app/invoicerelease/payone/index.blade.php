@section('container-class') container-fluid @endsection
@extends('app.layouts.app')
<?php
if(!isset($urlQuerry)) {
    $urlQuerry = '';
    $previous = URL::previous();
    if($pos = strpos($previous, '?')) {
        $urlQuerry = substr($previous, $pos+1);
    }
}
?>
@section('content')

    <div class="@yield('container-class', 'container')">

        <div class="card content-area">
                <div class="card-innr">

                    <div class="card-head">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="card-title card-title-lg">PayONE Rechnungsdaten</h4>
                            </div>

                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <?php
                            $urlQuerry='';
                            if(strpos(url()->full(), '?')) {
                                $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
                            }
                            if($urlQuerry=='' & strpos(url()->previous(), '?')) {
                                $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
                            }
                            ?>

                            @include('app.invoicerelease.payone.subnavi',['urlQuery' => $urlQuerry])

                            <form class="mb-0" id="submit-form" method="get" action="{{ route('app.invoicerelease.payone') }}">
                                <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                                @csrf
                                <?php
                                $date_from = ( isset($filter['date_from']) ? $filter['date_from'] : old('date_from') );
                                $date_to = ( isset($filter['date_to']) ? $filter['date_to'] : old('date_to') );
                                ?>

                                <div class="input-group">
                                     <span style="position: relative">
                                            <i data-toggle="tooltip" data-html="true" onclick="removedate('date_from')" data-original-title="Datum entfernen"  class="remove-date fas fa-times"></i>
                                            <input placeholder="Datum von / optional" readonly autocomplete="off" id="date_from_dummy" type="text" class="form-control bg-white" name="date_from_dummy" value="@empty(!$date_from)@date($date_from)@endempty">
                                            <input id="date_from" type="hidden" name="date_from" value="{{$date_from}}">
                                    </span>
                                    <div class="input-group-btn" style="flex: content" >
                                        <div class="input-group-append">
                                            <span class="input-group">
                                                <i data-toggle="tooltip" data-html="true" onclick="removedate('date_to')" data-original-title="Datum entfernen" class="remove-date fas fa-times"></i>
                                                <input  placeholder="Datum bis / optional" readonly autocomplete="off" id="date_to_dummy" type="text" class="form-control bg-white" name="date_to_dummy" value="@empty(!$date_to)@date($date_to)@endempty">
                                                <input id="date_to" type="hidden" name="date_to" value="{{$date_to}}">
                                            </span>
                                            <input id="ERP_Kundenr" style="border-radius: 0px 0px 0px 0px; border-left: 0px;" name="ERP_Kundenr" type="text" class="form-control" placeholder="Kundennummer" value="{{ old('ERP_Kundenr') ?? $filter['ERP_Kundenr'] ?? '' }}">
                                            <input id="Rechnungsnummern" style="border-radius: 0px 0px 0px 0px;"  name="Rechnungsnummern" type="text" class="form-control" placeholder="Rechnungsnummer" value="{{ old('Rechnungsnummern') ?? $filter['Rechnungsnummern'] ?? '' }}">
                                            <input id="custom_orders_id" style="border-radius: 0px 0px 0px 0px;"  name="custom_orders_id" type="text" class="form-control" placeholder="Bestellnummer" value="{{ old('custom_orders_id') ?? $filter['custom_orders_id'] ?? '' }}">
                                            <input id="txid" style="border-radius: 0px 0px 0px 0px;"  name="txid" type="text" class="form-control" placeholder="TXID" value="{{ old('txid') ?? $filter['txid'] ?? '' }}">
                                            <select class="border-radius-none border-sides-none form-control" name="clearingsubtype" id="clearingsubtype" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; height: 37px !important;">
                                                <option value="" >Jede Kontonummer</option>
                                                @foreach(\App\PayonePayments::getAccountnumber() AS $key => $accountnummer)
                                                <option value="{{$key}}" {{ ( isset($filter['clearingsubtype']) && $filter['clearingsubtype'] == $key) || ($key == old('clearingsubtype')) ? 'selected="selected"' : '' }} >{{$accountnummer}} / {{$key}}</option>
                                                @endforeach
                                         </select>
                                            <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                                            <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.invoicerelease.payone' ) }}"><i class="fas fa-redo"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </form>


                            <div class="table-responsive">
                                <table class="table table-bordered table-striped mt-2 mb-3">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>@sortablelink('booking_date', 'Datum')</th>
                                        <th class="text-right">@sortablelink('amount', 'Betrag')</th>
                                        <th>@sortablelink('currency', 'Währung')</th>
                                        <th>@sortablelink('ERP_Kundenr', 'Kundennummer')</th>
                                        <th>@sortablelink('Rechnungsnummern', 'Rechnungsnummern')</th>
                                        <th>@sortablelink('custom_orders_id', 'Bestellnummer')</th>
                                        <th>@sortablelink('txid', 'TXID')</th>
                                        <th>@sortablelink('clearingsubtype', 'Kontonummer')</th>
                                        @headicons('1', 'fa-cogs')
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ( $payone_payments  AS $payone_payment )
                                        <tr id="">
                                            <td>@if( !empty($payone_payment->booking_date) ) @date($payone_payment->booking_date) @endif  </td>
                                            <td class="text-right">@if( !empty($payone_payment->amount) ) @money($payone_payment->amount) @endif  </td>
                                            <td>{{ ( empty($payone_payment->currency) ? '' : $payone_payment->currency ) }}</td>
                                            <td>{{ ( empty($payone_payment->ERP_Kundenr) ? '' : $payone_payment->ERP_Kundenr ) }}</td>
                                            <td>{{ ( empty($payone_payment->Rechnungsnummern) ? '' : $payone_payment->Rechnungsnummern ) }}</td>
                                            <td>{{ ( empty($payone_payment->custom_orders_id) ? '' : $payone_payment->custom_orders_id ) }}</td>
                                            <td>{{ ( empty($payone_payment->txid) ? '' : $payone_payment->txid ) }}</td>
                                            <td>{{ ( empty($payone_payment->clearingsubtype) ? '' : $payone_payment->accountnumber[$payone_payment->clearingsubtype].' / '.$payone_payment->clearingsubtype ) }}</td>
                                            <td>
                                                @if( !empty($payone_payment->temp_order_id) )
                                                <a title="Bestellung ansehen" class="float-right" target="_blank" href="{{ route('app.temporder.show', ['temporder'=>$payone_payment->temp_order_id] ) }}"><i class="far fa-eye"></i></a>
                                                @else
                                                    <a title="Zuordnung noch nicht abgeschlossen, Daten des Intranets fehlen!" class="float-right" href="#"><i class="text-muted far fa-eye"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr id=""><td colspan="100%">Keine Zahlungsinformationen vorhanden</td></tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        {{ $payone_payments->withQueryString()->links() }}
                    </div>

                    <div class="row">
                        <div class="col-md-12 ">
                            <a style="min-width: 220px; line-height: unset; padding: 0px; border-radius: 0px; line-height: 35px;"  class="btn btn-secondary "  role="button" href="{{ route('app.invoicerelease.payone.csv', $filter) }}">&nbsp;Ergebnis für DATEV exportieren&nbsp;</a>
                         </div>
                    </div>


            </div>
        </div>
    </div>


@endsection



@section('footer_scripts')
    <script type="text/javascript">

        $(function() {

            $("#date_from_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#date_from",
                altFormat : "yy-mm-dd",
                dateFormat : "dd.mm.yy",
                changeMonth: true,
                changeYear: true,
                defaultDate: "@empty(!$date_from)@date($date_from)@endempty"
            }, $.datepicker.regional['de']);

            $("#date_to_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#date_to",
                altFormat : "yy-mm-dd",
                dateFormat : "dd.mm.yy",
                changeMonth: true,
                changeYear: true,
                defaultDate: "@empty(!$date_to)@date($date_to)@endempty"
            }, $.datepicker.regional['de']);
        });


    </script>
@stop