<ul class="nav nav-tabs-line mb-3">
    <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'app.invoicerelease.payone') active @endif"
           href="{{route('app.invoicerelease.payone', $urlQuerry) }}">Übersicht</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if( Route::currentRouteName() == 'app.invoicerelease.payone.importShow' ) active @endif"
           href="{{route('app.invoicerelease.payone.importShow', $urlQuerry) }}">Import</a>
    </li>
</ul>


