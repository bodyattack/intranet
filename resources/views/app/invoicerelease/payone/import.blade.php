@section('container-class') container-fluid @endsection
@extends('app.layouts.app')
<?php
if(!isset($urlQuerry)) {
    $urlQuerry = '';
    $previous = URL::previous();
    if($pos = strpos($previous, '?')) {
        $urlQuerry = substr($previous, $pos+1);
    }
}
?>
@section('content')

    <div class="container">

        <div class="card content-area">
                <div class="card-innr">

                    <div class="card-head">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="card-title card-title-lg">PayONE Import</h4>
                            </div>

                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <?php
                            $urlQuerry='';
                            if(strpos(url()->full(), '?')) {
                                $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
                            }
                            if($urlQuerry=='' & strpos(url()->previous(), '?')) {
                                $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
                            }
                            ?>

                            @include('app.invoicerelease.payone.subnavi',['urlQuery' => $urlQuerry])

                                <div class="card mt-3">

                                    <div class="card-header">
                                        <div class="col-md-12 mt-1">
                                            PayONE Import
                                        </div>
                                    </div>

                                    <div class="card-body">

                                        <div class="alert alert-warning" role="alert">
                                            Hier können PayONE Export Daten eingspielt werden, CSV Aufbau beachten!
                                            <br>
                                            PayONE Backend: "Export -> Zahlungen -> Datum wählen -> Export Klicken"
                                        </div>

                                        <form id="submit-form"  method="POST" action="{{ route('app.invoicerelease.payone.import') }}" enctype='multipart/form-data' >
                                            @csrf



                                            <div class="row">
                                                <label for="file" class="col-md-2 col-form-label text-md-right">Datei auswählen</label>

                                                <div class="col-md-5">
                                                    <input id="file" type="file" class="form-control" name="file" value="{{ old('file') }}" >
                                                </div>


                                                <div class="col-md-5">
                                                    <button type="submit" id="submit-button" name="submit-type" class="float-right btn btn-primary prevent-double-click">Verarbeiten</button>
                                                </div>

                                            </div>

                                        </form>

                                    </div>

                                </div>


                        </div>
                    </div>




            </div>
        </div>
    </div>


@endsection



@section('footer_scripts')
    <script type="text/javascript">

    </script>
@stop