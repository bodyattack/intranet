@extends('app.layouts.app')
@php $searchable_select=1; @endphp  {{-- ergänze die class "selectpicker" select tag um  filtern zu können --}}
@section('content')

    <script src="{{ asset('dropzone/dropzone.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('dropzone/dropzone.css') }}">
    
    <div class="container">
        <div class="row justify-content-center">


            <div class="col-md-12">

                <div class="card">

                    <div class="card-header">{{ $bill->presentation_short }} {{ empty($bill->id) ? ' erstellen' : 'bearbeiten' }}</div>

                    <div class="card-body">

                        <div class="col-md-8 offset-md-2 dropzone needsclick dz-clickable">
                            <div class="dz-message needsclick">
                                Hier klicken, um die PDF-Datei auszuwählen oder <br> per Drag & Drop in dieses Feld ziehen.
                            </div>
                        </div>
                        <a href="#" id="removeAllFilesBtn" style="display:none;" class="col-md-8 offset-md-2 btn btn-secondary  btn-block mt-2">Alle Dateien entfernen</a>


                        <div id="bill_form" class="mt-5" style="display:none;" >

                            @empty(!$bill->id)
                            <form id="submit-form" method="POST" action="{{ route('app.invoicerelease.bills.update', ['bill' => $bill->id]) }}">
                            @method('PATCH')
                            @else
                            <form id="submit-form"  method="POST" action="{{ route('app.invoicerelease.bills.store') }}">
                            @endempty

                            @csrf

                            <div class="form-group row mt-5">

                                <label for="biller_id" class="col-md-8 offset-md-2 col-form-label text-md">Rechnungssteller</label>

                                <div class="col-md-8 offset-md-2">
                                    <div class="input-with-label">
                                        <div class="select-wrapper">
                                            <select class="selectpicker input-bordered form-control" name="biller_id" id="biller_id"  data-actions-box="true">
                                                <option value="0">Bitte wählen</option>

                                                @foreach(\App\IrBiller::withDistributor() AS $biller)
                                                <option value="{{$biller->id}}" {{ $bill->id == $biller->id || old('biller_id') == $biller->id ? 'selected="selected"' : '' }} >{{$biller->name}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    @error('biller_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mt-2">

                                <label for="mailbox_id" class="col-md-8 offset-md-2 col-form-label text-md">Mandant</label>

                                <div class="col-md-8 offset-md-2">
                                    <div class="input-with-label">
                                        <div class="select-wrapper">
                                            <select class="input-bordered form-control" name="mandant_id" id="mandant_id">
                                                <option value="0">Bitte wählen</option>
                                                @foreach(\App\Mandant::all() AS $mmandant)
                                                <option value="{{$mmandant->id}}" {{  old('mandant_id', $bill->mandant_id) == $mmandant->id ? 'selected="selected"' : '' }} >{{$mmandant->name}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    @error('mandant_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mt-2">

                                <label for="mailbox_id" class="col-md-8 offset-md-2 col-form-label text-md">Status der Rechnung</label>

                                <div class="col-md-8 offset-md-2">
                                    <div class="input-with-label">
                                        <div class="select-wrapper">
                                            <select class="input-bordered" name="status" id="status">
                                                <option value="0" >Bitte wählen</option>
                                                <option value="bezahlen" {{ old('status', $bill->status) == 'bezahlen' ? 'selected="selected"' : '' }}>Bezahlen</option>
                                                <option value="bezahlt" {{ old('status', $bill->status) == 'bezahlt' ? 'selected="selected"' : '' }}>Bezahlt</option>
                                                <option value="neu" {{ old('status', $bill->status) == 'neu' ? 'selected="selected"' : '' }}>Zur Verteilung</option>
                                            </select>
                                        </div>
                                    </div>
                                    @error('mailbox_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                             <div class="form-group row mt-5 mb-0">
                                <div class="col-md-8 offset-md-2">
                                    <button type="submit" id="submit-button" name="submit-type" value="stay" class="btn btn-primary prevent-double-click">
                                        <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                        Speichern
                                    </button>
                                    <a style="float: right;" href="{{ route('app.invoicerelease.bills.index') }}" value="back" class="btn btn-primary">Zurück</a>
                                </div>
                            </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('footer_scripts')
    <script type="text/javascript">

        function displayAlert(text) {
            $(".alert-danger-text").html(text);
            $(".alert-danger").slideDown('slow');
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }

        Dropzone.autoDiscover = false;

        $(function() {
            var myDropzone = new Dropzone(".dropzone", {
                url: "{{ route('app.invoicerelease.bills.upload') }}",
                paramName: "file",
                maxFilesize: 20, // MB
                maxFiles: 1,
                parallelUploads: 1, // important!
                acceptedFiles: "application/pdf",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                error: function(file, message) {
                    displayAlert(message);
                    myDropzone.removeFile(file);
                    return false;
                },
                success: function() {
                     $("#bill_form").slideDown('fast');
                }
            });

            myDropzone.on("addedfile", function () {
                $("#removeAllFilesBtn").show();
            });

            function countfiles() {
                return myDropzone.files.length;
            }

            $("#removeAllFilesBtn").click(function () {
                myDropzone.removeAllFiles(),
                $.get("{{ route('app.invoicerelease.bills.remove-files') }}");
                $("#bill_form").hide();
            });

        });

        /*
        onclick="javascript:$('.alert-known').slideUp('fast');Cookies.set('go_known', '1', { expires: 365 });"
        if(Cookies.get('go_known') != '1') {
            $('.alert-known').slideDown('fast');
        }
        */
    </script>
@stop
