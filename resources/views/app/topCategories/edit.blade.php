@extends('app.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> Top Kategorie {{ empty($topCategorie->categories_top_id) ? ' erstellen' : 'bearbeiten' }} </div>

                    <div class="card-body">

                        @empty(!$topCategorie->categories_top_id)
                            <form id="submit-form" method="POST" action="{{ route('app.topCategories.update', ['topCategorie' => $topCategorie->categories_top_id]) }}">
                                <input id="HealthClaimOK" name="HealthClaimOK" type="hidden" value="{{$topCategorie->HealthClaimOK}}">
                                @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('app.topCategories.store') }}">
                                <input id="HealthClaimOK" name="HealthClaimOK" type="hidden" value="0">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="categories_top_id" class="col-md-4 col-form-label text-md-right">ID</label>

                            <div class="col-md-6">
                                <input id="categories_top_id" type="text" class="form-control {{ $errors->has('categories_top_id') ? ' is-invalid' : '' }}" name="categories_top_id" value="{{ old('categories_top_id', $topCategorie->categories_top_id) }}" required autofocus>

                                @if ($errors->has('categories_top_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categories_top_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="categories_name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="categories_name" type="text" class="form-control {{ $errors->has('categories_name') ? ' is-invalid' : '' }}" name="categories_name" value="{{ old('categories_name', $topCategorie->categories_name) }}" required>

                                @if ($errors->has('categories_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categories_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="categories_rewrite" class="col-md-4 col-form-label text-md-right">Rewrite</label>

                            <div class="col-md-6">
                                <input id="categories_rewrite" type="text" class="form-control {{ $errors->has('categories_rewrite') ? ' is-invalid' : '' }}" name="categories_rewrite" value="{{ old('categories_rewrite', $topCategorie->categories_rewrite) }}" required>

                                @if ($errors->has('categories_rewrite'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categories_rewrite') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="categories_title" class="col-md-4 col-form-label text-md-right">Titel</label>

                            <div class="col-md-6">
                                <input id="categories_title" type="text" class="form-control {{ $errors->has('categories_title') ? ' is-invalid' : '' }}" name="categories_title" value="{{ old('categories_title', $topCategorie->categories_title) }}" required>

                                @if ($errors->has('categories_title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categories_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="categories_description" class="col-md-4 col-form-label text-md-right">Beschreibung</label>

                            <div class="col-md-6">
                                <textarea rows="6" id="categories_description" type="text" class="form-control {{ $errors->has('categories_description') ? ' is-invalid' : '' }}" name="categories_description" >{{ old('categories_description', $topCategorie->categories_description) }}</textarea>

                                @if ($errors->has('categories_description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categories_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="categories_desc_unten" class="col-md-4 col-form-label text-md-right">Beschreibung unten</label>

                            <div class="col-md-6">
                                <textarea rows="8" id="categories_desc_unten" type="text" class="form-control {{ $errors->has('categories_desc_unten') ? ' is-invalid' : '' }}" name="categories_desc_unten">{{ old('categories_desc_unten', $topCategorie->categories_desc_unten) }}</textarea>

                                @if ($errors->has('categories_desc_unten'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categories_desc_unten') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="categories_top" class="col-md-4 col-form-label text-md-right">Top</label>

                            <div class="col-md-6">
                                <input id="categories_top" type="text" class="form-control {{ $errors->has('categories_top') ? ' is-invalid' : '' }}" name="categories_top" value="{{ old('categories_top', $topCategorie->categories_top) }}" required>

                                @if ($errors->has('categories_top'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categories_top') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                                <div class="form-group row">
                            <label for="categories_keywords" class="col-md-4 col-form-label text-md-right">Keywords</label>

                            <div class="col-md-6">
                                <input id="categories_keywords" type="text" class="form-control {{ $errors->has('categories_keywords') ? ' is-invalid' : '' }}" name="categories_keywords" value="{{ old('categories_keywords', $topCategorie->categories_keywords) }}" required>

                                @if ($errors->has('categories_keywords'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categories_keywords') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="categories_meta_title" class="col-md-4 col-form-label text-md-right">Meta Titel</label>

                            <div class="col-md-6">
                                <input id="categories_meta_title" type="text" class="form-control {{ $errors->has('categories_meta_title') ? ' is-invalid' : '' }}" name="categories_meta_title" value="{{ old('categories_meta_title', $topCategorie->categories_meta_title) }}" required>

                                @if ($errors->has('categories_meta_title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categories_meta_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="categories_meta_desc" class="col-md-4 col-form-label text-md-right">Meta Beschreibung</label>

                            <div class="col-md-6">
                                <textarea id="categories_meta_desc" type="text" class="form-control {{ $errors->has('categories_meta_desc') ? ' is-invalid' : '' }}" name="categories_meta_desc" required>{{ old('categories_meta_desc', $topCategorie->categories_meta_desc) }}</textarea>

                                @if ($errors->has('categories_meta_desc'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categories_meta_desc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="menuestatus" class="col-md-4 col-form-label text-md-right">Menüstatus</label>

                            <div class="col-md-6">
                                <select required  name="menuestatus" class="form-control" id="menuestatus">
                                    @foreach (['0' => 'Aus', '1' => 'AN'] as $key=>$value)
                                        <option value="{{ $key }}" @if($topCategorie->menuestatus == $key || old('menuestatus') == $key) selected @endif>{{ $value }} </option>
                                    @endforeach
                                </select>

                                @if ($errors->has('menuestatus'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('menuestatus') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click">Speichern</button>
                                <a style="float: right;" href="{{ route('app.topCategories.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection