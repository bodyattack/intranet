@extends('app.layouts.app')
<?php
$urlQuerry='';
if(strpos(url()->full(), '?')) {
    $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
}
if($urlQuerry=='' & strpos(url()->previous(), '?')) {
    $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
}
?>
@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $product->presentation_long }}</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="Produkt neu anlegen" style="float: right;" href="{{ route('app.product.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-md-12">

                        <form id="submit-form" method="get" action="{{ route('app.product.index') }}">
                            <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                            @csrf
                            <div class="input-group">
                                <input id="filter_word" name="filter_word" type="text" class="form-control" placeholder="Suchbegriff" value="{{ old('filter_word') ?? $filter_attributes['filter_word'] ?? '' }}">
                                <div class="input-group-btn" >
                                    <div class="input-group-append">
                                        <select  name="filter_row" id="filter_row" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="name" {{ old('filter_row') == 'name' || ( isset($filter_attributes['filter_row']) && $filter_attributes['filter_row'] == 'name' ) ? 'selected="selected"' : '' }}>in Name</option>
                                            <option value="ean" {{ old('filter_row') == 'ean' || ( isset($filter_attributes['filter_row']) && $filter_attributes['filter_row'] == 'ean' ) ? 'selected="selected"' : '' }}>in EAN</option>
                                            <option value="ansi" {{ old('filter_row') == 'ansi' || ( isset($filter_attributes['filter_row']) && $filter_attributes['filter_row'] == 'ansi' ) ? 'selected="selected"' : '' }}>in ANSI</option>
                                        </select>
                                        <select  name="filter_order" id="filter_order" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="desc" {{ old('filter_order') == 'desc' || ( isset($filter_attributes['filter_order']) && $filter_attributes['filter_order'] == 'desc' ) ? 'selected="selected"' : '' }}>Absteigend</option>
                                            <option value="asc"  {{ old('filter_order') == 'asc' || ( isset($filter_attributes['filter_order']) && $filter_attributes['filter_order'] == 'asc' ) ? 'selected="selected"' : '' }} >Aufsteigend</option>
                                        </select>
                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                                        <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.product.index', 'filter_reset=1') }}"><i class="fas fa-redo"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>

                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover mt-2 mb-3">
                                <thead>
                                <tr>
                                    <th class="head-icons-1">
                                    <th>Name</th>
                                    <th>VPE</th>
                                    <th>Menge</th>
                                    <th>Einheit</th>
                                    <th>EAN</th>
                                    <th>ANSI</th>
                                    @headicons('1', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>

                                @forelse ( $products AS $product )
                                    <tr style="cursor:pointer;" onclick="location.href='{{ route('app.product.edit', ['product' => $product, $urlQuerry]) }}'">
                                        <td>
                                            <a title="Artikel {{ $product->active ? 'deaktivieren' : 'aktivieren' }}" class="mr-2"  href="{{ route('app.product.toggleActive', $product->id) }}" role="button"><i class="fa fa-circle {{ $product->active ? 'text-success' : 'text-secondary' }}"></i></a>
                                        </td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->packaging_unit }}</td>
                                        <td>{{ $product->quantity }}</td>
                                        <td>{{ $product->measure }}</td>
                                        <td>{{ $product->ean }}</td>
                                        <td>{{ $product->ansi }}</td>
                                        <td></td>
                                        {{--
                                        <td>
                                            <a title="{{ $product->presentation_long }} Adressen" class="mr-2" href="{{ route('app.product.address.index', ['product'=>$product->id] ) }}"><i class="far fa-address-book"></i></a>
                                         </td>
                                         --}}

                                    </tr>
                                @empty
                                    <tr id=""><td colspan="7">Keine Benutzer vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-md-center">
                {{ $products->withQueryString()->links() }}
            </div>


        </div>
    </div>

@endsection
