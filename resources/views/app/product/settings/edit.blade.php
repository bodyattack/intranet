@extends('app.layouts.app')

@section('content')
@include('app.products.title')
<form method="POST" action="{{ route('app.products.settings.update', [$product]) }}" autocomplete="off">
    <input type="hidden" name="product_id" value="{{ $product->id }}">
    @csrf @method('PUT')
    <div>
        <a class="btn btn-primary" href="{{ route('app.products.settings.show', [$product]) }}"
            role="button">Zurück</a>
        <button type="submit" class="btn btn-primary">Speichern</button>
    </div>
    @include('app.products.subnavi')
    @include('app.products.settings.form')
</form>
@endsection