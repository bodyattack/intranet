@php($values = [0 => 'Nein', 1 => 'Ja'])
<div class="form-group">
    <label for="newsletter">Newsletter</label>
    <select required name="newsletter" class="form-control" id="newsletter">
        <option selected disabled>-- Bitte wählen --</option>
        @foreach ($values as $key => $value)
        <option value="{{ $key }}" @if(old('newsletter', $product->settings->newsletter) == $key)
            selected @endif>{{ $value }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="evn">Einzelverbindungs-Nachweis</label>
    <select required name="evn" class="form-control" id="evn">
        <option selected disabled>-- Bitte wählen --</option>
        @foreach ($values as $key => $value)
        <option value="{{ $key }}" @if(old('evn', $product->settings->evn) == $key)
            selected @endif>{{ $value }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="evn_small">Einzelverbindungs-Nachweis kompakt</label>
    <select required name="evn_small" class="form-control" id="evn_small">
        <option selected disabled>-- Bitte wählen --</option>
        @foreach ($values as $key => $value)
        <option value="{{ $key }}" @if(old('evn_small', $product->settings->evn_small) == $key)
            selected @endif>{{ $value }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="digital_stamp">Digitale Postmarke</label>
    <select required name="digital_stamp" class="form-control" id="digital_stamp">
        <option selected disabled>-- Bitte wählen --</option>
        @foreach ($values as $key => $value)
        <option value="{{ $key }}" @if(old('digital_stamp', $product->settings->digital_stamp) == $key)
            selected @endif>{{ $value }}</option>
        @endforeach
    </select>
</div>
