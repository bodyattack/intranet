@extends('app.layouts.app')

@section('content')
@include('app.products.title')

@if ($product->settings->id)
<form method="POST" action="{{ route('app.products.settings.destroy', [$product]) }}">
    @csrf @method('DELETE')
    <a class="btn btn-primary" href="{{ route('app.products.index') }}" role="button">Zurück</a>
    <a class="btn btn-primary" href="{{ route('app.products.settings.edit', [$product]) }}"
        role="button">Bearbeiten</a>
    <button class="btn btn-danger" onclick="return window.confirm('Wirklich löschen?');">Löschen</button>
</form>
@else
<a class="btn btn-primary" href="{{ route('app.products.index') }}" role="button">Zurück</a>
<a class="btn btn-primary" href="{{ route('app.products.settings.create', [$product]) }}"
    role="button">Einstellungen eintragen</a>
@endif

@include('app.products.subnavi')

@if ($product->settings->id)
<table class="table table-vertical">
    <tbody>
        <tr>
            <th class="w-25">Newsletter:</th>
            <td>{{ $product->settings->present()->newsletter }}</td>
        </tr>
        <tr>
            <th>Einzelverbindungs-Nachweis:</th>
            <td>{{ $product->settings->present()->evn }}</td>
        </tr>
        <tr>
            <th>Einzelverbidnungs-Nachweis kompakt:</th>
            <td>{{ $product->settings->present()->evn_small }}</td>
        </tr>
        <tr>
            <th>Digitale Postmarke:</th>
            <td>{{ $product->settings->present()->digital_stamp }}</td>
        </tr>
        <tr>
            <th>Angelegt am:</th>
            <td>{{ $product->settings->present()->created_at }}</td>
        </tr>
        <tr>
            <th>Verändert am:</th>
            <td>{{ $product->settings->present()->updated_at }}</td>
        </tr>
    </tbody>
</table>
@else
<p>Keine Einstellungen vorhanden</p>
@endif
@endsection