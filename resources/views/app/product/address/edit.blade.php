@extends('app.layouts.app')
<?php
$urlQuerry='';
if(strpos(url()->full(), '?')) {
    $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
}
if($urlQuerry=='' & strpos(url()->previous(), '?')) {
    $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
}
?>
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Sie {{ empty($address->id) ? ' erstellen' : 'bearbeiten' }} eine {{ $product->presentation_long }} Adresse</div>

                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12">
                                @include('app.product.subnavi')

                                @empty(!$address->id)
                                <form method="POST" action="{{ route('app.product.address.update', ['product' => $product->id, 'address' => $address->id, ]) }}">
                                @method('PATCH')
                                @else
                                <form method="POST" action="{{ route('app.product.address.store', ['product' => $product->id, 'address' => $address->id, ]) }}">

                                    @endempty
                                    @csrf

                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                    <input type="hidden" name="urlQuerry" value="{{ $urlQuerry }}">


                                    <table class="table table-vertical">
                                        <tbody>

                                        @include('app.product.address.form')

                                        </tbody>
                                    </table>

                                    <div class="form-group mb-0">
                                        <div class="">
                                            <a class="btn btn-primary" href="{{ route('app.product.address.index', ['product' => $product->id, old('urlQuerry', $urlQuerry)]) }}" role="button">Zurück</a>
                                            <button type="submit" name="submit-type" value="stay" class="btn btn-primary">Speichern</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
