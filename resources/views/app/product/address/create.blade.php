@extends('app.layouts.app')

@section('content')

<form method="POST" action="{{ route('app.product.address.store', [$product]) }}" autocomplete="off">
    <input type="hidden" name="product_id" value="{{ $product->id }}">
    @csrf
    <div>
        <a class="btn btn-primary" href="{{ route('app.product.address.show', [$product]) }}"
            role="button">Zurück</a>
        <button type="submit" class="btn btn-primary">Speichern</button>
    </div>
    @include('app.products.subnavi')
    @include('app.products.address.form')
</form>
@endsection