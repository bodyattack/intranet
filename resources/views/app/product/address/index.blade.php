@extends('app.layouts.app')
<?php
$urlQuerry='';
if(strpos(url()->full(), '?')) {
    $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
}
if($urlQuerry=='' & strpos(url()->previous(), '?')) {
    $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
}
?>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                        Sie {{ empty($product->id) ? ' registrieren' : 'bearbeiten' }} einen {{ $product->presentation_long }}
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            @include('app.product.subnavi')

                            <a title="Adresse neu anlegen" style="float: right;" href="{{ route('app.product.address.create', [$product]) }}"><i class="far fa-plus-square fa-2x"></i></a>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover mt-2 mb-3">
                                    <thead>
                                    <tr>
                                        <th class="head-icons-1">
                                        <th>Straße</th>
                                        <th>PLZ</th>
                                        <th>Ort</th>
                                        <th>Art</th>
                                        <th>Erstellt</th>
                                        @headicons('2', 'fa-cogs')
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($types=['global' => 'Stammadresse', 'invoice' => 'Rechnungsadresse', 'delivery' => 'Lieferadresse'])
                                    @forelse ( $address AS $data )
                                        <tr>
                                            <td>
                                                <a title="Kennzeichnung als primär {{ $data->primary ? ' entfernen' : ' setzen' }}" class="mr-2"  href="{{ route('app.product.address.togglePrimary', ['product'=>$product->id, 'address'=>$data->id, $urlQuerry]) }}" role="button"><i class="fa fa-circle {{ $data->primary ? 'text-success' : 'text-secondary' }}"></i></a>
                                            </td>
                                            <td> <a style="color:unset; display: block;" title="{{ $data->presentation_short }} bearbeiten" class="mr-2" href="{{ route('app.product.address.edit', ['product'=>$product->id, 'address'=>$data->id, $urlQuerry]) }}">{{ $data->address }}</a></td>
                                            <td>{{ $data->zip }}</td>
                                            <td>{{ $data->city }}</td>
                                            <td>{{ $types[$data->type] }}</td>
                                            <td>@datetime($data->created_at)</td>
                                            <td>
                                                <a title="{{ $data->presentation_short }} bearbeiten" class="mr-2" href="{{ route('app.product.address.edit', ['product'=>$product->id, 'address'=>$data->id, $urlQuerry]) }}"><i class="far fa-edit"></i></a>
                                                <a title="{{ $data->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                                   data-form-action="{{route('app.product.address.destroy', ['product'=>$product->id, 'address'=>$data->id])}}"
                                                   data-form-info="Wollen Sie die {{ $data->presentation_short }} '{{ $data->address }}' wirklich endgültig löschen?">
                                                    <i class="far fa-trash-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr id=""><td colspan="9">Keine Adressen vorhanden</td></tr>
                                    @endforelse
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <a class="btn btn-primary" href="{{ route('app.product.index', $urlQuerry) }}" role="button">Zurück</a>
            </div>
    </div>
</div>



@endsection
