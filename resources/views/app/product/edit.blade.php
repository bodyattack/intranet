@extends('app.layouts.app')
<?php
$urlQuerry='';
if(strpos(url()->full(), '?')) {
$urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
}
?>
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Sie {{ empty($product->id) ? ' registrieren' : 'bearbeiten' }} einen {{ $product->presentation_long }}</div>

                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12">
                                @include('app.product.subnavi')

                                @empty(!$product->id)
                                <form method="POST" action="{{ route('app.product.update', ['product' => $product->id]) }}">
                                @method('PATCH')
                                @else
                                <form method="POST" action="{{ route('app.product.store') }}">
                                    @endempty

                                    @csrf
                                    <input type="hidden" name="urlQuerry" value="{{ $urlQuerry }}">

                                    <table class="table table-vertical">
                                        <tbody>

                                        @include('app.product.form')

                                        </tbody>
                                    </table>

                                    <div class="form-group mb-0">
                                        <div class="">
                                            <a class="btn btn-primary" href="{{ route('app.product.index', [old('urlQuerry', $urlQuerry)]) }}" role="button">Zurück</a>
                                            <button type="submit" name="submit-type" value="stay" class="btn btn-primary">Speichern</button>
                                            <button type="button" title="{{ $product->presentation_short }} löschen" class="btn btn-danger float-right modal-delete"  href="#"
                                                    data-form-action="{{route('app.product.destroy', ['product'=>$product->id])}}"
                                                    data-form-info="Wollen Sie den Mitarbeiter '{{ $product->erp_kdnr }}' wirklich endgültig löschen?">Löschen
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
        @endsection


        @section('footer_scripts')
            <script type="text/javascript">

                function removedate(type) {
                    $('#'+type).val('');
                    $('#'+type+'_dummy').val('');
                }

                $(function() {

                    $("#dob_dummy").datepicker({
                        prevText: '&#x3c;zurück', prevStatus: '',
                        prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                        nextText: 'Vor&#x3e;', nextStatus: '',
                        nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                        currentText: 'heute', currentStatus: '',
                        todayText: 'heute', todayStatus: '',
                        clearText: '-', clearStatus: '',
                        closeText: 'schließen', closeStatus: '',
                        monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                        monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                        dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                        dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                        dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                        altField : "#dob",
                        altFormat : "yy-mm-dd",
                        dateFormat : "dd.mm.yy"
                    }, $.datepicker.regional['de']);
                });


            </script>
@stop