
<ul class="nav nav-tabs ml-0 mb-4">
    <li class="nav-item">
        <a class="nav-link @if(in_array(Route::currentRouteName(), ['app.product.show', 'app.product.edit'])) active @endif"
           href="{{ route('app.product.edit', ['product' => $product, old('urlQuerry', $urlQuerry)]) }}">Allgemein</a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'address')) active @endif"
           href="{{ route('app.product.address.index', ['product' => $product, old('urlQuerry', $urlQuerry)]) }}">Adressen</a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'settings')) active @endif"
           href="{{ route('app.product.settings.edit', ['product' => $product, old('urlQuerry', $urlQuerry)]) }}">Bestellungen</a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'settings')) active @endif"
           href="{{ route('app.product.settings.edit', ['product' => $product, old('urlQuerry', $urlQuerry)]) }}">Rechnungen</a>
    </li>


    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'settings')) active @endif"
           href="{{ route('app.product.settings.edit', ['product' => $product, old('urlQuerry', $urlQuerry)]) }}">Einstellungen</a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'notes')) active @endif"
           href="{{ route('app.product.comment.index', ['product' => $product, old('urlQuerry', $urlQuerry)]) }}">Notizen</a>
    </li>


</ul>
