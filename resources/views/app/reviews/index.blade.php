@extends('app.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Bewertungen beantworten</h4>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">

                    <div class="col-md-12">

                        <form id="submit-form" method="get" action="{{ route('app.reviews.show') }}">
                            <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                            @csrf
                            <div class="input-group">
                                <input id="reviewid" name="reviewid" type="text" class="form-control" placeholder="Review-ID" value="{{ old('reviewid') ?? $reviewid ?? '' }}">
                                <div class="input-group-btn" >
                                    <div class="input-group-append">
                                        <input id="kundennummer" name="kundennummer" type="text" class="form-control" style="border-radius: 0px 0px 0px 0px; border-left: 0px;"  placeholder="Kundennummer" value="{{ old('kundennummer') ?? $kundennummer ?? '' }}">
                                        <input id="ratingid" name="ratingid" type="text" style="border-radius: 0px 0px 0px 0px; border-left: 0px;" class="form-control" placeholder="Bewertungs-ID" value="{{ old('ratingid') ?? $ratingid ?? '' }}">

                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Bewertung suchen</button>
                                        <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.reviews.index', 'filter_reset=1') }}"><i class="fas fa-redo"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>


                    <div class="col-md-12 mt-4">

                        @if( !empty($review) )
                            <?php
                            $bewertung = $review['bewertung'];//bewertungstext
                            $rating = $review['rating'];//bewertungswert 1-5
                            $datum = $review['datum'];//datum des ersteintrag der bewertung
                            $kunde = $review['kunde'];//kundenname gekuerzt: vorname
                            $antwort = $review['antwort'];//antwort des beraters

                            $info =  "<b>".$kunde."</b>(".$kundennummer.") vergab <b>".$rating." Sterne</b> und schrieb am <b>".date('d.m.Y, H:i', $datum)." Uhr</b> zur Produkt ID <a target='_blank' href='https://www.body-attack.de/idbestand.php?id=".$ratingid."'>".$ratingid."</a> folgendes:<br>\"".trim($bewertung)."\"";

                            ?>
                            <p>{!! $info !!}</p>

                            <form id="submit-form"  method="POST" action="{{ route('app.reviews.store') }}">
                            @csrf

                            <label for="description" class="mt-4">{{ empty($antwort) ? 'Antworten' : 'Antwort bearbeiten' }}</label>
                            <div class="form-group row">

                                <input type="hidden" name="kundennummer" value="{{ $kundennummer ?? '' }}">
                                <input type="hidden" name="ratingid" value="{{ $ratingid ?? '' }}">
                                <input type="hidden" name="reviewid" value="{{ $reviewid ?? '' }}">

                                <div class="col-md-12">
                                    <textarea id="description" type="text" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" >{{ old('description', $antwort ?? '') }}</textarea>

                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <a title="Diese Bewertung aus dem Shop entfernen" class="mr-3 modal-delete"  href="#"
                               data-form-info="Wollen Sie die Bewertung von {{ $kunde }} wirklich endgültig löschen?"
                               data-form-action="{{route('app.reviews.destroy', ['reviewid'=> $reviewid, 'kundennummer'=>$kundennummer, 'ratingid'=>$ratingid])}}">Löschen
                            </a>
                            <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click float-right">Speichern</button>

                            </form>

                        @endif


                    </div>

                </div>
            </div>
        </div>

@endsection
