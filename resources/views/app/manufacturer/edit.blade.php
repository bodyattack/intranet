@extends('app.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> {{ $manufacturer->presentation_short }} {{ empty($manufacturer->id) ? ' erstellen' : 'bearbeiten' }} </div>

                    <div class="card-body">

                        @empty(!$manufacturer->id)
                            <form id="submit-form" method="POST" action="{{ route('app.manufacturer.update', ['manufacturer' => $manufacturer->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('app.manufacturer.store') }}">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $manufacturer->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Beschreibung</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" >{{ old('description', $manufacturer->description) }}</textarea>

                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="metatext" class="col-md-4 col-form-label text-md-right">Meta-Text</label>

                            <div class="col-md-6">
                                <input id="metatext" type="text" class="form-control {{ $errors->has('metatext') ? ' is-invalid' : '' }}" name="metatext" value="{{ old('metatext', $manufacturer->metatext) }}" >

                                @if ($errors->has('metatext'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('metatext') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                                
                        <div class="form-group row">
                            <label for="keywords" class="col-md-4 col-form-label text-md-right">Keywords</label>

                            <div class="col-md-6">
                                <input id="keywords" type="text" class="form-control {{ $errors->has('keywords') ? ' is-invalid' : '' }}" name="keywords" value="{{ old('keywords', $manufacturer->keywords) }}" >

                                @if ($errors->has('keywords'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('keywords') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                                

                        <div class="form-group row">
                            <label for="icon" class="col-md-4 col-form-label text-md-right">Name Bild-Icon</label>

                            <div class="col-md-6">
                                <input id="icon" type="text" class="form-control {{ $errors->has('icon') ? ' is-invalid' : '' }}" name="icon" value="{{ old('icon', $manufacturer->icon) }}" >

                                @if ($errors->has('icon'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('icon') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="rewrite" class="col-md-4 col-form-label text-md-right">Rewrite Link</label>

                            <div class="col-md-6">
                                <input id="rewrite" type="text" class="form-control {{ $errors->has('rewrite') ? ' is-invalid' : '' }}" name="rewrite" value="{{ old('rewrite', $manufacturer->rewrite) }}" >

                                @if ($errors->has('rewrite'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('rewrite') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click">Speichern</button>
                                <a style="float: right;" href="{{ route('app.manufacturer.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection