@extends('app.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $manufacturer->presentation_long }}</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="{{ $manufacturer->presentation_short }} neu anlegen" style="float: right;" href="{{ route('app.manufacturer.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">

                    <div class="col-md-12">

                        <form id="submit-form" method="get" action="{{ route('app.manufacturer.index') }}">
                            <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                            @csrf
                            <div class="input-group">
                                <input id="name" name="name" type="text" class="form-control" placeholder="Suchbegriff" value="{{ old('name') ?? $filter_attributes['name'] ?? '' }}">
                                <div class="input-group-btn" >
                                    <div class="input-group-append">
                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                                        <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.manufacturer.index', 'filter_reset=1') }}"><i class="fas fa-redo"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>


                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">

                                <thead class="thead-dark">
                                <tr>
                                    @headicons('1')
                                    <th>@sortablelink('name', 'Name', ['filter_row' => 'firstname'])</th>
                                    <th>@sortablelink('created_at', 'Erzeugt am', ['filter_row' => 'firstname'])</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody id="sortable-main" >
                                @forelse ( $manufacturers AS $manufacturer )
                                    <tr data-id="{{$manufacturer->id}}">
                                        <td title="Zeile verschieben zum ändern der Reihenfolge"><span class="ui-icon ui-icon-arrowthick-2-n-s pl-1"></span></td>
                                        {{--
                                        <td>
                                            <a title="{{ $manufacturer->presentation_short }}  {{ $manufacturer->active ? 'deaktivieren' : 'aktivieren' }}" class="mr-2"  href="{{ route('app.manufacturer.toggleActive', $manufacturer->id) }}" role="button"><i class="fa fa-circle {{ $manufacturer->active ? 'text-success' : 'text-secondary' }}"></i></a>
                                        </td>
                                        --}}

                                        <td>{{ $manufacturer->name }} </td>
                                        <td>@date($manufacturer->created_at)</td>
                                        <td>
                                            <a title="{{ $manufacturer->presentation_short }} bearbeiten" class="mr-2" href="{{ route('app.manufacturer.show', ['manufacturer'=>$manufacturer->id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="{{ $manufacturer->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die {{ $manufacturer->presentation_short }} '{{ $manufacturer->name }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('app.manufacturer.destroy', ['manufacturer'=>$manufacturer->id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $manufacturer->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        {{ $manufacturers->withQueryString()->links() }}

                    </div>

                </div>
            </div>
        </div>

@endsection

@section('footer_scripts')
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function() {

            $("#sortable-main").sortable({
                update: function() {
                    erg1 = [];
                    $('#sortable-main > tr').each(function(index) {
                        erg1[index]=$(this).data('id');
                    })
                    $.ajax({
                        method: "POST",
                        url: "{{route('app.manufacturer.updateOrder')}}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "pos": JSON.stringify(erg1)
                        }
                    });
                }
            });

        });
    </script>
@stop