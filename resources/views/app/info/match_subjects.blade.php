@extends('app.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="card-title card-title-lg">Rechnung intern weiterleiten über Zuordnungs-Betreff</h4>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">

                        <div class="alert alert-warning" role="alert">
                            Wenn Sie eine Rechnung direkt an Ihr persönliches Postfach erhalten haben, können Sie diese per Weiterleitung an die Rechnungskontrolle übergeben.
                            Dafür senden Sie die E-Mail einfach an die zugehörige E-Mail Adresse und verwenden als Betreff ausschließlich den hier genannten Zuordnungs-Betreff.
                            <div class="pb-3"></div>
                            @foreach(\App\IrMailbox::all() AS $mailbox) "{{$mailbox->user}}", für {{$mailbox->mandant->name}}<br> @endforeach
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Rechnungsteller</th>
                                    <th>Zuordnungs-Betreff</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $billers AS $biller )
                                    <tr id="">
                                        <td>{{ $biller->name }} </td>
                                        <td>{{ $biller->match_subject }} </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Es sind Keine Zuordnungen vorhanden, bitte wenden Sie sich an die Buchhaltung</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection

