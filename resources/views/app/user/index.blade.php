@extends('app.layouts.app')

@section('content')

<div class="container">
    <div class="card content-area">
        <div class="card-innr">
            <div class="card-head">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="card-title card-title-lg">Benutzer Übersicht</h4>
                    </div>
                    <div class="col-md-6">
                        @role('manager')
                        <a title="Neuen Benutzer anfragen" style="float: right;" href="{{ route('app.user.request') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        @endrole
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped mt-2 mb-3">
                            <thead class="thead-dark">
                            <tr>
                                <th>Name</th>
                                <th>E-Mail</th>
                                <th>Telefon</th>
                                <th>Abteilung</th>
                                <th>Letzter login</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ( $users AS $user )
                                <tr id="">
                                    <td>{{ $user->fullname() }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->tel }}</td>
                                    <td>{{ $user->department->name }}</td>
                                    <td>@datetime($user->last_login_at)</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
