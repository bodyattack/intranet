<ul class="nav nav-tabs-line mb-3">
    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'index')) active @endif"
           href="{{route('app.coupon.index', $urlQuerry) }}">Übersicht</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(strpos(Route::currentRouteName(), 'statistics')) active @endif"
           href="{{route('app.coupon.statistics', $urlQuerry) }}">Auswertung</a>
    </li>
</ul>


