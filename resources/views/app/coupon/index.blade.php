@extends('app.layouts.app')
@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $coupon->presentation_long }}</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="{{ $coupon->presentation_short }} neu anlegen" style="float: right;" href="{{ route('app.coupon.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">

                    <div class="col-md-12">

                        <?php
                        $urlQuerry='';
                        if(strpos(url()->full(), '?')) {
                            $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
                        }
                        if($urlQuerry=='' & strpos(url()->previous(), '?')) {
                            $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
                        }
                        ?>

                        @include('app.coupon.subnavi',['urlQuery' => $urlQuerry])

                        <form id="submit-form" method="get" action="{{ route('app.coupon.index') }}">
                            <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                            @csrf
                            <div class="input-group">
                                <input id="filter_code" name="filter_code" type="text" class="form-control" placeholder="Gutschein Code" value="{{ old('filter_code') ?? $filter_attributes['filter_code'] ?? '' }}">
                                <div class="input-group-btn" >
                                    <div class="input-group-append">
                                        <select name="filter_art" id="filter_art" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            @foreach (['0' => 'Jede Art', '1' => 'Art Gutschrift', '2' => 'Art Prozent', '3' => 'Art Gratisprodukt', '4' => 'Art Versandkostenfrei'] as $key=>$value)
                                                <option value="{{ $key }}"{{ old('filter_art') == $key || ( isset($filter_attributes['filter_art']) && $filter_attributes['filter_art'] == $key ) ? 'selected="selected"' : '' }} >{{ $value }} </option>
                                            @endforeach
                                        </select>
                                        <select  name="filter_multi" id="filter_multi" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            @foreach (['0' => 'Mit Multicodes', '1' => 'Nur Multicodes', '2' => 'Ohne Multicodes'] as $key=>$value)
                                                <option value="{{ $key }}"{{ old('filter_multi') == $key || ( isset($filter_attributes['filter_multi']) && $filter_attributes['filter_multi'] == $key ) ? 'selected="selected"' : '' }} >{{ $value }} </option>
                                            @endforeach
                                        </select>
                                        <select  name="filter_cheaper_check" id="filter_cheaper_check" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            @foreach (['1' => 'Mit Rabatt Check', '0' => 'Ohne Rabatt Check'] as $key=>$value)
                                                <option value="{{ $key }}"{{ old('filter_cheaper_check') == $key || ( isset($filter_attributes['filter_cheaper_check']) && $filter_attributes['filter_cheaper_check'] == $key ) ? 'selected="selected"' : '' }} >{{ $value }} </option>
                                            @endforeach
                                        </select>
                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                                        <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.coupon.index' ) }}"><i class="fas fa-redo"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>

                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">

                                <thead class="thead-dark">
                                <tr>
                                    @headicons('1')
                                    <th>Code</th>
                                    <th>Einlösungen</th>
                                    <th>Art</th>
                                    <th>Wert</th>
                                    <th>Rabatt Check</th>
                                    <th>Erstellt</th>
                                    @headicons('4', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>

                                @forelse ( $coupons AS $coupon )
                                    <?php
                                    $einloesungen_ist = $coupon->uses();
                                    $einloesungen = $einloesungen_ist. ' von '.$coupon->uses_max();
                                    if(empty($coupon->use_max)) {
                                        $einloesungen = $einloesungen_ist. ' von <span style="font-weight:lighter; font-size:20px">&infin;</span>';
                                    }
                                    $art = '';
                                    switch($coupon->art) {
                                        case '1' : $art = 'Gutschrift'; break;
                                        case '2' : $art = 'Prozent'; break;
                                        case '3' : $art = 'Gratisprodukt'; break;
                                        case '4' : $art = 'Versandkostenfrei'; break;
                                    }

                                    $row_color = '';
                                    $info_circle_class = 'text-success'; // green
                                    $info_circle_titel  = 'deaktivieren';

                                    if(!$coupon->isValid($einloesungen_ist)) {
                                        $row_color = '#bdbdbd';
                                        $info_circle_class = 'text-warning'; // gelb-orange
                                        $info_circle_titel = 'auf Grund seiner individuellen Konfiguration inaktiv';
                                        if(!$coupon->active) {
                                            $info_circle_class = 'text-secondary'; // red
                                            $info_circle_titel  = 'aktivieren';
                                        }
                                    }
                                    ?>

                                    <tr style="color: {{$row_color}};">
                                        <td>
                                            <a title="{{ $coupon->presentation_short }} {{ $info_circle_titel }}" class="mr-2"  href="{{ route('app.coupon.toggleActive', $coupon->id) }}" role="button"><i class="fa fa-circle {{ $info_circle_class }}"></i></a>
                                        </td>
                                        <td title="{{ $coupon->description_private }}">
                                            @if($coupon->multi!=1)
                                                <i>{{ $coupon->codes->count() }}x Multicodes</i>
                                            @else
                                                {{ $coupon->codes[0]->code }}
                                            @endif
                                        </td>
                                        <td>{!! $einloesungen  !!}</td>
                                        <td>{{ $art }} </td>
                                        <td>{{ $coupon->value }} </td>
                                        <td>{{ $coupon->cheaper_check ? 'JA' : 'Nein' }} </td>
                                        <td title="{{ $coupon->userCreate->fullName() }}">@date($coupon->created_at)</td>
                                        <td>
                                            <a title="{{ $coupon->presentation_short }} details" class="mr-2" href="{{ route('app.coupon.details', ['coupon'=>$coupon->id, $urlQuerry ] ) }}"><i class="far fa-eye"></i></a>
                                            <a title="{{ $coupon->presentation_short }} Statistik" class="mr-2" href="{{ route('app.coupon.statistics', $urlQuerry.'&filter=1&coupon='.$coupon->id ) }}"><i class="far fa-chart-bar"></i></a>
                                            <a title="{{ $coupon->presentation_short }} bearbeiten" class="mr-2" href="{{ route('app.coupon.show', ['coupon'=>$coupon->id, $urlQuerry] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="{{ $coupon->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die {{ $coupon->presentation_short }} '{{ $coupon->name }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('app.coupon.destroy', ['coupon'=>$coupon->id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $coupon->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row justify-content-md-center">
                {{ $coupons->withQueryString()->links() }}
            </div>

        </div>

@endsection
