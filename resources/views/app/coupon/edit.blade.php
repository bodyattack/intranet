@extends('app.layouts.app')
@section('content')
<?php
$searchable_select=1; // ergänze die class "selectpicker" select tag um  filtern zu können
$vs_free_display = 'show';
if($coupon->art==4) {
    $vs_free_display = 'none';
}

$urlQuerry='';
if(strpos(url()->full(), '?')) {
    $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
}
if($urlQuerry=='' & strpos(url()->previous(), '?')) {
    $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
}
?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> {{ $coupon->presentation_short }} {{ empty($coupon->id) ? ' erstellen' : 'bearbeiten' }} </div>

                    <div class="card-body">

                        @empty(!$coupon->id)
                            <form id="submit-form" method="POST" action="{{ route('app.coupon.update', ['coupon' => $coupon->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('app.coupon.store') }}">
                        @endempty

                        @csrf


                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-left font-italic font-bold">Pflicht Angaben</label>
                            <label for="name" class="col-md-2 col-form-label text-md-right">Art</label>

                            <div class="col-md-6">
                                <select required autofocus name="art" class="form-control" id="art">
                                    <option value="0" >-- Bitte auswählen --</option>
                                    @foreach (['1' => 'Gutschrift', '2' => 'Prozent', '3' => 'Gratisprodukt', '4' => 'Versandkostenfrei'] as $key=>$value)
                                        <option value="{{ $key }}" @if($coupon->art == $key || old('art') == $key) selected @endif>{{ $value }} </option>
                                    @endforeach
                                </select>

                                @if ($errors->has('art'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('art') }}</strong>
                            </span>
                                @endif

                            </div>
                        </div>

                        <?php
                        $mulitcode = false;
                        if(!empty($coupon->multi) && $coupon->multi!=1) $mulitcode = true;
                        $code = '';
                        if(!empty($coupon->multi) && !$mulitcode) $code = $coupon->codes[0]->code;
                        ?>

                        <div class="form-group row ">
                            <label for="code" class="col-md-4 col-form-label text-md-right">Code</label>

                            <div class="col-md-6">
                                <input title="{{ !$mulitcode ?: 'Bearbeitung nicht möglich, da Multicodes' }}" id="code" type="text" class="form-control {{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ old('code',$code)  }}" {{ !$mulitcode ?: 'disabled' }} >

                                @if ($errors->has('code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Werden Mulitcodes erstellt, kann das Feld auch frei gelassen werden.<br><br>Hinweis: Das Feld ist bei Verwendung von Multicodes nachträglich nicht mehr editierbar. Alle Optionen / Einstellungen sind gültig auch für jeden Multicode!" class="fas fa-info-circle"></i>
                            </div>
                        </div>


                        <div style="display: {{$vs_free_display}}"  class="form-group row vs-free">
                            <label for="value" class="col-md-4 col-form-label text-md-right">Wert</label>

                            <div class="col-md-6">
                                <input id="value" type="text" class="form-control {{ $errors->has('value') ? ' is-invalid' : '' }}" name="value" value="{{ old('value', $coupon->value) }}"  >

                                @if ($errors->has('value'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('value') }}</strong>
                            </span>
                                @endif

                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Bitte englische Schreibweise verwenden bei Kommazahlen -> 20.50" class="fas fa-info-circle"></i>
                            </div>
                        </div>



                        <div style="display: {{$vs_free_display}}"  class="form-group row vs-free">
                            <label for="cheaper_check" class="col-md-4 col-form-label text-md-right">Rabatt Prüfung</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="input-bordered form-control" name="cheaper_check" id="cheaper_check">
                                            <option value="1" {{ $coupon->cheaper_check == 1 || old('cheaper_check', $coupon->cheaper_check) == 1 ? 'selected' : ''  }}>Ja</option>
                                            <option value="0" {{ ($coupon->cheaper_check !== null && $coupon->cheaper_check == 0) || (old('cheaper_check', $coupon->cheaper_check)  !== null && old('cheaper_check', $coupon->cheaper_check) == 0) ? 'selected' : '' }}>Nein</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Bei JA, werden existierende Artikel Rabatte zunächst verrechnent.<br> Nur NEIN auswählen, wenn der Gutschein unabhänig von den Artikeln voll auf die Gesamtsumme angewendet werden soll!" class="fas fa-info-circle"></i>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="description_private" class="col-md-4 col-form-label text-md-right">Beschreibung intern</label>

                            <div class="col-md-6">
                                <textarea id="description_private" type="text" class="form-control {{ $errors->has('description_private') ? ' is-invalid' : '' }}" name="description_private" >{{ old('description_private', $coupon->description_private) }}</textarea>

                                @if ($errors->has('description_private'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description_private') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Nicht öffentlich, interne Notiz (Ausstellungsgrund / Kampange etc.)" class="fas fa-info-circle"></i>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label for="name" class="col-md-1 col-form-label text-md-left font-italic font-bold">Optional</label>
                            <label for="description" class="col-md-3 col-form-label text-md-right">Beschreibung öffentlich</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description', $coupon->description) }}" >

                                {{-- <textarea id="description" type="text" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" >{{ old('description', $coupon->description) }}</textarea> --}}

                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Wird auf der Shop Seite mit dargestellt, für Zeilenumbrüche ein  < / br > (ohne die Leerzeichen) an der entsprechenden Stelle einsetzen" class="fas fa-info-circle"></i>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="free_shipping_countries" class="col-md-4 col-form-label text-md-right">Versandkostenfei in</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select name="free_shipping_countries[]" class=" selectpicker form-control" id="free_shipping_countries" multiple data-actions-box="true">
                                            <option disabled >-- Bitte wählen --</option>
                                            @php( $free_shipping_countries = explode(',', $coupon->free_shipping_countries) )
                                            @foreach (['DE' => 'DE', 'AT' => 'AT', 'CH' => 'CH'] as $key=>$value)
                                                <option value="{{$key}}" {{ ( in_array($key , $free_shipping_countries) || in_array($key, old('free_shipping_countries', [])) ) ? 'selected="selected"' : '' }} >{{$value}}</option>

                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Kann generell immer zusätzlich gewählt werden, oder bei Art 'Versandkostenfrei' zur Begrenzung auf bestimmte Shops" class="fas fa-info-circle"></i>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="invoice_approvers" class="col-md-4 col-form-label text-md-right">Shop Zuordnung</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select name="shops[]" class=" selectpicker form-control" id="shops" multiple data-actions-box="true">
                                            <option disabled >-- Bitte wählen --</option>
                                            @php( $shops_to_approve = explode(',', $coupon->shops) )
                                            @foreach (['1' => 'body-attack.de/.at/.ch', '19' => 'body-attack.com/co.uk'] as $key=>$value)
                                                <option value="{{$key}}" {{ ( in_array($key , $shops_to_approve) || in_array($key, old('shops', [])) ) ? 'selected="selected"' : '' }} >{{$value}}</option>

                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Ohne Auswahl, gültig für alle Shops" class="fas fa-info-circle"></i>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="date_from_dummy" class="col-md-4 col-form-label text-md-right">Gültig von</label>
                            <div class="col-md-6">
                                <?php
                                $date_from_dummy= old('date_from_dummy', (!empty($coupon->date_from)? $coupon->date_from :''));
                                $date_from= old('date_from', $coupon->date_from);
                                ?>
                                <i data-toggle="tooltip" data-html="true" onclick="removedate('date_from')" data-original-title="Datum entfernen"  style="right: 20px; font-size: 20px; position: absolute; margin: 10px; color: #e69b9b;" class="fas fa-times"></i>
                                <input style="background: white" autocomplete="off" id="date_from_dummy" type="text"  data-target="date_from" class="datefield form-control{{ $errors->has('date_from_dummy') ? ' is-invalid' : '' }}" name="date_from_dummy"  value="@date($date_from_dummy)">
                                <input id="date_from" type="hidden" name="date_from" value="@datetimeDB($date_from)">

                                @if ($errors->has('date_from'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date_to_dummy" class="col-md-4 col-form-label text-md-right">Gültig bis</label>
                            <div class="col-md-6">
                                <?php
                                $date_to_dummy= old('date_to_dummy', (!empty($coupon->date_to)? $coupon->date_to :''));
                                $date_to= old('date_to', $coupon->date_to);
                                ?>
                                <i data-toggle="tooltip" data-html="true" onclick="removedate('date_to')" data-original-title="Datum entfernen"  style="right: 20px; font-size: 20px; position: absolute; margin: 10px; color: #e69b9b;" class="fas fa-times"></i>
                                <input style="background: white" autocomplete="off" id="date_to_dummy" type="text"  data-target="date_to" class="datefield form-control{{ $errors->has('date_to_dummy') ? ' is-invalid' : '' }}" name="date_to_dummy"  value="@date($date_to_dummy)">
                                <input id="date_to" type="hidden" name="date_to" value="@datetimeDB($date_to)">

                                @if ($errors->has('date_to'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Beide Datums Werte sind optional und können einen Gutschein im Zeitraum eingrenzen" class="fas fa-info-circle"></i>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="multi" class="col-md-4 col-form-label text-md-right">Multicodes / Gutschein-Anzahl</label>

                            <div class="col-md-6">
                                <input title="{{ !$mulitcode ?: 'Bearbeitung nicht möglich, da Multicodes' }}"  id="multi" type="text" class="form-control {{ $errors->has('multi') ? ' is-invalid' : '' }}" name="multi" value="{{ old('multi', $coupon->multi) ?? 1 }}"  {{ !$mulitcode ?: 'disabled' }}  >

                                @if ($errors->has('multi'))
                                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('multi') }}</strong>
                        </span>
                                @endif

                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Standardmäßig 1, es können bis zu 4000 individuelle Gutscheine erzeugt werden." class="fas fa-info-circle"></i>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="use_max" class="col-md-4 col-form-label text-md-right">Maximale Anzahl Einlösungen</label>

                            <div class="col-md-6">
                                <input id="use_max" type="text" class="form-control {{ $errors->has('use_max') ? ' is-invalid' : '' }}" name="use_max" value="{{ old('use_max', $coupon->use_max) }}" >

                                @if ($errors->has('use_max'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('use_max') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Unbegrenzt, wenn das Feld leer ist" class="fas fa-info-circle"></i>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="min_order_value" class="col-md-4 col-form-label text-md-right">Min-Bestellwert</label>

                            <div class="col-md-6">
                                <input id="min_order_value" type="text" class="form-control {{ $errors->has('min_order_value') ? ' is-invalid' : '' }}" name="min_order_value" value="{{ old('min_order_value', $coupon->min_order_value) }}" >

                                @if ($errors->has('min_order_value'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('min_order_value') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Ein Betrag der mindestens als Gegenwert im Warenkorb vorhanden sein muss, damit der Gutschein greift.<br>Ohne Angabe kann bei der Gutschein Art 'Absolut', bis 0€ rabattiert werden - Stichwort: Geschenkgutschein, Gewinnspiel." class="fas fa-info-circle"></i>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="max_order_value" class="col-md-4 col-form-label text-md-right">Max-Bestellwert</label>

                            <div class="col-md-6">
                                <input id="max_order_value" type="text" class="form-control {{ $errors->has('max_order_value') ? ' is-invalid' : '' }}" name="max_order_value" value="{{ old('max_order_value', $coupon->max_order_value) }}" >

                                @if ($errors->has('max_order_value'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('max_order_value') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Der Betrag auf den ein Gutschein maximal angewendet werden kann." class="fas fa-info-circle"></i>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mandatory_products" class="col-md-4 col-form-label text-md-right">Pflicht Produkte (Komma Getrennt)</label>

                            <div class="col-md-6">
                                <textarea id="mandatory_products" type="text" class="form-control {{ $errors->has('mandatory_products') ? ' is-invalid' : '' }}" name="mandatory_products" >{{ old('mandatory_products', $coupon->mandatory_products) }}</textarea>

                                @if ($errors->has('mandatory_products'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mandatory_products') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Artikel die im Warenkorb sein müssen, damit ein Gutschein greift (Produkt-ID des Shops)" class="fas fa-info-circle"></i>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="free_products" class="col-md-4 col-form-label text-md-right">Gratis Produkte</label>

                            <div class="col-md-6">
                                <textarea id="free_products" type="text" class="form-control {{ $errors->has('free_products') ? ' is-invalid' : '' }}" name="free_products" >{{ old('free_products', $coupon->free_products) }}</textarea>

                                @if ($errors->has('free_products'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('free_products') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Produkt-ID des Shops, Komma getrennt bei mehreren Produkten" class="fas fa-info-circle"></i>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="free_articles" class="col-md-4 col-form-label text-md-right">Gratis Artikel</label>

                            <div class="col-md-6">
                                <textarea id="free_articles" type="text" class="form-control {{ $errors->has('free_articles') ? ' is-invalid' : '' }}" name="free_articles" >{{ old('free_articles', $coupon->free_articles) }}</textarea>

                                @if ($errors->has('free_articles'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('free_articles') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="ERP-Nummer des Atrikels, Komma getrennt bei mehreren Artikeln" class="fas fa-info-circle"></i>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="manufacturers" class="col-md-4 col-form-label text-md-right">Herstellerbeschränkung</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select name="manufacturers[]" class=" selectpicker form-control" id="manufacturers" multiple data-actions-box="true">
                                            <option disabled >-- Bitte wählen --</option>
                                            <?php

                                            $manufacturers_data = array(
                                            array('manufacturers_id' => '272','manufacturers_name' => 'RCADIA NEURO NUTRITION'),
                                            array('manufacturers_id' => '254','manufacturers_name' => '3ATHLET Pure Hygienics'),
                                            array('manufacturers_id' => '241','manufacturers_name' => 'Barebells'),
                                            array('manufacturers_id' => '249','manufacturers_name' => 'Best Joy'),
                                            array('manufacturers_id' => '237','manufacturers_name' => 'BiFi'),
                                            array('manufacturers_id' => '162','manufacturers_name' => 'Biotona'),
                                            array('manufacturers_id' => '188','manufacturers_name' => 'BlenderBottle'),
                                            array('manufacturers_id' => '26','manufacturers_name' => 'Body Attack'),
                                            array('manufacturers_id' => '175','manufacturers_name' => 'Body Attack Sports Nutrition'),
                                            array('manufacturers_id' => '215','manufacturers_name' => 'Body Performance'),
                                            array('manufacturers_id' => '158','manufacturers_name' => 'BSN'),
                                            array('manufacturers_id' => '209','manufacturers_name' => 'Cellucor'),
                                            array('manufacturers_id' => '253','manufacturers_name' => 'Chiba'),
                                            array('manufacturers_id' => '148','manufacturers_name' => 'CLIF Bar'),
                                            array('manufacturers_id' => '105','manufacturers_name' => 'DAVINA'),
                                            array('manufacturers_id' => '219','manufacturers_name' => 'Dedicated Nutrition'),
                                            array('manufacturers_id' => '85','manufacturers_name' => 'EFX'),
                                            array('manufacturers_id' => '145','manufacturers_name' => 'Energy Cake GmbH'),
                                            array('manufacturers_id' => '236','manufacturers_name' => 'ESN'),
                                            array('manufacturers_id' => '137','manufacturers_name' => 'JabuVit'),
                                            array('manufacturers_id' => '122','manufacturers_name' => 'Jack Link´s'),
                                            array('manufacturers_id' => '178','manufacturers_name' => 'Lenny & Larry´s'),
                                            array('manufacturers_id' => '255','manufacturers_name' => 'M&M´S'),
                                            array('manufacturers_id' => '218','manufacturers_name' => 'MARS incorporated'),
                                            array('manufacturers_id' => '247','manufacturers_name' => 'Mutant'),
                                            array('manufacturers_id' => '108','manufacturers_name' => 'My Supps'),
                                            array('manufacturers_id' => '261','manufacturers_name' => 'Ironsport')
                                            );
                                            $manufacturers = explode(',', $coupon->manufacturers)
                                            ?>
                                            @foreach ($manufacturers_data as $manufacturer)
                                                <option value="{{$manufacturer['manufacturers_id']}}" {{ ( in_array($manufacturer['manufacturers_id'] , $manufacturers) || in_array($manufacturer['manufacturers_id'], old('manufacturers', [])) ) ? 'selected="selected"' : '' }} >{{$manufacturer['manufacturers_name']}}</option>

                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Auswählen wenn auf einen Hersteller beschränkt werden soll" class="fas fa-info-circle"></i>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click">Speichern</button>
                                <a style="float: right;" href="{{ route('app.coupon.index', $urlQuerry) }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer_scripts')
    <script type="text/javascript">

        $('#art').on('change', function(){
            var vs_free = $('.vs-free');
            if($(this).val()==4){
                vs_free.hide();
            }
            else {
                vs_free.show();
            }
        });

        $('.datefield').on('click', function(){
            $(this).select();
        });

        $('.datefield').on('keyup', function(e){

            var date = $(this).val();
            var target = $(this).data('target');
            var date_lenght = date.length;

            var new_date = '00.00.0000';
            var new_date_target = '2000-99-99';
            var change = false;

            if(date_lenght == 8 && date.match(/^(0[1-9]|[12]\d|3[01])(0[1-9]|1[0-2])\d{4}$/) != null) {

                var day = date.substring(0, 2);
                var month = date.substring(2, 4);
                var year = date.substring(4);

                new_date_target = year + '-' + month + '-' + day;
                new_date = day + '.' + month + '.' + year;

                change = true;

            }
            else if(date_lenght >= 8 && e.key != 'Tab')  {
                change = true;
            }

            if(change) {
                $(this).val(new_date);
                $('#' + target).val(new_date_target);
            }

        });


        function removedate(type) {
            $('#'+type).val('');
            $('#'+type+'_dummy').val('');
        }

        function displayOptions(e) {
            $('#options').hide();
            $('#comment').html('');
            if(e.value=='reklamieren') {
                $('#options').show();
            }
        }


        $(function() {

            $("#date_from_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#date_from",
                altFormat : "yy-mm-dd 00:00:01",
                dateFormat : "dd.mm.yy"
            }, $.datepicker.regional['de']);

            $("#date_to_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#date_to",
                altFormat : "yy-mm-dd 23:59:59",
                dateFormat : "dd.mm.yy"
            }, $.datepicker.regional['de']);

        });


    </script>
@stop