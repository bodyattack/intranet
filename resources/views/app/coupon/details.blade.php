@extends('app.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> {{ $coupon->presentation_short }} </div>

                    <div class="card-body">

                        <?php
                        $mulitcode = false;
                        if(!empty($coupon->multi) && $coupon->multi!=1) $mulitcode = true;
                        $code = '';

                        $art = '';
                        switch($coupon->art) {
                            case '1' : $art = 'Gutschrift'; break;
                            case '2' : $art = 'Prozent'; break;
                            case '3' : $art = 'Gratisprodukt'; break;
                            case '4' : $art = 'Versandkostenfrei'; break;
                        }

                        $codes = $coupon->codes()->with('orders')->get();
                        ?>

                        <?php
                        $urlQuerry='';
                        if(strpos(url()->full(), '?')) {
                            $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
                        }
                        if($urlQuerry=='' & strpos(url()->previous(), '?')) {
                            $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
                        }
                        ?>


                        <div class="form-group row mt-5">
                            <label for="name" class="col-md-4 col-form-label">Gutscheindaten</label>

                            <div class="col-md-6">
                                <p><span style="min-width: 250px; display: inline-block;">Art:</span>{{ $art }}</p>
                                @if(!empty($coupon->description_private))
                                <p><span style="min-width: 250px; display: inline-block;">Beschreibung intern:</span>{{ $coupon->description_private }}</p>
                                @endif
                                @if(!empty($coupon->description))
                                <p><span style="min-width: 250px; display: inline-block;">Beschreibung öffentlich:</span>{{ $coupon->description}}</p>
                                @endif
                                <p><span style="min-width: 250px; display: inline-block;">Rabatt Prüfung:</span>{{ ( $coupon->cheaper_check ? 'JA' : 'Nein' ) }}</p>
                                @if(!empty($coupon->free_shipping_countries))
                                <p><span style="min-width: 250px; display: inline-block;">Versandkostenfei in:</span>{{ $coupon->free_shipping_countries }}</p>
                                @endif
                                @if(!empty($coupon->shops))
                                <p><span style="min-width: 250px; display: inline-block;">Shop Zuordnung:</span>{{ $coupon->shops }}</p>
                                @endif
                                @if(!empty($coupon->date_from))
                                <p><span style="min-width: 250px; display: inline-block;">Gültig von:</span>{{ $coupon->date_from }}</p>
                                @endif
                                @if(!empty($coupon->date_to))
                                <p><span style="min-width: 250px; display: inline-block;">Gültig bis:</span>{{ $coupon->date_to }}</p>
                                @endif
                                @if(!empty($coupon->min_order_value))
                                <p><span style="min-width: 250px; display: inline-block;">Min-Bestellwert:</span>{{ $coupon->min_order_value }}</p>
                                @endif
                                @if(!empty($coupon->max_order_value))
                                <p><span style="min-width: 250px; display: inline-block;">Max-Bestellwert:</span>{{ $coupon->max_order_value }}</p>
                                @endif
                                @if(!empty($coupon->use_max))
                                <p><span style="min-width: 250px; display: inline-block;">Max-Einlösungen pro Code:</span>{{ $coupon->use_max }}</p>
                                @endif

                                <br>
                                <p><span style="min-width: 250px; display: inline-block;">Ersteller:</span>{{ $coupon->userCreate->fullName() }}, am @date($coupon->created_at)</p>
                                @if(!empty($coupon->updated_by))
                                <p><span style="min-width: 250px; display: inline-block;">Letzte Anpassung:</span>{{ $coupon->userUpdate->fullName() }}, am @date($coupon->updated_at)</p>
                                @endif
                                <br>
                                <p><span style="min-width: 250px; display: inline-block;">Aktuelle Einlösungen gesammt:</span>{{ array_sum($coupon->codes()->withCount('orders')->pluck('orders_count')->toArray()) }}</p>
                            </div>


                        </div>

                        @if($mulitcode)
                        <div class="form-group row mt-lg-5 mb-0">
                            <div class="col-md-6 offset-md-4">
                                <span data-href="{{ route('app.coupon.codesCsv', ['coupon'=>$coupon->id]) }}" id="export" class="btn btn-primary" onclick="exportCodes(event.target);">Codes CSV Export</span>
                                <a style="float: right;" href="{{ route('app.coupon.index', $urlQuerry) }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row mt-lg-4">
                            <label for="name" class="col-md-4 col-form-label">Nutzungsdaten</label>

                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped mt-2 mb-3">

                                        <thead class="thead-dark">
                                        <tr>

                                            <th>Code</th>
                                            <th>Einlösungen</th>
                                            <th>Bestellnummer/n</th>

                                        </thead>
                                        <tbody>

                                        @foreach($codes AS $code )

                                            <?php
                                            $use_count = $code->orders->count();
                                            $used = false;
                                            if(!empty($coupon->use_max) && $use_count>=$coupon->use_max)
                                            $used = true;
                                            ?>

                                            <tr id="">
                                                <td>{!! ( $used ? '<strike>'.$code->code.'</strike>' : $code->code ) !!} </td>
                                                @if( $code->orders->isEmpty() )
                                                    <td>0</td>
                                                    <td> - </td>
                                                @else
                                                    <td>{{ $use_count  }} </td>
                                                    <td>
                                                        @foreach($code->orders AS $order)
                                                            <span class="d-lg-inline-block w-100">{{ $order->transaction_key }}<a title="Ansehen" class="ml-2" target="_blank" href="{{ route('app.temporder.show', ['temporder'=>$order->id] ) }}"><i class="far fa-eye"></i></a></span>
                                                        @endforeach
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>



                        <div class="form-group row mt-4 mb-0">
                            <div class="col-md-6 offset-md-4">
                                <span data-href="{{ route('app.coupon.codesCsv', ['coupon'=>$coupon->id]) }}" id="export" class="btn btn-primary" onclick="exportCodes(event.target);">Codes CSV Export</span>
                                <a style="float: right;" href="{{ route('app.coupon.index', $urlQuerry) }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('footer_scripts')
    <script type="text/javascript">

        function exportCodes(_this) {
            let _url = $(_this).data('href');
            window.location.href = _url;
        }

    </script>
@stop