
@extends('app.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Gutscheine</h4>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">

                    <div class="col-md-12">

                        <?php
                        $urlQuerry='';
                        if(strpos(url()->full(), '?')) {
                            $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
                        }
                        if($urlQuerry=='' & strpos(url()->previous(), '?')) {
                            $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
                        }
                        ?>

                        @include('app.coupon.subnavi',['urlQuerry' => $urlQuerry])

                        <form id="submit-form" method="get" action="{{ route('app.coupon.statistics') }}">
                            <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                            @csrf
                            <div class="input-group">
                                <input id="code" name="code" type="text" class="form-control" placeholder="Hier Gutschein Code eingeben um Aufträge auszuwerten" value="{{ old('code') ?? $filter_attributes['code'] ?? '' }}">
                                <input id="coupon" name="coupon" type="hidden" value="{{ old('coupon') ?? $filter_attributes['coupon'] ?? '' }}">
                                <div class="input-group-btn" >
                                    <div class="input-group-append">
                                        <?php
                                        $datefrom_filter = ( isset($filter_attributes['datefrom_filter']) ? $filter_attributes['datefrom_filter'] : old('datefrom_filter') );
                                        $dateto_filter = ( isset($filter_attributes['dateto_filter']) ? $filter_attributes['dateto_filter'] : old('dateto_filter') );
                                        ?>
                                        <span class="input-group">
                                            <i data-toggle="tooltip" data-html="true" onclick="removedate('datefrom_filter')" data-original-title="Datum entfernen"  style="font-size: 16px; position: absolute; right: 0px; margin: 10px; color: #e69b9b; z-index: 1" class="fas fa-times"></i>
                                            <input placeholder="Bestelldatum von / optional" style="min-width: 250px; border-radius: 0px; background: white" readonly autocomplete="off" id="datefrom_filter_dummy" type="text" class="form-control" name="datefrom_filter_dummy" value="@empty(!$datefrom_filter)@date($datefrom_filter)@endempty">
                                            <input id="datefrom_filter" type="hidden" name="datefrom_filter" value="{{$datefrom_filter}}">
                                        </span>
                                        <span class="input-group">
                                            <i data-toggle="tooltip" data-html="true" onclick="removedate('dateto_filter')" data-original-title="Datum entfernen"  style="font-size: 16px; position: absolute; right: 0px; margin: 10px; color: #e69b9b; z-index: 1" class="fas fa-times"></i>
                                            <input  placeholder="Bestelldatum bis / optional" style="min-width: 250px;  border-radius: 0px; border-width: 1px 0px; background: white" readonly autocomplete="off" id="dateto_filter_dummy" type="text" class="form-control" name="dateto_filter_dummy" value="@empty(!$dateto_filter)@date($dateto_filter)@endempty">
                                            <input id="dateto_filter" type="hidden" name="dateto_filter" value="{{$dateto_filter}}">
                                       </span>
                                        <a style="min-width: 220px; line-height: unset; padding: 0px; border-radius: 0px; line-height: 35px;"  class="btn btn-secondary "  role="button" href="{{ route('app.coupon.exportCsv', 'filter=1&coupon='.($filter_attributes['coupon'] ?? '' )) }}">Daten als CSV Exportieren</a>
                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Anzeigen</button>
                                        <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.coupon.statistics' ) }}"><i class="fas fa-redo"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>

                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">

                                <thead class="thead-dark">
                                <tr>
                                    <th>Code</th>
                                    <th>Datum</th>
                                    <th>Best.-Nr.: Shop</th>
                                    <th>Best.-Nr.: Intranet</th>
                                    <th>Kunde</th>
                                    <th>Ort</th>
                                    <th>Zahlart</th>
                                    <th>Artikel</th>
                                    <th class="text-right">Warenwert</th>
                                    <th class="text-right">Nachlass in €</th>
                                    <th class="text-right">Nachlass in %</th>
                                    <th>Art</th>
                                    @headicons('1', 'fa-eye')
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                 // Statistik sum
                                $pos=0;
                                $global_sum['gesamtsumme']=0;
                                $global_sum['warenkorb_nachlass']=0;
                                $global_sum['artikel_count']=0;
                                $global_sum['rabatt']=0;

                                $sum_by_zahlart=[];

                                $couopn_arten[1]='Gutschrift';
                                $couopn_arten[2]='Prozent';
                                $couopn_arten[3]='Gratisprodukt';
                                $couopn_arten[4]='Versandkostenfrei';

                                $output = 0;
                                ?>

                                @forelse ( $couponCodes AS $code )
                                    <?php
                                    $output = 1;
                                    $coupon = $code->coupon;
                                    $orders = $code->orders;
                                    $art = '';
                                    ?>
                                    @foreach ( $orders AS $order )
                                    <?php
                                        
                                    $preis_vorher = $order->gesamtsumme + $order->warenkorb_nachlass;
                                    $rabatt = round((($order->warenkorb_nachlass * 100) / $preis_vorher), 2);
                                    $artikel_count = count($order->warenkorb_string);
                                    
                                    $pos++;
                                    $global_sum['gesamtsumme']+=$order->gesamtsumme;
                                    $global_sum['warenkorb_nachlass']+=$order->warenkorb_nachlass;
                                    $global_sum['artikel_count']+=$artikel_count;
                                    $global_sum['rabatt']+=$rabatt;

                                    if(!isset($sum_by_zahlart[$order->zahlungsart])) {
                                        $sum_by_zahlart[$order->zahlungsart]['use']=0;
                                        $sum_by_zahlart[$order->zahlungsart]['gesamtsumme']=0;
                                        $sum_by_zahlart[$order->zahlungsart]['warenkorb_nachlass']=0;
                                        $sum_by_zahlart[$order->zahlungsart]['artikel_count']=0;
                                        $sum_by_zahlart[$order->zahlungsart]['rabatt']=0;
                                    }

                                    $sum_by_zahlart[$order->zahlungsart]['use']++;
                                    $sum_by_zahlart[$order->zahlungsart]['gesamtsumme']+=$order->gesamtsumme;
                                    $sum_by_zahlart[$order->zahlungsart]['warenkorb_nachlass']+=$order->warenkorb_nachlass;
                                    $sum_by_zahlart[$order->zahlungsart]['artikel_count']+=$artikel_count;
                                    $sum_by_zahlart[$order->zahlungsart]['rabatt']+=$rabatt;

                                    ?>
                                    <tr id="">
                                        <td>{{ $code->code }} </td>
                                        <td>@date($order->datum)</td>
                                        <td><a title="Ansehen" class="ml-2" style="color:#212529;" target="_blank" href="{{ route('app.temporder.show', ['temporder'=>$order->id] ) }}">{{ $order->transaction_key }} </a></td>
                                        <td>{{ $order->intranet_id }} </td>
                                        <td>{{ $order->kunde['firstname'].' '.$order->kunde['lastname'] }} </td>
                                        <td>{{ $order->kunde['zip'].' '.$order->kunde['city'] }} </td>
                                        <td>{{ $order->zahlungsart }} </td>
                                        <td>{{ $artikel_count }}</td>
                                        <td class="text-right">@money($order->gesamtsumme / 100) </td>
                                        <td class="text-right">@money($order->warenkorb_nachlass / 100) </td>
                                        <td class="text-right">{{ $rabatt }} %</td>
                                        <td>{{ $couopn_arten[$coupon->art] }} </td>
                                        <td><a title="Ansehen" class="ml-2" target="_blank" href="{{ route('app.temporder.show', ['temporder'=>$order->id] ) }}"><i class="far fa-eye"></i></a></td>
                                    </tr>
                                    @endforeach
                                @empty
                                    <tr id=""><td colspan="100%">Keine Daten vorhanden</td></tr>
                                @endforelse
                                </tbody>

                                @if($output)
                                <thead class="thead-dark">
                                <tr>
                                    <td colspan="13"></td>
                                </tr>
                                <tr>
                                    <th>Anzahl Bestellungen</th>
                                    <td colspan=""></td>
                                    <th colspan="2">Zahlart</th>
                                    <th class="text-right">Warenwert vor Rabatt</th>
                                    <th class="text-right">Warenwert nach Abzug</th>
                                    <th class="text-right">Nachlass Gesamt</th>
                                    <th colspan=""></th>
                                    <th class="text-right">Artikel im &empty;</th>
                                    <th class="text-right">Warenwert im &empty;</th>
                                    <th class="text-right">Nachlass im &empty;</th>
                                    <th class="text-right" colspan="2">Nachlass % im &empty;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($pos>1)
                                <tr>
                                    <td>{{$pos}}</td>
                                    <td colspan=""></td>
                                    <td colspan="2">Alle kumuliert</td>
                                    <td class="text-right">@money($global_sum['gesamtsumme'] / 100 + $global_sum['warenkorb_nachlass'] / 100)</td>
                                    <td class="text-right">@money($global_sum['gesamtsumme'] / 100 )</td>
                                    <td class="text-right">@money($global_sum['warenkorb_nachlass'] / 100 )</td>
                                    <td colspan=""></td>
                                    <td class="text-right">{{ round($global_sum['artikel_count'] / $pos, 2) }}</td>
                                    <td class="text-right">@money(($global_sum['gesamtsumme'] / 100) / $pos)</td>
                                    <td class="text-right">@money(($global_sum['warenkorb_nachlass'] / 100) / $pos)</td>
                                    <td class="text-right" colspan="2">{{ round($global_sum['rabatt'] / $pos, 2) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="13"></td>
                                </tr>
                                @endif
                                <?php
                                foreach ($sum_by_zahlart AS $key => $data) {
                                    echo '<tr>';
                                    echo '<td>'.$data['use'].'</td>';
                                    echo '<td colspan=""></td>';
                                    echo '<td colspan="2">'.$key.'</td>';
                                    echo '<td class="text-right">'.number_format(str_replace(',', '.', $data['gesamtsumme']/ 100 + $data['warenkorb_nachlass']/ 100), 2, ',', ' ').' €</td>';
                                    echo '<td class="text-right">'.number_format(str_replace(',', '.', $data['gesamtsumme']/ 100), 2, ',', ' ').' €</td>';
                                    echo '<td class="text-right">'.number_format(str_replace(',', '.', $data['warenkorb_nachlass']/ 100), 2, ',', ' ').' €</td>';
                                    echo '<td colspan=""></td>';
                                    echo '<td class="text-right">'.round($data['artikel_count'] / $data['use'], 2) .' </td>';
                                    echo '<td class="text-right">'.number_format(str_replace(',', '.', ($data['gesamtsumme']/100)/$data['use']), 2, ',', ' ').' €</td>';
                                    echo '<td class="text-right">'.number_format(str_replace(',', '.', ($data['warenkorb_nachlass']/100)/$data['use']), 2, ',', ' ').' €</td>';
                                    echo '<td class="text-right" colspan="2">'. round($data['rabatt'] / $data['use'], 2).'</td>';
                                    echo '</tr>';
                                }
                                ?>
                                </tbody>
                                @endif

                            </table>
                        </div>
                    </div>

                </div>
            </div>


        </div>

@endsection


        @section('footer_scripts')
            <script type="text/javascript">


                function removedate(type) {
                    $('#'+type).val('');
                    $('#'+type+'_dummy').val('');
                }

                $(function() {

                    $("#datefrom_filter_dummy").datepicker({
                        prevText: '&#x3c;zurück', prevStatus: '',
                        prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                        nextText: 'Vor&#x3e;', nextStatus: '',
                        nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                        currentText: 'heute', currentStatus: '',
                        todayText: 'heute', todayStatus: '',
                        clearText: '-', clearStatus: '',
                        closeText: 'schließen', closeStatus: '',
                        monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                        monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                        dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                        dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                        altField : "#datefrom_filter",
                        altFormat : "yy-mm-dd",
                        dateFormat : "dd.mm.yy",
                        changeMonth: true,
                        changeYear: true,
                        defaultDate: "@empty(!$datefrom_filter)@date($datefrom_filter)@endempty"
                    }, $.datepicker.regional['de']);

                    $("#dateto_filter_dummy").datepicker({
                        prevText: '&#x3c;zurück', prevStatus: '',
                        prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                        nextText: 'Vor&#x3e;', nextStatus: '',
                        nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                        currentText: 'heute', currentStatus: '',
                        todayText: 'heute', todayStatus: '',
                        clearText: '-', clearStatus: '',
                        closeText: 'schließen', closeStatus: '',
                        monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                        monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                        dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                        dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                        altField : "#dateto_filter",
                        altFormat : "yy-mm-dd",
                        dateFormat : "dd.mm.yy",
                        changeMonth: true,
                        changeYear: true,
                        defaultDate: "@empty(!$dateto_filter)@date($dateto_filter)@endempty"
                    }, $.datepicker.regional['de']);
                });


            </script>
@stop