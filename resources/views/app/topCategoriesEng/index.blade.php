@extends('app.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Top Kategorie Englisch</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="Top Kategorie neu anlegen" style="float: right;" href="{{ route('app.topCategoriesEng.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">

                                <thead class="thead-dark">
                                <tr>
                                    @headicons('1')
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Navi</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $topCategories AS $topCategorie )
                                    <tr id="">
                                        <td>
                                            <a title="Top Kategorie {{ $topCategorie->HealthClaimOK ? 'deaktivieren' : 'aktivieren' }}" class="mr-2"  href="{{ route('app.topCategoriesEng.toggleActive', $topCategorie->categories_top_id) }}" role="button"><i class="fa fa-circle {{ $topCategorie->HealthClaimOK ? 'text-success' : 'text-secondary' }}"></i></a>
                                        </td>
                                        <td>{{ $topCategorie->categories_top_id }} </td>
                                        <td>{{ $topCategorie->categories_name }} </td>
                                        <td>{{ $topCategorie->menuestatus }} </td>
                                        <td>
                                            <a title="Top Kategorie bearbeiten" class="mr-2" href="{{ route('app.topCategoriesEng.show', ['topCategorie'=>$topCategorie->categories_top_id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="Top Kategorie löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die Top Kategorie '{{ $topCategorie->categories_name }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('app.topCategoriesEng.destroy', ['topCategorie'=>$topCategorie->categories_top_id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine Top Kategorie Englisch vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>



        </div>

@endsection
