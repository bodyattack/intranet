<div class="row  mt-4">
    <label for="name" class="col-md-3 col-form-label ">Transaktionen</label>

    <div class="col-md-9">

        <?php
        $transactions_data = $voucher_data['transactions'];
        ?>

        <div class="table-responsive">
            <table class="table table-bordered table-striped mt-2 mb-3">

                <thead class="thead-dark">
                <tr>
                    <th>Datum</th>
                    <th>Aktion</th>
                    <th>Wert</th>
                    <th>Ort</th>
                    <th>Belegnr.</th>
                    <th>Interne Daten</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($transactions_data AS $transaction)
                    <tr>
                        <td>@date($transaction['date'])</td>
                        <td>{{ $transaction['action'] }} </td>
                        <td>{{ $transaction['amount'] }} </td>
                        <td>{{ $transaction['outlet'] }} </td>
                        <td>{{ $transaction['receiptNumber'] ?? "" }} </td>
                        @if( $transaction['eposReturnParameters'] != 'NOT_APPLICABLE' )
                            <td><?php echo json_encode($transaction['eposReturnParameters']) ?> </td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
