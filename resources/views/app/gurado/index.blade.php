@extends('app.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Gurado Gutschein</h4>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">

                    <div class="col-md-12">

                        <form id="submit-form" method="get" action="{{ route('app.gurado.show') }}">
                            <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                            @csrf
                            <div class="input-group">
                                <input id="code" name="code" type="text" class="form-control" placeholder="Gutscheincode eingeben" value="{{ old('code') ?? $code ?? '' }}">
                                <div class="input-group-btn" >
                                    <div class="input-group-append">

                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Gutschein suchen</button>
                                        <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.gurado.index', 'filter_reset=1') }}"><i class="fas fa-redo"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>


                    <div class="col-md-12 mt-5">
                    @if( !empty($code_information) )

                        @php($voucher_data = $code_information['voucher'])

                        @include('app.gurado._code_information', ['voucher_data' => $voucher_data])

                        @if( !empty($voucher_data['transactions']) )

                            @include('app.gurado._code_transaktionen', ['transactions_data' => $voucher_data['transactions']])

                        @endif

                        @include('app.gurado._code_action_form')

                    @endif
                    </div>


                </div>
            </div>
        </div>

@endsection

