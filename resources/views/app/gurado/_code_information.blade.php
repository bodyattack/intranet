<div class="row">
    <label for="name" class="col-md-3 col-form-label ">Gutscheindaten</label>

    <div class="col-md-9">

        <?php
        // Kundendaten
        echo '<p><span style="min-width: 150px; display: inline-block;">Status:</span>'. $voucher_data['status'].'</p>';
        echo '<p><span style="min-width: 150px; display: inline-block;">Aktueller Wert:</span>'. $voucher_data['balance'].' </p>';
        echo '<hr>';
        echo '<p><span style="min-width: 150px; display: inline-block;">Verkaufspreis:</span>'. $voucher_data['product']['price'].'</p>';
        echo '<p><span style="min-width: 150px; display: inline-block;">Währung:</span>'. $voucher_data['currencyCode'].'</p>';
        echo '<p><span style="min-width: 150px; display: inline-block;">Produktname:</span>'.$voucher_data['product']['name'].'</p>';
        ?>
        <p><span style="min-width: 150px; display: inline-block;">Gültig ab:</span> @datetime($voucher_data['validFromDate']) </p>
        <p><span style="min-width: 150px; display: inline-block;">Gültig bis:</span> @datetime($voucher_data['validToDate']) </p>
        <hr>
        <p><span style="min-width: 150px; display: inline-block;">Erzeugt am:</span> @datetime($voucher_data['creationDate']) </p>
        <?php
        echo '<p><span style="min-width: 150px; display: inline-block;">Erzeugt von:</span>'. $voucher_data['soldBy'].'</p>';
        echo '<p><span style="min-width: 150px; display: inline-block;">Belegnummer:</span>'. $voucher_data['receiptNumber'] ?? " " .'</p>';
        echo '<p><span style="min-width: 150px; display: inline-block;">Interne SKU:</span>'. $voucher_data['product']['externalSku'].'</p>';
        echo '<p><span style="min-width: 150px; display: inline-block;">Zahlart:</span>'. $voucher_data['paymentMethod'].'</p>';
        ?>

    </div>
</div>