
<form id="submit-form"  method="POST" action="{{ route('app.gurado.store') }}">
    @csrf
    <input type="hidden" name="code" value="{{ $code ?? '' }}">

    <div class="row mt-5">
        <label for="name" class="col-md-3 col-form-label ">Gutschein belasten</label>

        <div class="col-md-9">

            <div class="form-group row">
                <label for="amount" class="col-md-3 col-form-label text-md-right">Betrag</label>
                <div class="col-md-6">
                    <input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}">
                </div>
                <div class="col-md-2">
                    <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Den zu belastenden Betrag eingeben, OHNE negatives Vorzeichen!<br><br>Bitte englische Schreibweise verwenden bei Kommazahlen -> 20.50" class="fas fa-info-circle"></i>
                </div>
            </div>

            <div class="form-group row">
                <label for="kassierernummer" class="col-md-3 col-form-label text-md-right">Kassierernummer</label>
                <div class="col-md-6">
                    <input id="kassierernummer" type="text" class="form-control" placeholder="optional" name="kassierernummer" value="{{ old('kassierernummer') }}">
                </div>
            </div>


            <div class="form-group row">
                <label for="bonnummer" class="col-md-3 col-form-label text-md-right">Bonnummer</label>
                <div class="col-md-6">
                    <input id="bonnummer" type="text" class="form-control" placeholder="optional" name="bonnummer" value="{{ old('bonnummer') }}">
                </div>
            </div>


        </div>

    </div>

    <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-9">
            <div class="form-group row">
                <div class="col-md-3">&nbsp;</div>
                <div class="col-md-6">
                    <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary btn-lg btn-block prevent-double-click ">Speichern</button>
                </div>
            </div>
        </div>
    </div>


</form>