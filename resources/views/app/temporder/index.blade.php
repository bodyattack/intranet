@extends('app.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $temporder->presentation_long }}</h4>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">

                        <?php
                        $urlQuerry='';
                        if(strpos(url()->full(), '?')) {
                            $urlQuerry = substr(url()->full(), strpos(url()->full(), '?')+1);
                        }
                        if($urlQuerry=='' & strpos(url()->previous(), '?')) {
                            $urlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
                        }
                        ?>

                        <form class="mb-0" id="submit-form" method="get" action="{{ route('app.temporder.index') }}">
                            <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                            @csrf
                            <div class="input-group">
                                <input id="number" style="border-radius: 0px 0px 0px 0px; min-width: 250px; "  name="number" type="text" class="form-control" placeholder="TX-ID, Intranet-Auftragsnummer oder Shop-Bestellnummer" value="{{ old('number') ?? $filter['number'] ?? '' }}">
                                <input id="kunde" style="border-radius: 0px 0px 0px 0px;  min-width: 250px;"  name="kunde" type="text" class="form-control" placeholder="Kundenname oder E-Mail Adresse" value="{{ old('kunde') ?? $filter['kunde'] ?? '' }}">

                                <div class="input-group-btn" style="flex: content">
                                    <div class="input-group-append">
                                        <?php
                                        $date_from = ( isset($filter['date_from']) ? $filter['date_from'] : old('date_from') );
                                        $date_to = ( isset($filter['date_to']) ? $filter['date_to'] : old('date_to') );
                                        ?>
                                        <span class="input-group" >
                                            <i data-toggle="tooltip" data-html="true" onclick="removedate('date_from')" data-original-title="Datum entfernen"  class="remove-date fas fa-times"></i>
                                            <input placeholder="Datum von" readonly autocomplete="off" id="date_from_dummy" type="text" class="form-control bg-white " name="date_from_dummy" value="@empty(!$date_from)@date($date_from)@endempty">
                                            <input id="date_from" type="hidden" name="date_from" value="{{$date_from}}">
                                        </span>
                                        <span class="input-group">
                                                <i data-toggle="tooltip" data-html="true" onclick="removedate('date_to')" data-original-title="Datum entfernen" class="remove-date fas fa-times"></i>
                                                <input  placeholder="Datum bis" readonly autocomplete="off" id="date_to_dummy" type="text" class="form-control bg-white " name="date_to_dummy" value="@empty(!$date_to)@date($date_to)@endempty">
                                                <input id="date_to" type="hidden" name="date_to" value="{{$date_to}}">
                                        </span>
                                        <select  name="transfer" id="transfer" style="max-width: 170px;  cursor: pointer; color:#495057; border: 1px solid #ced4da; padding-top: 8px; border-radius: 0px; height: 37px !important;">
                                            @foreach (['' => 'Transfer Status alle', '-1' => 'Transfer Status Fehler', '0' => 'Transfer Status offen', '1' => 'Transfer Status übertragen'] as $key=>$value)
                                                <option value="{{ $key }}"{{ old('transfer') === $key || ( isset($filter['transfer']) && $filter['transfer'] == $key ) ? 'selected="selected"' : '' }} >{{ $value }} </option>
                                            @endforeach
                                        </select>
                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                                        <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('app.temporder.index' ) }}"><i class="fas fa-redo"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">

                                <thead class="thead-dark">
                                <tr>
                                    @headicons('1', 'fa-sign-out-alt')
                                    <th>@sortablelink('datum', 'Bestelldatum')</th>
                                    <th>Kunde</th>
                                    <th class="text-right">@sortablelink('gesamtsumme', 'Wert')</th>
                                    <th class="text-right">Artikel</th>
                                    <th>@sortablelink('TxId', 'PayOne TXID')</th>
                                    <th>@sortablelink('transaction_key', 'Bestellnr. Shop')</th>
                                    <th>@sortablelink('intranet_id', 'Auftragsnr. Intranet')</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $temporders AS $temporder )
                                    <?php
                                    $order_title = '';
                                    $order_icon = 'fa-sign-out-alt';
                                    switch ($temporder->transfer) {
                                        case 0 : $order_title = 'Noch nicht übertragen'; $order_class = 'text-primary'; break;
                                        case 1 : $order_title = 'An Intranet alt übergeben'; $order_class = 'text-muted'; break;
                                        case -1 : $order_title = 'Fehler bei der Übertragung'; $order_icon = 'fa-exclamation-triangle'; $order_class = 'text-warning'; break;
                                    }

                                    ?>
                                    <tr id="">
                                        <td title="{{ $order_title }}"><i class="fas {{ $order_icon }} {{ $order_class }}"></i></td>
                                        <td>@date($temporder->datum)</td>
                                        <td>{{ $temporder->kunde['firstname'] }} {{ $temporder->kunde['lastname'] }} </td>
                                        <td class="text-right">@money($temporder->gesamtsumme/ 100)</td>
                                        <td class="text-right">{{ count($temporder->warenkorb_string) }}</td>
                                        <td>{{ $temporder->TxId }} </td>
                                        <td>{{ $temporder->transaction_key }} </td>
                                        <td><a style="color:#212529;" target="_blank" href="http://192.168.1.141/zentraladministration/module/kundencenter.cfm?CHTEMP=auftrag.cfm&CHUSER=781&suchtyp=1&suchbegriff={{ $temporder->intranet_id }}">{{ $temporder->intranet_id }}</a></td>
                                        <td>
                                            <a title="{{ $temporder->presentation_short }} ansehen" class="float-right" href="{{ route('app.temporder.show', ['temporder'=>$temporder->id, $urlQuerry] ) }}"><i class="far fa-eye"></i></a>
                                            @if(!$temporder->transfer && array_key_exists('bestellungen-loschen', $entry['permissions']))
                                            <a title="{{ $temporder->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die {{ $temporder->presentation_short }} '{{ $temporder->transaction_key }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('app.temporder.destroy', ['temporder'=>$temporder->id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $temporder->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row justify-content-md-center">
                {{ $temporders->withQueryString()->links() }}
            </div>

        </div>

@endsection

@section('footer_scripts')
    <script type="text/javascript">

        $(function() {

            $("#date_from_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#date_from",
                altFormat : "yy-mm-dd 00:00:00",
                dateFormat : "dd.mm.yy",
                changeMonth: true,
                changeYear: true,
                defaultDate: "@empty(!$date_from)@date($date_from)@endempty"
            }, $.datepicker.regional['de']);

            $("#date_to_dummy").datepicker({
                prevText: '&#x3c;zurück', prevStatus: '',
                prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
                nextText: 'Vor&#x3e;', nextStatus: '',
                nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
                currentText: 'heute', currentStatus: '',
                todayText: 'heute', todayStatus: '',
                clearText: '-', clearStatus: '',
                closeText: 'schließen', closeStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                altField : "#date_to",
                altFormat : "yy-mm-dd 23:59:59",
                dateFormat : "dd.mm.yy",
                changeMonth: true,
                changeYear: true,
                defaultDate: "@empty(!$date_to)@date($date_to)@endempty"
            }, $.datepicker.regional['de']);
        });


    </script>
@stop