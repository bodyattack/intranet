
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Warenkorb JSON bearbeiten</div>

                    <div class="card-body">

                        <div class="alert alert-warning" role="alert">
                            <h5>Achtung!</h5>
                            Das hier dargestelle JSON wird für die Übertragung an das Intranet Alt verwendet, Änderungen müssen 100% JSON Konform sein.
                            Keine Zusätzlichen Zeiilen-Umbrüche einfügen und unbedingt die korrekte Sytax (Komma, Klammern etc.) zur Datentrennung der JSON Elemente achten!<br><br>
                            Änderungen wirken sich NICHT auf den Gesamtpreis aus, da dieser schon über PayONE kassiert wurde. Das Formular dient lediglich dazu,
                            defekte Datensätze zu reparieren, um diese dann übertragen zu können.
                        </div>

                        <form id="submit-form"  method="POST" action="{{route('app.temporder.setOrderWarenkorb', ['temporder'=>$temporder->id])}}">
                        @csrf

                        <div class="form-group row">
                            <label for="warenkorb_string" class="col-md-4 col-form-label text-md-right">Warenkorb JSON</label>

                            <div class="col-md-6">
                                <textarea id="warenkorb_string" type="text" rows="20" class="form-control {{ $errors->has('warenkorb_string') ? ' is-invalid' : '' }}" name="warenkorb_string" >{{ old('warenkorb_string', $warenkorb_string) }}</textarea>

                                @if ($errors->has('warenkorb_string'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('warenkorb_string') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click">Speichern</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
