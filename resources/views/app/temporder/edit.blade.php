@extends('app.layouts.app')
@section('content')
<?php
if(!isset($urlQuerry)) {
    $urlQuerry = '';
    $previous = URL::previous();
    if($pos = strpos($previous, '?')) {
        $urlQuerry = substr($previous, $pos+1);
    }
}
?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> {{ $temporder->presentation_short }} vom @datetime($temporder->datum) Uhr</div>

                    <div class="card-body">


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label ">Kundendaten</label>

                            <div class="col-md-6">
                                <?php

                                $anrede = '';
                                switch ($temporder->kunde['salutation']) {
                                    case 'm' : $anrede='Herr'; break;
                                    case 'w' :
                                    case 'f' : $anrede='Frau'; break;
                                    case 'd' : $anrede='Divers'; break;
                                }

                                // Kundendaten
                                echo '<p><span style="min-width: 150px; display: inline-block;">Anrede:</span>'. $anrede.'</p>';
                                echo '<p><span style="min-width: 150px; display: inline-block;">Name:</span>'. $temporder->kunde['firstname'].' '. $temporder->kunde['lastname'].'</p>';
                                echo '<p><span style="min-width: 150px; display: inline-block;">Straße:</span>'. $temporder->kunde['street'].'</p>';
                                echo '<p><span style="min-width: 150px; display: inline-block;">PLZ:</span>'. $temporder->kunde['zip'].' '. $temporder->kunde['city'].' ('. $temporder->kunde['country'].')</p>';
                                echo '<p><span style="min-width: 150px; display: inline-block;">Zusatz:</span>'. $temporder->kunde['company'].'</p>';
                                echo '<p><span style="min-width: 150px; display: inline-block;">Telefon:</span>'. $temporder->kunde['telephonenumber'].'</p>';
                                echo '<p><span style="min-width: 150px; display: inline-block;">E-Mai:</span>'. $temporder->kunde['email'].'</p>';

                                // ggf. abweichende Lieferdaten
                                if (isset($temporder->kunde['lfirstname']) && !empty(trim($temporder->kunde['lfirstname']))) {
                                    echo '<p><span style="min-width: 150px; display: inline-block;"><u>Abweichende Lieferanschrift</u></span></p>';
                                    echo '<p><span style="min-width: 150px; display: inline-block;">Name:</span>'. $temporder->kunde['lfirstname'].' '. $temporder->kunde['llastname'].'</p>';
                                    echo '<p><span style="min-width: 150px; display: inline-block;">Straße:</span>'. $temporder->kunde['lstreet'].'</p>';
                                    echo '<p><span style="min-width: 150px; display: inline-block;">PLZ:</span>'. $temporder->kunde['lzip'].' '. $temporder->kunde['lcity'].' ('. $temporder->kunde['lcountry'].')</p>';
                                    echo '<p><span style="min-width: 150px; display: inline-block;">Zusatz:</span>'. $temporder->kunde['lcompany'].'</p>';
                                }
                                ?>

                            </div>
                            <div class="col-md-2">
                                @if( count($temporder->warenkorb_string) >2 )
                                <a style="float: right;" href="{{ route('app.temporder.index') }}" value="back" class="btn btn-primary">Zurück</a>
                                @endif
                                @if( $temporder->transfer <= 0 && array_key_exists('bestellungen-adresse-bearbeiten', $entry['permissions']))
                                <a style="float: right;" class="btn btn-primary mt-2 modal-large"  href="#"
                                   data-footer-show="0" data-content-url="{{route('app.temporder.getOrderAddress', ['temporder'=>$temporder->id])}}">Adresse bearbeiten</a>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mt-5">
                            <label for="name" class="col-md-4 col-form-label">Warenkorb</label>

                            <div class="col-md-6">
                                <?php
                                $warenwert = 0;
                                $guradowert = 0;
                                foreach ($temporder->warenkorb_string AS $key => $item) {

                                    $variante=''; // Ohne Staffel
                                    if (isset($item['variante']) and !empty(trim($item['variante']))) {
                                        $variante=$item['variante'];
                                    }
                                    $staffel=''; // Nur Staffel
                                    if (isset($item['staffel']) and !empty(trim($item['staffel']))) {
                                        $staffel=$item['staffel'];
                                    }

                                    echo '<p><span style="min-width: 150px; display: inline-block;"><u>Position '.($key+1).'</u></span></p>';
                                    if (isset($item['product_name']) and !empty(trim($item['product_name']))) {
                                        echo '<p><span style="min-width: 150px; display: inline-block;">Produkt (ID):</span>'.$item['product_name'].' (<a target="_blank" href="https://www.body-attack.de/idbestand.php?id='.$item['product_id'].'">'.$item["product_id"].'</a>)</p>';
                                    }
                                    else {
                                        echo '<p><span style="min-width: 150px; display: inline-block;">Produkt (ID):</span><a target="_blank" href="https://www.body-attack.de/idbestand.php?id='.$item['product_id'].'">'.$item["product_id"].'</a></p>';
                                    }
                                    if(!empty($variante)) {
                                        echo '<p><span style="min-width: 150px; display: inline-block;">Variante</span>'.$variante.'</p>';
                                    }
                                    echo '<p><span style="min-width: 150px; display: inline-block;">Menge:</span>'.$item['menge'].' (Einzelpreis: '.round($item['einzelpreis'], 2).')</p>';

                                    $warenwert += (intval($item['menge']) * $item['einzelpreis']);
                                }

                                ?>
                            </div>
                            <div class="col-md-2">
                                @if(  $temporder->transfer <= 0  && array_key_exists('bestellungen-warenkorb-bearbeiten', $entry['permissions']))
                                    <a style="float: right;" class="btn btn-primary mt-2 modal-large"  href="#"
                                       data-footer-show="0" data-content-url="{{route('app.temporder.getOrderWarenkorb', ['temporder'=>$temporder->id])}}">Warenkorb bearbeiten</a>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mt-5">
                            <label for="name" class="col-md-4 col-form-label">Zahlungsdaten</label>

                            <div class="col-md-6">
                                <p>
                                    <span style="min-width: 150px; display: inline-block;">Gesamtsumme:</span>@money($warenwert+$temporder->versandkosten/100)</span>
                                    <br><span style="min-width: 150px; display: inline-block; font-style: italic"">Warenwert:</span>@money($warenwert)</span>
                                    <br><span style="min-width: 150px; display: inline-block;  font-style: italic">Versandkosten:</span>@money( $temporder->versandkosten/100 )
                                </p>
                                <p><span style="min-width: 150px; display: inline-block;">Zahlart:</span>{{ $temporder->zahlungsart }}</p>

                                @if( !empty($temporder->gurado) )
                                    <p>
                                        @foreach($temporder->gurado AS $key => $voucher)
                                            @if( $key == 0 )
                                                <span style="min-width: 150px; display: inline-block;">Gurado-Code: </span>
                                            @else
                                                <span style="min-width: 150px; display: inline-block;"> </span>
                                            @endif
                                            {{ $voucher['code'] }}&nbsp;(@money($voucher['amount']))&nbsp;<a title="Vorgang ansehem" class="mr-2" target="_blank" href="{{ route('app.gurado.show', ['code'=>$voucher['code']] ) }}"><i class="far fa-eye"></i></a><br>
                                            @php($guradowert+=$voucher['amount'])
                                        @endforeach
                                    </p>
                                @endif

                                @if( !$temporder->coupon_codes->isEmpty() )
                                    <p>
                                        @foreach($temporder->coupon_codes AS $key => $codes)
                                            @if( $key == 0 )
                                                <span style="min-width: 150px; display: inline-block;">Gutschein-Code: </span>
                                            @else
                                                <span style="min-width: 150px; display: inline-block;"> </span>
                                            @endif
                                            {{ $codes->code }} &nbsp;<a title="Ansehen"  target="_blank" href="{{ route('app.coupon.show', ['coupon'=>$codes->coupon_id] ) }}"><i class="far fa-eye"></i></a><br>
                                        @endforeach
                                        <span style="min-width: 150px; display: inline-block; font-style: italic">Nachlass:</span> @money($temporder->warenkorb_nachlass/100)
                                    </p>
                                @endif

                                @if( !empty($guradowert) )
                                    <p><span style="min-width: 150px; display: inline-block;">Zahlung Gurado:</span>@money($guradowert)</p>
                                @endif

                                @if( !empty($temporder->gesamtsumme) )
                                    <p><span style="min-width: 150px; display: inline-block;">Zahlung PayONE:</span>@money($temporder->gesamtsumme/100)</p>
                                @endif

                            </div>
                        </div>


                        <div class="form-group row mt-5">
                            <label for="name" class="col-md-4 col-form-label">Merkmale</label>

                            <div class="col-md-6">

                                <p><span style="min-width: 150px; display: inline-block;">Shop Bestellung:</span>@datetime($temporder->datum) Uhr</p>
                                <p><span style="min-width: 150px; display: inline-block;">Intranet Erfassung:</span>@datetime($temporder->created_at) Uhr</p>
                                <p><span style="min-width: 150px; display: inline-block;">Bestellnr. Intranet:</span><a style="color:#212529;" target="_blank" href="http://192.168.1.141/zentraladministration/module/kundencenter.cfm?CHTEMP=auftrag.cfm&CHUSER=781&suchtyp=1&suchbegriff={{ $temporder->intranet_id }}">{{ $temporder->intranet_id }}</a></p>
                                <p><span style="min-width: 150px; display: inline-block;">PayOne TXID:</span>{{ $temporder->TxId }}</p>
                                <p><span style="min-width: 150px; display: inline-block;">Bestellnr. Shop:</span>{{ $temporder->transaction_key }}</p>
                                @if( !empty($temporder->userid_payone) )
                                    <p><span style="min-width: 150px; display: inline-block;">PayOne Kunden-ID:</span>{{ $temporder->userid_payone }}</p>
                                @endif
                             </div>
                            <div class="col-md-2">
                                @if( $temporder->transfer == -1  && array_key_exists('bestellungen-erneut-ubertragen', $entry['permissions']))
                                    <a style="float: right;" href="{{ route('app.temporder.orderRetransmit', ['temporder'=>$temporder->id]) }}" value="back" class="btn btn-primary">Fehlerhafte Übertragung erneut anstoßen</a>
                                @endif
                                @if( $temporder->transfer == 1  && array_key_exists('bestellungen-erneut-ubertragen', $entry['permissions']))
                                    <a style="float: right;" href="{{ route('app.temporder.orderRetransmitNew', ['temporder'=>$temporder->id]) }}" value="back" class="btn btn-primary">Job Übertragung nochmals auslösen</a>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row mt-4 mb-0">
                            <div class="col-md-12">
                                <a style="float: right;" href="{{ route('app.temporder.index', old('urlQuerry', $urlQuerry)) }}" value="back" class="btn btn-primary">Zurück</a>
                                @if( $temporder->cron_processing )
                                    <a title="Cron Status reset" class="btn btn-primary" style="float: left;"  href="{{ route('app.temporder.toggleCron', $temporder->id) }}" role="button">Cron Status reset</a>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection