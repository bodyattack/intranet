
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Adresse bearbeiten</div>

                    <div class="card-body">

                        <form id="submit-form"  method="POST" action="{{route('app.temporder.setOrderAddress', ['temporder'=>$temporder->id])}}">
                        @csrf

                        <div class="form-group row">
                            <label for="free_shipping_countries" class="col-md-4 col-form-label text-md-right">Anrede</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select name="salutation" class=" form-control" id="salutation">
                                            <option disabled >-- Bitte wählen --</option>
                                            @php( $salutation = $temporder->kunde['salutation'] )
                                            @foreach (['m' => 'Mann', 'f' => 'Frau', 'd' => 'Divers'] as $key=>$value)
                                                <option value="{{$key}}" {{ ( $key == $salutation || $key == old('salutation') ) ? 'selected="selected"' : '' }} >{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="firstname" class="col-md-4 col-form-label text-md-right">Vorname</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control {{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname', $temporder->kunde['firstname']) }}" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">Nachname</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control {{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname', $temporder->kunde['lastname']) }}" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="company" class="col-md-4 col-form-label text-md-right">Zusatz / Firma / Postnummer</label>

                            <div class="col-md-6">
                                <input id="company" type="text" class="form-control {{ $errors->has('company') ? ' is-invalid' : '' }}" name="company" value="{{ old('company', $temporder->kunde['company']) }}"  >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="street" class="col-md-4 col-form-label text-md-right">Straße</label>

                            <div class="col-md-6">
                                <input id="street" type="text" class="form-control {{ $errors->has('street') ? ' is-invalid' : '' }}" name="street" value="{{ old('street', $temporder->kunde['street']) }}" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="zip" class="col-md-4 col-form-label text-md-right">PLZ</label>

                            <div class="col-md-6">
                                <input id="zip" type="text" class="form-control {{ $errors->has('zip') ? ' is-invalid' : '' }}" name="zip" value="{{ old('zip', $temporder->kunde['zip']) }}" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">Stadt</label>

                            <div class="col-md-6">
                                <input id="city" type="text" class="form-control {{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city', $temporder->kunde['city']) }}" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="country" class="col-md-4 col-form-label text-md-right">Land</label>

                            <div class="col-md-6">
                                <input id="country" type="text" class="form-control {{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country', $temporder->kunde['country']) }}"  >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="telephonenumber" class="col-md-4 col-form-label text-md-right">Telefon</label>

                            <div class="col-md-6">
                                <input id="telephonenumber" type="text" class="form-control {{ $errors->has('telephonenumber') ? ' is-invalid' : '' }}" name="telephonenumber" value="{{ old('telephonenumber', $temporder->kunde['telephonenumber']) }}"  >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $temporder->kunde['email']) }}"  >
                            </div>
                        </div>

                        @if( $temporder->lieferlandabweichung OR  (isset($temporder->kunde['lfirstname']) && !empty(trim($temporder->kunde['lfirstname'])) ) )

                                <div class="form-group row">
                                    <label for="lfirstname" class="col-md-4 col-form-label text-md-right">Liefer Vorname</label>

                                    <div class="col-md-6">
                                        <input id="lfirstname" type="text" class="form-control {{ $errors->has('lfirstname') ? ' is-invalid' : '' }}" name="lfirstname" value="{{ old('lfirstname', $temporder->kunde['lfirstname']) }}" required >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="llastname" class="col-md-4 col-form-label text-md-right">Liefer Nachname</label>

                                    <div class="col-md-6">
                                        <input id="llastname" type="text" class="form-control {{ $errors->has('llastname') ? ' is-invalid' : '' }}" name="llastname" value="{{ old('llastname', $temporder->kunde['llastname']) }}" required >
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="lstreet" class="col-md-4 col-form-label text-md-right">Liefer Straße</label>

                                    <div class="col-md-6">
                                        <input id="lstreet" type="text" class="form-control {{ $errors->has('lstreet') ? ' is-invalid' : '' }}" name="lstreet" value="{{ old('lstreet', $temporder->kunde['lstreet']) }}" required >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lzip" class="col-md-4 col-form-label text-md-right">Liefer PLZ</label>

                                    <div class="col-md-6">
                                        <input id="lzip" type="text" class="form-control {{ $errors->has('lzip') ? ' is-invalid' : '' }}" name="lzip" value="{{ old('lzip', $temporder->kunde['lzip']) }}" required >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lcity" class="col-md-4 col-form-label text-md-right">Liefer Stadt</label>

                                    <div class="col-md-6">
                                        <input id="lcity" type="text" class="form-control {{ $errors->has('lcity') ? ' is-invalid' : '' }}" name="lcity" value="{{ old('lcity', $temporder->kunde['lcity']) }}" required >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lcountry" class="col-md-4 col-form-label text-md-right">Liefer Land</label>

                                    <div class="col-md-6">
                                        <input id="lcountry" type="text" class="form-control {{ $errors->has('lcountry') ? ' is-invalid' : '' }}" name="lcountry" value="{{ old('lcountry', $temporder->kunde['lcountry']) }}"  >
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click">Speichern</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
