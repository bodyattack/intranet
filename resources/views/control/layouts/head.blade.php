<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Intranet')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    @if( isset($searchable_select) && $searchable_select==1)
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <style>
            .show > .btn-light.dropdown-toggle,
            .btn-light,
            .btn-light:focus,
            .btn-light:active,
            .btn-light:hover{
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                padding: 10px 15px;
                line-height: 20px;
                font-weight: 400;
            }
            .bootstrap-select > .dropdown-toggle::after {
                display: none;
            }
        </style>
    @endif

    @yield('header_css')

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    <script src="https://unpkg.com/popper.js" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/script.js') }}" defer></script>
    <script src="{{ asset('js/main.js') }}" defer></script>

    @yield('header_scripts')

    <script>
        $( document ).ready(function() {
            $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
        });
    </script>

</head>
<body class="@yield('body_class', '')">