@if ($errors->any())
    <div class="container mb-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>

                    <ul>
                        @foreach($errors->all() AS $error)
                            <li>{{ $error }}</li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif


@if(Session::has('success'))
    <div class="container mb-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <ul>
                        <strong>{{Session::get('success')}}</strong>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
@if(Session::has('danger'))
    <div class="container mb-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <ul>
                        <strong>{{Session::get('danger')}}</strong>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif

