<div class="topbar-wrap" style="margin-bottom: 15px">
    <div class="topbar is-sticky">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <ul class="topbar-nav d-lg-none">
                    <li class="topbar-nav-item relative">
                        <a class="toggle-nav" href="#">
                            <div class="toggle-icon">
                                <span class="toggle-line"></span>
                                <span class="toggle-line"></span>
                                <span class="toggle-line"></span>
                                <span class="toggle-line"></span>
                            </div>
                        </a>
                    </li><!-- .topbar-nav-item -->
                </ul><!-- .topbar-nav -->
                @auth('backend_user')
                    <a class="topbar-logo w-50 " href="{{ route('control.dashboard') }}">
                        <img style="margin-left: -20px;"  src="{{ asset('img/BA_logo_23_weiss.png') }}" alt="logo">&nbsp;&nbsp;<span class='ml-3 navbar-brand' style="color: #fff">{{ config('app.name') }} / Admin</span>
                    </a>
                    <ul class="topbar-nav">
                        <li class="topbar-nav-item relative">
                            <span class="user-welcome d-none d-lg-inline-block">Willkommen, {{ Auth::user()->name }}!</span>
                        </li><!-- .topbar-nav-item -->
                        <li>
                            <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i title=" {{ __('Logout') }}" class="fas fa-sign-out-alt"></i>
                            </a>
                            <form id="logout-form" action="{{ route('control.logout.submit') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul><!-- .topbar-nav -->
                @endauth
                @guest
                    <a class="topbar-logo w-50 " href="{{ route('start') }}">
                        <img src="{{ asset('img/logo_weiss.svg') }}" alt="logo">&nbsp;&nbsp;<span class='ml-3 navbar-brand' style="color: #fff">{{ config('app.name') }} / Admin</span>
                    </a>
                @endauth
            </div>
        </div><!-- .container -->
    </div><!-- .topbar -->

    <div class="navbar">
        <div class="container">
            <div class="navbar-innr">

                @auth('backend_user')
                <ul class="navbar-menu">
                    <li><a href="{{ route('control.dashboard') }}"><em class="ti ti-layout-grid2"></em>Dashboard</a></li>
                    <li class="has-dropdown">
                        <a class="drop-toggle" href="#"><em class="ti ti-home"></em>Unternehmen</a>
                        <ul class="navbar-dropdown">
                            <li><a href="{{ route('control.user.index') }}">Mitarbeiter</a>
                            <li><a href="{{ route('control.department.index') }}">Abteilungen</a>
                            <li><a href="{{ route('control.mandant.index') }}">Mandanten</a>
                        </ul>
                    </li>
                    <li class="has-dropdown">
                        <a class="drop-toggle"  href="#"><em class="ti ti-dropbox "></em>Tools</a>
                        <ul class="navbar-dropdown">
                            <li>
                                <a href="{{ route('control.invoicerelease.index') }}">Rechnungskontolle</a>
                            </li>

                        </ul>
                    </li>
                </ul>

                <ul class="navbar-menu float-lg-right">
                    <li class="has-dropdown">
                        <a class="drop-toggle"  href="#"><em class="ti ti-settings"></em>Konfiguration</a>
                        <ul class="navbar-dropdown">
                            {{--  <li><a href="{{ route('control.level.index') }}">Funktionen</a> --}}
                            <li class="has-dropdown">
                                <a class="drop-toggle" href="{{ route('control.areas.index') }}">Bereiche</a>
                                <ul class="navbar-dropdown">
                                    <li><a href="{{ route('control.permissions.index') }}">Berechtigungen</a></li>
                                </ul>
                            </li>
                           {{-- <li> <a class="drop-toggle" href="{{ route('control.apiaccount.index') }}">Api Accounts</a></li> --}}
                            <li> <a class="drop-toggle" href="{{ route('control.country.index') }}">Länder</a></li>
                            <li> <a class="drop-toggle" href="{{ route('control.seal.index') }}">Siegel</a></li>
                            <li> <a class="drop-toggle" href="{{ route('control.allergen.index') }}">Allergene</a></li>
                        </ul>
                    </li>
                    <li class="has-dropdown ">
                        <a class="drop-toggle" href="#"><em class="ti ti-user"></em>Admin</a>
                        <ul class="navbar-dropdown">
                            <li><a href="{{ route('control.backenduser.showPasswortChangeForm') }}">Passwort ändern</a></li>
                            <li>
                                <a href="{{ route('control.backenduser.index') }}">Admin Benutzer</a>
                            </li>

                        </ul>
                    </li>
                </ul>
                @endauth

            </div><!-- .navbar-innr -->
        </div><!-- .container -->
    </div><!-- .navbar -->


</div><!-- .topbar-wrap -->