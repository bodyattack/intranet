
        <div class="modal fade" id="modal-lg" tabindex="-1" role="dialog" aria-labelledby="modallgLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modallgLabel">Hinweis</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <form action="" method="post">
                            <button type="submit" class="btn btn-danger">Ausführen</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>