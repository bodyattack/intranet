<div class="footer-bar position-sticky">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-8">
                <ul class="footer-links">
                    <li><a href="https://www.body-attack.de/datenschutz.html" target="_blank">Datenschutzerklärung</a></li>
                    <li><a href="https://www.body-attack.de/impressum.html" target="_blank">Impressum</a></li>
                </ul>
            </div><!-- .col -->
            <div class="col-md-4 mt-2 mt-sm-0">
                <div class="d-flex justify-content-between justify-content-md-end align-items-center guttar-25px pdt-0-5x pdb-0-5x">
                    <div class="copyright-text">&copy; Body Attack Sports Nutrition GmbH & Co. KG</div>
                </div>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .footer-bar -->

@yield ('footer_scripts')

@if( isset($searchable_select) && $searchable_select==1)

    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        $('.selectpicker').selectpicker({
            selectAllText: 'Alle auswählen',
            deselectAllText: 'Alle abwählen',
            noneSelectedText: 'Nichts ausgewählt',
            liveSearch: true,
            noneResultsText: 'Keine Treffer zu {0}.',
        });
    </script>
@endif