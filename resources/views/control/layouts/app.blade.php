@include('control.layouts.head')

<div id="app">

    @include('control.layouts.nav')


    <!-- Modal Large -->
    <div class="modal fade" id="modal-news" tabindex="-1">
        <div class="modal-dialog modal-dialog-lg modal-dialog-top">
            <div class="modal-content">
                <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body popup-body-lg news-content"></div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>
    <!-- Modal End -->


    <main class="py-4">
        @include('control.layouts.errors')
        @yield('content')
    </main>

</div>

@include('control.layouts.foot')

@include('control.layouts.modal.delete')
@include('control.layouts.modal.large')

</body>
</html>
