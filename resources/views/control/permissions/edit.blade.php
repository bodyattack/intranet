@extends('control.layouts.app')
@empty($permission) @inject('permission', 'App\Permission') @endempty
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">Berechtigung {{ empty($permission->id) ? ' anlegen' : 'bearbeiten' }}</div>

                    <div class="card-body">
                        @empty(!$permission->id)
                            <form  method="POST" action="{{ route('control.permissions.update', ['permission'=>$permission->id]) }}">
                            @method('PATCH')
                        @else
                            <form method="POST" action="{{ route('control.permissions.store') }}">
                        @endempty
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $permission->name) }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" permission="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Beschreibung (optional)<br><i>Könnte bei späteren Verwendung nützlich sein!</i></label>

                            <div class="col-md-6">
                                <textarea rows="2" id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description">{{ old('description', $permission->description) }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="area_id" class="col-md-4 col-form-label text-md-right">Bereichszuweisung</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="input-bordered" name="area_id" id="area_id">
                                            <option value="false">Bitte wählen</option>
                                            @foreach(\App\Area::all() AS $area)
                                                <option value="{{$area->id}}" {{ $permission->area_id == $area->id || old('area_id') == $area->id ? 'selected="selected"' : '' }} >{{$area->name}}</option>
                                            @endforeach
                                            <option value="0" {{ $permission->area_id == '0' || old('area_id') == '0' ? 'selected="selected"' : '' }}>Global</option>
                                        </select>
                                    </div>
                                </div>
                                @error('area_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                @empty(!$permission->id)
                                    <button type="submit" name="submit-type" value="stay" class="btn btn-primary">Speichern</button>
                                @else
                                    <button type="submit" name="submit-type" value="back" class="btn btn-primary">Speichern</button>
                                @endempty
                                    <a style="float: right;" href="{{ route('control.permissions.index') }}" value="back" class="btn btn-primary"> {{ __('Zurück') }}</a>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection