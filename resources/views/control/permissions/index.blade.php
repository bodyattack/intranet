@extends('control.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Berechtigungen</h4>
                        </div>
                        <div class="col-md-6">
                            <a  title="Berechtigung anlegen" style="float: right;" href="{{ route('control.permissions.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="alert alert-warning" role="alert">
                            <h5>Achtung!</h5>
                            Die hier erstellten Berechtigungen, der "Slug" wird im Code zur Steuerung abgefragt, Änderungen führen zu Fehlern im System!<br><br>
                            Primär sollten hier neue Berechtigungen erstellt und auch direkt im Code implementiert werden. Bestenfalls sollte auch ein Bereich zugewiesen werden,<br>
                            um die Strukturen übersichtlicht zu halten. Damit die Berechtigung für User nutzbar wird, muss diese mit einem Bereich verknüpft werden!
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Bereich</th>
                                    <th>Erzeugt am</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ( $permissions AS $permissions )
                                    <?php
                                    $area = 'Global';
                                    if(!empty($permissions->area)) {
                                        $area = $permissions->area->name;
                                    }
                                    ?>
                                    <tr id="row_{{ $permissions->id }}">
                                        <td>{{ $permissions->name }}</td>
                                        <td>{{ $permissions->slug }}</td>
                                        <td>{{ $area }}</td>
                                        <td>{{ $permissions->created_at->format('d. m. Y - H:i') }}</td>
                                        <td>
                                            <a title="Berechtigung bearbeiten"  class="mr-2" href="{{ route('control.permissions.update', ['permission'=>$permissions->id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="Berechtigung löschen" class="mr-2 modal-delete"  href="#" data-form-action="{{route('control.permissions.delete', $permissions->id)}}"><i class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('footer_scripts')
    <script type="text/javascript">
        @isset ($insert_id)
        $(document ).ready(function() {
            $("#row_{{ $insert_id }}");
        });
        @endisset
    </script>
@stop


