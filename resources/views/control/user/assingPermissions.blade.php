@php(isset($display)?:$display='none')
@if($area->permissions->isEmpty())
<div  class="pb-4"  id="area_permissions_{{ $area->id }}" style="display: {{$display}};">
    <li class="list-group-item">
        <span class="ml-3">- Dieser Bereich hat keine individuellen Berechtigungen</span>
    </li>
</div>

@else

<div class="pb-4" id="area_permissions_{{ $area->id }}" style="display: {{$display}};">
@foreach($area->permissions AS $permission)
    <li class="list-group-item">
        <span title="{{ $permission->description }}" class="ml-3">- {{ $permission->name }} {!! $permission->area_id == 0 ? "<i>(global)</i>" : "" !!}</span>
        <label title="{{ $permission->name }}" class="switch ">
            <input value="{{ $area->id }}_{{$permission->id}}"
                   data-form-action="{{ route('control.user.permission.assing', ['user' => $user->id, 'permission' => $permission->id]) }}"
                   type="checkbox" class="custom_switch_{{ $area->id }} success" {{ $user->hasPermission($permission) ? 'checked="checked"' : '' }}>
            <span class="slider"></span>
            <span class="custom-success" id="row_{{ $area->id }}_{{$permission->id}}"><i class="fas fa-check"></i></span>
        </label>
    </li>
@endforeach
</div>

<script type="text/javascript">
    $(".custom_switch_{{ $area->id }}").on('click', function(){
        var form_action = $(this).attr('data-form-action');
        var value = $(this).val();
        $.ajax({
            url: form_action,
            type: 'GET',
            success: function() {
                $('#row_'+value).show('fast').delay(150).hide('slow');
            }
        });
    });
</script>
@endempty