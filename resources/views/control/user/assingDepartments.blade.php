@extends('control.layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> <h3>Abteilungen für Benutzer "{{ $user->fullname() }}"</h3></div>

                    <div class="card-body">
                        <div class="alert alert-warning" role="alert">
                            Hier den Benutzer mit einer Abteilung verknüpfen. Für verknüpfte Abteilungen darf der Mitarbeiter Rechnungen "vorab-freigeben", sofern er auch Zugriff auf die Rechnungskontrolle hat.
                            Die finale Rechnungs-Freigabe ist nur dem Abteilungsleiter (siehe Abteilungen) oder den Mitarbeitern der Buchhaltung erlaubt. <br> Abteilungsleiter müssen Mitglied der Abteilung sein!
                            Wird ein Abteilungsleiter aus einer Abteilung entfernt, entfällt auch die Abteilngsleiter-Position.
                        </div>
                        <div class="col-md-6 offset-md-3">
                            <div class="card" style="">
                                <!-- Default panel contents -->

                                <ul class="list-group list-group-flush pt-3">

                                    @foreach(\App\Department::all() AS $department)
                                        <li class="list-group-item">
                                            {{ $department->name }}
                                            <label class="switch ">
                                                <input value="{{$department->id}}"
                                                       data-form-action="{{ route('control.user.department.assing', ['user' => $user->id, 'department' => $department->id]) }}"
                                                       type="checkbox" class="custom_switch success" {{ $user->hasDepartment($department) ? 'checked="checked"' : '' }}>
                                                <span class="slider"></span>
                                                <span class="custom-success" id="row_{{$department->id}}"><i class="fas fa-check"></i></span>
                                            </label>
                                    @endforeach


                                </ul>
                            </div>

                        </div>

                        <a style="float: right;" href="{{ route('control.user.index') }}" value="back" class="btn btn-primary">Zurück zur Benutzerübersicht</a>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
