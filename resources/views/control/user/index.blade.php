@extends('control.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Mitarbeiter / Benutzer</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="Benutzer neu anlegen" style="float: right;" href="{{ route('control.user.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12">

                        <form id="submit-form" method="get" action="{{ route('control.user.index') }}">
                            <input id="filter" name="filter" type="hidden" class="form-control" value="1" >
                            @csrf
                            <div class="input-group">
                                <input id="filter_word" name="filter_word" type="text" class="form-control" placeholder="Suchbegriff" value="{{ old('filter_word') ?? $filter_attributes['filter_word'] ?? '' }}">
                                <div class="input-group-btn" >
                                    <div class="input-group-append">
                                        <select  name="filter_row" id="filter_row" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="name" {{ old('filter_row') == 'name' || ( isset($filter_attributes['filter_row']) && $filter_attributes['filter_row'] == 'name' ) ? 'selected="selected"' : '' }}>in Name</option>
                                            <option value="email" {{ old('filter_row') == 'email' || ( isset($filter_attributes['filter_row']) && $filter_attributes['filter_row'] == 'email' ) ? 'selected="selected"' : '' }}>in Email</option>
                                            <option value="tel" {{ old('filter_row') == 'tel' || ( isset($filter_attributes['filter_row']) && $filter_attributes['filter_row'] == 'tel' ) ? 'selected="selected"' : '' }}>in Telefon</option>
                                        </select>
                                        <select  name="filter_department" id="filter_department" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="" >Abteilung</option>
                                            @foreach(\App\Department::all() AS $department)
                                                <option value="{{$department->id}}" {{ isset($filter_attributes['filter_department']) && $filter_attributes['filter_department'] == $department->id  || old('mandant_id') == $department->id ? 'selected="selected"' : '' }} >{{$department->name}}</option>
                                            @endforeach
                                        </select>
                                        <select  name="filter_order" id="filter_order" style="cursor: pointer; color:#495057; border: 1px solid #ced4da; border-radius: 0px; height: 37px !important;">
                                            <option value="desc" {{ old('filter_order') == 'desc' || ( isset($filter_attributes['filter_order']) && $filter_attributes['filter_order'] == 'desc' ) ? 'selected="selected"' : '' }}>Absteigend</option>
                                            <option value="asc"  {{ old('filter_order') == 'asc' || ( isset($filter_attributes['filter_order']) && $filter_attributes['filter_order'] == 'asc' ) ? 'selected="selected"' : '' }} >Aufsteigend</option>
                                        </select>
                                        <button style="line-height: unset; padding: 0px; border-radius: 0px " type="submit" class="btn btn-secondary btn-search" >Filtern</button>
                                        <a title="Zurücksetzen"  style="line-height: unset; padding: 0px; border-radius: 0px 4px 4px 0px; line-height: 35px; min-width: 45px"  class="btn btn-secondary "  role="button" href="{{ route('control.user.index' ) }}"><i class="fas fa-redo"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>

                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">
                                <thead class="thead-dark">
                                <tr>
                                    <th class="head-icons-1">
                                    <th>Benutzer</th>
                                    <th>E-Mail</th>
                                    <th>Telefon</th>
                                    <th>Letzter Login</th>
                                    @headicons('4', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $users AS $user )
                                    <tr id="">
                                        <td>
                                            <a title="Benutzer {{ $user->active ? 'deaktivieren' : 'aktivieren' }}" class="mr-2"  href="{{ route('control.user.isActiveToggle', $user->id) }}" role="button"><i class="fa fa-circle {{ $user->active ? 'text-success' : 'text-secondary' }}"></i></a>
                                         </td>
                                        <td>{{ $user->name }} {{ $user->surname }} <a class="float-right" href="{{ route('control.user.switch.start', $user->id)  }}"><i title="Einloggen als {{ $user->fullName() }} " class="fas fa-key"></i></a></td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->tel }}</td>
                                        <td>@datetime($user->last_login_at)</td>
                                        <td>
                                            <a class="mr-2" href="{{ route('control.user.department.assing.show', ['user'=>$user->id] ) }}">
                                                <i style="font-size: inherit; color: currentColor; cursor: pointer;" data-toggle="tooltip" data-html="true" data-original-title="Benutzer Abteilungen: <br> {{ implode(', ', $user->departments->pluck('name')->toArray()) }}" class="fa fa-users"></i>
                                            </a>
                                            <a title="Benutzer Rollen" class="mr-2" href="{{ route('control.user.area.assing.show', ['user'=>$user->id] ) }}">
                                                <i style="font-size: inherit; color: currentColor; cursor: pointer;" data-toggle="tooltip" data-html="true" data-original-title="Benutzer Bereiche: <br> {{ implode(', ', $user->areas->pluck('name')->toArray()) }}" class="fas fa-user-shield"></i>
                                            <a title="Benutzer bearbeiten" class="mr-2" href="{{ route('control.user.show', ['user'=>$user->id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="Benutzer löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-action="{{route('control.user.delete', ['user'=>$user->id])}}"
                                               data-form-info="Wollen Sie den Mitarbeiter '{{ $user->fullName() }}' wirklich endgültig löschen?">
                                               <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="6">Keine Benutzer vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    {{ $users->withQueryString()->links() }}
                </div>

                <div class="row mt-3">
                    <div class="col-md-12 ">
                        @php($colums='"name","surname","email","tel","active","last_login_at","created_at"')
                        @php($headers='"Name","Nachname","E-Mail","Telefon","Aktiv","Zuletzt online","Erstellt"')
                        <button onclick="generateCSV({  action: '{{route("control.user.csv")}}', columns: '[{{$colums}}]', headers: '[{{$headers}}]', filename: 'Mitarbeiter.csv'  })" class="btn btn-primary">Exportiere Daten als CSV</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

