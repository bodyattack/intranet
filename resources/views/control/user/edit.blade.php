@extends('control.layouts.app')
@empty($user) @inject('user', 'App\User') @endempty
<?php
$searchable_select=1; // ergänze die class "selectpicker" select tag um  filtern zu können

if(!isset($previousUrlQuerry)) {
    $previousUrlQuerry = substr(url()->previous(), strpos(url()->previous(), '?')+1);
}
?>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">Sie {{ empty($user->id) ? ' registrieren' : 'bearbeiten' }} einen Mitarbeiter</div>

                    <div class="card-body">

                        <?php
                        #dd(old());

                        ?>

                        @empty(!$user->id)
                            <form method="POST" action="{{ route('control.user.update', ['user' => $user->id]) }}">
                        @method('PATCH')
                        @else
                            <form method="POST" action="{{ route('control.user.store') }}">
                        @endempty

                        @csrf
                        <input id="filterQuerry" type="hidden" name="urlQuerry" value="{{ old('urlQuerry', $previousUrlQuerry) }}">

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">@lang('auth.register.name')</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $user->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="surname" class="col-md-4 col-form-label text-md-right">@lang('auth.register.surname')</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname', $user->surname) }}" required>

                                @if ($errors->has('surname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">@lang('auth.register.email')</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $user->email) }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tel" class="col-md-4 col-form-label text-md-right">Telefon</label>

                            <div class="col-md-6">
                                <input id="tel" type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" name="tel" value="{{ old('tel', $user->tel) }}" >

                                @if ($errors->has('tel'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tel') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="erp_name" class="col-md-4 col-form-label text-md-right">ERP-Name</label>

                            <div class="col-md-6">
                                <input id="erp_name" type="text" class="form-control{{ $errors->has('erp_name') ? ' is-invalid' : '' }}" name="erp_name" value="{{ old('erp_name', $user->erp_name) }}" >

                                @if ($errors->has('erp_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('erp_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="invoice_approvers" class="col-md-4 col-form-label text-md-right">Erweiterte Rechnungsfreigabe</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="selectpicker input-bordered form-control" name="invoice_approvers[]" id="invoice_approvers"  multiple data-actions-box="true">
                                            @php( $departments_to_approve = $user->auditorForDepartments()->pluck('id')->toArray() )
                                            @foreach(\App\Department::all() AS $department)
                                                <option value="{{$department->id}}" {{ ( in_array($department->id , $departments_to_approve) || in_array($department->id , old('invoice_approvers', [])) ) ? 'selected="selected"' : '' }} >{{$department->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @error('position_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Soll ein Mitarbeiter Rechnungen 'Vorab-Freigeben' dürfen, ohne selbst in der Abteilung tätig zu sein, muss er hier zugewiesen werden." class="fas fa-info-circle"></i>
                            </div>
                        </div>

                                {{--  ACHTUNG! Level ID in User_Tabelle ist ein Problem bei fehlendem Bezug, muss ausgelagert werden.
                        <div class="form-group row">
                            <label for="position_id" class="col-md-4 col-form-label text-md-right">Funktion</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="input-bordered" name="position_id" id="position_id">
                                            <option value="0">Bitte wählen</option>
                                            @foreach(\App\Level::all() AS $position)
                                                <option value="{{$position->id}}" {{ $user->position_id == $position->id  || old('position_id')== $position->id ? 'selected="selected"' : '' }} >{{$position->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @error('position_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="role" class="col-md-4 col-form-label text-md-right">Berechtigung</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="input-bordered" name="role" id="role">
                                            <option value="0">Bitte wählen</option>
                                            @foreach(\App\Role::all() AS $role)
                                                <option value="{{$role->id}}" {{ $user->hasRole($role->slug) || old('role')== $role->id ? 'selected="selected"' : '' }} >{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @error('role')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                                --}}

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">@lang('auth.register.password')</label>

                            <div class="col-md-6">
                                <input placeholder="{{ !empty($user->id) ? 'Wird durch Eingabe überschrieben' : '' }}"  id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>

                                @empty($user->id)
                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@lang('auth.register.password_confirm')</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                                </div>
                            </div>
                                @endempty


                            <div class="form-group row">
                                <label for="api_active" class="col-md-4 col-form-label text-md-right">Api Zugriff</label>

                                <div class="col-md-6">
                                    <div class="input-with-label">
                                        <div class="select-wrapper">
                                            <select class="input-bordered form-control" name="api_active" id="api_active">
                                                <option value="0">Nein</option>
                                                <option value="1" {{ $user->api_key ? 'selected' : '' }}>Ja</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="API Anmeldung erlauben (BA-Shop), Key ändert sich mit Änderung der Freigabe!" class="fas fa-info-circle"></i>
                                </div>
                            </div>

                            <div class="form-group row" id="api_key_field" style="display: {{ $user->api_key ? 'show': 'none' }}">
                                <label for="api_key" class="col-md-4 col-form-label text-md-right">API Key</label>

                                <div class="col-md-6">
                                    <div class="input-with-label">
                                        <input type="text" name="api_key" readonly required class="form-control" id="api_key"
                                               value="{{ old('api_key', $user->api_key) }}" >

                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit"  name="submit-type" value="stay" class="btn btn-primary">@lang('auth.register.save')</button>
                                    <a style="float: right;" href="{{ route('control.user.index', old('urlQuerry', $previousUrlQuerry)) }}" value="back" class="btn btn-primary"> {{ __('Zurück') }}</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer_scripts')
    <script type="text/javascript">

        $('#api_active').on('change', function () {

            if($(this).val()=='1') {
                $('#api_key').val(generateApiKey(48));
                $('#api_key_field').show();
            }
            else {
                $('#api_key').val('');
                $('#api_key_field').hide();
            }


        })

        function generateApiKey(length=0) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;

            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }

            return result;
        };

        /*
        $(document).ready(function() {

           function generateApiKey() {
                var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 40;
                var result = '';
                var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                var charactersLength = characters.length;

                for (var i = 0; i < length; i++) {
                    result += characters.charAt(Math.floor(Math.random() * charactersLength));
                }

                return result;
            };
        });

         */

    </script>
@stop

