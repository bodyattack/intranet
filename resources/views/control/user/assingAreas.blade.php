@extends('control.layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> <h3>Bereiche für Nutzer "{{ $user->fullname() }}"</h3></div>

                    <div class="card-body">
                        <div class="alert alert-warning" role="alert">
                            Bereiche können zugewiesen werden, um einem Mitarbeiter Zugang zu geben.<br>
                            Die jeweiligen Unterpunkte sind individuelle Berechtigungen zum Bereich und nur bei Bedarf zu vergeben.
                        </div>
                        <div class="col-md-8 offset-md-2">
                            <div class="card" style="">
                                <!-- Default panel contents -->

                                <ul class="list-group list-group-flush pt-3">

                                    @foreach(\App\Area::all() AS $area)
                                        @php($has_area = $user->hasArea($area))
                                        <li class="list-group-item pb-1" id="area_{{ $area->id }}">
                                            <span class="font-size17">{{ $area->name }}</span>
                                            <label title="{{ $area->name }}" class="switch ">
                                                <input value="{{$area->id}}"
                                                       data-form-action="{{ route('control.user.area.assing', ['user' => $user->id, 'area' => $area->id]) }}"
                                                       type="checkbox" class="custom_switch success" {{ $has_area ? 'checked="checked"' : '' }}>
                                                <span class="slider"></span>
                                                <span class="custom-success" id="row_{{$area->id}}"><i class="fas fa-check"></i></span>
                                            </label>
                                        </li>
                                        @if($has_area)
                                            @include('control.user.assingPermissions', ['display' => 'show'])
                                        @endif
                                    @endforeach

                                </ul>
                            </div>

                        </div>

                        <a style="float: right;" href="{{ route('control.user.index') }}" value="back" class="btn btn-primary">Zurück zur Benutzerübersicht</a>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
