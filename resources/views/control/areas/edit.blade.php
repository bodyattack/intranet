@extends('control.layouts.app')
@empty($area) @inject('area', 'App\Area') @endempty
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">Bereich {{ empty($area->id) ? ' anlegen' : 'bearbeiten' }}</div>

                    <div class="card-body">
                        @empty(!$area->id)
                            <form  method="POST" action="{{ route('control.areas.update', ['area'=>$area->id]) }}">
                            @method('PATCH')
                        @else
                            <form method="POST" action="{{ route('control.areas.store') }}">
                        @endempty
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $area->name) }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" area="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                @empty(!$area->id)
                                    <button type="submit" name="submit-type" value="stay" class="btn btn-primary">Speichern</button>
                                @else
                                    <button type="submit" name="submit-type" value="back" class="btn btn-primary">Speichern</button>
                                @endempty
                                    <a style="float: right;" href="{{ route('control.areas.index') }}" value="back" class="btn btn-primary"> {{ __('Zurück') }}</a>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection