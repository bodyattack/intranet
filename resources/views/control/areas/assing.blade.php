@extends('control.layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> <h3>Bereichtigungs-Optionen für Bereich "{{ $area->name }}"</h3></div>

                    <div class="card-body">
                        <div class="alert alert-warning" role="alert">
                            Diese Einstellungen dienen als Vorauswahl, welche Rechte einem Benutzer über diesen Bereich zugewiesen werden können.<br>
                            Die Finale Zuordnung und Aktivierung muss direkt über den Benutzer selbst erfolgen.
                        </div>
                        <div class="col-md-8 offset-md-2">
                            <div class="card" style="">
                                <!-- Default panel contents -->
                                <ul class="list-group list-group-flush pt-3">

                                    @foreach($permissions['area'] AS $permission)
                                        <li class="list-group-item">
                                            {{ $permission->name }}
                                            <label class="switch ">
                                                <input value="{{$permission->id}}"
                                                       data-form-action="{{ route('control.areas.permissionAssing', ['area' => $area->id, 'permission' => $permission->id]) }}"
                                                       type="checkbox" class="custom_switch success" {{ $area->hasPermission($permission) ? 'checked="checked"' : '' }}>
                                                <span title="{{ $permission->name }}" class="slider"></span>
                                                <span class="custom-success" id="row_{{$permission->id}}"><i class="fas fa-check"></i></span>
                                            </label>
                                    @endforeach


                                </ul>

                                <hr>
                                <ul class="list-group list-group-flush pt-2">

                                    @foreach($permissions['global'] AS $permission)
                                        <li class="list-group-item">
                                            {{ $permission->name }} <i>(global)</i>
                                            <label class="switch ">
                                                <input value="{{$permission->id}}"
                                                       data-form-action="{{ route('control.areas.permissionAssing', ['area' => $area->id, 'permission' => $permission->id]) }}"
                                                       type="checkbox" class="custom_switch success" {{ $area->hasPermission($permission) ? 'checked="checked"' : '' }}>
                                                <span  title="{{ $permission->name }}"  class="slider"></span>
                                                <span class="custom-success" id="row_{{$permission->id}}"><i class="fas fa-check"></i></span>
                                            </label>
                                    @endforeach


                                </ul>
                            </div>

                        </div>

                        <a style="float: right;" href="{{ route('control.areas.index') }}" value="back" class="btn btn-primary">Zurück zur Bereichsübersicht</a>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
