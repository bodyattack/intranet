@extends('control.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Bereiche / Gruppen</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="Bereiche hinzufügen" style="float: right;" href="{{ route('control.areas.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="alert alert-warning" role="alert">
                            Dies sind primär Arbeitsbereiche im Intranet, über Zuordnung zum Benutzer wird Sichtbarkeit und damit verbunden auch Berechtigungen geregelt.
                            <br>Die Bereiche können auch als Gruppe verwendet werden, um globale Berechtigungen zusammenzufassen und mit Benutzern verknüpfen zu können.
                            <br>Zuweisung von Rechten zu einem Bereich definiert gewissermaßen ein Template möglicher Berechtigungen, innerhalb eines Bereichs.
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Erzeugt am</th>
                                    @headicons('3', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ( $areas AS $areas )
                                    <tr id="row_{{ $areas->id }}">
                                        <td>{{ $areas->name }}</td>
                                        <td>{{ $areas->slug }}</td>
                                        <td>{{ $areas->created_at->format('d. m. Y - H:i') }}</td>
                                        <td>
                                            <a title="Rechte Optionen zuordnen"  class="mr-2" href="{{ route('control.areas.assing', ['area'=>$areas->id] ) }}"><i class="fas fa-user-shield"></i></a>
                                            <a title="Bereic bearbeiten" class="mr-2" href="{{ route('control.areas.show', ['area'=>$areas->id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="Bereic löschen" class="mr-2 modal-delete"  href="#" data-form-action="{{route('control.areas.delete', $areas->id)}}"><i class="far fa-trash-alt"></i></a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('footer_scripts')
    <script type="text/javascript">
        @isset ($insert_id)
        $(document ).ready(function() {
            $("#row_{{ $insert_id }}");
        });
        @endisset
    </script>
@stop


