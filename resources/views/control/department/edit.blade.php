@extends('control.layouts.app')
@php $searchable_select=1; @endphp  {{-- ergänze die class "selectpicker" select tag um  filtern zu können --}}
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">{{ $department->presentation_short }} {{ empty($department->id) ? ' erstellen' : 'bearbeiten' }}</div>

                    <div class="card-body">

                        @empty(!$department->id)
                            <form id="submit-form" method="POST" action="{{ route('control.department.update', ['department' => $department->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('control.department.store') }}">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Bezeichnung</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $department->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="short_name" class="col-md-4 col-form-label text-md-right">Kurzbezeichnung</label>

                            <div class="col-md-6">
                                <input id="short_name" type="text" class="form-control{{ $errors->has('short_name') ? ' is-invalid' : '' }}" name="short_name" value="{{ old('short_name', $department->short_name) }}" >

                                @if ($errors->has('short_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('short_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="user_id" class="col-md-4 col-form-label text-md-right">Leitung</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="selectpicker input-bordered form-control" name="user_id" id="user_id">
                                            <option value="0">Bitte wählen</option>
                                            @foreach($department->user AS $user)
                                                <option value="{{$user->id}}" {{ $department->user_id == $user->id  || old('user_id')== $user->id ? 'selected="selected"' : '' }} >{{$user->fullName()}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @error('position_id')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Nur ein Mitarbeiter der Abteilung (Zuordnung über Mitarbeiter -> Abteilungen) kann als Leitung festgelegt werden." class="fas fa-info-circle"></i>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="invoice_approvers" class="col-md-4 col-form-label text-md-right">Erweiterte Rechnungsfreigabe</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="selectpicker input-bordered form-control" name="invoice_approvers[]" id="invoice_approvers"  multiple data-actions-box="true">
                                            @php( $department_invoice_approvers = $department->invoiceApprovers()->pluck('id')->toArray() )
                                            @foreach(\App\User::all() AS $user)
                                                <option value="{{$user->id}}" {{ ( in_array($user->id , $department_invoice_approvers) || in_array($user->id , old('invoice_approvers', [])) ) ? 'selected="selected"' : '' }} >{{$user->fullName()}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @error('position_id')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>

                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Soll ein Mitarbeiter Rechnungen 'Vorab-Freigeben' dürfen, ohne selbst in der Abteilung tätig zu sein, muss er hier zugewiesen werden." class="fas fa-info-circle"></i>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="submit-button" name="submit-type" value="stay" class="btn btn-primary prevent-double-click">
                                    <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                    Speichern
                                </button>
                                <a style="float: right;" href="{{ route('control.department.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function() {
        });
    </script>
@stop


