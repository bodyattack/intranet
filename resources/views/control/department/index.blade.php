@extends('control.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $department->presentation_long }}</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="{{ $department->presentation_short }} neu anlegen" style="float: right;" href="{{ route('control.department.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="alert alert-warning" role="alert">
                            Hier werden die Abteilungen angelegt, es muss ein Abteilungsleiter zugewiesen werden, welcher somit Rechnungen final freigeben darf.
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Bezeichnung</th>
                                    <th>Leitung</th>
                                    <th>Anzahl Mitarbeiter</th>
                                    <th>Erzeugt am</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $departments AS $department )
                                    <tr id="">
                                        <td>{{ $department->name }} {{ $department->surname }}</td>
                                        <td>{{ (empty($department->leader)?'':$department->leader['name']) }}</td>
                                        <td>{{ count($department->user) }}</td>
                                        <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $department->created_at)->format('d.m.Y') }}</td>
                                        <td>
                                            <a title="{{ $department->presentation_short }} bearbeiten" class="mr-2" href="{{ route('control.department.show', ['department'=>$department->id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="{{ $department->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die {{ $department->presentation_short }} '{{ $department->name }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('control.department.destroy', ['department'=>$department->id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $department->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
@stop

