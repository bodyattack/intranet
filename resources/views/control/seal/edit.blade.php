@extends('control.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> {{ $seal->presentation_short }} {{ empty($seal->id) ? ' erstellen' : 'bearbeiten' }} </div>

                    <div class="card-body">

                        @empty(!$seal->id)
                            <form id="submit-form" method="POST" action="{{ route('control.seal.update', ['seal' => $seal->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('control.seal.store') }}">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $seal->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="name_en" class="col-md-4 col-form-label text-md-right">Name Englisch</label>

                            <div class="col-md-6">
                                <input id="name_en" type="text" class="form-control{{ $errors->has('name_en') ? ' is-invalid' : '' }}" name="name_en" value="{{ old('name_en', $seal->name_en) }}" >

                                @if ($errors->has('name_en'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name_en') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>


                                <div class="form-group row">
                                    <label for="description" class="col-md-4 col-form-label text-md-right">Beschreibung</label>

                                    <div class="col-md-6">
                                        <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description', $allergen->description) }}">

                                        @if ($errors->has('description'))
                                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="description_en" class="col-md-4 col-form-label text-md-right">Beschreibung Englisch</label>

                                    <div class="col-md-6">
                                        <input id="description_en" type="text" class="form-control{{ $errors->has('description_en') ? ' is-invalid' : '' }}" name="description_en" value="{{ old('description_en', $allergen->description_en) }}">

                                        @if ($errors->has('description_en'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description_en') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                        <div class="form-group row">
                            <label for="icon" class="col-md-4 col-form-label text-md-right">Name Bild-Icon</label>

                            <div class="col-md-6">
                                <input id="icon" type="text" class="form-control{{ $errors->has('icon') ? ' is-invalid' : '' }}" name="icon" value="{{ old('icon', $seal->icon) }}" >

                                @if ($errors->has('icon'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('icon') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="filter_link" class="col-md-4 col-form-label text-md-right">Filter Link</label>

                            <div class="col-md-6">
                                <input id="filter_link" type="text" class="form-control{{ $errors->has('filter_link') ? ' is-invalid' : '' }}" name="filter_link" value="{{ old('filter_link', $seal->filter_link) }}" >

                                @if ($errors->has('filter_link'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('filter_link') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="filter_link_en" class="col-md-4 col-form-label text-md-right">Filter Link Englisch</label>

                            <div class="col-md-6">
                                <input id="filter_link_en" type="text" class="form-control{{ $errors->has('filter_link_en') ? ' is-invalid' : '' }}" name="filter_link_en" value="{{ old('filter_link_en', $seal->filter_link_en) }}" >

                                @if ($errors->has('filter_link_en'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('filter_link_en') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click">Speichern</button>
                                <a style="float: right;" href="{{ route('control.seal.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection