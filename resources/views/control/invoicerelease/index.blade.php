@extends('control.layouts.app')

@section('content')

    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Rechnungskontolle</h4>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">

                        @include('control.mailbox.index')

                        <div class="card mt-3">

                            <div class="card-header">
                                <div class="col-md-12 mt-1">
                                    Rechnungssteller Upload
                                </div>
                            </div>

                            <div class="card-body">

                                <div class="alert alert-warning" role="alert">
                                    Hier können Rechnungsteller per CSV-Datei eingspielt werden (CSV Aufbau beachten!)
                                    <br>
                                    <pre>Headrow: "Bezeichnung;Freigabe Abteilung;Zahlart;Zuordnungs- Betreff;Zuordnungs- E-Mail;Zahlungsziel;Skonto Tage;Skonto %;Kerditoren-Nr."<br>Datarow: "Rechnungsteller 1;1;Rechnung;;rechnung@is24.de;14;0;0;234566"</pre>
                                </div>

                                <form id="submit-form"  method="POST" action="{{ route('control.invoicerelease.upload_biller') }}" enctype='multipart/form-data' >
                                @csrf



                                        <div class="row">
                                        <label for="file" class="col-md-2 col-form-label text-md-right">Datei auswählen</label>

                                        <div class="col-md-4">
                                            <input id="file" type="file" class="form-control" name="file" value="{{ old('file') }}" >
                                        </div>

                                        <div class="col-md-3">
                                            <div class="select-wrapper">
                                                <select class="input-bordered" name="head_row" id="head_row">
                                                    <option value="1">Datei hat Kopfzeilen</option>
                                                    <option value="0">Datei ohne Kopfzeilen</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click">Verarbeiten</button>
                                        </div>

                                        </div>

                                </form>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection