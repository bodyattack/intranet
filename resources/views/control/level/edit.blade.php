@extends('control.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">{{ $level->presentation_short }} {{ empty($level->id) ? ' erstellen' : 'bearbeiten' }}</div>

                    <div class="card-body">

                        @empty(!$level->id)
                            <form id="submit-form" method="POST" action="{{ route('control.level.update', ['level' => $level->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('control.level.store') }}">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Bezeichnung</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $level->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button onclick="$('#submit-button').attr('disabled', true); $('.spinner-border').removeClass('d-none'); $('#submit-form').submit();" type="submit" id="submit-button" name="submit-type" value="stay" class="btn btn-primary">
                                    <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                    Speichern
                                </button>
                                <a style="float: right;" href="{{ route('control.level.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection