@extends('control.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $allergen->presentation_long }}</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="{{ $allergen->presentation_short }} neu anlegen" style="float: right;" href="{{ route('control.allergen.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">

                                <thead class="thead-dark">
                                <tr>
                                    @headicons('1')
                                    <th>Name</th>
                                    <th>Name En</th>
                                    <th>Erzeugt am</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $allergens AS $allergen )
                                    <tr id="">
                                        <td>
                                            <a title="{{ $allergen->presentation_short }}  {{ $allergen->active ? 'deaktivieren' : 'aktivieren' }}" class="mr-2"  href="{{ route('control.allergen.toggleActive', $allergen->id) }}" role="button"><i class="fa fa-circle {{ $allergen->active ? 'text-success' : 'text-secondary' }}"></i></a>
                                        </td>
                                        <td>{{ $allergen->name }} </td>
                                        <td>{{ $allergen->name_en }} </td>
                                        <td>@date($allergen->created_at)</td>
                                        <td>
                                            <a title="{{ $allergen->presentation_short }} bearbeiten" class="mr-2" href="{{ route('control.allergen.show', ['allergen'=>$allergen->id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="{{ $allergen->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die {{ $allergen->presentation_short }} '{{ $allergen->name }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('control.allergen.destroy', ['allergen'=>$allergen->id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $allergen->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

@endsection
