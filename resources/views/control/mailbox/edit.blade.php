@extends('control.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> {{ $mailbox->presentation_short }} {{ empty($mailbox->id) ? ' erstellen' : 'bearbeiten' }} </div>

                    <div class="card-body">

                        @empty(!$mailbox->id)
                            <form id="submit-form" method="POST" action="{{ route('control.mailbox.update', ['mailbox' => $mailbox->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('control.mailbox.store') }}">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Bezeichnung</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $mailbox->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="conf" class="col-md-4 col-form-label text-md-right">IMAP Config</label>

                            <div class="col-md-6">
                                <input id="conf" type="text" class="form-control{{ $errors->has('conf') ? ' is-invalid' : '' }}" name="conf" value="{{ old('conf', $mailbox->conf) }}" >

                                @if ($errors->has('conf'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('conf') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user" class="col-md-4 col-form-label text-md-right">Benutzer</label>

                            <div class="col-md-6">
                                <input id="user" type="text" class="form-control{{ $errors->has('user') ? ' is-invalid' : '' }}" name="user" value="{{ old('user', $mailbox->user) }}" >

                                @if ($errors->has('user'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="pass" class="col-md-4 col-form-label text-md-right">Passwort</label>

                            <div class="col-md-6">
                                <input id="pass" type="text" class="form-control{{ $errors->has('pass') ? ' is-invalid' : '' }}" name="pass" value="{{ old('pass', $mailbox->pass) }}" >

                                @if ($errors->has('pass'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pass') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mandant_id" class="col-md-4 col-form-label text-md-right">Mandant</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select class="input-bordered" name="mandant_id" id="mandant_id">
                                            <option value="0">Bitte wählen</option>
                                            @foreach(\App\Mandant::all() AS $mandant)
                                                <option value="{{$mandant->id}}" {{ $mailbox->mandant_id == $mandant->id  || old('mandant_id')== $mandant->id ? 'selected="selected"' : '' }} >{{$mandant->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @error('mandant_id')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Über den hier hinterlegten Mandant, erfolgt die Zuordnung einer Rechnung" class="fas fa-info-circle"></i>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click">Speichern</button>
                                <a style="float: right;" href="{{ route('control.invoicerelease.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection