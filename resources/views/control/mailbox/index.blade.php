
<div class="card mt-3">

    <div class="card-header">
        <div class="col-md-12 mt-1">
            <a title="{{ $mailbox->presentation_short }} neu anlegen" style="float: right;" href="{{ route('control.mailbox.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
            {{ $mailbox->presentation_long }}
        </div>
    </div>

    <div class="card-body">

        <div class="alert alert-warning" role="alert">
            Hier werden die abzurufenden Postfächer angelegt. Abruf erfolg via IMAP (PHP).
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-striped mt-2 mb-3">

                <thead class="thead-dark">
                <tr>
                    @headicons('1')
                    <th>Bezeichnung</th>
                    <th>Mandant</th>
                    <th>User</th>
                    <th>Pass</th>
                    <th>Erzeugt am</th>
                    @headicons('2', 'fa-cogs')
                </tr>
                </thead>
                <tbody>
                @forelse ( $mailboxes AS $mailbox )
                    <tr id="">
                        <td>
                            <a title="{{ $mailbox->presentation_short }}  {{ $mailbox->active ? 'deaktivieren' : 'aktivieren' }}" class="mr-2"  href="{{ route('control.mailbox.toggleActive', $mailbox->id) }}" role="button"><i class="fa fa-circle {{ $mailbox->active ? 'text-success' : 'text-secondary' }}"></i></a>
                        </td>
                        <td>{{ $mailbox->name }} </td>
                        <td>{{ $mailbox->mandant->name }}</td>
                        <td>{{ $mailbox->user }}</td>
                        <td>{{ $mailbox->pass }}</td>
                        <td>@date($mailbox->created_at)</td>
                        <td>
                            <a title="{{ $mailbox->presentation_short }} bearbeiten" class="mr-2" href="{{ route('control.mailbox.show', ['mailbox'=>$mailbox->id] ) }}"><i class="far fa-edit"></i></a>
                            <a title="{{ $mailbox->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                               data-form-info="Wollen Sie die {{ $mailbox->presentation_short }} '{{ $mailbox->name }}' wirklich endgültig löschen?"
                               data-form-action="{{route('control.mailbox.destroy', ['mailbox'=>$mailbox->id])}}">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr id=""><td colspan="100%">Keine {{ $mailbox->presentation_long }} vorhanden</td></tr>
                @endforelse
                </tbody>
            </table>
        </div>

    </div>

</div>

