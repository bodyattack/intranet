@extends('control.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">Admin Benutzer</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="Benutzer neu anlegen" style="float: right;" href="{{ route('control.backenduser.create') }}"><i class="far fa-plus-square fa-2x"></i></a>

                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Name</th>
                                    <th>E-Mail</th>
                                    <th>Erzeugt am</th>
                                    <th class="" style="width:40px"><i class="fas fa-cogs float-right"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ( $users AS $user )
                                    <tr id="">
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->created_at->format('d.m.Y') }}</td>
                                        <td>
                                            <a title="Control Nutzer löschen" class="mr-2 modal-delete"  href="#" data-form-action="{{route('control.backenduser.delete', ['user'=>$user->id] )}}"><i class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <p>{{ $api_token }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
    <script type="text/javascript">

    </script>
@stop

