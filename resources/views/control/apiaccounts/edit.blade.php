@extends('control.layouts.app')
@php $searchable_select=0; @endphp  {{-- ergänze die class "selectpicker" select tag um  filtern zu können --}}
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">{{ $apiaccount->presentation_short }} {{ empty($apiaccount->id) ? ' erstellen' : 'bearbeiten' }}</div>

                    <div class="card-body">

                        @empty(!$apiaccount->id)
                            <form id="submit-form" method="POST" action="{{ route('control.apiaccount.update', ['apiaccount' => $apiaccount->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('control.apiaccount.store') }}">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="user_id" class="col-md-4 col-form-label text-md-right">Benutzer</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <div class="select-wrapper">
                                        <select onchange="generateApiElements($(this))" class="selectpicker input-bordered form-control" name="user_id" id="user_id">
                                            <option value="0">Bitte wählen</option>
                                            @foreach(\App\User::all() AS $user)
                                                <option value="{{$user->id}}" {{ $apiaccount->user_id == $user->id  || old('user_id')== $user->id ? 'selected="selected"' : '' }} >{{$user->fullName()}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @error('position_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">API User</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <input type="text" name="username" readonly required class="form-control" id="username"
                                               value="{{ old('username', $apiaccount->username) }}" >

                                </div>
                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="key" class="col-md-4 col-form-label text-md-right">API Key</label>

                            <div class="col-md-6">
                                <div class="input-with-label">
                                    <input type="text" name="key" readonly required class="form-control" id="key"
                                               value="{{ old('key', $apiaccount->key) }}" >

                                </div>
                                @error('key')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button onclick="$('#submit-button').attr('disabled', true); $('.spinner-border').removeClass('d-none'); $('#submit-form').submit();" type="submit" id="submit-button" name="submit-type" value="stay" class="btn btn-primary">
                                    <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                    Speichern
                                </button>
                                <a style="float: right;" href="{{ route('control.apiaccount.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">

        function generateApiElements(e) {
            var name = e.children("option:selected").text();
            $('#username').val(slugify(name));
            $('#key').val(generateApiKey(48));
        }

        $(document).ready(function() {
            window.generateApiKey = function () {
                var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 40;
                var result = '';
                var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                var charactersLength = characters.length;

                for (var i = 0; i < length; i++) {
                    result += characters.charAt(Math.floor(Math.random() * charactersLength));
                }

                return result;
            };

            window.slugify = function () {
                var string = arguments[0] !== undefined ? arguments[0] : '';
                const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
                const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
                const p = new RegExp(a.split('').join('|'), 'g')

                return string.toString().toLowerCase()
                    .replace(/\s+/g, '-') // Replace spaces with -
                    .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
                    .replace(/&/g, '-and-') // Replace & with 'and'
                    .replace(/[^\w\-]+/g, '') // Remove all non-word characters
                    .replace(/\-\-+/g, '-') // Replace multiple - with single -
                    .replace(/^-+/, '') // Trim - from start of text
                    .replace(/-+$/, '') // Trim - from end of text
            }

            window.camelize = function () {
                var str = arguments[0] !== undefined ? arguments[0] : '';
                return str.replace(/\W+(.)/g, function(match, chr)
                {
                    return chr.toUpperCase();
                });
            }

        });
    </script>
@stop

