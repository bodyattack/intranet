@extends('control.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $apiaccount->presentation_long }}</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="{{ $apiaccount->presentation_short }} neu anlegen" style="float: right;" href="{{ route('control.apiaccount.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Username</th>
                                    <th>Key</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $apiaccounts AS $apiaccount )
                                    <tr id="">
                                        <td>{{ $apiaccount->username }} </td>
                                        <td>{{ $apiaccount->key }} </td>
                                        <td>
                                            <a title="{{ $apiaccount->presentation_short }} bearbeiten" class="mr-2" href="{{ route('control.apiaccount.show', ['apiaccount'=>$apiaccount->id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="{{ $apiaccount->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die {{ $apiaccount->presentation_short }} '{{ $apiaccount->name }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('control.apiaccount.destroy', ['apiaccount'=>$apiaccount->id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $apiaccount->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection


