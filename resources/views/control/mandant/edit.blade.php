@extends('control.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">{{ $mandant->presentation_short }} {{ empty($mandant->id) ? ' erstellen' : 'bearbeiten' }}</div>

                    <div class="card-body">

                        @empty(!$mandant->id)
                            <form id="submit-form" method="POST" action="{{ route('control.mandant.update', ['mandant' => $mandant->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('control.mandant.store') }}">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Bezeichnung</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $mandant->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="short_name" class="col-md-4 col-form-label text-md-right">Kurzbezeichnung</label>

                            <div class="col-md-6">
                                <input id="short_name" type="text" class="form-control{{ $errors->has('short_name') ? ' is-invalid' : '' }}" name="short_name" value="{{ old('short_name', $mandant->short_name) }}" >

                                @if ($errors->has('short_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('short_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="datev_email" class="col-md-4 col-form-label text-md-right">DATEV / Weiterleitungs-E-Mail Adresse</label>

                            <div class="col-md-6">
                                <input id="datev_email" type="text" class="form-control {{ $errors->has('datev_email') ? ' is-invalid' : '' }}"  name="datev_email" value="{{ old('datev_email', $mandant->datev_email) }}" >
                                @if ($errors->has('datev_email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('datev_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <i style="font-size: 18px; float: left; margin-left: -20px; margin-top:3px" data-toggle="tooltip" data-html="true" title="" data-original-title="Benötigt für Rechnungskontrolle. Bitte nicht einfach so entfernen, da es dann zu Fehlern kommen würde." class="fas fa-info-circle"></i>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button onclick="$('#submit-button').attr('disabled', true); $('.spinner-border').removeClass('d-none'); $('#submit-form').submit();" type="submit" id="submit-button" name="submit-type" value="stay" class="btn btn-primary">
                                    <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                                    Speichern
                                </button>
                                <a style="float: right;" href="{{ route('control.mandant.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection