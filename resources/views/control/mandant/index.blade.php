@extends('control.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $mandant->presentation_long }}</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="{{ $mandant->presentation_short }} neu anlegen" style="float: right;" href="{{ route('control.mandant.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Bezeichnung</th>
                                    <th>Kurzname</th>
                                    <th>Erzeugt am</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $mandanten AS $mandant )
                                    <tr id="">
                                        <td>{{ $mandant->name }} </td>
                                        <td>{{ $mandant->short_name }} </td>
                                        <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $mandant->created_at)->format('d.m.Y') }}</td>
                                        <td>
                                            <a title="{{ $mandant->presentation_short }} bearbeiten" class="mr-2" href="{{ route('control.mandant.show', ['mandant'=>$mandant->id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="{{ $mandant->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die {{ $mandant->presentation_short }} '{{ $mandant->name }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('control.mandant.destroy', ['mandant'=>$mandant->id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $mandant->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
@stop

