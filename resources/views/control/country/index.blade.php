@extends('control.layouts.app')

@section('content')
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title card-title-lg">{{ $country->presentation_long }}</h4>
                        </div>
                        <div class="col-md-6">
                            <a title="{{ $country->presentation_short }} neu anlegen" style="float: right;" href="{{ route('control.country.create') }}"><i class="far fa-plus-square fa-2x"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mt-2 mb-3">

                                <thead class="thead-dark">
                                <tr>
                                    @headicons('1')
                                    <th>Name</th>
                                    <th>Name DE</th>
                                    <th>Code</th>
                                    <th>Versandkosten</th>
                                    <th>Versandfrei ab</th>
                                    <th>Erzeugt am</th>
                                    @headicons('2', 'fa-cogs')
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ( $countrys AS $country )
                                    <tr id="">
                                        <td>
                                            <a title="{{ $country->presentation_short }}  {{ $country->active ? 'deaktivieren' : 'aktivieren' }}" class="mr-2"  href="{{ route('control.country.toggleActive', $country->id) }}" role="button"><i class="fa fa-circle {{ $country->active ? 'text-success' : 'text-secondary' }}"></i></a>
                                        </td>
                                        <td>{{ $country->name }} </td>
                                        <td>{{ $country->name_de }} </td>
                                        <td>{{ $country->code }}</td>
                                        <td>{{ $country->shipping }}</td>
                                        <td>{{ $country->shipping_free_limit }}</td>
                                        <td>@date($country->created_at)</td>
                                        <td>
                                            <a title="{{ $country->presentation_short }} bearbeiten" class="mr-2" href="{{ route('control.country.show', ['country'=>$country->id] ) }}"><i class="far fa-edit"></i></a>
                                            <a title="{{ $country->presentation_short }} löschen" class="mr-2 modal-delete"  href="#"
                                               data-form-info="Wollen Sie die {{ $country->presentation_short }} '{{ $country->name }}' wirklich endgültig löschen?"
                                               data-form-action="{{route('control.country.destroy', ['country'=>$country->id])}}">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr id=""><td colspan="100%">Keine {{ $country->presentation_long }} vorhanden</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

@endsection
