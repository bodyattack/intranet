@extends('control.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header"> {{ $country->presentation_short }} {{ empty($country->id) ? ' erstellen' : 'bearbeiten' }} </div>

                    <div class="card-body">

                        @empty(!$country->id)
                            <form id="submit-form" method="POST" action="{{ route('control.country.update', ['country' => $country->id]) }}">
                        @method('PATCH')
                        @else
                            <form id="submit-form"  method="POST" action="{{ route('control.country.store') }}">
                        @endempty

                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $country->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="name_de" class="col-md-4 col-form-label text-md-right">Name DE</label>

                            <div class="col-md-6">
                                <input id="name_de" type="text" class="form-control{{ $errors->has('name_de') ? ' is-invalid' : '' }}" name="name_de" value="{{ old('name_de', $country->name_de) }}" required autofocus>

                                @if ($errors->has('name_de'))
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name_de') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>                                

                        <div class="form-group row">
                            <label for="code" class="col-md-4 col-form-label text-md-right">Code</label>

                            <div class="col-md-6">
                                <input id="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ old('code', $country->code) }}" >

                                @if ($errors->has('code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="shipping" class="col-md-4 col-form-label text-md-right">Versandkosten</label>

                            <div class="col-md-6">
                                <input id="shipping" type="text" class="form-control{{ $errors->has('shipping') ? ' is-invalid' : '' }}" name="shipping" value="{{ old('shipping', $country->shipping) }}" >

                                @if ($errors->has('shipping'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('shipping') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="shipping_free_limit" class="col-md-4 col-form-label text-md-right">Versandfrei ab</label>

                            <div class="col-md-6">
                                <input id="shipping_free_limit" type="text" class="form-control{{ $errors->has('shipping_free_limit') ? ' is-invalid' : '' }}" name="shipping_free_limit" value="{{ old('shipping_free_limit', $country->shipping_free_limit) }}" >

                                @if ($errors->has('shipping_free_limit'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('shipping_free_limit') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" id="submit-button" name="submit-type" class="btn btn-primary prevent-double-click">Speichern</button>
                                <a style="float: right;" href="{{ route('control.country.index') }}" value="back" class="btn btn-primary">Zurück</a>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection