//------- Modals --------//
function removedate(type) {
    $('#'+type).val('');
    $('#'+type+'_dummy').val('');
}

$(document).ready(function() {

    $('.modal-delete').click(function(){

        var content = $('#modal-delete');
        var form_action = $(this).attr('data-form-action');
        var form_info = $(this).attr('data-form-info');

        console.log(form_action);

        if (form_info === undefined) form_info = 'Den Eintrag endgültig löschen?';

        content.find('form').attr('action', form_action);
        content.find('.modal-body').html(form_info);
        content.modal('show');
    });


    $('.modal-large').click(function(){


       var content = $('#modal-lg');
       var modal_content_url = $(this).attr('data-content-url');
       var modal_action_url = $(this).attr('data-action-url');
       var modal_info = $(this).attr('data-info');
       var modal_title = $(this).attr('data-title');
       var modal_footer_show = $(this).attr('data-footer-show');

       if (modal_action_url === undefined) modal_action_url = '';
       if (modal_content_url === undefined) modal_content_url = '';
       if (modal_info === undefined) modal_info = '';
       if (modal_title === undefined) modal_title = 'Hinweis';
       if (modal_footer_show === undefined) modal_footer_show = 'true';

       if(modal_content_url!='') {
           $.ajax({
               url: modal_content_url,
               type: 'GET',
               success: function (response) {
                   content.find('.modal-body').html(response);
               }
           });
       }

       content.find('form').attr('action', modal_action_url);
       content.find('.modal-body').html(modal_info);
       if(modal_footer_show == 'false' || modal_footer_show == "0") content.find('.modal-footer').hide();

       content.modal('show');
    });


    $(".custom_switch").on('click', function(){
        var form_action = $(this).attr('data-form-action');
        var value = $(this).val();
        $.ajax({
            url: form_action,
            type: 'GET',
            success: function(response) {
                $('#row_'+value).show('fast').delay(150).hide('slow');
                permissionSlot(response)
            }
        });
    });


    function permissionSlot(response) {

        if(response.url=='') {
            $('#area_permissions_' + response.area).remove();
        }
        else {
            $('#area_'+ response.area).after('<div style="color: #CC0000;" id="spinner_'+response.area+'" class="d-flex justify-content-center mt-3 mb-5"><div class="spinner-border" role="status"></div></div>');
            $.ajax({
                url: response.url,
                type: 'GET',
                success: function (erg) {
                    console.log(erg);
                   $('#area_' + response.area).after(erg).ready(function () {
                       $('#spinner_' + response.area).remove();
                       $('#area_permissions_' + response.area).show('slow');
                    });
                }
            });
        }

    }


 $('.prevent-double-click').click(function(){

        if (typeof document.createElement("input").checkValidity == "function") {
            $('input[required]').on('valid', function(){
                $(this).attr('disabled', true);
                $(this).prepend('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
                $('#submit-form').submit();
            });
        }
        else {
            $(this).attr('disabled', true);
            $(this).prepend('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
            $('#submit-form').submit();
        }

    });


});

function generateCSV(args) {

    var columns, headers, formArray, filter_array = {};

    columns = args.columns || null;
    headers = args.headers || null;

    formArray = $('#submit-form').serializeArray();
    for (let i=0; i<formArray.length; i++) {
        var key = formArray[i].name
        filter_array[key] = formArray[i].value;
    }

    $.ajax({
        url: args.action,
        type: 'POST',
        data:  filter_array,
        success: function(response) {

            var data, filename, link;
            var csv = convertArrayOfObjectsToCSV({
                data: response,
                columns: columns,
                headers: headers,
            });

            if (csv == null) return;

            filename = args.filename || 'export.csv';
            if (!csv.match(/^data:text\/csv/i)) {
                csv = 'data:text/csv;charset=utf-8,' + csv;
            }
            data = encodeURI(csv);

            link = document.createElement('a');
            link.setAttribute('href', data);
            link.setAttribute('download', filename);
            link.click();


        }
    });

}


function convertArrayOfObjectsToCSV(args) {

    var result, ctr, keys, columns, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }

    columnDelimiter = args.columnDelimiter || ';';
    lineDelimiter = args.lineDelimiter || '\n';

    if (args.headers == null) {
        keys = Object.keys(data[0]);
    }
    else {
        keys = JSON.parse(args.headers);
        columns = JSON.parse(args.columns);
    }

    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        if (args.columns == null) {
            keys.forEach(function(key) {
                if (ctr > 0) result += columnDelimiter;

                result += item[key];
                ctr++;
            });
        }
        else {
            columns.forEach(function(key) {
                if (ctr > 0) result += columnDelimiter;

                result += item[key];
                ctr++;
            });
        }
        result += lineDelimiter;
    });

    return result;
}
